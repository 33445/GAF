#查看是否存在GAF项目路径
check_gaf_project_exist(){
    if [ -d "${Gaf_Project_Path}/gaf-web-m" ] && [ -d "${Gaf_Project_Path}/gaf-cloud" ]; then
        echo " "
    else
        echo "GAF工程项目不存在，无法继续执行构建"
        exit 1
    fi
}

#检查是否有对应seata数据库
function check_database_seata_exist(){
  local database=$1
  if [ "$( docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 psql -q -h $HOSTIP -p 5432 -U admin -d postgres -tAc "SELECT 1 FROM pg_database WHERE datname='gaf-seata'" )" == '1' ]; then
    echo 1
  else
    docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 createdb -h gaf-postgres -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME gaf-seata
  fi
}

#检查是否有对应seata数据表
function check_database_seata_exist_from(){
  if [ "$( docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 psql -q -h $HOSTIP -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME -d gaf-seata -tAc "select count(*) from pg_class where relname = 'global_table'" )" == '1' ]; then
    echo 1
  else
    #初始化seata数据表
    source $Root_Current_Dir/jobs/initializing_seata_server_table.sh
  fi
}

#检查是否有对应gaf-storage数据库
function check_database_storage_exist(){
  local database=$1
  if [ "$( docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 psql -q -h $HOSTIP -p 5432 -U admin -d postgres -tAc "SELECT 1 FROM pg_database WHERE datname='gaf-storage'" )" == '1' ]; then
    echo 1
  else
    docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 createdb -h gaf-postgres -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME gaf-storage
  fi
}

#检查是否有对应gaf-storage数据库表
function check_database_storage_exist_form(){
  if [ "$( docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 psql -q -h $HOSTIP -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME -d gaf-bpm -tAc "select count(*) from pg_class where relname = 'storage_space'" )" == '1' ]; then
    echo 1
  else
    #初始化工作流配置
    source $Root_Current_Dir/jobs/initializing_gaf_storage_table.sh
  fi
}

#检查docker是否有对应容器名正在运行
check_container_exist(){
  local component=$1
  local container_name=$(docker ps --format "{{.Names}}"|grep -w $component)
  if [ -z "$container_name" ]; then
    echo 0
  else
    echo 1
  fi
}

#检查容器健康状态
check_container_health(){
    local health_json=`docker inspect --format '{{json .State.Health}}' $1`
    local health_pattern="\"Status\":\"healthy\""
    if [[ $health_json == *"$health_pattern"* ]]; then
        echo 0
    else
        echo 1
    fi
}

#等待容器健康状态正常
wait_container_health(){
    echo "等待服务启动..."
    local health_service_count=0
    local param_arr=($*)
    while [ ! $health_service_count -eq $# ]; do
        for i in ${!param_arr[@]}; do
            local check_result=`check_container_health ${param_arr[i]}`
            if [ "$check_result" -eq 0 ]; then
                health_service_count=`expr $health_service_count + 1`
                echo "【${param_arr[i]}】服务已启动..."
                unset param_arr[i]
            fi
            sleep 1
        done
    done
}

#等待minio数据库启动
wait_minio(){
    bash $Root_Current_Dir/bin/wait-for-it.sh ${HOSTIP}:9000 --timeout=600 --strict --  echo "minio准备就绪"
}


#判断命令是否安装
check_commands(){
  commands_flag=""
  for command_name in $*; do
      if ! [ -x "$(command -v $command_name)" ]; then
          commands_flag=$command_name
      fi
  done
  if [ -n "$commands_flag" ]; then
      echo "环境缺少：[$commands_flag]！"
      exit 1
  fi
}

#创建docker网络
#create_docker_network(){
#    [[ ! `docker network ls | grep $1`  ]] && docker network create $1
#}

#等待数据库启动
wait_gaf_db(){
    bash $Root_Current_Dir/bin/wait-for-it.sh ${HOSTIP}:5432 --timeout=600 --strict --  echo "pg数据库准备就绪"
}
#等待mongodb数据库启动
wait_mongodb(){
    bash $Root_Current_Dir/bin/wait-for-it.sh ${HOSTIP}:27017 --timeout=600 --strict --  echo "mongodb数据库准备就绪"
}
#等待minio数据库启动
wait_minio(){
    bash $Root_Current_Dir/bin/wait-for-it.sh ${HOSTIP}:9000 --timeout=600 --strict --  echo "minio准备就绪"
}
#加载db数据
load_db_data(){
    wait_gaf_db

    local restore_files=$(find $Root_Current_Dir/data/db  -type f |sort)
    
    local restore_file
    for restore_file in $restore_files; do
        local file_extension=$(basename $restore_file|cut -d . -f2)
        if [ "$file_extension" == "sql" ]; then
            load_gaf_sql $restore_file
        else
            dump_restore $restore_file
        fi
    done
}
#数据库备份件恢复
dump_restore(){
    local dump_file=$1
    local restore_db=$(basename $dump_file |cut -d . -f1)
    PGPASSWORD=123456 pg_restore -h $HOSTIP -p 5432  -U admin --no-owner -d $restore_db $dump_file
}
#gaf数据库执行sql文件
load_gaf_sql(){
    local sql_file=$1
    PGPASSWORD=123456 psql -o /dev/null -q -h $HOSTIP -p 5432 -U admin -d gaf -f $sql_file
}
#合并config和GAF_ENV_CONFIG.env文件为.all_config.env
merge_config() {
  cat $Root_Current_Dir/conf/GAF_ENV_CONFIG.env $Root_Current_Dir/config > $Root_Current_Dir/.all_config.env
}

#替换环境配置文件中ip变量
sed_config_env(){
    sed -i 's/${HOSTIP}/'$HOSTIP'/g' ${GAF_VOL_DIR}/GAF_ENV_CONFIG.env
    . $Root_Current_Dir/conf/GAF_ENV_CONFIG.env
}

#开启防火墙端口
port(){
    for i in $*; do
    	firewall-cmd --zone=public --add-port=$i/tcp --permanent
    done
	firewall-cmd --reload
}

#开启gaf服务的防火墙端口
port_gaf() {
    port 9000 30777
}


#构建打包所有GAF应用
build_frontend() {
    export NODE_OPTIONS=--max_old_space_size=4096

    cd $Root_Current_Dir/../../../

    cd gaf-web-m/packages/map
    yarn install --update-checksums
    cd ../../
    yarn build
    sh generateDirs.sh
    chmod 755 generateConf.sh
    ./generateConf.sh
    docker build -t ${GAF_REGISTRY}gaf-web:${GAF_REGISTRY_TAG} .

    cd $Root_Current_Dir
}

build_images() {
    [[ -z "$1" ]] && build_images_args="" || build_images_args="-pl $1 -am"
    mvn clean package dockerfile:build -Ddockerfile.build.skip -Dmaven.test.skip=true -DCUSTOM_REGISTRY=${GAF_REGISTRY} -DCUSTOM_TAG=${GAF_REGISTRY_TAG} $build_images_args
}


#修改某些挂载卷的权限
edit_vol_permission() {
    chmod -R 777 ${GAF_VOL_DIR}/etc/etc_prometheus/data
    chmod -R 777 ${GAF_VOL_DIR}/etc/etc_elasticsearch/data
}