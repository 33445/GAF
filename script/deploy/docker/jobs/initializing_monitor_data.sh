[ ! $GAF_ENV_DATASOURCE_DRIVER ] && exit 1
[ ! $GAF_ENV_DATASOURCE_URL ] && exit 1
[ ! $GAF_ENV_DATASOURCE_USERNAME ] && exit 1
[ ! $GAF_ENV_DATASOURCE_PASSWORD ] && exit 1
[ ! $Root_Current_Dir ] && exit 1
[ ! $GAF_VOL_DIR ] && exit 1
[ ! $GAF_BASE_DATA_PATH ] && exit 1


wait_gaf_db
cp -rf $Root_Current_Dir/data/etc ${GAF_VOL_DIR}
docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 createdb -h gaf-postgres -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME grafana 2> /dev/null || echo "database grafana already exists"
