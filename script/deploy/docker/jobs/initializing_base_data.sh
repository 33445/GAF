[ ! $GAF_BASE_DATA_PATH ] && exit 1
[ ! $GAF_ENV_DATASOURCE_DRIVER ] && exit 1
[ ! $GAF_ENV_DATASOURCE_URL ] && exit 1
[ ! $GAF_ENV_DATASOURCE_USERNAME ] && exit 1
[ ! $GAF_ENV_DATASOURCE_PASSWORD ] && exit 1


wait_gaf_db
docker run --rm --net=gaf-net -v $GAF_BASE_DATA_PATH:/opt/liquibase-data registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 \
    liquibase \
      --driver=$GAF_ENV_DATASOURCE_DRIVER \
      --classpath=/usr/local/liquibase/liquibase-classpath/postgresql-42.2.23.jar \
      --url=$GAF_ENV_DATASOURCE_URL \
      --username=$GAF_ENV_DATASOURCE_USERNAME \
      --password=$GAF_ENV_DATASOURCE_PASSWORD \
      --changeLogFile=liquibase-data/new/system_tenant/all.xml \
      update
# docker run --rm --net=gaf-net -e PGPASSWORD=$GAF_ENV_DATASOURCE_PASSWORD registry.cn-hangzhou.aliyuncs.com/supermap-gaf/build-tools:v1.0 createdb -h gaf-postgres -p 5432 -U $GAF_ENV_DATASOURCE_USERNAME gaf-storage 2> /dev/null || echo "database gaf-storage already exists"