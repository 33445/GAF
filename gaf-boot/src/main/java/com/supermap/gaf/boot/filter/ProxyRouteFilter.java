package com.supermap.gaf.boot.filter;

import com.supermap.gaf.boot.listener.RoutesChangeListener;
import com.supermap.gaf.boot.util.BootCommonUtils;
import com.supermap.gaf.boot.util.ResponseUtils;
import com.supermap.gaf.gateway.GatewayPredicateDefinition;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 仅支持path断言
 */
public class ProxyRouteFilter implements Filter {
    private Logger log = LoggerFactory.getLogger(ProxyRouteFilter.class);
    public static final String GATEWAY_ROUTE_ATTR = "gatewayRoute";
    private List<GatewayRouteDefinition> initHttpRoutes;

    public ProxyRouteFilter(List<GatewayRouteDefinition> fixedRoute) {
        this.initHttpRoutes = fixedRoute;
    }

    GatewayRouteDefinition findRouteByPath(String path) {
        AntPathMatcher pathMatcher = new AntPathMatcher();
        GatewayRouteDefinition re = null;
        List<GatewayRouteDefinition> httpRoutesCache = RoutesChangeListener.getHttpRoutesCache();

        List[] search = {this.initHttpRoutes, httpRoutesCache};
        for (List routes : search) {
            if (CollectionUtils.isEmpty(routes)) {
                continue;
            }
            List<GatewayRouteDefinition> routeDefinitions = (List<GatewayRouteDefinition>) routes;
            for (GatewayRouteDefinition item : routeDefinitions) {
                for (GatewayPredicateDefinition predicateDefinition : item.getPredicates()) {
                    if ("Path".equals(predicateDefinition.getName())) {
                        String p = predicateDefinition.getArgs();
                        if (pathMatcher.match(p, path)) {
                            return item;
                        }
                    }
                }
            }
        }
        return re;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String path = BootCommonUtils.getURI(request).getPath();
        GatewayRouteDefinition routeDefinition = findRouteByPath(path);
        if (routeDefinition == null) {
            ResponseUtils.error((HttpServletResponse) servletResponse, String.format(ResponseUtils.HTML_CONTENT_FORMAT, "404 NOT FOUND"), HttpStatus.NOT_FOUND, MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            return;
        }
        servletRequest.setAttribute(GATEWAY_ROUTE_ATTR, routeDefinition);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
