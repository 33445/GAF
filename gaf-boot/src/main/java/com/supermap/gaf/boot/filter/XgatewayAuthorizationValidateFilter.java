package com.supermap.gaf.boot.filter;

import com.supermap.gaf.authority.enums.ResourceApiMethodEnum;
import com.supermap.gaf.extend.spi.ValidateAuthority;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import com.supermap.gaf.gateway.util.AntPathMatcherUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME;

/**
 * 注意： 该代码对应gaf-microservice-gateway中的同名的filter,功能逻辑等应该保持一致
 * @author wxl
 * @since 2021/7/7
 */
public class XgatewayAuthorizationValidateFilter implements Filter {

    private ValidateAuthority validateAuthority;

    public XgatewayAuthorizationValidateFilter(ValidateAuthority validateAuthority) {
        this.validateAuthority = validateAuthority;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ExchangeAuthenticationAttribute attribute = ((ExchangeAuthenticationAttribute) request.getAttribute(EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME));
        if (attribute.getIsPublicUrl() || attribute.getIsProfileUrl()){
            chain.doFilter(request, response);
            return;
        }

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String uri = httpServletRequest.getRequestURI();
        List<String> notVerifyPermissionUrls = attribute.getGatewaySecurityProperties().getNotVerifyPermissionUrls();
        boolean match = AntPathMatcherUtils.match(uri, notVerifyPermissionUrls);
        if (match) {
            chain.doFilter(request, response);
            return;
        }
        boolean apiAuthzEnabled = attribute.getGatewaySecurityProperties().isApiAuthzEnable();
        if (!apiAuthzEnabled) {
            chain.doFilter(request, response);
            return;
        }

        com.supermap.gaf.extend.commontypes.AuthenticationResult authenticationResult = attribute.getAuthenticationResult();
        com.supermap.gaf.extend.commontypes.AuthorizationParam authorizationParam = new com.supermap.gaf.extend.commontypes.AuthorizationParam();
        authorizationParam.setUsername(authenticationResult.getUsername());
        authorizationParam.setUri(uri);
        authorizationParam.setMethod( ResourceApiMethodEnum.valueOf(httpServletRequest.getMethod()).getValue());
        Boolean result = validateAuthority.hasPermission(authorizationParam);
        if (!BooleanUtils.isTrue(result)) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
            httpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(),"API资源访问权限不足");
            return;
        }
        chain.doFilter(request, response);
    }

}
