/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.boot.filter;

import cn.hutool.core.net.URLDecoder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.collect.Lists;
import com.supermap.gaf.boot.util.BootCommonUtils;
import com.supermap.gaf.boot.util.ResponseUtils;
import com.supermap.gaf.boot.util.ThrowExceptionRunnable;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import com.supermap.gaf.gateway.commontypes.ExchangeAuthenticationAttribute;
import com.supermap.gaf.gateway.util.IserverProxyUtils;
import com.supermap.gaf.webgis.client.ServiceShareSettingClient;
import com.supermap.gaf.webgis.client.WebgisServiceClient;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.gaf.webgis.enums.ServiceTypeEnum;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;
import lombok.SneakyThrows;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static com.supermap.gaf.boot.filter.ProxyRouteFilter.GATEWAY_ROUTE_ATTR;
import static com.supermap.gaf.gateway.commontypes.constant.GatewayConst.EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME;

/**
 * iserver代理权限
 */
public class XgatewayIserverFilter implements Filter {
    private static final Pattern I_SERVER_FILTER_URL_REGIX = Pattern.compile("(^/service-proxy/[^/]*)(/.*)");
    /**
     * The constant REST_DATA_SPEC_URL_REGIX.
     */
    public static final String REST_DATA_SPEC_URL_REGIX = "^/.*/services/[^/]*/rest/data/";
    /**
     * The constant DATASETNAMS_PARAMS_REGIX.
     */
    public static final String DATASETNAMS_PARAMS_REGIX = "^datasetNames[^:]*:(\\[[\\u4e00-\\u9fa5|\\d|_|\\w|\"':,|\\s]*\\])";
    /**
     * The constant RESTMAP_TILE_REGIX.
     */
    public static final String RESTMAP_TILE_REGIX = "^(.*/services/.+/rest/maps/.+)/(image|tileImage|tileimage|tileFeature|zxyTileImage.*)[.](webp|png|bmp|gif|jpg|mvt|rjson|json|html|xml|kml|WEBP|PNG|BMP|GIF|JPG|MVT|RJSON|JSON|HTML|XML|KML)";
    /**
     * The constant OR_PERMISSION_SERVICE.
     */
    public static final String OR_PERMISSION_SERVICE = "OR_PERMISSION_SERVICE";
    /**
     * The constant SHARE_SETTING.
     */
    public static final String SHARE_SETTING = "SHARE_SETTING";

    /**
     * The constant ETAG_REGIX.
     */
    public static final String ETAG_REGIX = "^(W/){0,1}\"([\\x00-\\x7F]+)\"";
    /**
     * The constant FEATURE_URL_REGIX.
     */
    public static final String FEATURE_URL_REGIX = "^.*/(queryResults[^/]*|featureResults[^/]*|feature/[^/]*)$";
    /**
     * The constant FEATURE_INDEX_URL_REGIX.
     */
    public static final String FEATURE_INDEX_URL_REGIX = "(^/.*/services/[^/]*/rest/data)/feature/([\\d])-([\\d])-[\\d][\\.^/]*";

    // 缓存查询feature接口获取数据源或者数据集
    private static final Cache<String, JSONObject> FEATURE_CACHE
            = Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).initialCapacity(10).maximumSize(100).build();




    private WebgisServiceClient webgisServiceClient;

    private ServiceShareSettingClient serviceShareSettingClient;

    /**
     * Instantiates a new Xgateway iserver filter.
     *
     * @param webgisServiceClient       the webgis service client
     * @param serviceShareSettingClient the service share setting client
     */
    public XgatewayIserverFilter(WebgisServiceClient webgisServiceClient, ServiceShareSettingClient serviceShareSettingClient) {
        this.webgisServiceClient = webgisServiceClient;
        this.serviceShareSettingClient = serviceShareSettingClient;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 重要
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request, response), true);
        ThrowExceptionRunnable re = filter(request, response, filterChain);
        re.run();
    }


    /**
     * Gets attribute.
     *
     * @param exchange the exchange
     * @param key      the key
     * @return the attribute
     */
    static Object getAttribute(HttpServletRequest exchange, String key) {
        return exchange.getAttribute(key);
    }

    /**
     * Sets attribute.
     *
     * @param exchange the exchange
     * @param key      the key
     * @param value    the value
     */
    static void setAttribute(HttpServletRequest exchange, String key, Object value) {
        exchange.setAttribute(key, value);
    }

    /**
     * Gets uri.
     *
     * @param exchange the exchange
     * @return the uri
     */
    static URI getURI(HttpServletRequest exchange) {
        return BootCommonUtils.getURI(exchange);
    }

    /**
     * Gets query params.
     *
     * @param exchange the exchange
     * @return the query params
     */
    static MultiValueMap<String, String> getQueryParams(HttpServletRequest exchange) {
        URI uri = getURI(exchange);
        return UriComponentsBuilder.fromHttpUrl(URLDecoder.decode(uri.toString(), StandardCharsets.UTF_8)).build().getQueryParams();
    }

    /**
     * Gets response header first.
     *
     * @param exchange   the exchange
     * @param headerName the header name
     * @return the response header first
     */
    static String getResponseHeaderFirst(HttpServletResponse exchange, String headerName) {
        return exchange.getHeader(headerName);
    }

    /**
     * Gets request header first.
     *
     * @param exchange   the exchange
     * @param headerName the header name
     * @return the request header first
     */
    static String getRequestHeaderFirst(HttpServletRequest exchange, String headerName) {
        return exchange.getHeader(headerName);
    }

    /**
     * Gets request content type.
     *
     * @param exchange the exchange
     * @return the request content type
     */
    static MediaType getRequestContentType(HttpServletRequest exchange) {
        String contentType = getRequestHeaderFirst(exchange, HttpHeaders.CONTENT_TYPE);
        return contentType == null ? null : MediaType.valueOf(contentType);
    }

    /**
     * Gets response content type.
     *
     * @param exchange the exchange
     * @return the response content type
     */
    static MediaType getResponseContentType(HttpServletResponse exchange) {
        String contentType = getResponseHeaderFirst(exchange, HttpHeaders.CONTENT_TYPE);
        return contentType == null ? null : MediaType.valueOf(contentType);
    }

    /**
     * Gets response etag.
     *
     * @param exchange the exchange
     * @return the response etag
     */
    static String getResponseEtag(ServerWebExchange exchange) {
        return exchange.getResponse().getHeaders().getETag();
    }

    /**
     * Sets response etag.
     *
     * @param exchange the exchange
     * @param etag     the etag
     */
    static void setResponseEtag(ServerWebExchange exchange, String etag) {
        exchange.getResponse().getHeaders().setETag(etag);
    }

    /**
     * Reset response.
     *
     * @param exchange the exchange
     */
    static void resetResponse(HttpServletResponse exchange) {
        exchange.reset();
    }


    /**
     * Filter service by proxy prefix list.
     *
     * @param webgisServices the webgis services
     * @param proxyPrefix    the proxy prefix
     * @return the list
     */
    List<WebgisService> filterServiceByProxyPrefix(List<WebgisService> webgisServices,String proxyPrefix){
        if(CollectionUtils.isEmpty(webgisServices)){
            return new ArrayList<>();
        }
        return webgisServices.stream().filter(item->item.getAddress().contains(proxyPrefix)).collect(Collectors.toList());
    }

    /**
     * Gets or permission services.
     *
     * @param exchange the exchange
     * @param service  the service
     * @return the or permission services
     */
    List<WebgisService> getOrPermissionServices(HttpServletRequest exchange, WebgisService service) {
        Map<String, List<String>> map = (Map<String, List<String>>) getAttribute(exchange, OR_PERMISSION_SERVICE);
        if (map == null) {
            return null;
        }
        List<String> urls = map.get(service.getGisServiceId());
        ServiceTypeEnum type = ServiceTypeEnum.valueOf(service.getTypeCode());
        if (!CollectionUtils.isEmpty(urls)) {
            MessageResult<List<WebgisService>> result = webgisServiceClient.noRealAddress(urls, type.getCode());
            checkMessageResult(result);
            return result.getData();
        }
        return null;
    }


    /**
     * Filter throw exception runnable.
     *
     * @param exchange the exchange
     * @param response the response
     * @param chain    the chain
     * @return the throw exception runnable
     */
    public ThrowExceptionRunnable filter(HttpServletRequest exchange, HttpServletResponse response, FilterChain chain) {
        String path = getURI(exchange).getPath();
        path = URLDecoder.decode(path, StandardCharsets.UTF_8);
        ExchangeAuthenticationAttribute attribute = (ExchangeAuthenticationAttribute) getAttribute(exchange, EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME);
        Matcher m = I_SERVER_FILTER_URL_REGIX.matcher(path);
        // 不是服务请求或者静态资源则放行
        if (!m.matches() || !IserverProxyUtils.needAuth(path)) {
            return () -> chain.doFilter(exchange, response);
        }
        // 查询当前需要访问哪些具体服务
        List<WebgisService> webgisServices = getServiceInfoFromPath(m.group(1), m.group(2), exchange);
        // 服务不存在
        if (CollectionUtils.isEmpty(webgisServices)) {
            return () -> ResponseUtils.error(response, String.format(ResponseUtils.HTML_CONTENT_FORMAT, "404 NOT FOUND"), HttpStatus.NOT_FOUND, MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
        }
        for (WebgisService webgisService : webgisServices) {
            ThrowExceptionRunnable error = () -> ResponseUtils.error(response, String.format(ResponseUtils.HTML_CONTENT_FORMAT, "无法访问"), HttpStatus.FORBIDDEN, MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            ;
            boolean hasPermission = true;
            // 服务不可用
            if (!BooleanUtils.isTrue(webgisService.getStatus())) {
                hasPermission = false;
                error = () -> ResponseUtils.error(response, String.format(ResponseUtils.HTML_CONTENT_FORMAT, "服务不可用"), HttpStatus.SERVICE_UNAVAILABLE, MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            }
            // 服务可用下进行服务权限检查
            if (error != null && !checkUserPermission(exchange, attribute.getAuthenticationResult(), webgisService)) {
                hasPermission = false;
                error = () -> ResponseUtils.error(response, String.format(ResponseUtils.HTML_CONTENT_FORMAT, "没有全部权限"), HttpStatus.FORBIDDEN, MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            }
            // 如果不能访问检查是否有开放的上级资源权限
            if (!hasPermission) {
                List<WebgisService> orPermissionServices = getOrPermissionServices(exchange, webgisService);
                if (!CollectionUtils.isEmpty(orPermissionServices)) {
                    for (WebgisService orPermissionService : orPermissionServices) {
                        // 服务不可用
                        if (!BooleanUtils.isTrue(orPermissionService.getStatus())) {
                            continue;
                        }
                        // 服务权限检查
                        if (!checkUserPermission(exchange, attribute.getAuthenticationResult(), orPermissionService)) {
                            continue;
                        }
                        hasPermission = true;
                        break;
                    }
                }
            }

            // 最终判断无法访问则退出
            if (!hasPermission) {
                return error;
            }

        }
        return handle(m.group(2), exchange, response, chain);
    }


    /**
     * Check message result.
     *
     * @param re the re
     */
    void checkMessageResult(MessageResult re) {
        if (!re.isSuccessed()) {
            throw new GafException(re.getMessage());
        }
    }


    /**
     * Gets service info from path.
     *
     * @param proxyPrefix the proxy prefix
     * @param path        the path
     * @param exchange    the exchange
     * @return the service info from path
     */
    List<WebgisService> getServiceInfoFromPath(String proxyPrefix, String path, HttpServletRequest exchange) {
        List<WebgisService> re = new ArrayList<>();
        Pattern p = Pattern.compile(String.format("(%s)featureResults.*", REST_DATA_SPEC_URL_REGIX));
        Matcher m = p.matcher(path);
        do {
            if (m.matches()) {
                // 数据服务featureResults特殊处理
                byte[] requestBody = (byte[]) getAttribute(exchange, XgatewayCacheRequestBodyFilter.MY_CACHED_REQUEST_DATA);
                List<WebgisService> list = getWebgisServiceFromFeatureResultsRequestAndCheck(getURI(exchange).getQuery(), requestBody, getRequestContentType(exchange), m.group(1));
                list = filterServiceByProxyPrefix(list,proxyPrefix);
                re.addAll(list);
                break;
            }
            p = Pattern.compile(FEATURE_INDEX_URL_REGIX);
            m = p.matcher(path);
            if(m.matches()){
                // 数据服务feature接口特殊处理
                int datasourceIndex = Integer.valueOf(m.group(2));
                int datasetIndex = Integer.valueOf(m.group(3));
                GatewayRouteDefinition route = (GatewayRouteDefinition) getAttribute(exchange, GATEWAY_ROUTE_ATTR);
                String datasourcesUrl = getIserverHost(route)+m.group(1)+"/datasources";
                JSONObject result = FEATURE_CACHE.get(datasourcesUrl, url->new RestTemplate().getForObject(url+".json", JSONObject.class));
                JSONArray jsonArray = result.getJSONArray("datasourceNames");
                String datasourceName = jsonArray.getString(datasourceIndex);
                String datasetUrl = datasourcesUrl+"/"+datasourceName+"/datasets";
                result = FEATURE_CACHE.get(datasetUrl,url->new RestTemplate().getForObject(url+".json", JSONObject.class));
                jsonArray = result.getJSONArray("datasetNames");
                String datasetName = jsonArray.getString(datasetIndex);

                String proxyAddress = m.group(1)+"/datasources/"+datasourceName+"/datasets/"+datasetName;
                MessageResult<List<WebgisService>> messageResult = webgisServiceClient.noRealAddress(Arrays.asList(proxyAddress), ServiceTypeEnum.RESTDATA.getCode());
                checkMessageResult(messageResult);
                List<WebgisService>  services = filterServiceByProxyPrefix(messageResult.getData(),proxyPrefix);
                if (!CollectionUtils.isEmpty(services)) {
                    re.add(services.get(0));
                }
                break;
            }
            ServiceTypeEnum type = null;
            String regix = null;
            m = null;
            ServiceTypeEnum[] serviceTypeEnums = ServiceTypeEnum.values();
            Arrays.sort(serviceTypeEnums, (o1, o2) -> o2.getMatchesPriority().compareTo(o1.getMatchesPriority()));
            for (ServiceTypeEnum item : serviceTypeEnums) {
                if (item.getPathPatterns().size() == 1 && item.getPathPatterns().get(0).equals(ServiceTypeEnum.DEFAULT_PATTERN)) {
                    continue;
                }
                List<String> pathPatterns = item.getPathPatterns();
                pathPatterns.sort((o1, o2) -> o2.compareTo(o1));
                Map<String, List<String>> orPermissionServiceUrlMap = new HashMap<>();
                WebgisService webgisService = null;
                for (int i = 0; i < pathPatterns.size(); ++i) {
                    String regixItem = item.getPathPatterns().get(i);
                    p = Pattern.compile(String.format("(%s).*", regixItem));
                    m = p.matcher(path);
                    if (m.matches()) {
                        if (webgisService == null) {
                            type = item;
                            regix = regixItem;
                            String proxyAddress = m.group(1);
                            MessageResult<List<WebgisService>> messageResult = webgisServiceClient.noRealAddress(Arrays.asList(proxyAddress), type.getCode());
                            checkMessageResult(messageResult);
                            List<WebgisService>  services = filterServiceByProxyPrefix(messageResult.getData(),proxyPrefix);
                            if (!CollectionUtils.isEmpty(services)) {
                                WebgisService service = services.get(0);
                                webgisService = service;
                                re.add(service);
                            }
                        } else if (regix.startsWith(regixItem)) {
                            List<String> orPermissionServiceUrls = orPermissionServiceUrlMap.get(webgisService.getGisServiceId());
                            String orPermissionServiceUrl = m.group(1);
                            if (orPermissionServiceUrls != null) {
                                orPermissionServiceUrls.add(orPermissionServiceUrl);
                            } else {
                                orPermissionServiceUrlMap.put(webgisService.getGisServiceId(), Lists.newArrayList(orPermissionServiceUrl));
                                setAttribute(exchange, OR_PERMISSION_SERVICE, orPermissionServiceUrlMap);
                            }
                        }
                    }
                }
                if (webgisService != null) {
                    break;
                }
            }
        } while (false);

        if (re.isEmpty()) {
            MessageResult<List<WebgisService>> messageResult = webgisServiceClient.noRealAddress(Arrays.asList(path), null);
            checkMessageResult(messageResult);
            List<WebgisService>  services = filterServiceByProxyPrefix(messageResult.getData(),proxyPrefix);
            if (!CollectionUtils.isEmpty(services)) {
                return Arrays.asList(services.get(0));
            }
        }
        return re;
    }

    /**
     * Check user permission boolean.
     *
     * @param exchange             the exchange
     * @param authenticationResult the authentication result
     * @param webgisService        the webgis service
     * @return the boolean
     */
    boolean checkUserPermission(HttpServletRequest exchange, AuthenticationResult authenticationResult, WebgisService webgisService) {
        // 创建者有权限
        boolean re = authenticationResult.getUsername().equals(webgisService.getCreatedBy());
        if (re) {
            return true;
        }
        // 共享权限检查
        MessageResult<Page<ServiceShareSetting>> settings = serviceShareSettingClient.pageList(ServiceShareSettingSelectVo.builder()
                .gisServiceId(webgisService.getGisServiceId()).userId(authenticationResult.getUserId()).build(), 1, 1);
        checkMessageResult(settings);

        re = settings.getData() != null && !CollectionUtils.isEmpty(settings.getData().getPageList());
        if (re && (ServiceTypeEnum.RESTMAP.getCode().equals(webgisService.getTypeCode()) || ServiceTypeEnum.RESTDATA.getCode().equals(webgisService.getTypeCode()))) {
            ServiceShareSetting serviceShareSetting = settings.getData().getPageList().get(0);
            setAttribute(exchange, SHARE_SETTING, serviceShareSetting);
        }
        return re;
    }


    /**
     * Gets webgis service from feature results request and check.
     *
     * @param query       the query
     * @param bodyBytes   the body bytes
     * @param contentType the content type
     * @param dataPath    the data path
     * @return the webgis service from feature results request and check
     */
    List<WebgisService> getWebgisServiceFromFeatureResultsRequestAndCheck(String query, byte[] bodyBytes, MediaType contentType, String dataPath) {
        List<WebgisService> re = new ArrayList<>();
        Charset charset = (contentType==null ||  contentType.getCharset() == null) ? StandardCharsets.UTF_8 : contentType.getCharset();
        String paramStr = "";
        if(query!=null){
            paramStr+=query;
        }
        if(bodyBytes!=null){
            paramStr+=new String(bodyBytes, charset);
        }
        int index = paramStr.indexOf("datasetNames");
        String datasetUrlFormat = "%sdatasources/%s/datasets/%s";
        if (index != -1) {
            final Pattern p = Pattern.compile(DATASETNAMS_PARAMS_REGIX);
            Matcher m = p.matcher(paramStr.substring(index));
            if (m.find()) {
                String datasetNames = m.group(1);
                if (!StringUtils.isEmpty(datasetNames)) {
                    List<String> list = JSON.parseArray(datasetNames, String.class);
                    List<String> proxyAddresss = new ArrayList<>();
                    for (String item : list) {
                        String[] itemArr = item.split(":");
                        proxyAddresss.add(String.format(datasetUrlFormat, dataPath, itemArr[0], itemArr[1]));
                    }
                    if (proxyAddresss.size() > 0) {
                        MessageResult<List<WebgisService>> result = webgisServiceClient.noRealAddress(proxyAddresss, ServiceTypeEnum.RESTDATA.getCode());
                        checkMessageResult(result);
                        // 参数中的数据集必须全部被查询到才允许访问，要么返回全部要么返回空
                        if (!CollectionUtils.isEmpty(result.getData()) &&
                                proxyAddresss.size() == result.getData().stream().map(WebgisService::getProxyAddress).collect(Collectors.toSet()).size()) {
                            return result.getData();
                        }
                    }
                }
            }
        }
        return re;
    }

    /**
     * Handle mono.
     *
     * @param decodePath the decode path
     * @param exchange   the exchange
     * @param response   the response
     * @param chain      the chain
     * @return the mono
     */
    public ThrowExceptionRunnable handle(String decodePath, HttpServletRequest exchange, HttpServletResponse response, FilterChain chain) {
        resetResponse(response);
        response.reset();
        // tileImage 請求
        Pattern p = Pattern.compile(RESTMAP_TILE_REGIX);
        Matcher m = p.matcher(decodePath);
        boolean isTileRequest = m.matches();
        boolean isFeatureRequest = decodePath.matches(FEATURE_URL_REGIX);

        HttpServletRequest req =  modifyRequest(exchange, isTileRequest, isFeatureRequest);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response) {

            @Override
            public PrintWriter getWriter() throws IOException {
                return new PrintWriter(byteArrayOutputStream);
            }

            @Override
            public ServletOutputStream getOutputStream() throws IOException {
                return new ServletOutputStream() {
                    @Override
                    public boolean isReady() {
                        return false;
                    }

                    @Override
                    public void setWriteListener(WriteListener writeListener) {

                    }

                    @Override
                    public void write(int b) throws IOException {
                        byteArrayOutputStream.write(b);
                    }
                };
            }
        };
        return () -> {
            chain.doFilter(req, responseWrapper);
            if (isTileRequest) {
                String etag = responseWrapper.getHeader(HttpHeaders.ETAG);
                if (etag != null) {
                    etag = etag.replaceAll(ETAG_REGIX, String.format("$1\"%s$2\"", getEtabPrefix(exchange)));
                    responseWrapper.setHeader(HttpHeaders.ETAG, etag);
                }
            }
            byte[] bytes = byteArrayOutputStream.toByteArray();
            if (HttpStatus.valueOf(responseWrapper.getStatus()).is2xxSuccessful()) {
                // 修改响应
                MediaType contentType = MediaType.valueOf(responseWrapper.getContentType());
                ServiceShareSetting shareSetting = (ServiceShareSetting) exchange.getAttribute(SHARE_SETTING);
                boolean needFilterSpatial = (isTileRequest && shareSetting != null && !StringUtils.isEmpty(shareSetting.getSpatialSetting()));
                boolean needReplaceHost = IserverProxyUtils.needReplace(contentType.toString());
                if (needFilterSpatial || needReplaceHost) {

                    // 解压
                    if (StringUtils.equals("gzip", responseWrapper.getHeader(HttpHeaders.CONTENT_ENCODING))) {
                        bytes = unzipByteBuf(bytes);
                    }
                    GatewayRouteDefinition route = (GatewayRouteDefinition) exchange.getAttribute(GATEWAY_ROUTE_ATTR);
                    if (needReplaceHost) {
                        bytes = replaceHost(bytes, contentType, exchange, route);
                    }
                    if (needFilterSpatial) {
                        String spatialSetting = shareSetting.getSpatialSetting();
                        List<String> spatialSettings = JSON.parseArray(spatialSetting, String.class);
                        String mapUrl = getIserverHost(route) + m.group(1) + ".json";
                        String format = m.group(3);
                        try {
                            URI uri = BootCommonUtils.getURI(exchange);
                            bytes = IserverProxyUtils.clipImageByGeometrys(bytes, UriComponentsBuilder.fromHttpUrl(URLDecoder.decode(uri.toString(), StandardCharsets.UTF_8)).build().getQueryParams(), mapUrl, contentType, spatialSettings, format);
                        } catch (IOException e) {
                            throw new GafException("空间范围过滤失败");
                        }
                    }

                    // 压缩
                    if (StringUtils.equals("gzip", responseWrapper.getHeader(HttpHeaders.CONTENT_ENCODING))) {
                        bytes = zipByteBuf(bytes);
                    }
                }
            }
            if (bytes != null) {
                response.getOutputStream().write(bytes);
                response.setContentLengthLong(bytes.length);
            }
        };
    }


    /**
     * Need request body boolean.
     *
     * @param exchange the exchange
     * @return the boolean
     */
    public static boolean needRequestBody(HttpServletRequest exchange) {
        String path = getURI(exchange).getPath();
        path = URLDecoder.decode(path, StandardCharsets.UTF_8);
        Matcher m = I_SERVER_FILTER_URL_REGIX.matcher(path);
        if (m.find()) {

            if (m.group(2).matches(REST_DATA_SPEC_URL_REGIX + "featureResults.*")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets etab prefix.
     *
     * @param exchange the exchange
     * @return the etab prefix
     */
    String getEtabPrefix(HttpServletRequest exchange) {
        String re = "";
        ExchangeAuthenticationAttribute attribute = (ExchangeAuthenticationAttribute) getAttribute(exchange, EXCHANGE_AUTHENTICATION_ATTRIBUTE_NAME);
        ServiceShareSetting shareSetting = (ServiceShareSetting) getAttribute(exchange, SHARE_SETTING);
        String sherSettingUpdateTime = shareSetting != null ? shareSetting.getUpdatedTime().getTime() + "" : "";
        String etag_prefix = String.format("%s:%s", attribute.getAuthenticationResult().getUserId(), sherSettingUpdateTime);
        re = Hex.toHexString(etag_prefix.getBytes(StandardCharsets.UTF_8));
        return re;
    }


    /**
     * Modify request server http request.
     *
     * @param exchange         the exchange
     * @param isTileRequest    the is tile request
     * @param isFeatureRequest the is feature request
     * @return the server http request
     */
    HttpServletRequest modifyRequest(HttpServletRequest exchange, boolean isTileRequest, boolean isFeatureRequest) {
        String path = getURI(exchange).getPath();
        HeaderRequestWrapper re = new HeaderRequestWrapper(exchange);
        re.removeHeader(HttpHeaders.AUTHORIZATION);
        re.removeHeader(HttpHeaders.ACCEPT_ENCODING);
        if (isTileRequest) {
            List<String> noneMatches = Collections.list(re.getHeaders(HttpHeaders.IF_NONE_MATCH));
            if (!CollectionUtils.isEmpty(noneMatches)) {
                noneMatches = noneMatches.stream().map(item -> item.replace(getEtabPrefix(exchange), "")).collect(Collectors.toList());
            }
            re.putHeader(HttpHeaders.IF_NONE_MATCH, noneMatches);
        }
        ServiceShareSetting shareSetting = (ServiceShareSetting) getAttribute(exchange, SHARE_SETTING);
        boolean needFilterFields = (isFeatureRequest && shareSetting != null && !StringUtils.isEmpty(shareSetting.getFieldsSetting()) && !"*".equals(shareSetting.getFieldsSetting().trim()));
        if (isFeatureRequest) {
            if (needFilterFields) {
                re.setQueryParam("fields", URLEncoder.encode(shareSetting.getFieldsSetting().trim()));
            } else {
                re.setQueryParam("fields","");
            }
        }
        return re;
    }

    /**
     * Unzip byte buf byte [ ].
     *
     * @param bytes the bytes
     * @return the byte [ ]
     */
    static byte[] unzipByteBuf(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (ByteArrayInputStream in = new ByteArrayInputStream(bytes);
             GZIPInputStream ungzip = new GZIPInputStream(in)) {
            byte[] buffer = new byte[1024];
            int n;
            while ((n = ungzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getCause());
        }
        return out.toByteArray();
    }

    /**
     * Zip byte buf byte [ ].
     *
     * @param bytes the bytes
     * @return the byte [ ]
     */
    static byte[] zipByteBuf(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (GZIPOutputStream gzip = new GZIPOutputStream(out)) {
            gzip.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e.getCause());
        }
        return out.toByteArray();
    }


    /**
     * Replace host byte [ ].
     *
     * @param bytes       the bytes
     * @param contentType the content type
     * @param exchange     the request
     * @param route       the route
     * @return the byte [ ]
     */
    @SneakyThrows
    byte[] replaceHost(byte[] bytes, MediaType contentType, HttpServletRequest exchange, GatewayRouteDefinition route) {
        Charset charset = (contentType==null || contentType.getCharset() == null) ? StandardCharsets.UTF_8 : contentType.getCharset();
        URI sourceRequestUri = getURI(exchange);
        String sourceHttpHost = sourceRequestUri.toString().replace(sourceRequestUri.getRawPath(),"");
        URI targetRequestUri = new URI(route.getUri());
        String targetHttpHost = targetRequestUri.getScheme()+"://"+targetRequestUri.getHost();
        Matcher m = I_SERVER_FILTER_URL_REGIX.matcher(getURI(exchange).getPath());
        if (m.matches()) {
            String content = new String(bytes, charset);
            content = content.replaceAll(targetHttpHost+"(:\\d+)?", sourceHttpHost+m.group(1));
            String[] items = m.group(2).split("/");
            if(items.length>2){
                content = content.replaceAll("([\"'`]{1}\\s*)/"+items[1],"$1"+m.group(1)+"/"+items[1]);
            }
            return content.getBytes(charset);
        }
        return bytes;
    }

    /**
     * Get iserver host string.
     *
     * @param route the route
     * @return the string
     */
    String getIserverHost(GatewayRouteDefinition route){
        return route.getRouteId().substring("iserver-".length());
    }

}
