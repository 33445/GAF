package com.supermap.gaf.boot.servlet;

import com.supermap.gaf.boot.filter.ProxyRouteFilter;
import com.supermap.gaf.boot.util.BootCommonUtils;
import com.supermap.gaf.common.storage.servlet.ProxyServlet;
import com.supermap.gaf.gateway.GatewayFilterDefinition;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.utils.URIUtils;
import org.springframework.security.web.header.HeaderWriterFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class RouteProxyServlet extends ProxyServlet {

    @Override
    protected String getTargetUri(HttpServletRequest servletRequest) {
        GatewayRouteDefinition routeDefinition = (GatewayRouteDefinition) servletRequest.getAttribute(ProxyRouteFilter.GATEWAY_ROUTE_ATTR);
        return routeDefinition.getUri();
    }

    @Override
    protected HttpHost getTargetHost(HttpServletRequest servletRequest) {
        try {
            return URIUtils.extractHost(new URI(getTargetUri(servletRequest)));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    protected void initTarget() throws ServletException {
        return;
    }

    @Override
    protected String rewritePathInfoFromRequest(HttpServletRequest servletRequest) {
        String re = BootCommonUtils.getURI(servletRequest).getPath();
        GatewayRouteDefinition routeDefinition = (GatewayRouteDefinition) servletRequest.getAttribute(ProxyRouteFilter.GATEWAY_ROUTE_ATTR);
        List<GatewayFilterDefinition> filterDefinitions = routeDefinition.getFilters();
        for(GatewayFilterDefinition item:filterDefinitions){
            if("StripPrefix".equals(item.getName())){
                int stripCount = Integer.parseInt(item.getArgs().trim());
                String[] paths = re.split("/");
                if(paths.length>stripCount){
                    return re.substring((paths[0]+"/"+paths[1]+"/"+paths[2]).length());
                }else{
                    return "/";
                }
            }
        }
        return re;
    }


}
