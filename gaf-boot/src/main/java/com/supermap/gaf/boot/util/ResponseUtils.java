package com.supermap.gaf.boot.util;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author wxl
 * @since 2021/7/7
 */
public class ResponseUtils {

    public static final String HTML_CONTENT_FORMAT = "\n" +
            "<html>\n" +
            "<head><title>访问失败</title></head>\n" +
            "<body bgcolor=\"white\">\n" +
            "<center><h1>%s</h1></center>\n" +
            "<hr></body>\n" +
            "</html>";
    private ResponseUtils() {
    }


    public static void unAuth(HttpServletResponse httpServletResponse, String message) throws IOException {
        MessageResult<String> result = MessageResult.failed(String.class).status(HttpStatus.UNAUTHORIZED.value()).message(message).build();
        httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.getWriter().print(JSON.toJSONString(result));
    }


    public static void error(HttpServletResponse httpServletResponse, String content, HttpStatus httpStatus, MediaType contentType) throws IOException {
        httpServletResponse.setStatus(httpStatus.value());
        httpServletResponse.setContentType(contentType.toString());
        httpServletResponse.getWriter().print(content);
    }



}
