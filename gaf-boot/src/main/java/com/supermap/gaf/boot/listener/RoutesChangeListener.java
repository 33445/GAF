package com.supermap.gaf.boot.listener;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import com.supermap.gaf.gateway.client.GatewayRouteClient;
import com.supermap.gaf.gateway.commontypes.RouteSearchParam;
import com.supermap.gaf.gateway.event.RoutesChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class RoutesChangeListener implements ApplicationListener<RoutesChangeEvent> {
    private Logger log = LoggerFactory.getLogger(RoutesChangeListener.class);
    static List<GatewayRouteDefinition> httpRoutesCache = new ArrayList<>();

    @Autowired
    private GatewayRouteClient gatewayRouteClient;

    @Async
    @Override
    public void onApplicationEvent(RoutesChangeEvent routesChangeEvent) {
        log.info("路由更新：{}", JSON.toJSONString(routesChangeEvent.getRoute()));
        initHttpRoutesCache();
    }

    public static List<GatewayRouteDefinition> getHttpRoutesCache() {
        return httpRoutesCache;
    }

    @PostConstruct
    void initHttpRoutesCache() {
        RouteSearchParam searchParam = new RouteSearchParam();
        searchParam.setUriPrefix("http");
        MessageResult<List<GatewayRouteDefinition>> result = gatewayRouteClient.queryGatewayRoutes(JSON.toJSONString(searchParam));
        if (!result.isSuccessed()) {
            throw new RuntimeException("路由查询失败：" + result.getMessage());
        }
        List<GatewayRouteDefinition> routes = result.getData();
        if (routes != null) {
            httpRoutesCache = routes;
        } else {
            httpRoutesCache = new ArrayList<>();
        }
        log.info("路由初始化成功");
    }
}
