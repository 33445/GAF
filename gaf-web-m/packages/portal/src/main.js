import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './axios'
import GafUI from '@gaf/ui'
import antd from 'ant-design-vue'
import './assets/theme/theme.less'

import SlideVerify from 'vue-monoplasty-slide-verify'
import VueCookies from 'vue-cookies'

Vue.use(VueCookies)
Vue.use(SlideVerify)
Vue.use(GafUI)
Vue.use(antd)

Vue.prototype.$axios = axios
Vue.config.productionTip = false



new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
