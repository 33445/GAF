import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export const routes = [
  {
    key: 'home',
    title: '主页',
    path: '/',
    name: 'home',
    // component: () => import('../pages/Home.vue')
    component: () => import('../pages/portal')
  },
  {
    key: 'login',
    title: '登录',
    path: '/login',
    name: 'login',
    component: () => import('../pages/authentication/login/index.vue')
  },
  {
    key: 'tenantswitch',
    title: '租户切换',
    path: '/tenant/switch',
    name: 'tenantswitch',
    component: () => import('../pages/authentication/login/TenantSwitch.vue')
  },
  {
    key: 'logout',
    title: '退出',
    path: '/logout',
    name: 'logout',
    component: () => import('../pages/authentication/login/index.vue')
  },
  {
    key: 'authentication',
    title: '鉴权',
    path: '/authentication/login',
    name: 'authentication',
    component: () => import('../pages/authentication/login/index.vue')
  },
  {
    key: 'customization',
    title: '门户定制',
    path: '/customization',
    name: 'customization',
    component: () => import('../pages/portal/customization/index.vue')
  },
  {
    key: 'microFrontend',
    title: '微应用配置',
    path: '/microFrontend',
    name: 'microFrontend',
    component: () => import('../pages/microFrontend/index.vue')
  },
  {
    key: 'documentTemplate',
    title: '模板说明',
    path: '/documentTemplate',
    name: 'documentTemplate',
    component: () => import('../pages/documentTemplate/index.vue')
  },
  {
    key: 'apps-log1',
    title: 'log 主页',
    path: '/log/'
  },
  {
    key: 'apps-log2',
    title: 'log 列表页',
    path: '/log/table'
  },
  {
    key: 'apps-config1',
    title: 'config 主页',
    path: '/config/'
  },
  {
    key: 'apps-config2',
    title: 'config 列表页',
    path: '/config/table'
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
