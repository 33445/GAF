import Home from '../pages/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Login.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Table.vue')
  },
  {
    path: '/DataQualityModel',
    name: 'DataQualityModel',
    component: () => import(/* webpackChunkName: "about" */ '../pages/DataQualityModel/index.vue')
  },
  {
    path: '/DatasetList',
    name: 'DatasetList',
    component: () => import(/* webpackChunkName: "about" */ '../pages/DatasetList/index.vue')
  },
  {
    path: '/Datasync',
    name: 'Datasync',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Datasync/index.vue')
  },
  {
    path: '/model',
    name: 'model',
    component: () => import(/* webpackChunkName: "about" */ '../pages/model/index.vue')
  },
  {
    path: '/QualityInspectionPlan',
    name: 'QualityInspectionPlan',
    component: () => import(/* webpackChunkName: "about" */ '../pages/QualityInspectionPlan/index.vue')
  },
  {
    path: '/tile',
    name: 'Tile',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Tile/index.vue')
  },
  {
    path: '/SpaceDatasource',
    name: 'SpaceDatasource',
    component: () => import(/* webpackChunkName: "about" */ '../pages/SpaceDatasource/index.vue')
  },
  {
    path: '/StandardDatasource',
    name: 'StandardDatasource',
    component: () => import(/* webpackChunkName: "about" */ '../pages/StandardDatasource/index.vue')
  },
  {
    path: '/dataWorkspace',
    name: 'DataWorkspace',
    component: () => import(/* webpackChunkName: "about" */ '../pages/DataWorkspace/index.vue')
  },
  {
    path: '/SpaceDatasource',
    name: 'SpaceDatasource',
    component: () => import(/* webpackChunkName: "about" */ '../pages/SpaceDatasource/index.vue')
  },
  {
    path: '/StandardManagement',
    name: 'StandardManagement',
    component: () => import(/* webpackChunkName: "about" */ '../pages/StandardManagement/index.vue')
  },
  {
    path: '/ServicePublishing',
    name: 'ServicePublishing',
    component: () => import('../pages/ServicePublishing/index.vue')
  }
]

export default routes
