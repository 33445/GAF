
// 批量删除后,重新计算当前页
function computeNewCurrent(total, current, pageSize, deleteCount) {
  const left = total - deleteCount;
  const curretTotal = current * pageSize;
  if (left < curretTotal) {
    let newCurrent =
      left % pageSize == 0
        ? left / pageSize
        : Math.floor(left / pageSize) + 1;
    return newCurrent < 1 ? 1 : newCurrent;
  } else {
    return current;
  }
}

export {
  computeNewCurrent
}

