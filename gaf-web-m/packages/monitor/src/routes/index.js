import Home from '../pages/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Login.vue')
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../pages/Table.vue')
  },
  {
    path: '/containers',
    name: 'containers',
    component: () => import(/* webpackChunkName: "about" */ '../pages/containers')
  },
  {
    path: '/hosts',
    name: 'hosts',
    component: () => import(/* webpackChunkName: "about" */ '../pages/hosts')
  }
]

export default routes
