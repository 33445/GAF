#！/bin/bash

apps=""
substr="SUBAPPDIR"
string="\n  location /apps-SUBAPPDIR/ {\n    root /usr/share/nginx/html/apps;\n    index  index.html index.htm;\n    try_files \$uri \$uri/ /apps-SUBAPPDIR/index.html;\n  }"

for dir in `ls packages`
do
  app=${string//$substr/$dir}
  apps=$apps$app"\n"
done

sed -i "s#apps#$apps#g" default.conf
