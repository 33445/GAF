package com.supermap.gaf.extend.spi.provider;

import com.supermap.gaf.authentication.client.ValidateClient;
import com.supermap.gaf.extend.commontypes.AuthenticationParam;
import com.supermap.gaf.extend.commontypes.AuthenticationResult;
import com.supermap.gaf.extend.commontypes.MessageResult;
import com.supermap.gaf.extend.spi.ValidateAuthentication;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * 根据提取的凭证参数 远程调用gaf-authentication 返回用户等信息
 * @author wxl
 * @since 2022/4/7
 */
public abstract class AbstractValidateAuthenticationProvider implements ValidateAuthentication {

    @Autowired
    private ValidateClient validateClient;

    /**
     * 根据提取的凭证参数 远程调用gaf-authentication 返回用户等信息
     *
     * @param param
     * @return
     */
    @Override
    public MessageResult<AuthenticationResult> doValidateAuthentication(AuthenticationParam param) {
        if (param == null)  return null;
        com.supermap.gaf.authentication.entity.entity.AuthenticationParam authenticationParam = new com.supermap.gaf.authentication.entity.entity.AuthenticationParam();
        BeanUtils.copyProperties(param,authenticationParam);
        com.supermap.gaf.authentication.entity.entity.AuthenticationResult authenticationResult = validateClient.authentication(authenticationParam).checkAndGetData();
        AuthenticationResult result = new AuthenticationResult();
        BeanUtils.copyProperties(authenticationResult,result);
        return MessageResult.data(result).build();
    }


}
