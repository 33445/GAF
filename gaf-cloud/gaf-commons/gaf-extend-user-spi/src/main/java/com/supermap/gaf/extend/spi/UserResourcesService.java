package com.supermap.gaf.extend.spi;

import com.supermap.gaf.extend.commontypes.Menu;

import java.util.List;

/**
 * 获取用户的可访问的资源
 * 包括 菜单
 */
public interface UserResourcesService {

    /**
     * 获取当前用户的菜单
     * @return
     */
    List<Menu> listMenus();
}
