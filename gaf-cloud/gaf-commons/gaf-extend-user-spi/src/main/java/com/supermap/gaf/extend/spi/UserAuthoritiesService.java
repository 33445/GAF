package com.supermap.gaf.extend.spi;


import com.supermap.gaf.extend.commontypes.Role;

import java.util.List;

public interface UserAuthoritiesService {

    /**
     * 获取当前用户的角色列表
     * @return
     */
    List<Role> listRoles();
}
