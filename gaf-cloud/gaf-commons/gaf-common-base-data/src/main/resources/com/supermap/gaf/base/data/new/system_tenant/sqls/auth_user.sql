-- liquibase formatted sql logicalFilePath:system_tenant/auth_user
-- changeset SYS:20220303-0
CREATE TABLE "auth_user" (
  "user_id" varchar(36) NOT NULL,
  "sort_sn" int4,
  "name" varchar(50) NOT NULL,
  "password" varchar(100) ,
  "real_name" varchar(50) NOT NULL,
  "id_number" varchar(30) ,
  "mobile_number" varchar(30) ,
  "email" varchar(100) ,
  "address" varchar(200) ,
  "expiration_time" timestamp(6),
  "status" bool DEFAULT true,
  "description" varchar(500) ,
  "last_login_time" timestamp(6),
  "created_time" timestamp(6),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "state" bool,
  CONSTRAINT "auth_user_pkey" PRIMARY KEY ("user_id")
)
;



COMMENT ON COLUMN "auth_user"."user_id" IS '用户id。主键,uuid';

COMMENT ON COLUMN "auth_user"."sort_sn" IS '排序序号。同级中的序号';

COMMENT ON COLUMN "auth_user"."name" IS '登录用户名。登录名，可含字母数字下划线';

COMMENT ON COLUMN "auth_user"."password" IS '密码。存到认证中心，db可不存';

COMMENT ON COLUMN "auth_user"."real_name" IS '真实姓名。中文名称';

COMMENT ON COLUMN "auth_user"."id_number" IS '身份证号。';

COMMENT ON COLUMN "auth_user"."mobile_number" IS '手机号。';

COMMENT ON COLUMN "auth_user"."email" IS '邮箱。';

COMMENT ON COLUMN "auth_user"."address" IS '地址。';

COMMENT ON COLUMN "auth_user"."expiration_time" IS '授权截止时间。为空则永不过期';

COMMENT ON COLUMN "auth_user"."status" IS '状态。true:有效，false:过期/无效';

COMMENT ON COLUMN "auth_user"."description" IS '描述。';

COMMENT ON COLUMN "auth_user"."last_login_time" IS '上次登录时间。';

COMMENT ON COLUMN "auth_user"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "auth_user"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "auth_user"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "auth_user"."updated_by" IS '修改人。修改人user_id';

COMMENT ON COLUMN "auth_user"."state" IS '状态,是否启用.true启用,false禁用,默认启用';

COMMENT ON TABLE "auth_user" IS '用户表';

INSERT INTO "auth_user"("user_id", "sort_sn", "name", "password", "real_name", "id_number", "mobile_number", "email", "address", "expiration_time", "status", "description", "last_login_time", "created_time", "created_by", "updated_time", "updated_by", "state") VALUES ('user_000000', 1, 'sys_admin', '$2a$10$MHqyBinUz4ZU7X4OZlh/FuLU1CiQLwh9R3YbPhTTt.PhvDkJSJ4BK', '系统管理员', NULL, NULL, 'gaf_sys_admin@163.com', NULL, NULL, 't', '系统管理员用户，勿删', '2022-03-03 05:53:40.470625', '2020-10-29 22:45:13', 'SYS', '2021-12-14 02:19:52.00779', 'SYS', 't');
