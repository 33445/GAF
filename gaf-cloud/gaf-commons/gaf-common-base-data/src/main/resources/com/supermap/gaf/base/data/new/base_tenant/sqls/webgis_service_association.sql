-- liquibase formatted sql logicalFilePath:base_tenant/webgis_service_association
-- changeset SYS:20220303-0
CREATE TABLE "webgis_service_association" (
  "gis_service_assoc_id" varchar(36) NOT NULL,
  "gis_service_id" varchar(36) NOT NULL,
  "gis_data_service_id" varchar(36) NOT NULL,
  "description" varchar(500),
  "created_time" timestamp(6),
  "created_by" varchar(255),
  "updated_time" timestamp(6),
  "updated_by" varchar(255),
  CONSTRAINT "webgis_service_association_pkey" PRIMARY KEY ("gis_service_assoc_id")
)
;

COMMENT ON COLUMN "webgis_service_association"."gis_service_assoc_id" IS 'GIS服务关联id。主键,uuid';

COMMENT ON COLUMN "webgis_service_association"."gis_service_id" IS 'GIS服务。服务id，一个地图服务，对应一外或多个数据服务';

COMMENT ON COLUMN "webgis_service_association"."gis_data_service_id" IS 'GIS数据服务id。数据服务的服务,webgis_service中的一个数据服务';

COMMENT ON COLUMN "webgis_service_association"."description" IS '描述。';

COMMENT ON COLUMN "webgis_service_association"."created_time" IS '创建时间。生成时间不可变更';

COMMENT ON COLUMN "webgis_service_association"."created_by" IS '创建人。创建人user_id';

COMMENT ON COLUMN "webgis_service_association"."updated_time" IS '修改时间。修改时更新';

COMMENT ON COLUMN "webgis_service_association"."updated_by" IS '修改人。修改人user_id';

COMMENT ON TABLE "webgis_service_association" IS 'GIS服务关联表';