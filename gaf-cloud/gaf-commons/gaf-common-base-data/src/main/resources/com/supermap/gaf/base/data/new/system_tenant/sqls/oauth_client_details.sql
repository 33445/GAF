-- liquibase formatted sql logicalFilePath:system_tenant/oauth_client_details
-- changeset SYS:20220303-0

CREATE TABLE "oauth_client_details" (
  "client_id" varchar(256) NOT NULL,
  "resource_ids" varchar(256) ,
  "client_secret" varchar(256) ,
  "scope" varchar(256) ,
  "authorized_grant_types" varchar(256) ,
  "web_server_redirect_uri" varchar(256) ,
  "authorities" varchar(256) ,
  "access_token_validity" int4,
  "refresh_token_validity" int4,
  "additional_information" varchar(4096) ,
  "autoapprove" varchar(256)
)
;
COMMENT ON TABLE "oauth_client_details" IS 'oauth2 client表';

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO "oauth_client_details" VALUES ('custom_client', 'custom_client_rid', '$2a$10$cfHuOZPGdPU1LfPOsltQ9.FOfA6iD9R6FuQR.5rjd5AEd7qOl4eCq', 'all', 'password,authorization_code,refresh_token', 'http://localhost:10000', NULL, 7200, 72000, NULL, 'true');

-- ----------------------------
-- Primary Key structure for table oauth_client_details
-- ----------------------------
ALTER TABLE "oauth_client_details" ADD CONSTRAINT "oauth_client_details_pkey" PRIMARY KEY ("client_id");
