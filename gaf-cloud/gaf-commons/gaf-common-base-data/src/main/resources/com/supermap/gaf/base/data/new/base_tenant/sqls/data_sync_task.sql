-- liquibase formatted sql logicalFilePath:base_tenant/data_sync_task
-- changeset SYS:20220303-0
CREATE TABLE "data_sync_task" (
  "data_sync_task_id" varchar(36) NOT NULL,
  "data_sync_task_name" varchar(1000) NOT NULL,
  "data_sync_status" int2 NOT NULL,
  "spatial_datasource_id" varchar(36) NOT NULL,
  "standard_datasource_id" varchar(36) NOT NULL,
  "mapping_info" text ,
  "alert_email" varchar(1000) ,
  "alert_type" varchar(50) ,
  "data_sync_cron" varchar(1000) ,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  CONSTRAINT "data_sync_task_pkey" PRIMARY KEY ("data_sync_task_id"),
  CONSTRAINT "uk_task_name" UNIQUE ("data_sync_task_name")
)
;



COMMENT ON COLUMN "data_sync_task"."data_sync_task_id" IS '主键';

COMMENT ON COLUMN "data_sync_task"."data_sync_task_name" IS '同步任务名称';

COMMENT ON COLUMN "data_sync_task"."data_sync_status" IS '同步任务状态';

COMMENT ON COLUMN "data_sync_task"."spatial_datasource_id" IS '空间数据源id';

COMMENT ON COLUMN "data_sync_task"."standard_datasource_id" IS '标准数据源id';

COMMENT ON COLUMN "data_sync_task"."mapping_info" IS '映射信息 格式如：[{srcName:"A表",destName:"B表",fieldsMapping:[{srcName:"X1字段",destName:"X1字段"},{srcName:"X2字段",destName:"X2字段"}]},{}]';

COMMENT ON COLUMN "data_sync_task"."alert_email" IS '通知邮箱';

COMMENT ON COLUMN "data_sync_task"."alert_type" IS '通知方式 多选 1开始时通知、2结束时通知、3报错时通知；多选时用,作为分隔符';

COMMENT ON COLUMN "data_sync_task"."data_sync_cron" IS '定时任务cron';

COMMENT ON COLUMN "data_sync_task"."description" IS '描述';

COMMENT ON COLUMN "data_sync_task"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_sync_task"."created_by" IS '创建人';

COMMENT ON COLUMN "data_sync_task"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_sync_task"."updated_by" IS '更新人';

COMMENT ON TABLE "data_sync_task" IS '数据同步任务表';