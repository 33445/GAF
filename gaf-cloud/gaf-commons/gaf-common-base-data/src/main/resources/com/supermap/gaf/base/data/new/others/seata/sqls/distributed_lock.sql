-- liquibase formatted sql logicalFilePath:others/seata/distributed_lock
-- changeset SYS:20220409-0
CREATE TABLE IF NOT EXISTS distributed_lock (
      lock_key     VARCHAR(20)  NOT NULL,
      lock_value        VARCHAR(20)  NOT NULL,
      expire       BIGINT       NOT NULL,
      CONSTRAINT pk_distributed_lock_table PRIMARY KEY (lock_key)
);

INSERT INTO distributed_lock (lock_key, lock_value, expire) VALUES ('HandleAllSession', ' ', 0);
