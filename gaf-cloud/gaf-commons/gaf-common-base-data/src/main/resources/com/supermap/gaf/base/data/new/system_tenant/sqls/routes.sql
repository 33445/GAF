-- liquibase formatted sql logicalFilePath:system_tenant/routes
-- changeset SYS:20220303-0

CREATE TABLE "routes" (
  "id" varchar(50) NOT NULL,
  "route_id" varchar(64) ,
  "route_uri" varchar(128) ,
  "route_order" int4,
  "predicates" text ,
  "filters" text ,
  "enable" bool,
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "type" varchar(10)
)
;
COMMENT ON COLUMN "routes"."route_id" IS '路由id';
COMMENT ON COLUMN "routes"."route_uri" IS '转发目标uri';
COMMENT ON COLUMN "routes"."route_order" IS '路由执行顺序';
COMMENT ON COLUMN "routes"."predicates" IS '断言字符串集合，字符串结构：jsonArray.toString';
COMMENT ON COLUMN "routes"."filters" IS '过滤器字符串集合，字符串结构：jsonArray.toString';
COMMENT ON COLUMN "routes"."enable" IS '是否启用';
COMMENT ON COLUMN "routes"."create_time" IS '创建时间';
COMMENT ON COLUMN "routes"."update_time" IS '修改时间';
COMMENT ON COLUMN "routes"."type" IS '类型';
COMMENT ON TABLE "routes" IS '路由表';

-- ----------------------------
-- Records of routes
-- ----------------------------
INSERT INTO "routes" VALUES ('bEnAZj', 'gaf-microservice-conf', 'lb://gaf-microservice-conf', 0, '[{"args":"/api/configcenter/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2019-09-19 10:19:41', '2019-09-19 10:19:41', 'TENANT');
INSERT INTO "routes" VALUES ('m22Mz3', 'gaf-portal', 'lb://gaf-portal', 1, '[{"args":"/api/portal/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2019-08-02 08:16:36', '2019-09-27 17:22:58', 'TENANT');
INSERT INTO "routes" VALUES ('m22Mz6', 'gaf-map', 'lb://gaf-map', 0, '[{"args":"/api/map/**,/api/manager/map/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2019-08-02 08:16:36', '2019-08-30 00:59:17', 'TENANT');
INSERT INTO "routes" VALUES ('nYf2a7', 'gaf-microservice-api', 'lb://gaf-microservice-api', 0, '[{"args":"/api/docs/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2019-09-04 02:21:55', '2020-03-18 06:50:14', 'TENANT');
INSERT INTO "routes" VALUES ('Nbeuim', 'gaf-project', 'lb://gaf-project', 0, '[{"args":"/api/proj/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2020-08-14 07:40:36', '2020-08-28 06:16:52', 'TENANT');
INSERT INTO "routes" VALUES ('JnEJRn', 'gaf-authority', 'lb://GAF-AUTHORITY', 0, '[{"args":"/api/authority/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2020-09-27 09:27:59', '2020-09-27 10:25:52', 'TENANT');
INSERT INTO "routes" VALUES ('aEFzMa', 'gaf-microservice-governance', 'lb://GAF-MICROSERVICE-GOVERNANCE', 0, '[{"args":"/api/srv-governance/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2020-11-25 16:45:36', '2020-11-25 16:45:36', 'TENANT');
INSERT INTO "routes" VALUES ('6bmyEn', 'gaf-data-mgt', 'lb://gaf-data-mgt', 0, '[{"args":"/api/data-mgt/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2021-02-24 08:18:54', '2021-02-24 08:18:54', NULL);
INSERT INTO "routes" VALUES ('qqaUr2', 'gaf-prometheus', 'http://gaf-prometheus:9090', 0, '[{"args":"/api/prometheus/**","name":"Path"}]', '[{"args":"2","name":"StripPrefix"}]', 't', '2020-12-26 02:25:20', '2020-12-26 02:27:19', NULL);
INSERT INTO "routes" VALUES ('uu6vEr', 'gaf-sys-mgt', 'lb://gaf-sys-mgt', 0, '[{"args":"/api/sys-mgt/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2021-04-23 04:30:33', '2021-04-23 04:30:33', NULL);
INSERT INTO "routes" VALUES ('Bje6be', 'gaf-storage', 'lb://gaf-storage', 0, '[{"args":"/api/storage/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2021-04-22 06:18:06', '2021-04-22 08:38:32', NULL);
INSERT INTO "routes" VALUES ('6Vbi2x', 'gaf-authentication', 'lb://GAF-AUTHENTICATION', 0, '[{"args":"/api/oauth/**,/api/authentication/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2020-10-19 07:45:09', '2020-11-22 22:51:18', 'TENANT');

-- ----------------------------
-- Primary Key structure for table routes
-- ----------------------------
ALTER TABLE "routes" ADD CONSTRAINT "routes_pkey" PRIMARY KEY ("id");

-- changeset SYS:20220303-5
INSERT INTO routes ( id, route_id, route_uri, route_order, predicates, filters, enable, create_time, update_time, type ) VALUES ( 'uu6vEy', 'gaf-platform', 'lb://gaf-platform', 0, '[{"args":"/api/platform/**","name":"Path"}]', '[{"args":"1","name":"StripPrefix"}]', 't', '2021-04-23 04:30:33', '2021-04-23 04:30:33', NULL );