-- liquibase formatted sql logicalFilePath:base_tenant/data_quality_model
-- changeset SYS:20220303-0
CREATE TABLE "data_quality_model" (
  "data_quality_model_id" varchar(36) NOT NULL,
  "data_quality_model_name" varchar(1000) NOT NULL,
  "datasource_id" varchar(36) NOT NULL,
  "sort_sn" int2 DEFAULT 1,
  "description" text ,
  "created_time" timestamp(6) NOT NULL DEFAULT now(),
  "created_by" varchar(255) ,
  "updated_time" timestamp(6),
  "updated_by" varchar(255) ,
  "datasource_type" varchar(255) ,
  CONSTRAINT "data_quality_model_pkey" PRIMARY KEY ("data_quality_model_id")
)
;



COMMENT ON COLUMN "data_quality_model"."data_quality_model_id" IS '主键';

COMMENT ON COLUMN "data_quality_model"."data_quality_model_name" IS '数据质量管理模型名称';

COMMENT ON COLUMN "data_quality_model"."datasource_id" IS '数据源id';

COMMENT ON COLUMN "data_quality_model"."sort_sn" IS '排序';

COMMENT ON COLUMN "data_quality_model"."description" IS '描述';

COMMENT ON COLUMN "data_quality_model"."created_time" IS '创建时间';

COMMENT ON COLUMN "data_quality_model"."created_by" IS '创建人';

COMMENT ON COLUMN "data_quality_model"."updated_time" IS '更新时间';

COMMENT ON COLUMN "data_quality_model"."updated_by" IS '更新人';

COMMENT ON COLUMN "data_quality_model"."datasource_type" IS '数据源类型。0，普通数据源，1，标准数据源';

COMMENT ON TABLE "data_quality_model" IS '数据质量管理-模型表';