package com.supermap.gaf.docker.entity;

import lombok.Data;

/**
 * @author dqc
 * @date 2022/4/29
 */
@Data
public class DockerBuildParamVo {
    /**
     *  镜像地址
     */
    private String tagFullName;
    /**
     * 仓库地址
     */
    private String registryAddress;
    /**
     * 仓库用户名
     */
    private String registryUsername;
    /**
     * 仓库密码
     */
    private String registryPassword;
}
