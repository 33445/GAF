/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.supermap.gaf.annotation.LogicDeleteField;
import com.supermap.gaf.annotation.ParentIdField;
import com.supermap.gaf.annotation.SortSnField;
import com.supermap.gaf.annotation.UpdatedTimeField;
import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;


/**
 * @author dqc
 * @date:2021/3/25
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户")
public class AuthUserSupplementVo implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String TENANT_ADMIN_ROLE_ID = "role_000001";
    @Id
    @ApiModelProperty("用户id")
    private String userId;
    @NotNull
    @ApiModelProperty("登录用户名")
    private String name;
    @ApiModelProperty("密码")
    private String password;
    @NotNull
    @ApiModelProperty("真实姓名")
    private String realName;
    @ApiModelProperty("身份证号")
    private String idNumber;
    @ApiModelProperty("手机号")
    private String mobileNumber;

    @Pattern(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message = "不满足邮箱格式")
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("授权截止时间")
    private Date expirationTime;


    @ApiModelProperty("状态,是否启用.true启用,false禁用,默认启用")
    private Boolean state;

    @ApiModelProperty("逻辑删除字段,false表示已删除")
    @JSONField(name = "isStatus")
    @LogicDeleteField
    private Boolean status = true;


    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("上次登录时间")
    private Date lastLoginTime;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    @UpdatedTimeField
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;

    // transient
    // 开发租户功能，用户表表拆分后，非平台属性移动如下
    @ApiModelProperty("部门id")
    @ParentIdField
    private String departmentId;
    @ApiModelProperty("岗位id")
    private String postId;


    @ApiModelProperty("部门名称")
    private String departmentName;
    @ApiModelProperty("岗位名称")
    private String postName;


    @ApiModelProperty(value = "排序序号", example = "1", allowableValues = "range[1,infinity]")
    @SortSnField
    private Integer sortSn;

    public AuthUser extractAuthUser(){
        AuthUser authUser = new AuthUser();
        authUser.setUserId(this.userId);
        authUser.setName(this.name);
        authUser.setPassword(this.password);
        authUser.setRealName(this.realName);
        authUser.setIdNumber(this.idNumber);
        authUser.setMobileNumber(this.mobileNumber);
        authUser.setEmail(this.email);
        authUser.setAddress(this.address);
        authUser.setExpirationTime(this.expirationTime);
        authUser.setState(this.state);
        authUser.setStatus(this.status);
        authUser.setDescription(this.description);
        authUser.setLastLoginTime(this.lastLoginTime);
        authUser.setCreatedTime(this.createdTime);
        authUser.setCreatedBy(this.createdBy);
        authUser.setUpdatedTime(this.updatedTime);
        authUser.setUpdatedBy(this.updatedBy);
        return authUser;
    }

    public AuthUserSupplement extractAuthUserSupplement(){
        AuthUserSupplement authUserSupplement = new AuthUserSupplement();
        authUserSupplement.setUserId(userId);
        authUserSupplement.setUserName(name);
        authUserSupplement.setCreatedBy(createdBy);
        authUserSupplement.setUpdatedBy(updatedBy);
        authUserSupplement.setCreatedTime(createdTime);
        authUserSupplement.setUpdatedTime(updatedTime);
        authUserSupplement.setDepartmentId(departmentId);
        authUserSupplement.setPostId(postId);
        authUserSupplement.setSortSn(sortSn);
        return authUserSupplement;
    }



    public AuthUserSupplementVo(AuthUser authUser, AuthUserSupplement authUserSupplement){
        if (authUser != null){
            this.userId = authUser.getUserId();
            this.name = authUser.getName();
            this.password = authUser.getPassword();
            this.realName = authUser.getRealName();
            this.idNumber = authUser.getIdNumber();
            this.mobileNumber = authUser.getMobileNumber();
            this.email = authUser.getEmail();
            this.address = authUser.getAddress();
            this.expirationTime = authUser.getExpirationTime();
            this.state = authUser.getState();
            this.status = authUser.getStatus();
            this.description = authUser.getDescription();
            this.lastLoginTime = authUser.getLastLoginTime();
            this.createdTime = authUser.getCreatedTime();
            this.createdBy = authUser.getCreatedBy();
            this.updatedTime = authUser.getUpdatedTime();
            this.updatedBy = authUser.getUpdatedBy();
        }
        if (authUserSupplement != null){
            this.departmentId = authUserSupplement.getDepartmentId();
            this.sortSn = authUserSupplement.getSortSn();
            this.postId = authUserSupplement.getPostId();
        }
    }

}
