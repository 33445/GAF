package com.supermap.gaf.data.mgt.util;

import com.supermap.data.*;
import com.supermap.gaf.commontypes.ExceptionFunction;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.FieldMapping;
import com.supermap.gaf.data.mgt.entity.MmField;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.ChangeName;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.CreateFromTemplate;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.Delete;
import com.supermap.gaf.data.mgt.entity.changeop.dataset.ImportFromOtherDataset;
import com.supermap.gaf.data.mgt.entity.vo.TablePage;
import com.supermap.gaf.exception.GafException;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.*;

/**
 * The type Iobject utils.
 */
public class IobjectUtils {
    private static final String GeoCoordSys_GeoDatumPlane = "大地基准面";
    private static final String GeoCoordSys_ReferenceSpheroid = "参考椭球体";
    private static final String GeoSpheroid_Axis = "椭球长半轴";
    private static final String GeoSpheroid_Flatten = "椭球扁率";
    private static final String CenterBasisMeridian = "中央子午线";
    private static final String Projection_ProjectionType = "投影方式";
    private static final String CenterMeridian = "中央经线";
    private static final String CentralParallel = "原点纬线";
    private static final String StandardParallel1 = "标准纬线(1)";
    private static final String StandardParallel2 = "标准纬线(2)";
    private static final String FalseEasting = "水平偏移量";
    private static final String FalseNorthing = "垂直偏移量";
    private static final String ScaleFactor = "比例因子";
    private static final String Azimuth = "方位角";
    private static final String FirstPointLongitude = "第一经纬线";
    private static final String SecondPointLongitude = "第二经纬线";

    /**
     * Gets dataset properties.
     *
     * @param dataset the dataset
     * @return the dataset properties
     */
    public static Map<String, Object> getDatasetProperties(Dataset dataset) {
        Map<String, Object> re = new HashMap<>();
        re.put("type", dataset.getType().name());
        re.put("name", dataset.getName());
        re.put("tableName", dataset.getTableName());
        re.put("bounds", getDatasetBounds(dataset));
        return re;
    }

    public static Set<DatasetType> getContainDatasetTypes(Workspace workspace){
        Set<DatasetType> re = new HashSet<>();
        if (workspace != null && workspace.getDatasources() != null && workspace.getDatasources().getCount() > 0) {
            for(int i = 0; i < workspace.getDatasources().getCount(); ++i) {
                Datasource datasource = workspace.getDatasources().get(i);
                if (datasource.getDatasets() != null && datasource.getDatasets().getCount() > 0) {
                    for(int index = 0; index < datasource.getDatasets().getCount(); ++index) {
                        Dataset dataset = datasource.getDatasets().get(index);
                        re.add(dataset.getType());
                    }
                }
            }
        }
        return re;
    }

    /**
     * Gets dataset bounds.
     *
     * @param dataset the dataset
     * @return the dataset bounds
     */
    public static Map<String, Object> getDatasetBounds(Dataset dataset) {

        Map<String, Object> re = new HashMap<>();
        Rectangle2D bounds = dataset.getBounds();
        re.put("left", Double.toString(bounds.getLeft()));
        re.put("top", Double.toString(bounds.getTop()));
        re.put("right", Double.toString(bounds.getRight()));
        re.put("bottom", Double.toString(bounds.getBottom()));
        return re;
    }

    /**
     * Gets prj coord sys propertes.
     *
     * @param dataset the dataset
     * @return the prj coord sys propertes
     */
    public static Map<String, Object> getPrjCoordSysPropertes(Dataset dataset) {
        Map<String, Object> re = new HashMap<>();
        if (!DatasetType.TABULAR.equals(dataset.getType())) {
            PrjCoordSys prjCoordSys = dataset.getPrjCoordSys();
            re.put("name", prjCoordSys.getName());
            re.put("coordUnit", prjCoordSys.getCoordUnit().toString());
            re.put("detail", getPrjCoordSysDetails(prjCoordSys));
        }
        return re;
    }

    /**
     * Gets prj coord sys details.
     *
     * @param prj the prj
     * @return the prj coord sys details
     */
    public static Map<String, Object> getPrjCoordSysDetails(PrjCoordSys prj) {
        Map<String, Object> re = new LinkedHashMap<>();
        if (prj.getType() != PrjCoordSysType.PCS_NON_EARTH) {
            re.put("EPSG_Code", prj.getEPSGCode());
            re.put(GeoCoordSys_GeoDatumPlane, prj.getGeoCoordSys().getGeoDatum().getName());
            re.put(GeoCoordSys_ReferenceSpheroid, prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getName());
            re.put(GeoSpheroid_Axis, prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getAxis());
            re.put(GeoSpheroid_Flatten, prj.getGeoCoordSys().getGeoDatum().getGeoSpheroid().getFlatten());
            re.put(CenterBasisMeridian, prj.getGeoCoordSys().getGeoPrimeMeridian().getLongitudeValue());
            if (prj.getType() != PrjCoordSysType.PCS_EARTH_LONGITUDE_LATITUDE) {
                re.put(Projection_ProjectionType, prj.getProjection().getType().name());
                re.put(CenterMeridian, prj.getPrjParameter().getCentralMeridian());
                re.put(CentralParallel, prj.getPrjParameter().getCentralParallel());
                re.put(StandardParallel1, prj.getPrjParameter().getStandardParallel1());
                re.put(StandardParallel2, prj.getPrjParameter().getStandardParallel2());
                re.put(FalseEasting, prj.getPrjParameter().getFalseEasting());
                re.put(FalseNorthing, prj.getPrjParameter().getFalseNorthing());
                re.put(ScaleFactor, prj.getPrjParameter().getScaleFactor());
                re.put(Azimuth, prj.getPrjParameter().getAzimuth());
                re.put(FirstPointLongitude, prj.getPrjParameter().getFirstPointLongitude());
                re.put(SecondPointLongitude, prj.getPrjParameter().getSecondPointLongitude());
            }
        }
        return re;
    }

    /**
     * Gets recordset property.
     *
     * @param dataset the dataset
     * @return the recordset property
     */
    public static List<MmField> getRecordsetProperty(Dataset dataset) {
        List<MmField> re = new ArrayList<>();
        if (dataset instanceof DatasetVector) {
            DatasetVector datasetVector = (DatasetVector) dataset;
            FieldInfos fieldInfos = datasetVector.getFieldInfos();
            for (int i = 0; i < fieldInfos.getCount(); ++i) {
                FieldInfo fieldInfo = fieldInfos.get(i);
                if (fieldInfos.get(i).isSystemField()) {
//                    item.put("fieldName", "*" + fieldInfo.getName());
                    continue;
                }
                re.add(DatamgtCommonUtils.convert2MmField(fieldInfo,type->"sdx_"+type));
            }
        }
        return re;
    }

    /**
     * Is modify field enable boolean.
     *
     * @param dataset the dataset
     * @return the boolean
     */
    public static boolean isModifyFieldEnable(Dataset dataset){
        EngineType engineType = dataset.getDatasource().getEngineType();
        return dataset instanceof DatasetVector && (engineType == EngineType.ORACLEPLUS || engineType == EngineType.SQLPLUS || engineType == EngineType.POSTGRESQL || engineType == EngineType.KINGBASE || engineType == EngineType.MYSQL);
    }

    /**
     * Modify filed.
     *
     * @param dataset      the dataset
     * @param fieldName    the field name
     * @param newFieldInfo the new field info
     */
    public static void modifyFiled(Dataset dataset,String fieldName,FieldInfo newFieldInfo){
        if(!isModifyFieldEnable(dataset)){
            throw new IllegalArgumentException("该数据集不支持修改字段操作");
        }
        FieldInfos fieldInfos = ((DatasetVector)dataset).getFieldInfos();
        fieldInfos.modify(fieldName, newFieldInfo);
    }

    /**
     * Add field.
     *
     * @param dataset      the dataset
     * @param newFieldInfo the new field info
     */
    public static void addField(Dataset dataset,FieldInfo newFieldInfo){
        if(!(dataset instanceof DatasetVector)){
            throw new IllegalArgumentException("该数据集类型不支持增加字段操作");
        }
        FieldInfos fieldInfos = ((DatasetVector)dataset).getFieldInfos();
        fieldInfos.add(newFieldInfo);
    }

    /**
     * Remove field.
     *
     * @param dataset    the dataset
     * @param fieldNames the field names
     */
    public static void removeFields(Dataset dataset,List<String> fieldNames){
        if(!(dataset instanceof DatasetVector)){
            throw new IllegalArgumentException("该数据集类型不支持删除字段操作");
        }
        FieldInfos fieldInfos = ((DatasetVector)dataset).getFieldInfos();
        for(String fieldName:fieldNames){
            fieldInfos.remove(fieldName);
        }
    }

    /**
     * Get maps list.
     *
     * @param workspace the workspace
     * @return the list
     */
    public static List<String> getMaps(Workspace workspace){
        List<String> re = new ArrayList<>();
        Maps maps = workspace.getMaps();
        int count = maps.getCount();
        for(int i=0;i<count;++i){
            re.add(maps.get(i));
        }
        return re;
    }

    /**
     * Get scenes list.
     *
     * @param workspace the workspace
     * @return the list
     */
    public static List<String> getScenes(Workspace workspace){
        List<String> re = new ArrayList<>();
        Scenes scenes = workspace.getScenes();
        int count = scenes.getCount();
        for(int i=0;i<count;++i){
            re.add(scenes.get(i));
        }
        return re;
    }

    /**
     * Get record amount long.
     *
     * @param dataset the dataset
     * @return the long
     */
    public static long getRecordAmount(Dataset dataset){
        Recordset recordset = query(dataset,"",new String[]{"count(*)"},false);
        Map<Integer, Feature> featureMap = recordset.getAllFeatures();
        long re = 0;
        for(Integer seq:featureMap.keySet()){
            Feature feature = featureMap.get(seq);
            re = feature.getInt64(0);
        }
        return re;
    }

    /**
     * Query recordset.
     *
     * @param dataset   the dataset
     * @param where     the where
     * @param fields    the fields
     * @param returnGeo the return geo
     * @return the recordset
     */
    public static Recordset query(Dataset dataset,String where,String[] fields,boolean returnGeo){
        if(!(dataset instanceof DatasetVector)){
            throw new IllegalArgumentException("该数据集类型不支持查询记录操作");
        }
        DatasetVector datasetVector = (DatasetVector)dataset;
        QueryParameter queryParameter = new QueryParameter();
        queryParameter.setCursorType(CursorType.STATIC);
        queryParameter.setHasGeometry(returnGeo);
        queryParameter.setAttributeFilter(where);
        queryParameter.setResultFields(fields);
        Recordset recordset = datasetVector.query(queryParameter);
        return recordset;
    }

    /**
     * Query recordset by page table page.
     *
     * @param dataset   the dataset
     * @param pageIndex the page index
     * @param pageSize  the page size
     * @param returnGeo the return geo
     * @return the table page
     */
    public static TablePage queryRecordsetByPage(Dataset dataset, Integer pageIndex, Integer pageSize, boolean returnGeo){
//        if (datasetVector.getDatasource().getEngineType().equals(EngineType.MONGODB)) {
//            return datasetVector.getRecordset(false, CursorType.DYNAMIC);
//        }
        if(!(dataset instanceof DatasetVector)){
            throw new IllegalArgumentException("该数据集类型不支持查询记录操作");
        }
        if(pageIndex==null){
            pageIndex = 1;
        }
        if(pageSize==null){
            pageSize = 50;
        }
        DatasetVector datasetVector = (DatasetVector)dataset;
        int minSmID = getMinSmID(datasetVector);
        int maxSmID = getMaxSmID(datasetVector);
        QueryParameter queryParameter = new QueryParameter();
        queryParameter.setCursorType(CursorType.STATIC);
        String smIDField = datasetVector.getFieldNameBySign(FieldSign.ID);
        if (maxSmID > pageSize) {
            queryParameter.setAttributeFilter(smIDField + ">= " + ((pageIndex - 1L) * pageSize + minSmID) + "  AND " + smIDField + " < " + (minSmID + pageIndex * pageSize));
        }
        queryParameter.setOrderBy(new String[]{smIDField});
        queryParameter.setHasGeometry(returnGeo);
        Recordset recordset = datasetVector.query(queryParameter);
        Map<Integer, Feature> featureMap = recordset.getAllFeatures();
        List<Map> list = new ArrayList();
        List<TablePage.TableHeader> tableHeaders = new ArrayList<>();
        FieldInfos fieldInfos = recordset.getFieldInfos();
        for(int i=0;i<fieldInfos.getCount();++i){
            FieldInfo fieldInfo = fieldInfos.get(i);
            String rowId = fieldInfo.getName();
            String rowName = StringUtils.isEmpty(fieldInfo.getCaption())?fieldInfo.getName():fieldInfo.getCaption();
            TablePage.TableHeader header = new TablePage.TableHeader(rowId,rowName,3);
            header.put("isSystemField",fieldInfo.isSystemField());
            tableHeaders.add(header);
        }
        for(Integer seq:featureMap.keySet()){
            Feature feature = featureMap.get(seq);
            Map<String,Object> item = new HashMap<>();
            for(int i=0;i<fieldInfos.getCount();++i){
                String fieldName = fieldInfos.get(i).getName();
                item.put(fieldName,feature.getValue(fieldName));
            }
            list.add(item);
        }
        return TablePage.create(pageIndex,pageSize, (int) (maxSmID-minSmID+1),tableHeaders, list);
    }

    /**
     * Gets min sm id.
     *
     * @param datasetVector the dataset vector
     * @return the min sm id
     */
    public static int getMinSmID(DatasetVector datasetVector) {
        if (datasetVector.getDatasource().getEngineType().equals(EngineType.MONGODB)) {
            return 0;
        } else {
            QueryParameter queryParameter = new QueryParameter();
            queryParameter.setCursorType(CursorType.DYNAMIC);
            queryParameter.setResultFields(new String[]{MessageFormat.format("min({0})", datasetVector.getFieldNameBySign(FieldSign.ID))});
            queryParameter.setHasGeometry(false);
            Recordset recordset = datasetVector.query(queryParameter);
            return recordset.getRecordCount() > 0 && recordset.getValues()[0] != null ? recordset.getInt32(0) : 0;
        }
    }

    /**
     * Gets max sm id.
     *
     * @param datasetVector the dataset vector
     * @return the max sm id
     */
    public static int getMaxSmID(DatasetVector datasetVector) {
        if (datasetVector.getDatasource().getEngineType().equals(EngineType.MONGODB)) {
            return 0;
        } else {
            QueryParameter queryParameter = new QueryParameter();
            queryParameter.setCursorType(CursorType.DYNAMIC);
            queryParameter.setResultFields(new String[]{MessageFormat.format("max({0})", datasetVector.getFieldNameBySign(FieldSign.ID))});
            queryParameter.setHasGeometry(false);
            Recordset recordset = datasetVector.query(queryParameter);
            return recordset.getRecordCount() > 0 && recordset.getValues()[0] != null ? recordset.getInt32(0) : 0;
        }
    }




    /**
     * Gets dataset type zh.
     *
     * @param datasetType the dataset type
     * @return the dataset type zh
     */
    public static String getDatasetTypeZh(DatasetType datasetType) {
        if (DatasetType.UNKNOWN.equals(datasetType)) {
            return "未知";
        } else if (DatasetType.TABULAR.equals(datasetType)) {
            return "纯属性";
        } else if (DatasetType.POINT.equals(datasetType)) {
            return "点";
        } else if (DatasetType.LINE.equals(datasetType)) {
            return "线";
        } else if (DatasetType.REGION.equals(datasetType)) {
            return "面";
        } else if (DatasetType.TEXT.equals(datasetType)) {
            return "文本";
        } else if (DatasetType.CAD.equals(datasetType)) {
            return " 复合";
        } else if (DatasetType.LINKTABLE.equals(datasetType)) {
            return "数据库表";
        } else if (DatasetType.NETWORK.equals(datasetType)) {
            return "网络";
        } else if (DatasetType.NETWORK3D.equals(datasetType)) {
            return "三维网络";
        } else if (DatasetType.LINEM.equals(datasetType)) {
            return "路由";
        } else if (DatasetType.PARAMETRICLINE.equals(datasetType)) {
            return "复合参数化线";
        } else if (DatasetType.PARAMETRICREGION.equals(datasetType)) {
            return "复合参数化面";
        } else if (DatasetType.PARAMETRICPOINT.equals(datasetType)) {
            return "复合参数化点";
        } else if (DatasetType.PARAMETRICTEXT.equals(datasetType)) {
            return "复合参数化文本";
        } else if (DatasetType.GRIDCOLLECTION.equals(datasetType)) {
            return "栅格集合";
        } else if (DatasetType.IMAGECOLLECTION.equals(datasetType)) {
            return "影像集合";
        } else if (DatasetType.MODEL.equals(datasetType)) {
            return "模型";
        } else if (DatasetType.TEXTURE.equals(datasetType)) {
            return "纹理";
        } else if (DatasetType.IMAGE.equals(datasetType)) {
            return "影像";
        } else if (DatasetType.WMS.equals(datasetType)) {
            return "WMS";
        } else if (DatasetType.WCS.equals(datasetType)) {
            return "WCS";
        } else if (DatasetType.GRID.equals(datasetType)) {
            return "栅格";
        } else if (DatasetType.VOLUME.equals(datasetType)) {
            return "栅格体";
        } else if (DatasetType.TOPOLOGY.equals(datasetType)) {
            return "拓扑";
        } else if (DatasetType.POINT3D.equals(datasetType)) {
            return "未知";
        } else if (DatasetType.LINE3D.equals(datasetType)) {
            return "三维点";
        } else if (DatasetType.REGION3D.equals(datasetType)) {
            return "三维面";
        } else if (DatasetType.POINTEPS.equals(datasetType)) {
            return "清华山维点";
        } else if (DatasetType.LINEEPS.equals(datasetType)) {
            return "清华山维线";
        } else if (DatasetType.REGIONEPS.equals(datasetType)) {
            return "清华山维面";
        } else if (DatasetType.TEXTEPS.equals(datasetType)) {
            return "清华山维文本";
        } else if (DatasetType.VECTORCOLLECTION.equals(datasetType)) {
            return "矢量集合";
        } else if (DatasetType.MOSAIC.equals(datasetType)) {
            return "镶嵌";
        } else if (DatasetType.VIDEO.equals(datasetType)) {
            return "视频";
        } else {
            throw new GafException("不支持的数据集类型" + datasetType.name());
        }
    }

    /**
     * Dataset processor object.
     *
     * @param connectionInfo the connection info
     * @param datasetName    the dataset name
     * @param process        the process
     * @return the object
     */
    public static Object datasetProcessor(DatasourceConnectionInfo connectionInfo, String datasetName, ExceptionFunction<Dataset, Object> process) {
        Datasource datasource = new Datasource(connectionInfo.getEngineType());
        Object re = null;
        try {
            boolean isOpen = datasource.open(connectionInfo);
            if(!isOpen){
                throw new GafException("数据源打开失败");
            }
            Datasets datasets = datasource.getDatasets();
            Dataset dataset = datasets.get(datasetName);
            if(dataset == null){
                throw new GafException(datasetName+"数据集不存在");
            }

            return process.apply(dataset);
        } finally {
            if (datasource != null && datasource.isOpened()) {
                try {
                    datasource.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Object datasetProcessor(Datasource datasource, String datasetName, ExceptionFunction<Dataset, Object> process) {
        Dataset dataset = null;
        try {
            Datasets datasets = datasource.getDatasets();
            dataset = datasets.get(datasetName);
            if(dataset == null){
                throw new GafException(datasetName+"数据集不存在");
            }
            return process.apply(dataset);
        } finally {
            if (dataset != null && dataset.isOpen()) {
                try {
                    dataset.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Dataset processor t.
     *
     * @param <T>     the type parameter
     * @param dataset the dataset
     * @param process the process
     * @return the t
     */
    public static <T> T datasetProcessor(Dataset dataset, ExceptionFunction<Dataset, T> process) {
        try {
            if (!dataset.isOpen()) {
                dataset.open();
            }
            return process.apply(dataset);
        } catch (Exception e) {
            throw new GafException(e.getMessage(),e);
        } finally {
            if (dataset != null && dataset.isOpen()) {
                try {
                    dataset.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Datasource processor object.
     *
     * @param connectionInfo the connection info
     * @param process        the process
     * @return the object
     */
    public static Object datasourceProcessor(DatasourceConnectionInfo connectionInfo, ExceptionFunction<Datasource, Object> process) {
        Datasource datasource = new Datasource(connectionInfo.getEngineType());
        Object re = null;
        try {
            datasource.open(connectionInfo);
            re = process.apply(datasource);
            return re;
        } catch (Exception e) {
            throw new GafException(e.getMessage());
        } finally {
            if (datasource != null && datasource.isOpened()) {
                try {
                    datasource.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Workspace processor object.
     *
     * @param workspaceConnectionInfo the workspace connection info
     * @param process                 the process
     * @return the object
     */
    public static Object workspaceProcessor(WorkspaceConnectionInfo workspaceConnectionInfo, ExceptionFunction<Workspace, Object> process) {
        return workspaceProcessor2(workspaceConnectionInfo,process);
    }

    /**
     * Datasource processor t.
     *
     * @param <T>        the type parameter
     * @param datasource the datasource
     * @param process    the process
     * @return the t
     */
    public static <T> T datasourceProcessor(Datasource datasource, ExceptionFunction<Datasource, T> process) {
        try {
            return process.apply(datasource);
        } catch (Exception e) {
            throw new GafException(e.getMessage(),e);
        } finally {
            if (datasource != null && datasource.isOpened()) {
                try {
                    datasource.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Workspace processor 2 t.
     *
     * @param <T>                     the type parameter
     * @param workspaceConnectionInfo the workspace connection info
     * @param process                 the process
     * @return the t
     */
    public static <T> T workspaceProcessor2(WorkspaceConnectionInfo workspaceConnectionInfo, ExceptionFunction<Workspace, T > process) {
        Workspace workspace = null;
        Datasource datasource = null;
        T re = null;
        try {
            workspace = new Workspace();
            workspace.open(workspaceConnectionInfo);
            re = process.apply(workspace);
            return re;
        } catch (Exception e) {
             throw new GafException(e.getMessage(),e);
        } finally {
            if (datasource != null && datasource.isOpened()) {
                try {
                    datasource.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (workspace != null) {
                workspace.close();
                workspace.dispose();
            }
        }
    }


    public static DatasourceConnectionInfo copyDatasourceConnectionInfo(DatasourceConnectionInfo connectionInfo) {
        DatasourceConnectionInfo result = new DatasourceConnectionInfo();
        result.setServer(connectionInfo.getServer());
        result.setDriver(connectionInfo.getDriver());
        result.setEngineType(connectionInfo.getEngineType());
        result.setPassword(connectionInfo.getPassword());
        result.setUser(connectionInfo.getUser());
        result.setAlias(connectionInfo.getAlias());
        result.setReadOnly(connectionInfo.isReadOnly());
        result.setDatabase(connectionInfo.getDatabase());
        return result;
    }
}
