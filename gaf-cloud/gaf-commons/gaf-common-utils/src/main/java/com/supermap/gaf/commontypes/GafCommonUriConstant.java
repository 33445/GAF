package com.supermap.gaf.commontypes;

/**
 * @author : duke
 * @since 2022/3/8 4:34 PM
 */
public class GafCommonUriConstant {
    public static final String URI_TENANT_LIST = "/platform/tenant-users/tenantList";
}
