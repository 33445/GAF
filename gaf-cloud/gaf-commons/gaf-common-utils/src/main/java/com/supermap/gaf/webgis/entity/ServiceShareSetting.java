package com.supermap.gaf.webgis.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 服务共享设置表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("服务共享设置表")
public class ServiceShareSetting implements Serializable{
    @NotNull
    @ApiModelProperty("GIS服务id。主键,uuid")
    private String gisServiceId;
    @NotNull
    @ApiModelProperty("用户id。主键,uuid")
    private String userId;
    @NotNull
    @ApiModelProperty("服务共享设置id。主键,uuid")
    private String serviceShareSettingId;
    @ApiModelProperty("字段设置")
    private String fieldsSetting;
    @ApiModelProperty("空间设置")
    private String spatialSetting;
    @ApiModelProperty("创建时间。生成时间不可变更")
    private Date createdTime;
    @ApiModelProperty("创建人。创建人user_id")
    private String createdBy;
    @ApiModelProperty("修改时间。修改时更新")
    private Date updatedTime;
    @ApiModelProperty("修改人。修改人user_id")
    private String updatedBy;
    @ApiModelProperty("用户名")
    private String username;
}