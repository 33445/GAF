package com.supermap.gaf.constant;

/**
 * @author : duke
 * @since 2021/5/28 4:08 PM
 */
public class CommonConstant {
    public static String JWT_HEADER = "Authorization";
    public static String JWT_PREFIX = "Bearer";
    public static String JWT_USER_NAME_FIELD = "user_name";
    public static final String JWT_USER_ID_FIELD = "user_id";
    public static String TIME_ZONE = "Asia/Shanghai";
    /**
     * mongo集合名称
     */
    public static final String METADATA_COLLECTION_NAME = "metadata";
    public static final String METADATA_INDEX_COLLECTION_NAME = "metadata_index";
}
