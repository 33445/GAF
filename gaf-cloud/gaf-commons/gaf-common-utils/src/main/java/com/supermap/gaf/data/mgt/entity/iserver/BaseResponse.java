package com.supermap.gaf.data.mgt.entity.iserver;

import com.supermap.gaf.data.mgt.util.IserverUtils;
import com.supermap.gaf.exception.GafException;
import lombok.Data;
import org.apache.http.util.Asserts;
import org.glassfish.jersey.internal.Errors;


/**
 * The type Base response.
 * 对应iserver iportal的错误响应格式，并将正确响应合并到data属性。通过一个接口封装iserver正确和错误两种格式
 *
 * @param <T> the type parameter
 */
@Data
public class BaseResponse<T> {
    private Boolean succeed;
    private ErrorState error;
    private T data;

    /**
     * Error base response.
     *
     * @param errorState the error state
     * @return the base response
     */
    public static final BaseResponse error(ErrorState errorState){
        BaseResponse re = new BaseResponse<>();
        re.setSucceed(false);
        re.setError(errorState);
        return re;
    }

    public  static <T> BaseResponse<T> ok(T data){
        BaseResponse re = new BaseResponse<>();
        re.setSucceed(true);
        re.setData(data);
        return re;
    }

    public final GafException toGafException(){
        Asserts.check(!getSucceed(),"BaseResponse 必须失败才能转换");
        ErrorState errorState = getError()!=null?getError():new ErrorState(500, "未知错误");
        throw new GafException(errorState.getErrorMsg(),errorState.getCode());
    }
}
