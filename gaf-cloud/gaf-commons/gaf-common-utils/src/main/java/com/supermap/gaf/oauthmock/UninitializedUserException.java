package com.supermap.gaf.oauthmock;

public class UninitializedUserException extends Exception {
    public UninitializedUserException() {
        super("该用户还未初始化");
    }

    public UninitializedUserException(String message) {
        super(message);
    }
}
