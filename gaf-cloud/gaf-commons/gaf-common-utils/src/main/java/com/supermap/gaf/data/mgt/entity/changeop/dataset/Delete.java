package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.Datasource;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import org.apache.http.util.Asserts;

/**
 * The type Delete.
 */
public class Delete extends DatasetChangeChain.DatasetChangeOp {

    /**
     * Instantiates a new Delete.
     *
     * @param outer the outer
     */
    public Delete(DatasetChangeChain outer){
        outer.super();
    }
    @Override
    public void update(Datasource datasource) {
        String datasetName = getDatasetName();
        datasource.getDatasets().delete(datasetName);
        setDatasetName(null);
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<Delete,DatasetChangeChain> {
        @Override
        public Delete build(){
            return new Delete(parentChain);
        }
    }
}
