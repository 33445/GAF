package com.supermap.gaf.webgis.vo;

import com.supermap.gaf.validator.StringRange;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 服务共享设置表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("服务共享设置表条件查询实体")
public class ServiceShareSettingSelectVo {
    @QueryParam("searchFieldName") 
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = ServiceShareSetting.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = ServiceShareSetting.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = ServiceShareSetting.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("gisServiceId")
    @ApiModelProperty("GIS服务id。主键,uuid")
    private String gisServiceId;
    @QueryParam("userId")
    @ApiModelProperty("用户id。主键,uuid")
    private String userId;
    @QueryParam("username")
    @ApiModelProperty("用户名")
    private String username;
    @QueryParam("serviceShareSettingId")
    @ApiModelProperty("服务共享设置id。主键,uuid")
    private String serviceShareSettingId;
    @QueryParam("fieldsSetting")
    @ApiModelProperty("字段设置")
    private String fieldsSetting;
    @QueryParam("spatialSetting")
    @ApiModelProperty("空间设置")
    private String spatialSetting;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间。生成时间不可变更")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人。创建人user_id")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("修改时间。修改时更新")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("修改人。修改人user_id")
    private String updatedBy;

    
}