package com.supermap.gaf.data.mgt.entity.iserver;

import com.supermap.gaf.exception.GafException;
import com.supermap.services.rest.management.ServiceType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javafx.beans.binding.ObjectExpression;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@ApiModel("发布工作空间参数")
public class PublishParam extends HashMap<String,Object> {
    private static final String workspaceConnectionInfo = "workspaceConnectionInfo";
    private static final String servicesTypes= "servicesTypes";
    private static final String isDataEditable= "isDataEditable";
    private static final String isMultiInstance = "isMultiInstance";
    private static final String instanceCount = "instanceCount";
    private static final String mapEditable= "mapEditable";
    private static final String dataProviderDelayCommitSetting= "dataProviderDelayCommitSetting";

    public PublishParam() {
        super();
        put(isDataEditable,false);
        put(isMultiInstance,false);
        put(instanceCount,0);
        put(mapEditable,false);

    }

    @Override
    public Object put(String key, Object value) {
        if(PublishParam.servicesTypes.equalsIgnoreCase(key) && value!=null){
            try{
                if(value instanceof String[]){
                    value = Arrays.stream((String[])value).map(item->ServiceType.valueOf(item)).toArray(ServiceType[]::new);
                }else if(value instanceof List){
                    value =  ((List<String>) value).stream().map(item->ServiceType.valueOf(item)).toArray(ServiceType[]::new);
                }
                return super.put(key,value);
            }catch (Exception e){
                throw new GafException("servicesTypes 解析成enum ServiceType[]失败");
            }
        }
        return super.put(key,value);
    }

    @ApiModelProperty(position = 1, required = true, value="工作空间连接信息")
    public String getWorkspaceConnectionInfo() {
        return (String) get(PublishParam.workspaceConnectionInfo);
    }

    public void setWorkspaceConnectionInfo(String workspaceConnectionInfo) {
        put(PublishParam.workspaceConnectionInfo,workspaceConnectionInfo);
    }

    public ServiceType[] getServicesTypes() {
        return (ServiceType[]) get(PublishParam.servicesTypes);
    }

    public void setServicesTypes(ServiceType[] servicesTypes) {
        put(PublishParam.servicesTypes,servicesTypes);
    }

    public boolean isDataEditable() {
        return (boolean) get(PublishParam.isDataEditable);
    }

    public void setIsDataEditable(boolean isDataEditable) {
        put(PublishParam.isDataEditable,isDataEditable);

    }

    public boolean isMultiInstance() {
        return (boolean) get(PublishParam.isMultiInstance);
    }

    public void setIsMultiInstance(boolean isMultiInstance) {
        put(PublishParam.isMultiInstance,isMultiInstance);
    }

    public int getInstanceCount() {
        return (int) get(PublishParam.instanceCount);
    }

    public void setInstanceCount(int instanceCount) {
        put(PublishParam.instanceCount,instanceCount);
    }

    public boolean isMapEditable() {
        return (boolean) get(PublishParam.mapEditable);
    }

    public void setMapEditable(boolean mapEditable) {
        put(PublishParam.mapEditable,mapEditable);
    }

    public Map<String, Object> getDataProviderDelayCommitSetting() {
        return (Map<String, Object>) get(PublishParam.dataProviderDelayCommitSetting);
    }

    public void setDataProviderDelayCommitSetting(Map<String, Object> dataProviderDelayCommitSetting) {
        put(PublishParam.dataProviderDelayCommitSetting,dataProviderDelayCommitSetting);
    }
}

