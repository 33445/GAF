package com.supermap.gaf.authority.commontype;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息补充表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户信息补充表")
public class AuthUserSupplement implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("主键")
    private String userId;
    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("部门id")
    private String departmentId;
    @ApiModelProperty("排序字段")
    private Integer sortSn;
    @ApiModelProperty("岗位id")
    private String postId;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}