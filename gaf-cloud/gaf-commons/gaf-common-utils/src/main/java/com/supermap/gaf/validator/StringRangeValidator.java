package com.supermap.gaf.validator;


import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 校验字符串是否在指定的集合中
 *
 * @author wxl，zrc
 * @since 2021/6/18
 */
public class  StringRangeValidator implements ConstraintValidator<StringRange, Object> {

    private Set<String> range = new HashSet<>();

    private boolean ignoreCase;

    @SneakyThrows
    @Override
    public void initialize(StringRange constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        ignoreCase = constraintAnnotation.ignoreCase();
        Collections.addAll(range,constraintAnnotation.value());
        if(constraintAnnotation.fromEnum()!= StringRange.NoThing.class){
            Class<? extends Enum> enumClass = constraintAnnotation.fromEnum();
            Enum[] enumConstants = ((Class<Enum>)enumClass).getEnumConstants();
            for(Enum enumItem:enumConstants){
                String enumGetter = constraintAnnotation.enumGetter();
                if(StringUtils.isEmpty(enumGetter) || "name".equals(enumGetter)){
                    add(enumItem.name());
                }else{
                    add(BeanUtils.getProperty(enumItem,enumGetter));
                }
            }
        }
        Class<?> entityClass = constraintAnnotation.entityClass();
        if(entityClass!=void.class){
            Field[] declaredFields = entityClass.getDeclaredFields();
            for(Field item:declaredFields){
                if(Modifier.isStatic(item.getModifiers())){
                    continue;
                }
                String name = constraintAnnotation.isCamelCaseToUnderscore()?item.getName().replaceAll("([A-Z])", "_$1").toLowerCase():item.getName();
                add(name);
            }
        }
    }

    void add(Object value){
        if(ignoreCase && value instanceof String){
            range.add(((String)value).toLowerCase());
        }else{
            range.add(value.toString());
        }
    }


    boolean isValid(Object value){
        if (null == value) return true;
        boolean re = false;
        if(value instanceof Iterable){
            re = true;
            Iterator iterator = ((Iterable) value).iterator();
            while (iterator.hasNext()){
                re&=isValid(iterator.next());
            }
        }else if(ignoreCase && value instanceof String){
            re =  range.contains(((String)value).toLowerCase());
        }else{
            re = range.contains(value.toString());
        }
        return re;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean re = isValid(value);
        if(!re){
            String errorMsg =  String.format("必须在[%s]中选择",String.join(",",range));
            if(value instanceof String){
                errorMsg+=String.format(",%s区分大小写",ignoreCase?"不":"");
            }
            context.unwrap(HibernateConstraintValidatorContext.class)
                    .addMessageParameter("com.supermap.gaf.validator.StringRange.message",errorMsg);
        }
        return re;
    }

}
