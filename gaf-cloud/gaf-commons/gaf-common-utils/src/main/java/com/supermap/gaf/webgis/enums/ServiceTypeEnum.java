package com.supermap.gaf.webgis.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务类型
 *
 * @author wxl
 */
public enum ServiceTypeEnum {
    /**
     * 数据服务
     */
    RESTDATA("RESTDATA", "SUPERMAP_REST", "数据服务",20,"^/.*/services/[^/]*/rest/data/datasources/[^/]*/datasets/[^\\./]+"),
    /**
     * 三维服务
     */
    RESTMAP("RESTMAP", "SUPERMAP_REST","地图服务",20,"^/.*/services/[^/]+/rest/maps/[^\\./]+"),
    /**
     * 地图服务
     */
    RESTREALSPACE("RESTREALSPACE", "SUPERMAP_REST","三维服务",20,"^/.*/services/[^/]*/rest/realspace/datas/[^\\./]+","^/.*/services/[^/]*/rest/realspace"),
    /**
     * 空间分析服务
     */
    RESTSPATIALANALYST("RESTSPATIALANALYST", "SUPERMAP_REST","空间分析服务",10,"^/.*/services/[^/]*/[^\\./]+"),
    /**
     * ArcGIS地图服务
     */
    ARCGISMAP("ARCGISMAP","ARCGIS_REST","ArcGlS地图服务"),
    /**
     * WMS服务
     */
    WMS("WMS", "WMS", "WMS服务"),
    /**
     * WMTS服务
     */
    WMTS("WMTS", "WMTS", "WMTS服务"),
    /**
     * WFS服务
     */
    WFS("WFS", "WFS","WFS服务"),
    /**
     * WCS服务
     */
    WCS("WCS", "WCS","WCS服务"),

    /**
     * WPS服务
     */
    WPS("WPS", "WPS","WPS服务"),

    /**
     * 栅格地形
     */
    GRID_DEM("GRID_DEM","OTHERS", "栅格地形"),
    /**
     * TIN地形
     */
    TIN_DEM("TIN_DEM","OTHERS", "TIN地形",20,"^/.*/services/[^/]*/rest/realspace/datas/[^\\./]+"),
    /**
     * 天地图
     */
    MAPWORLD("MAPWORLD","OTHERS", "天地图"),
    /**
     * 影像
     */
    YINGXIANG("YINGXIANG", "OTHERS","影像",20,"^/.*/services/[^/]*/rest/realspace/datas/[^\\./]+"),

    /**
     * 百度REST
     */
    BAIDUREST("BAIDUREST","OTHERS","百度REST"),
    /**
     * 谷歌REST
     */
    GOOGLEREST("GOOGLEREST","OTHERS","谷歌REST"),
    /**
     * 其它
     */
    OTHER("OTHER","OTHERS","其它");
    public static final String DEFAULT_PATTERN = ".*";


    private String code;

    private String typeInIportal;
    /**
     * 模板类型说明
     */
    private String name;

    private List<String> pathPatterns;

    private Integer matchesPriority = 0;

    ServiceTypeEnum(String type, String typeInIportal, String description) {
        this.code = type;
        this.name = description;
        this.pathPatterns = Arrays.asList(DEFAULT_PATTERN);
        this.typeInIportal = typeInIportal;
    }
    //
    ServiceTypeEnum(String type, String typeInIportal, String description, Integer matchesPriority ,String... pathPattern) {
        this.code = type;
        this.name = description;
        this.pathPatterns = Arrays.asList(pathPattern);
        this.matchesPriority = matchesPriority;
        this.typeInIportal = typeInIportal;
    }

    public String getTypeInIportal() {
        return typeInIportal;
    }

    public Integer getMatchesPriority() {
        return matchesPriority;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getNameByCode(String code) {
        ServiceTypeEnum[] values = values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].getCode().equals(code)) {
                return values[i].getName();
            }
        }
        throw new IllegalArgumentException("不支持的服务类型"+code);
        
    }
    public List<String> getPathPatterns() {
        return pathPatterns;
    }
    public static Map<String, String> getMap() {
        ServiceTypeEnum[] values = values();
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < values.length; i++) {
            map.putIfAbsent(values[i].getCode(), values[i].getName());
        }
        return map;
    }
}
