package com.supermap.gaf.data.mgt.entity;

import com.supermap.data.Datasource;
import com.supermap.gaf.data.mgt.entity.changeop.ChangeOp;
import com.supermap.gaf.data.mgt.entity.changeop.ChangeOpChain;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Dataset modify param.
 *
 * @param <DatasetChangeChain> the type parameter
 */
@Data
public class DatasetChangeChain extends ChangeOpChain {

    /**
     * Instantiates a new Dataset change chain.
     *
     * @param name the name
     */
    public DatasetChangeChain(String name) {
        this.name = name;
    }
    /**
     * 数据集名
     */
    private String name;

    /**
     * The type Dataset change op.
     */
    public abstract class DatasetChangeOp  implements ChangeOp {
        /**
         * Get dataset name string.
         *
         * @return the string
         */
        protected String getDatasetName(){
            return name;
        }

        /**
         * Set dataset name.
         *
         * @param newName the new name
         */
        protected void setDatasetName(String newName){
            name = newName;
        }
        @Override
        public abstract void update(Datasource datasource);
    }

    /**
     * The type Dataset change op chain.
     */
    public class DatasetChangeOpChain extends ChangeOpChain {
        /**
         * Get dataset name string.
         *
         * @return the string
         */
        protected String getDatasetName(){
            return name;
        }

        /**
         * Set dataset name.
         *
         * @param newName the new name
         */
        protected void setDatasetName(String newName){
            name = newName;
        }
    }



}


