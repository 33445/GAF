package com.supermap.gaf.data.mgt.util;

import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.EngineType;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.exception.GafException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author : duke
 * @since 2021/9/14 11:34 AM
 */
public class DatasourceConnectionInfoConvertHelper {

    /**
     * sysResourceDatasource转换为iobject需要的数据源链接信息
     * @param sysResourceDatasource
     * @return
     */
    public static DatasourceConnectionInfo conver2DatasourceConnectionInfo(SysResourceDatasource sysResourceDatasource) {
        if (!sysResourceDatasource.getIsSdx()) {
            throw new GafException("不支持非空间数据源连接转换为空间数据源连接");
        }
        EngineType engineType = (EngineType) EngineType.parse(EngineType.class, sysResourceDatasource.getTypeCode());
        String tns = sysResourceDatasource.getAddr();
        String database = sysResourceDatasource.getDbName();
        String alias = sysResourceDatasource.getDsName();
        if (StringUtils.isEmpty(alias)) {
            alias = tns+"_"+database;
        }
        String user = sysResourceDatasource.getUserName();
        String password = sysResourceDatasource.getPassword();
        DatasourceConnectionInfo datasourceConnectionInfo = new DatasourceConnectionInfo(tns, database, alias, user, password);
        datasourceConnectionInfo.setEngineType(engineType);
        if (EngineType.SQLPLUS.equals(engineType)) {
            datasourceConnectionInfo.setDriver("SQL SERVER");
        }
        return datasourceConnectionInfo;
    }

}
