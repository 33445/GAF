package com.supermap.gaf.data.mgt.entity.changeop.dataset;

import com.supermap.data.Dataset;
import com.supermap.data.Datasource;
import com.supermap.data.EncodeType;
import com.supermap.gaf.data.mgt.entity.DatasetChangeChain;
import com.supermap.gaf.data.mgt.entity.changeop.InnerChangeOpBuilder;
import org.apache.http.util.Asserts;


/**
 * The type Copy from existed dataset.
 */
public class CopyFromExisted extends DatasetChangeChain.DatasetChangeOp {

    private Dataset fromDataset;

    private EncodeType encodeType;

    private CopyFromExisted(DatasetChangeChain outer, Dataset fromDataset, EncodeType encodeType){
        outer.super();
        this.fromDataset = fromDataset;
        this.encodeType = encodeType;
    }
    @Override
    public void update(Datasource datasource) {
        datasource.copyDataset(fromDataset,getDatasetName(),encodeType);
    }

    /**
     * Builder builder.
     *
     * @return the builder
     */
    public static Builder builder(){
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static class Builder extends InnerChangeOpBuilder<CopyFromExisted,DatasetChangeChain> {
        private Dataset fromDataset;

        private EncodeType encodeType;

        /**
         * From dataset builder.
         *
         * @param fromDataset the from dataset
         * @return the builder
         */
        public Builder fromDataset(Dataset fromDataset){
            this.fromDataset = fromDataset;
            return this;
        }

        /**
         * Encode type builder.
         *
         * @param encodeType the encode type
         * @return the builder
         */
        public Builder encodeType(EncodeType encodeType){
            this.encodeType = encodeType;
            return this;
        }

        @Override
        public CopyFromExisted build(){
            Asserts.notNull(fromDataset,"Builder.fromDataset(xx) required");
            Asserts.notNull(encodeType,"Builder.encodeType(xx) required");
            return new CopyFromExisted(parentChain, fromDataset, encodeType);
        }
    }
}
