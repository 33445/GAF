/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.gateway.commontypes.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 网关安全设置
 * </p>
 *
 * @author ${Author}
 * @version ${Version}
 * @date:2021/3/25
 * @since 1.0.0
 */
@Component
@ConfigurationProperties(prefix = "gateway.security")
public class GatewaySecurityProperties {

    public static String DEFAULT_PROFILE_URL = "/api/portal/user/profile/detail";
    public static String DEFAULT_PUBLIC_URLS = "/**/view/**,/**/static/**,/**/_static/**";
    public static String DEFAULT_NOT_VERIFY_PERMISSION_URLS="/api/platform/tenant-users/tenantList, /api/platform/tenants/switch,/api/storage/**";

    private String profileUrl = DEFAULT_PROFILE_URL;

    private boolean apiAuthzEnable = false;

    private List<String> publicUrls = Arrays.asList(DEFAULT_PUBLIC_URLS);

    private List<String> notVerifyPermissionUrls = Arrays.asList(DEFAULT_NOT_VERIFY_PERMISSION_URLS.split(","));

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public List<String> getPublicUrls() {
        return publicUrls;
    }

    public void setPublicUrls(List<String> publicUrls) {
        this.publicUrls = publicUrls;
    }

    public List<String> getNotVerifyPermissionUrls() {
        return notVerifyPermissionUrls;
    }

    public void setNotVerifyPermissionUrls(List<String> notVerifyPermissionUrls) {
        this.notVerifyPermissionUrls = notVerifyPermissionUrls;
    }


    public boolean isApiAuthzEnable() {
        return apiAuthzEnable;
    }

    public void setApiAuthzEnable(boolean apiAuthzEnable) {
        this.apiAuthzEnable = apiAuthzEnable;
    }
}
