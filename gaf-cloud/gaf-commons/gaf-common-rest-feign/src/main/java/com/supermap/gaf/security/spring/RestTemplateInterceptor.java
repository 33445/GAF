package com.supermap.gaf.security.spring;

import com.supermap.gaf.security.RequestUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * restTemplate拦截器，用于远程调用时，传递cookie和 token
 * 使用时，创建restTemplate并调用设置拦截器方法
 *
 *  RestTemplate restTemplate = new RestTemplate();
 *  restTemplate.setInterceptors(Collections.singletonList( new RestTemplateInterceptor()));
 *
 * @author wxl
 * @since 2022/5/6
 */
public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String COOKIE = "Cookie";


    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();

        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest httpServletRequest = requestAttributes.getRequest();
            String cookie = httpServletRequest.getHeader(COOKIE);
            headers.set(COOKIE,cookie);
        }

        String accessToken = RequestUtil.getToken();
        if (StringUtils.isNoneEmpty(accessToken)) {
            headers.set(AUTHORIZATION_HEADER, "Bearer " + accessToken);
        } else {
            if (requestAttributes != null) {
                HttpServletRequest httpServletRequest = requestAttributes.getRequest();
                String authorization = httpServletRequest.getHeader(AUTHORIZATION_HEADER);
                headers.set(AUTHORIZATION_HEADER, authorization);
            }
        }
        ClientHttpResponse response = execution.execute(request, body);
        return response;
    }
}
