package com.supermap.gaf.extend.auth.service;


import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.authority.commontype.AuthUser;

import java.util.List;

/**
 * @author : duke
 * @since 2022/3/22 8:52 AM
 */
public interface GafUserService {

    /**
     * 获取当前用户id
     * @return
     */
    String getUserId();

    /**
     * 获取当前用户名称
     * @return
     */
    String getUserName();

    /**
     * 获取当前租户
     * @return
     */
    String getTenantId();

    /**
     * 获取当前用户信息
     * @return
     */
    AuthUser getUserInfo();

    /**
     * 获取当前用户角色
     * @return
     */
    List<AuthRole> getUserRoles();


}
