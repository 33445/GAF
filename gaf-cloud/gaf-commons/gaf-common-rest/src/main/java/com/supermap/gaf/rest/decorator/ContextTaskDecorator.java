package com.supermap.gaf.rest.decorator;

import org.springframework.core.task.TaskDecorator;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Nonnull;

/**
 * 子线程设置request上下文
 * @author dqc
 * @date 2022/5/24
 */
public class ContextTaskDecorator implements TaskDecorator {
    @Nonnull
    @Override
    public Runnable decorate(@Nonnull Runnable runnable) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return () -> {
            try {
                RequestContextHolder.setRequestAttributes(requestAttributes);
                runnable.run();
            }finally {
                RequestContextHolder.resetRequestAttributes();
            }
        };
    }
}
