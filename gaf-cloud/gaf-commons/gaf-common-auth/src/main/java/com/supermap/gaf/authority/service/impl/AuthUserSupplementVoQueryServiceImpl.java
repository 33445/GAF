/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.service.impl;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.service.AuthUserSupplementQueryService;
import com.supermap.gaf.authority.service.AuthUserSupplementVoQueryService;
import com.supermap.gaf.authority.vo.AuthUserSupplementVo;
import com.supermap.gaf.platform.client.AuthUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author dqc
 * @date:2021/3/25 服务类
 */
@Service
public class AuthUserSupplementVoQueryServiceImpl implements AuthUserSupplementVoQueryService {
    @Autowired
    private AuthUserClient authUserClient;
    @Autowired
    private AuthUserSupplementQueryService authUserSupplementQueryService;

    /**
     * id查询
     *
     * @return
     */
    @Override
    public AuthUserSupplementVo getById(String userId) {
        AuthUser authUser = authUserClient.getByUserId(userId).checkAndGetData();
        AuthUserSupplement authUserSupplement = authUserSupplementQueryService.getById(userId);
        return new AuthUserSupplementVo(authUser, authUserSupplement);
    }

    @Override
    public AuthUserSupplementVo getByUserName(String username) {
        AuthUser authUser = authUserClient.getByUsername(username).checkAndGetData();
        AuthUserSupplement authUserSupplement = authUserSupplementQueryService.getByUserName(username);
        return new AuthUserSupplementVo(authUser, authUserSupplement);
    }

}
