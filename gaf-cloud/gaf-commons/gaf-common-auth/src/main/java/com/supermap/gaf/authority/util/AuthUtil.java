package com.supermap.gaf.authority.util;

import com.supermap.gaf.authority.commontype.AuthRole;

import java.util.List;

/**
 * @author wxl
 * @since 2022/8/5
 */
public class AuthUtil {

    /**
     * 判断是否是管理员角色
     *
     * @return
     */
    public static boolean isAdmin(List<AuthRole> authRoles) {
        for (AuthRole role : authRoles) {
            if ("admin".equals(role.getNameEn())) {
                return true;
            }
        }
        return false;
    }
}
