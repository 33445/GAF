package com.supermap.gaf.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@SpringBootApplication
@ComponentScan(basePackages = {"com.supermap.gaf"}, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.supermap.gaf.common.storage.*"))
@EnableDiscoveryClient
@EnableHystrix
@EnableTransactionManagement
@MapperScan(basePackages = {"com.supermap.gaf.**.dao","com.supermap.gaf.**.mapper"},sqlSessionTemplateRef = "myProjSqlSessionTemplate")
@EnableFeignClients(basePackages = "com.supermap.gaf")
public class GafProjServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GafProjServiceApplication.class, args);
	}
}