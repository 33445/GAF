package com.supermap.gaf.project.engine;

import com.alibaba.fastjson.JSONObject;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author:yw
 * @Date 2021/3/17
 * 遍历模板文件将最终合并输出到zip
 */
public class ZipOutputStreamFileVisitor extends SimpleFileVisitor<Path> {

    private Map<String,String> vmFileMappings;

    private VelocityEngine ve;

    private String vmName;

    private Path vmRoot;

    private String templateResources;

    private VelocityContext ctx;

    private ZipOutputStream zipOutputStream;
    private static final String KEYPRETABLENAME = "tablename";
    private static final String KEYPREPACKAGENAME = "packagename";
    private static final String KEYPRETABLEMETADATAINFO = "tableMetadataInfo";
    private static final String KEYPRETABLEUPPERCAMELNAME= "tableUpperCamelName";

    public ZipOutputStreamFileVisitor(GafVmEngine vmEngine, String vmName, VelocityContext ctx, ZipOutputStream zipOutputStream, Map<String,String> vmFileMappings){
        super();
        this.ve = vmEngine.getVe();
        this.templateResources = vmEngine.getTemplateResources();
        this.vmName = vmName;
        this.vmRoot = FileSystems.getDefault().getPath(vmEngine.getVmRootPath()).resolve(vmName+"/"+templateResources);
        this.ctx = ctx;
        this.zipOutputStream = zipOutputStream;
        this.vmFileMappings = vmFileMappings;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        //动态文件名
        String relativePath = file.subpath(vmRoot.getNameCount(), file.getNameCount()).toString();
        relativePath = relativePath.replaceAll("[\\\\]+","/");
        Template template = ve.getTemplate("/"+vmName+"/"+templateResources+"/"+relativePath);
        if(!CollectionUtils.isEmpty(vmFileMappings)){
            String pattern = vmFileMappings.get(relativePath);
            StringWriter newFileName = new StringWriter();
            if(!StringUtils.isEmpty(pattern)){
                Velocity.evaluate( ctx, newFileName, "", pattern);
                if(vmRoot.getNameCount()==file.getNameCount()-1){
                    relativePath = newFileName.toString();
                }else{
                    relativePath = file.subpath(vmRoot.getNameCount(), file.getNameCount()-1)+"/"+newFileName.toString();
                }
            }
        }

        if(relativePath.contains(KEYPRETABLENAME)){
            relativePath = relativePath.replaceAll(KEYPRETABLENAME,getTableUpperCamelName());
        }

        if(relativePath.contains(KEYPREPACKAGENAME)){
            relativePath = relativePath.replaceAll(KEYPREPACKAGENAME,getpackageName());
        }

        ZipEntry zipEntry = new ZipEntry(vmName+"/"+relativePath);
        zipOutputStream.putNextEntry(zipEntry);
        //合并模板文件
        Writer writer = new PrintWriter(zipOutputStream);
        template.merge(ctx, writer);
        writer.flush();
        zipOutputStream.closeEntry();
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        String dirName = vmName;
        //创建文件夹
        if(vmRoot.compareTo(dir)!=0){
            dirName += "/"+dir.subpath(vmRoot.getNameCount(), dir.getNameCount()).toString();

        }
        if(dirName.contains(KEYPRETABLENAME)){
            dirName = dirName.replaceAll(KEYPRETABLENAME,getTableUpperCamelName());
        }

        if(dirName.contains(KEYPREPACKAGENAME)){
            dirName = dirName.replaceAll(KEYPREPACKAGENAME,getpackageName());
        }

        dirName = dirName.replaceAll("[\\\\]+","/");
        zipOutputStream.putNextEntry(new ZipEntry(dirName + "/"));
        zipOutputStream.closeEntry();
        return FileVisitResult.CONTINUE;
    }

    private String getTableUpperCamelName(){
        Object tableMetadataInfo = this.ctx.get(KEYPRETABLEMETADATAINFO);
        String tableUpperCamel = JSONObject.toJSONString(tableMetadataInfo);
        return (String) JSONObject.parseObject(tableUpperCamel).get(KEYPRETABLEUPPERCAMELNAME);
    }

    private String getpackageName(){
        String packageName = (String)this.ctx.get(KEYPREPACKAGENAME);
        return packageName.replaceAll("\\.","/");
    }
}
