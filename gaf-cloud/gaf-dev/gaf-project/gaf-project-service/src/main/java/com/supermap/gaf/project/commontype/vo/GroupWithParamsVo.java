package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
* @author:yw
* @Date 2021/3/17
**/
@Data
@ApiModel("代码模板分组参数实体")
public class GroupWithParamsVo {
    /**
     * 代码模板参数分组id。主键,uuid
     */
    @ApiModelProperty(value = "代码模板参数分组id。主键,uuid")
    private String codeTemplateParamGrpId;
    /**
     * 分组名称
     */
    @ApiModelProperty(value = "分组名称")
    private String paramGroupName;
    /**
     * 参数中文名称。
     */
    @ApiModelProperty(value = "参数中文名称")
    private String paramGroupNameCn;
    /**
     * 描述。
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 是否必填。必填的分组，界面上默认选中
     */
    @ApiModelProperty(value = "是否必填")
    private Boolean isRequired;
    @ApiModelProperty(value = "代码模板参数对象数组")
    private List<ParamVo> paramVos;
}
