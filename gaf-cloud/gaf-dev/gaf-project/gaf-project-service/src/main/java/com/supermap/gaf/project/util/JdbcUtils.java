package com.supermap.gaf.project.util;

import com.alibaba.fastjson.JSON;
import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import com.microsoft.sqlserver.jdbc.SQLServerConnection;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.supermap.gaf.project.commontype.FieldMetadataInfo;
import com.supermap.gaf.project.commontype.JdbcConnectionMetadata;
import com.supermap.gaf.project.commontype.TableMetadataInfo;
import com.supermap.gaf.utils.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Slf4j
public class JdbcUtils {
    private static Logger logger = LogUtil.getLocLogger(JdbcUtils.class);
    private static ClassLoader loader = Thread.currentThread().getContextClassLoader();
    public static Connection openConnection(String url,String username,String password){
        Connection connection = null;
        try{
            Properties props =new Properties();
            props.setProperty("user", username);
            props.setProperty("password", password);
            props.setProperty("remarks", "true");
            connection = DriverManager.getConnection(url,props);
        } catch (SQLException e) {
            throw new IllegalArgumentException("数据库连接失败，请检查连接参数");
        }
        return connection;
    }

    public static boolean checkConnectionInfo(String url,String username,String password){
        Connection connection = openConnection(url,username,password);
        try {
            connection.close();
        } catch (SQLException e) {
            log.error("数据库连接关闭失败：{}",e.getStackTrace());
        }
        return true;
    }

    public static JdbcConnectionMetadata getJdbcConnectionInfo(Connection connection){
        try{
            if(connection.isClosed()){
                throw new IllegalArgumentException("连接已关闭");
            }
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            String databaseType = databaseMetaData.getURL().split(":")[1];
            String database = connection.getCatalog();
            String schema = connection.getSchema();
            String databaseVersion = databaseMetaData.getDatabaseProductVersion();
            return JdbcConnectionMetadata.builder()
                    .database(database)
                    .databaseType(databaseType)
                    .schema(schema)
                    .databaseVersion(databaseVersion)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static List<TableMetadataInfo> getTableMetadatas(Connection connection, boolean detail){
        ResultSet tableRs = null;
        List<TableMetadataInfo> tableMetadataInfos = new ArrayList<>();
        try {
            JdbcConnectionMetadata jdbcConnectionInfo = getJdbcConnectionInfo(connection);
            DatabaseMetaData databaseMetaData = connection.getMetaData();

            String typeJson = StreamUtils.copyToString(loader.getResourceAsStream("typeMapping.json"), StandardCharsets.UTF_8);
            Map<String,String> typeMap = JSON.parseObject(typeJson,Map.class);
            //获取表信息
            String[] type = {"TABLE"};
            tableRs = databaseMetaData.getTables(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(),null,type);
            while(tableRs.next()){
                String tableName = tableRs.getString("TABLE_NAME");
                TableMetadataInfo tableMetadataInfo = TableMetadataInfo.builder()
                        .tableName(tableName)
                        .tableUpperCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,tableName))
                        .tableLowCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,tableName))
                        .remarks(tableRs.getString("REMARKS"))
                        .build();

                ResultSet primaryKeyRs = databaseMetaData.getPrimaryKeys(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(),tableName);
                String pkField = null;
                while(primaryKeyRs.next()){
                    pkField =  primaryKeyRs.getString("COLUMN_NAME");
                    tableMetadataInfo
                            .setPkName(pkField)
                            .setPkLowCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,pkField))
                            .setPkUpperCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,pkField));
                }
                primaryKeyRs.close();
                if(detail){
                    //获取字段信息
                    ResultSet fieldsRs = databaseMetaData.getColumns(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(), tableName, null);
                    List<FieldMetadataInfo> list = list(fieldsRs, pkField, typeMap, tableMetadataInfo);
                    if(connection instanceof SQLServerConnection){
                        tableMetadataInfo = injectTableRemarks2SqlServer((SQLServerConnection) connection,tableMetadataInfo,tableName);
                        list = injectColumnRemarks2Sqlserver((SQLServerConnection) connection,list,tableName);
                    }
                    tableMetadataInfo.setFieldMetadataInfos(list);
                }

                tableMetadataInfos.add(tableMetadataInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(tableRs!=null){
                    tableRs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMetadataInfos;
    }
    public static TableMetadataInfo getFieldMetadata(Connection connection,String tableName){
        ResultSet tableRs = null;
        TableMetadataInfo tableMetadataInfo = null;
        try {
            JdbcConnectionMetadata jdbcConnectionInfo = getJdbcConnectionInfo(connection);
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            //type json
            String typeJson = StreamUtils.copyToString(loader.getResourceAsStream("typeMapping.json"), StandardCharsets.UTF_8);
            Map<String,String> typeMap = JSON.parseObject(typeJson,Map.class);
            //获取表信息
            String[] type = {"TABLE"};
            tableRs = databaseMetaData.getTables(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(),tableName,type);
            while(tableRs.next()){
                tableMetadataInfo = TableMetadataInfo.builder()
                        .tableName(tableName)
                        .tableUpperCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,tableName))
                        .tableLowCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,tableName))
                        .remarks(tableRs.getString("REMARKS"))
                        .build();

                ResultSet primaryKeyRs = databaseMetaData.getPrimaryKeys(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(),tableName);
                String pkField = null;
                while(primaryKeyRs.next()){
                    pkField =  primaryKeyRs.getString("COLUMN_NAME");
                    tableMetadataInfo
                            .setPkName(pkField)
                            .setPkLowCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,pkField))
                            .setPkUpperCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,pkField));
                }
                primaryKeyRs.close();
                //获取字段信息
                ResultSet fieldsRs = databaseMetaData.getColumns(jdbcConnectionInfo.getDatabase(),jdbcConnectionInfo.getSchema(), tableName, null);

                List<FieldMetadataInfo> list = list(fieldsRs, pkField, typeMap, tableMetadataInfo);

                if(connection instanceof SQLServerConnection){
                    tableMetadataInfo = injectTableRemarks2SqlServer((SQLServerConnection) connection,tableMetadataInfo,tableName);
                    list = injectColumnRemarks2Sqlserver((SQLServerConnection) connection,list,tableName);
                }
                tableMetadataInfo.setFieldMetadataInfos(list);
            }
            if(tableMetadataInfo == null){
                throw new IllegalArgumentException(tableName+"表不存在于数据库"+jdbcConnectionInfo.getDatabase()+"中");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(tableRs!=null){
                    tableRs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMetadataInfo;
    }

    public static TableMetadataInfo injectTableRemarks2SqlServer(SQLServerConnection connection, TableMetadataInfo tableMetadataInfo, String tableName){
        TableMetadataInfo re = TableMetadataInfo.builder().build();
        BeanUtils.copyProperties(tableMetadataInfo,re);
        final String selectTableRemarksSql = "SELECT\n" +
                "A.name,\n" +
                "C.value\n" +
                "FROM sys.tables A\n" +
                "inner JOIN sys.extended_properties C ON C.major_id = A.object_id  and minor_id=0\n" +
                "WHERE A.name = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement(selectTableRemarksSql);
            ps.setString(1,tableName);
            rs = ps.executeQuery();

            while(rs.next()){
                re.setRemarks(rs.getString("value"));
                break;
            }
            rs.close();
            ps.close();
        } catch (SQLServerException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (ps != null){
                    ps.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return re;
    }

    public static List<FieldMetadataInfo> injectColumnRemarks2Sqlserver(SQLServerConnection connection,List<FieldMetadataInfo> fieldMetadatas,String tableName){
        Map<String,FieldMetadataInfo> fieldMetadataMap = fieldMetadatas.stream().collect(Collectors.toMap(FieldMetadataInfo::getFieldName, s->{
            FieldMetadataInfo fieldMetadata = new FieldMetadataInfo();
            BeanUtils.copyProperties(s,fieldMetadata);
            return fieldMetadata;
        }));
        final String selectColumnRemarksSql = "SELECT\n" +
                "B.name AS column_name,\n" +
                "C.value\n" +
                "FROM sys.tables A\n" +
                "INNER JOIN sys.columns B ON B.object_id = A.object_id\n" +
                "LEFT JOIN sys.extended_properties C ON C.major_id = B.object_id AND C.minor_id = B.column_id\n" +
                "WHERE A.name = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement(selectColumnRemarksSql);
            ps.setString(1,tableName);
            rs = ps.executeQuery();

            while(rs.next()){
                String columnName = rs.getString("column_name");
                String columnRemarks = rs.getString("value");
                fieldMetadataMap.get(columnName).setRemarks(columnRemarks);
            }
            rs.close();
            ps.close();
        } catch (SQLServerException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (rs != null){
                    rs.close();
                }
                if (ps != null){
                    ps.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return Lists.newArrayList(fieldMetadataMap.values());
    }
    public static String escapeString(String in){
        if(StringUtils.isEmpty(in)) return null;
        return in.replaceAll("[\\r\\n|\\r|\\n|\"]+"," ");
    }
    public static void main(String[] args) {
        Connection connection  = openConnection("jdbc:postgresql://localhost:5432/postgres","postgres","123456");
        //Connection connection  = openConnection("jdbc:sqlserver://localhost:1433;databaseName=zrc;","sa","root");
        //List<TableMetadataInfo> tableMetadataInfos = getTableMetadatas(connection,false);
        TableMetadataInfo tableMetadataInfo = getFieldMetadata(connection,"user");
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private static List<FieldMetadataInfo> list(ResultSet fieldsRs, String pkField,Map<String,String> typeMap,TableMetadataInfo tableMetadataInfo) throws SQLException {
        List<FieldMetadataInfo> list = new ArrayList<>();
        while (fieldsRs.next()) {
            String fieldName = fieldsRs.getString("COLUMN_NAME");
            boolean isPk = (pkField != null && fieldName.equals(pkField));
            FieldMetadataInfo fieldMetadata = FieldMetadataInfo.builder()
                    .fieldName(fieldName)
                    .fieldLowCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,fieldName))
                    .fieldUpperCamelName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,fieldName))
                    .sqlType(fieldsRs.getString("TYPE_NAME"))
                    .jdbcDataType(JDBCType.valueOf(fieldsRs.getInt("DATA_TYPE")).getName())
                    //默认Object类型
                    .javaType("Object")
                    .defaultValue(fieldsRs.getString("COLUMN_DEF"))
                    .remarks(escapeString(fieldsRs.getString("REMARKS")))
                    .isNullable("YES".equals(fieldsRs.getString("IS_NULLABLE")))
                    .columnSize(fieldsRs.getInt("COLUMN_SIZE"))
                    .isAutoincrement("YES".equals(fieldsRs.getString("IS_AUTOINCREMENT")))
                    .build();
            String javaType = typeMap.get(fieldMetadata.getJdbcDataType());
            if(javaType!=null) {
                fieldMetadata.setJavaType(javaType);
            }
            if(isPk){
                fieldMetadata.setIsPrimaryKey(true);
                tableMetadataInfo
                        .setPkJavaType(fieldMetadata.getJavaType())
                        .setPkSqlType(fieldMetadata.getSqlType())
                        .setPkJdbcType(fieldMetadata.getJavaType());
            }
            list.add(fieldMetadata);
        }
        return list;
    }

}
