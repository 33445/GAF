package com.supermap.gaf.project.commontype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 代码模板参数分组表
 * 
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:45
 */
@Data
@TableName("proj_code_template_param_grp")
@ApiModel("工程模板参数分组")
public class ProjCodeTemplateParamGrpEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 代码模板参数分组id。主键,uuid
	 */
	@TableId
	@ApiModelProperty("模板参数分组id")
	private String codeTemplateParamGrpId;
	/**
	 * 模板id。该值为000000，则为参数样板分组
	 */
	@ApiModelProperty("模板id")
	private String codeTemplateId;
	/**
	 * 分组名称。
	 */
	@ApiModelProperty("分组名称")
	private String paramGroupName;
	/**
	 * 参数中文名称。
	 */
	@ApiModelProperty("参数中文名称")
	private String paramGroupNameCn;
	/**
	 * 描述。
	 */
	@ApiModelProperty("描述")
	private String description;
	/**
	 * 参考分组id。参考的样板分组id
	 */
	@ApiModelProperty("参考分组id")
	private String referredGroupId;
	/**
	 * 创建时间。
	 */
	@ApiModelProperty("创建时间")
	private Date createdTime;
	/**
	 * 创建人。
	 */
	@ApiModelProperty("创建人")
	private String createdBy;
	/**
	 * 修改时间。
	 */
	@ApiModelProperty("修改时间")
	private Date updatedTime;
	/**
	 * 修改人。
	 */
	@ApiModelProperty("修改人")
	private String updatedBy;

	/**
	 * 是否必填。必填的分组，界面上默认选中
	 */
	@ApiModelProperty("是否必填。必填的分组，界面上默认选中")
	private Boolean isRequired;
}
