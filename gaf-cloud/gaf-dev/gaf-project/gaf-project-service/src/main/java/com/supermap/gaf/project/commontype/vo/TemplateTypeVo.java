package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Data
@ApiModel("模板类型数据实体")
public class TemplateTypeVo {
    /**
     * 模板类型编码
     */
    @NotNull
    @ApiModelProperty("模板类型数字")
    private int type;
    /**
     * 模板类型说明
     */
    @NotNull
    @ApiModelProperty("模板类型说明")
    private String description;
}
