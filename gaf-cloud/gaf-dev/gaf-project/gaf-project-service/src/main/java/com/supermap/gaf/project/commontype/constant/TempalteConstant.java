package com.supermap.gaf.project.commontype.constant;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
public class TempalteConstant {
    /**
     * 模板表的字段名，表示模板的类型 可参考枚举类TemplateTypeEnum
     */
    public static final String TEMPLATE_TYPE = "template_type";

    /**
     * 代码模板元信息描述文件名
     */
    public static final String METADATA_XML = "metadata.xml";

    /**
     * 工程模板或代码模板渲染的参数名
     */
    public static final String ARTIFACT_ID = "artifactId";
}
