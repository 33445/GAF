package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
* 模板表
* @author wangxiaolong
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("模板表")
public class ProjCodeTemplateEntityVo {
    @NotNull
    @ApiModelProperty("代码模板id")
    private String projCodeTemplateId;
    @NotNull
    @ApiModelProperty("模板名称")
    private String templateName;
    @NotNull
    @ApiModelProperty("模板中文名称")
    private String templateNameCn;
    @NotNull
    @ApiModelProperty("模板类型")
    private Integer templateType;

    @ApiModelProperty("模板类型名")
    private String templateTypeName;

    @ApiModelProperty("描述")
    private String description;
    @NotNull
    @ApiModelProperty("版本")
    private String version;
    @ApiModelProperty("模板地址")
    private String templatePath;
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;
}