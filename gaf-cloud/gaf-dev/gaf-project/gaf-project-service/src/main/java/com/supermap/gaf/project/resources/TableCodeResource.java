package com.supermap.gaf.project.resources;


import cn.hutool.core.util.ZipUtil;
import com.alibaba.fastjson.JSONObject;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.project.commontype.DatasourceConnParam;
import com.supermap.gaf.project.commontype.JdbcConnectionInfo;
import com.supermap.gaf.project.commontype.TableMetadataInfo;
import com.supermap.gaf.project.commontype.constant.TempalteConstant;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateEntity;
import com.supermap.gaf.project.commontype.utils.Constant;
import com.supermap.gaf.project.engine.DirOrFileConditionRenderVisitor;
import com.supermap.gaf.project.engine.GafVmEngine;
import com.supermap.gaf.project.service.ProjCodeTemplateService;
import com.supermap.gaf.project.util.ConvertUtils;
import com.supermap.gaf.project.util.JdbcUtils;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.LogUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.zip.ZipOutputStream;

import static com.supermap.gaf.project.commontype.constant.TempalteConstant.ARTIFACT_ID;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
@Component
@Api(value = "单表curd模板接口")
public class TableCodeResource {
    private static Logger logger = LogUtil.getLocLogger(TableCodeResource.class);
    /**
     * 表示系统临时文件夹
     */
    public static String SYSTEM_TEMP_DIRECTORY="java.io.tmpdir";

    @Autowired
    private ProjCodeTemplateService projCodeTemplateService;

    @Autowired
    private StorageClient storageClient;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path(value = "/field-metadata")
    public MessageResult<TableMetadataInfo> getFieldMetadata(DatasourceConnParam datasourceConnParam, @QueryParam("tablename") String tableName){
        Connection connection = null;
        TableMetadataInfo tableMetadataInfo = null;
        try{
            JdbcConnectionInfo jdbcConnectionInfo = ConvertUtils.convert2JdbcConnectionInfo(datasourceConnParam);
            connection = JdbcUtils.openConnection(jdbcConnectionInfo.getUrl(),jdbcConnectionInfo.getUsername(),jdbcConnectionInfo.getPassword());
            tableMetadataInfo = JdbcUtils.getFieldMetadata(connection,tableName);
        }catch (Exception e){
            return MessageResult.failed(TableMetadataInfo.class).message(e.getMessage()).build();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return MessageResult.successe(TableMetadataInfo.class).data(tableMetadataInfo).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path(value = "/tables-metadata")
    public MessageResult<List> getTableMetadatas(DatasourceConnParam datasourceConnParam, @QueryParam("detail") boolean detail){
        Connection connection = null;
        List<TableMetadataInfo> tableMetadataInfos = null;
        try{
            JdbcConnectionInfo jdbcConnectionInfo = ConvertUtils.convert2JdbcConnectionInfo(datasourceConnParam);
            connection = JdbcUtils.openConnection(jdbcConnectionInfo.getUrl(),jdbcConnectionInfo.getUsername(),jdbcConnectionInfo.getPassword());
            tableMetadataInfos = JdbcUtils.getTableMetadatas(connection,detail);
        }catch (Exception e){
            return MessageResult.failed(List.class).message(e.getMessage()).build();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return MessageResult.successe(List.class).data(tableMetadataInfos).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path(value = "/connection-param/check")
    public MessageResult checkConnectionParam(DatasourceConnParam datasourceConnParam){

        try{
            JdbcConnectionInfo jdbcConnectionInfo = ConvertUtils.convert2JdbcConnectionInfo(datasourceConnParam);
            JdbcUtils.checkConnectionInfo(jdbcConnectionInfo.getUrl(),jdbcConnectionInfo.getUsername(),jdbcConnectionInfo.getPassword());
        }catch (Exception e){
            return MessageResult.failed(TableMetadataInfo.class).message(e.getMessage()).build();
        }
        return MessageResult.successe(Void.class).message("连接成功").build();
    }

    @POST
    @Path("/generator-zip")
    @ApiOperation(value = "模板生成与zip文件下载", notes = "模板生成与zip文件下载")
    public Response downloadNew(Map<String,Object> properties,
                                @NotEmpty @QueryParam("projCodeTemplateId") String projCodeTemplateId,
                                @Context HttpServletRequest request) throws IOException {
        ProjCodeTemplateEntity template = projCodeTemplateService.getById(projCodeTemplateId);
        if (StringUtils.isEmpty(template.getTemplatePath())) {
            throw new GafException("找不到模板");
        }
        String templatePath = template.getTemplatePath();

        String systemTempDir = System.getProperties().getProperty(SYSTEM_TEMP_DIRECTORY);
        java.nio.file.Path dir = Paths.get(systemTempDir, UUID.randomUUID().toString());
        if (!Files.exists(dir)) Files.createDirectories(dir);
        java.nio.file.Path destPath = Paths.get(dir.toString(), template.getTemplateName());

        storageClient.downloadFile(templatePath, SecurityUtilsExt.getTenantId(),destPath);

        String unzipPath = Paths.get(systemTempDir,UUID.randomUUID().toString()).toString();
        File unzip = ZipUtil.unzip(destPath.toString(), unzipPath);

        if (properties.containsKey("packagename")) {
            properties.putIfAbsent("packageName",properties.get("packagename"));
        }

        File[] filelist = unzip.listFiles();
        if (filelist == null || filelist.length <= 0 ) {
            throw new GafException("模板解压失败");
        }
        File vmFile = filelist[0];
        String vmName = vmFile.getName();
        String vmRootPath = unzipPath;
        // 渲染指定文件内容和文件夹名
        // 获取artifactId
        Object artifactId = properties.get(ARTIFACT_ID);
        if (artifactId != null && !StringUtils.isEmpty(artifactId.toString())) {
            properties.putIfAbsent(ARTIFACT_ID, artifactId.toString());
            VelocityContext ctx = new VelocityContext(properties);
            java.nio.file.Path vmRoot = FileSystems.getDefault().getPath(vmRootPath);
            HashSet<String> fileNames = new HashSet<>();
            fileNames.add(TempalteConstant.METADATA_XML);
            DirOrFileConditionRenderVisitor dirOrFileConditionRenderVisitor = new DirOrFileConditionRenderVisitor(ctx, true, Collections.emptySet(), false, fileNames);
            Files.walkFileTree(vmRoot, dirOrFileConditionRenderVisitor);
        }

        StreamingOutput streamingOutput  = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                try(ZipOutputStream zipOut = new ZipOutputStream(outputStream)){
                    new GafVmEngine(vmRootPath).generatorVm(vmName,properties,zipOut);
                }finally {
                    if( outputStream!=null ) {
                        outputStream.flush();
                        outputStream.close();
                    }
                }
            }
        };
        //unzip.deleteOnExit();
        try{
            Response.ResponseBuilder responseBuilder= Response.ok(streamingOutput);
            responseBuilder.header("Content-Type", "application/octet-stream;charset=UTF-8");
            String userAgent = org.apache.commons.lang3.StringUtils.EMPTY;
            String keyPre = "User-Agent";
            if (StringUtils.isNotBlank(request.getHeader(keyPre))) {
                userAgent = request.getHeader(keyPre).toLowerCase();
            }
            String key = "firefox";
            if (userAgent.contains(key)) {
                responseBuilder.header("Content-disposition", "attachment;filename*=UTF-8" + URLEncoder.encode(vmName+".zip", "utf-8"));
            } else {
                responseBuilder.header("Content-disposition", "attachment;filename=" + URLEncoder.encode(vmName+".zip", "utf-8"));
            }

            Response response = responseBuilder.build();
            return response;
        }catch (RuntimeException e){
            MessageResult result = MessageResult.failed(Void.class).message(e.getMessage()).status(400).build();
            return Response.ok(result, MediaType.APPLICATION_JSON).status(400).build();
        }

    }
}
