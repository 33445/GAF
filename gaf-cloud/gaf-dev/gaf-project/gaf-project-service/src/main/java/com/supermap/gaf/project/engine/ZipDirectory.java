package com.supermap.gaf.project.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author:yw
 * @Date 2021/3/17
 **/
public class ZipDirectory {
    public static void main(String[] args) throws IOException {
        String sourceFile = "D:\\template\\vms\\tk-mybaties-vm";
        FileOutputStream fos = new FileOutputStream("D:\\template\\vms\\tk-mybaties-vm.zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(sourceFile);
        zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();
    }

    private static void zipFile(File sourceFile, String fileName, ZipOutputStream zipOut) throws IOException {
        if (sourceFile.isHidden()) {
            return;
        }
        String keyPre = "/";
        if (sourceFile.isDirectory()) {
            if (fileName.endsWith(keyPre)) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = sourceFile.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        try (FileInputStream fis = new FileInputStream(sourceFile)) {
            ZipEntry zipEntry = new ZipEntry(fileName);
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[4096];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}