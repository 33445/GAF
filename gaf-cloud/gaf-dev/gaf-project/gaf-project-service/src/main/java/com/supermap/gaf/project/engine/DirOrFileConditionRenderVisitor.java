package com.supermap.gaf.project.engine;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;
import java.util.UUID;


/**
 * @author:yw
 * 只渲染指定的文件夹的名字
 * @Date 2021/3/17
 **/
public class DirOrFileConditionRenderVisitor implements FileVisitor<Path> {
    public static final String LOG_TAG = "randerDirectoryOrFileByCondition";

    private VelocityContext ctx;
    private Boolean isVisitAllDirectory;
    private Set<String> directoryNames;
    private Boolean isVisitAllFile;
    private Set<String> fileNames;

    public DirOrFileConditionRenderVisitor() {
    }

    public DirOrFileConditionRenderVisitor(VelocityContext ctx) {
        this.ctx = ctx;
    }

    public DirOrFileConditionRenderVisitor(VelocityContext ctx, Boolean isVisitAllDirectory, Set<String> directoryNames, Boolean isVisitAllFile, Set<String> fileNames) {
        this.ctx = ctx;
        this.isVisitAllDirectory = isVisitAllDirectory;
        this.directoryNames = directoryNames;
        this.isVisitAllFile = isVisitAllFile;
        this.fileNames = fileNames;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (isVisitAllFile || fileNames.contains(file.toFile().getName())) {
            Path pathAfterRender = fileNameRender(file);
            Path renamePath = FileSystems.getDefault().getPath(file.getParent().toString(), UUID.randomUUID().toString());
            Path move = Files.move(file, renamePath);
            File fileAfterRender = pathAfterRender.toFile();
            fileAfterRender.createNewFile();
            try (Reader reader = new BufferedReader(new FileReader(move.toFile()));
                 Writer out = new BufferedWriter(new FileWriter(fileAfterRender))) {
                Velocity.evaluate(ctx, out, LOG_TAG, reader);
            }
            move.toFile().delete();
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (isVisitAllDirectory || directoryNames.contains(dir.toFile().getName())) {
            Path pathAfterRender = fileNameRender(dir);
            Files.move(dir,pathAfterRender);
        }
        return FileVisitResult.CONTINUE;
    }

    /**
     * 渲染文件夹或文件名 eg: ${你的变量名}文件文件 => 变量值文本文件
     *
     * @param filePath 文件夹或文件路径
     * @return 渲染后的文件路径
     */
    private Path fileNameRender(Path filePath) throws IOException {
        Path parent = filePath.getParent();
        String name = filePath.toFile().getName();
        try( StringWriter stringWriter = new StringWriter()){
            boolean renderIsSuccess = Velocity.evaluate(ctx, stringWriter, LOG_TAG, name);
            if (renderIsSuccess) {
                String newName = stringWriter.toString();
                return FileSystems.getDefault().getPath(parent.toString(), newName);
            } else {
                throw new RuntimeException("文件名渲染失败");
            }
        }
    }



    public Boolean getVisitAllDirectory() {
        return isVisitAllDirectory;
    }

    public void setVisitAllDirectory(Boolean visitAllDirectory) {
        isVisitAllDirectory = visitAllDirectory;
    }

    public Set<String> getDirectoryNames() {
        return directoryNames;
    }

    public void setDirectoryNames(Set<String> directoryNames) {
        this.directoryNames = directoryNames;
    }

    public Boolean getVisitAllFile() {
        return isVisitAllFile;
    }

    public void setVisitAllFile(Boolean visitAllFile) {
        isVisitAllFile = visitAllFile;
    }

    public Set<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(Set<String> fileNames) {
        this.fileNames = fileNames;
    }

    public VelocityContext getCtx() {
        return ctx;
    }

    public void setCtx(VelocityContext ctx) {
        this.ctx = ctx;
    }
}
