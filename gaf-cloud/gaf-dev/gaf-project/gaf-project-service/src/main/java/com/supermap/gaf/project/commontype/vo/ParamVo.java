package com.supermap.gaf.project.commontype.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author:yw
* @Date 2021/3/17
**/
@Data
@ApiModel("代码模板参数实体")
public class ParamVo {
    /**
     * 代码模板参数id。主键,uuid
     */
    @ApiModelProperty(value = "代码模板参数id。主键,uuid")
    private String projCodeTemplateParamId;
    /**
     * 参数名称。
     */
    @ApiModelProperty(value = "参数名称")
    private String paramName;
    /**
     * 参数中文名称。
     */
    @ApiModelProperty(value = "参数中文名称")
    private String paramNameCn;
    /**
     * 描述。
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 默认值。
     */
    @ApiModelProperty(value = "默认值")
    private String defaultValue;
}
