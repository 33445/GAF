package com.supermap.gaf.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.supermap.gaf.project.commontype.entity.ProjCodeTemplateEntity;
import com.supermap.gaf.project.commontype.utils.PageUtils;
import com.supermap.gaf.project.commontype.vo.ProjCodeTemplateEntityVo;
import com.supermap.gaf.project.commontype.vo.ProjCodeTemplateVo;
import com.supermap.gaf.project.commontype.vo.TemplateTypeVo;

import java.util.List;

/**
 * 代码模板表
 *
 * @author wxl
 * @email wxl@gmail.com
 * @date 2020-09-07 10:53:44
 */
public interface ProjCodeTemplateService extends IService<ProjCodeTemplateEntity> {

    /**
     * 单字段条件 分页查询
     * @param templateType
     * @param searchFieldName 字段名
     * @param searchFieldValue 字段值
     * @param orderFieldName 排序字段名
     * @param orderMethod 排序字段值
     * @param pageNum 当前页
     * @param pageSize 页大小
     * @return 分页信息和查询到的数据
     */
    PageUtils queryPageCondition(Integer templateType, String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Long pageNum, Long pageSize);

    /**
     * 查询所有模板类型
     * @return
     */
    List<TemplateTypeVo> getAllTemplateType();

    /**
     * 修改模板表信息
     * @param projCodeTemplateEntity
     */
    void updateProjCodeTemplate(ProjCodeTemplateEntity projCodeTemplateEntity);

    /**
     * 批量删除模板
     * @param projCodeTemplateIds
     */
    void delatebath(List<String> projCodeTemplateIds);

    /**
     * 新增模板
     * @param projCodeTemplateEntity
     */
    void insertProjCodeTemplate(ProjCodeTemplateEntity projCodeTemplateEntity);

    /**
     * 批量新增模板
     * @param projCodeTemplateEntities
     */
    void inserBath(List<ProjCodeTemplateEntity> projCodeTemplateEntities);

    /**
     * 根据模板id 查询模板信息
     * @param projCodeTemplateId
     * @return
     */
    ProjCodeTemplateEntityVo selectById(String projCodeTemplateId);

    /**
     * 获取所有带参数的工程模板信息
     * @return
     */
    List<ProjCodeTemplateVo> getAllTemplates();

    /**
     * 根据模板id 删除模板分组 与 模板参数
     * @param projCodeTemplateId
     */
    void deleteTemplateAndGroupsAndParams(String projCodeTemplateId);


    /**
     * 查询所有类型为工程的模板 类型参考 TemplateTypeEnum枚举类
     * @return 若未查询到则返回null
     */
    List<ProjCodeTemplateVo> getProjTemplates();

    /**
     * 查询所有类型为代码的模板 类型参考 TemplateTypeEnum枚举类
     * @return 若未查询到则返回null
     */
    List<ProjCodeTemplateVo> getCodeTemplates();
}

