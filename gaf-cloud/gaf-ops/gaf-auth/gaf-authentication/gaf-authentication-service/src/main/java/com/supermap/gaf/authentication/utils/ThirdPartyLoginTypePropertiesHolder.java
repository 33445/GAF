/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authentication.utils;

import com.supermap.gaf.authentication.entity.entity.properties.LoginClientProperties;
import com.supermap.gaf.authentication.entity.entity.properties.LoginProperties;
import com.supermap.gaf.authentication.entity.enums.ThirdPartyLoginTypeEumn;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * @author : duke
 * @date:2021/3/25
 * @since 2020/11/16 10:57 AM
 */
@Component
@RefreshScope
public class ThirdPartyLoginTypePropertiesHolder implements InitializingBean {

    @Autowired
    private LoginProperties loginProperties;
    @Autowired
    private LoginClientProperties loginClientProperties;

    private Boolean enableThirdParty;

    private String thirdPartyLoginModel;
    /**
     * 当使用single登录模式时使用
     * eg: oidc:keycloak
     */
    private String thirdPartyName;

    private Map<String, Object> thirdPartyContext;


    @Override
    public void afterPropertiesSet() throws Exception {
        this.enableThirdParty = loginProperties.getEnableThirdParty();
        this.thirdPartyLoginModel = loginProperties.getThirdPartyLoginModel();
        this.thirdPartyName = loginProperties.getThirdPartyName();

        this.thirdPartyContext = new HashMap<>(16);
        //组装所有注册的第三方登录信息
        //oidc
        Map<String, LoginClientProperties.ClientInfo> oidcClientInfoMap = loginClientProperties.getOidc();
        Set<String> oidcKeySet = oidcClientInfoMap.keySet();
        for (String key : oidcKeySet) {
            String newKey = ThirdPartyLoginTypeEumn.OIDC.name() + ":" + key;
            thirdPartyContext.put(newKey.toLowerCase(), oidcClientInfoMap.get(key));
        }
        Map<String, LoginClientProperties.ClientInfo> oauth2ClientInfoMap = loginClientProperties.getOauth2();
        Set<String> oauthKeySet = oauth2ClientInfoMap.keySet();
        for (String key : oauthKeySet) {
            String newKey = ThirdPartyLoginTypeEumn.OAUTH2.name() + ":" + key;
            thirdPartyContext.put(newKey.toLowerCase(), oauth2ClientInfoMap.get(key));
        }
    }

    public String getThirdPartyLoginModel() {
        return thirdPartyLoginModel;
    }

    public String getThirdPartyName() {
        return thirdPartyName;
    }

    public Boolean getEnableThirdParty() {
        return enableThirdParty;
    }

    public Map<String, Object> getThirdPartyContext() {
        return thirdPartyContext;
    }

}
