///*
// * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
// * This program are made available under the terms of the Apache License, Version 2.0
// * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
// */
//package com.supermap.gaf.authentication.service.impl;
//
//import com.supermap.gaf.authentication.dao.TenantUserQueryMapper;
//import com.supermap.gaf.authentication.entity.entity.AuthenticationParam;
//import com.supermap.gaf.authentication.entity.entity.AuthenticationResult;
//import com.supermap.gaf.authentication.service.CustomLoginService;
//import com.supermap.gaf.authentication.service.ValidateAuthenticationService;
//import com.supermap.gaf.platform.entity.TenantUser;
//import com.supermap.gaf.platform.entity.vo.TenantUserSelectVo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import static com.supermap.gaf.authentication.entity.constant.LoginConstant.REDIS_LOGIN_SESSION_PREFIX;
//
//
///**
// * @author : duke
// * @date:2021/3/25
// * @since 2020/11/23 10:48 AM
// */
////@Slf4j
////@Service
//public class ValidateAuthenticationServiceImpl implements ValidateAuthenticationService {
//    @Autowired
//    private CustomLoginService customLoginService;
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//    @Autowired
//    private TenantUserQueryMapper tenantUserQueryMapper;;
//
//    @Override
//    public AuthenticationResult authentication(AuthenticationParam authenticationParam) {
//        String customSessionId = authenticationParam.getCustomSessionId();
//        String jwtToken = authenticationParam.getAuthorization();
//
//        AuthenticationResult result = new AuthenticationResult();
//        if (!StringUtils.isEmpty(jwtToken)) {
//            try {
//                Map<String, ?> map = customLoginService.checkJwtToken(jwtToken);
//                if (null != map) {
//                    result.setUserId((String)map.get("user_id"));
//                    result.setUsername((String) map.get("user_name"));
//                    result.setJwtToken(jwtToken);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//        if (StringUtils.isEmpty(result.getUsername()) && !StringUtils.isEmpty(customSessionId)) {
//            try {
//                Map<String, String> oAuth2AccessToken = (Map<String, String>) redisTemplate.opsForHash().get(REDIS_LOGIN_SESSION_PREFIX + customSessionId, "token");
//                if (oAuth2AccessToken != null) {
//                    jwtToken = oAuth2AccessToken.get("access_token");
//                    Map<String, ?> map = null;
//                    try {
//                        map = customLoginService.checkJwtToken(jwtToken);
//                        if (null != map) {
//                            result.setUserId((String)map.get("user_id"));
//                            result.setUsername((String) map.get("user_name"));
//                            result.setJwtToken(jwtToken);
//                        }
//                    } catch (Exception e) {
//                        //检查到过期可以使用refreshtoken 刷新token
//                        String refreshToken = oAuth2AccessToken.get("refresh_token");
//                        OAuth2AccessToken refreshOauth2AccessToken = customLoginService.refreshOauth2AccessToken(refreshToken);
//                        //更新redis存储的认证信息
//                        customLoginService.refreshStoreLoginSession(customSessionId, refreshOauth2AccessToken);
//
//                        jwtToken = refreshOauth2AccessToken.getValue();
//                        map = customLoginService.checkJwtToken(jwtToken);
//                        if (null != map) {
//                            result.setUserId((String)map.get("user_id"));
//                            result.setUsername((String) map.get("user_name"));
//                            result.setJwtToken(jwtToken);
//                        }
//                        //String username = (String) redisTemplate.opsForHash().get(REDIS_LOGIN_SESSION_PREFIX + customSessionId, "username");
//                        //result.setUsername(username);
//                        //result.setJwtToken(refreshOauth2AccessToken.getValue());
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        if (result.getUserId() != null ) {
//            List<TenantUser> tenantUsers = tenantUserQueryMapper.selectList(TenantUserSelectVo.builder().userId(result.getUserId() ).build());
//            List<String> tenantStringList = new ArrayList<>();
//            for (TenantUser tenantUser : tenantUsers){
//                tenantStringList.add(tenantUser.getTenantId());
//            }
//            result.setTenantList(tenantStringList);
//        }
//        return result;
//    }
//
//
//    /**cur
//     *  2 1 4
//     *  2 1 1
//     *  1   1
//     */
//    /**
//     * re
//     * 1 -3 3      1 4 4
//     * 0 -2 0      1 3 3
//     *-3 -3 -3     4
//     * @param args
//     */
//    public static void main(String[] args) {
//        int[][] dungeon = {{-2,-3,3},{-5,-10,1},{10,30,-5}};
//        System.out.println(calculateMinimumHP(dungeon));;
//    }
//    public static int calculateMinimumHP(int[][] dungeon){
//        // 广度优先遍历 队列
//        int cur = 1;
//        int m = dungeon.length;
//        int n = dungeon[0].length;
//        Integer cache[][] = new Integer[m][n];
//        int curs[][] = new int[m][n];
//        cache[0][0] = dungeon[0][0]>0?cur:cur-dungeon[0][0];
//        curs[0][0] = dungeon[0][0]>0?cur+dungeon[0][0]:cur;
//        List<String> nextSteps = new ArrayList<>();
//        nextSteps.add("0:0");
//        int step = 0;
//        while(!nextSteps.isEmpty()){
//            List<String> newNextSteps = new ArrayList<>();
//            for(String item:nextSteps){
//                String[] position = item.split(":");
//                int i =Integer.valueOf(position[0]);
//                int j =Integer.valueOf(position[1]);
//                Integer value = cache[i][j];
//                cur = curs[i][j];
//                if(i+1<m){
//                    int a = i+1;
//                    int b = j;
//                    int nextValue = dungeon[a][b];
//                    String nextStep = String.format("%d:%d",a,b);
//                    int newCache = 0;
//                    if(nextValue<0){
//                        int last = cur+nextValue;
//                        curs[a][b] = 1;
//                        newCache = value + (last>0?0:1-last);
//                    }else{
//                        curs[a][b] = cur+nextValue;
//                        newCache = value;
//                    }
//                    if(cache[a][b]==null){
//                        cache[a][b] = newCache;
//                    }else{
//                        cache[a][b] = Math.min(newCache,cache[a][b]);
//                    }
//                    newNextSteps.add(nextStep);
//                }
//                if(j+1<n){
//                    int a = i;
//                    int b = j+1;
//                    int nextValue = dungeon[a][b];
//                    String nextStep = String.format("%d:%d",a,b);
//                    int newCache = 0;
//                    if(nextValue<0){
//                        int last = cur+nextValue;
//                        curs[a][b] = 1;
//                        newCache = value + (last>0?0:1-last);
//                    }else{
//                        curs[a][b] = cur+nextValue;
//                        newCache = value;
//                    }
//                    if(cache[a][b]==null){
//                        cache[a][b] = newCache;
//                    }else{
//                        cache[a][b] = Math.min(newCache,cache[a][b]);
//                    }
//                    newNextSteps.add(nextStep);
//                }
//            }
//            nextSteps = newNextSteps;
//        }
//        int re = cache[m-1][n-1];
//        return re;
//
//    }
//}
