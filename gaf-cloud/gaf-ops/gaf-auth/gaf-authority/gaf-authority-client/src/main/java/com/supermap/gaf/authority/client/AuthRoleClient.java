/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.client;

import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * 某用户的角色列表
 * @author wxl
 * @date:2021/3/25
 */
@Component
@FeignClient(name = "GAF-AUTHORITY", contextId = "authRoleClient")
public interface AuthRoleClient {
    @GetMapping("/authority/auth-roles/{userId}/roles")
    MessageResult<List<AuthRole>> roleList(@NotEmpty @PathVariable("userId") String userId);
}
