/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.client;

import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 当前用户角色列表
 * @author wxl
 * @date:2021/3/25
 */
@Component
@FeignClient(name = "GAF-AUTHORITY", contextId = "authRoleCurrentClient")
public interface AuthRoleCurrentClient {
    @GetMapping("/authority/auth-roles/current/list")
    MessageResult<List<AuthRole>> currentRoleList();
}
