/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.resources;

import com.supermap.gaf.authority.client.AuthAuthorizationClient;
import com.supermap.gaf.authority.commontype.AuthResourceMenu;
import com.supermap.gaf.authority.service.AuthAuthorizationQueryService;
import com.supermap.gaf.authority.util.AuthUtil;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.security.SecurityUtilsExt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * 用户的权限资源信息查询
 *
 * @author wxl
 * /authority/auth-authorization/
 */
@Component
@Api(value = "用户的权限资源信息查询")
public class AuthAuthorizationResource implements AuthAuthorizationClient {

    @Autowired
    private AuthAuthorizationQueryService authAuthorizationQueryService;


    @ApiOperation(value = "当前用户的菜单查询")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/menus")
    @Override
    public MessageResult<List<AuthResourceMenu>> listAuthResourceMenus() {
        String userId = SecurityUtilsExt.getUserId();
        List<AuthResourceMenu> menus = authAuthorizationQueryService.listAuthorizationMenu(userId);
        return MessageResult.data(menus).build();
    }
}
