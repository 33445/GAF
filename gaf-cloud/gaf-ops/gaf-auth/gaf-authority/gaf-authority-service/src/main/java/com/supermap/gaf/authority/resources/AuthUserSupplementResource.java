/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.authority.resources;

import com.supermap.gaf.authority.client.AuthUserSupplementClient;
import com.supermap.gaf.authority.commontype.AuthRole;
import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.service.AuthAuthorizationQueryService;
import com.supermap.gaf.authority.service.AuthUserSupplementQueryService;
import com.supermap.gaf.authority.service.AuthUserSupplementService;
import com.supermap.gaf.authority.vo.AuthUserSupplementVo;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 用户补充接口
 *
 * @author wxl
 * @since 2022/3/10
 */
@Component
@Api(value = "用户补充接口")
public class AuthUserSupplementResource implements AuthUserSupplementClient {


    @Autowired
    private AuthUserSupplementService authUserSupplementService;

    @Autowired
    private AuthUserSupplementQueryService authUserSupplementQueryService;

    @Autowired
    private AuthAuthorizationQueryService authAuthorizationQueryService;


    /**
     * 该接口返回网关对接gaf-storage时需要的一些信息
     * @param username
     * @return
     */
    @ApiOperation(value = "根据用户名查询用户部分信息", notes = "根据用户名查询用户部分信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{username}/some-info")
    public MessageResult<Map<String,Object>> someInfo(@PathParam("username") String username) {

        AuthUserSupplement authUser = authUserSupplementQueryService.getByUserName(username);
        Map<String,Object>  re = new HashMap<>();
        if (authUser != null) {
            List<AuthRole> data = authAuthorizationQueryService.listAuthorizationRole(authUser.getUserId());
            re.put(SOME_INFO_ROLE_IDS_KEY,data.stream().map(item -> item.getRoleId()).collect(Collectors.joining(",")));

        }
        return MessageResult.data(re).message("查询成功").build();
    }


    @ApiOperation(value = "查询部门用户树", notes = "返回的节点已组织为树形结构")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/tree")
    public MessageResult<List<TreeNode>> getUserTree() {
        List<TreeNode> result = authUserSupplementService.getUserTree();
        return MessageResult.data(result).message("查询成功").build();
    }


    @ApiOperation(value = "查询某岗位下的用户", notes = "根据岗位id查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "postId", value = "岗位id", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/list-by-post/{postId}")
    public MessageResult<List<AuthUserSupplementVo>> listUserByPost(@PathParam("postId") String postId) {
        List<AuthUserSupplementVo> data = authUserSupplementService.listUserByPost(postId);
        return MessageResult.data(data).message("查询成功").build();
    }

    @ApiOperation(value = "查询某部门下的用户", notes = "根据部门id查询该部门下的用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "departmentId", value = "部门id", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/list-by-department/{departmentId}")
    public MessageResult<List<AuthUserSupplementVo>> listUserByDepartment(@NotEmpty @PathParam("departmentId") String departmentId) {
        List<AuthUserSupplementVo> result = authUserSupplementService.listUserByDepartmentWithName(departmentId);
        return MessageResult.data(result).message("查询成功").build();
    }


    @ApiOperation(value = "分页条件查询用户及其补充信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1", defaultValue = "1", allowableValues = "range[1,infinity]", paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10", allowableValues = "range(0,infinity]", paramType = "query", dataType = "integer"),
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<Page<AuthUserSupplementVo>> pageList(@Valid @BeanParam AuthUserSelectVo authUserSearchVo,
                                                                            @DefaultValue("1") @QueryParam("pageNum") Integer pageNum,
                                                                            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize){

        Page<AuthUserSupplementVo> authUserSupplementVoPage = authUserSupplementService.page(authUserSearchVo, pageNum, pageSize);

        return  MessageResult.data(authUserSupplementVoPage).message("查询成功").build();
    }


    @ApiOperation(value = "查询当前用户的信息")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/info")
    public MessageResult<AuthUserSupplementVo> getInfo() {
        AuthUserSupplementVo authUser = authUserSupplementService.getUserInfo();
        return MessageResult.data(authUser).message("查询成功").build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增用户及其补充信息", notes = "新增用户及其补充信息")
    @Path("/user")
    public MessageResult<Void> saveUser(AuthUserSupplementVo authUserSupplementVo){
        authUserSupplementService.saveUser(authUserSupplementVo);
        return MessageResult.successe(Void.class).message("新增成员成功").build();
    }


    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新用户及其补充信息", notes = "更新用户及其补充信息")
    @Path("/user")
    public MessageResult<Void> updateUser(AuthUserSupplementVo userSupplementVo){
        authUserSupplementService.updateUser(userSupplementVo);
        return MessageResult.successe(Void.class).message("保存用户补充信息成功").build();
    }

    @ApiOperation(value = "删除用户及其补充信息", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/users/{userId}")
    public MessageResult<Void> reomveUser(@PathParam("userId") String userId) {
        authUserSupplementService.reomveUser(userId);
        return MessageResult.successe(Void.class).message("删除操作成功").build();
    }

    @ApiOperation(value = "批量删除用户及其补充信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userIds", value = "用户id集合", paramType = "path", dataType = "string", required = true)
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch/users")
    public MessageResult<Void> reomveUsers(List<String> userIds) {
        authUserSupplementService.reomveUsers(userIds);
        return MessageResult.successe(Void.class).message("删除操作成功").build();
    }


    @ApiOperation(value = "查询用户补充信息", notes = "查询用户补充信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{userId}")
    public MessageResult<AuthUserSupplement> getById(@PathParam("userId") String userId) {
        return MessageResult.data(authUserSupplementService.getById(userId)).message("查询成功").build();
    }


    @ApiOperation(value = "删除用户补充信息", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{userId}")
    public MessageResult<Void> deleteAuthUserSupplement(@PathParam("userId") String userId) {
        authUserSupplementService.deleteAuthUserSupplement(userId);
        return MessageResult.successe(Void.class).message("删除操作成功").build();
    }

    @ApiOperation(value = "批量删除用户补充", notes = "根据id批量删除用户补充")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    public MessageResult<Void> batchDelete(List<String> userIds) {
        authUserSupplementService.batchDelete(userIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新用户补充")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authUserSupplement", value = "用户补充", paramType = "body", dataTypeClass = AuthUser.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户补充id", paramType = "path", dataType = "string", required = true)
    })
    @Path("/{userId}")
    public MessageResult<Void> updateAuthUser(@Valid AuthUserSupplement authUserSupplement, @PathParam("userId") String userId) {
        authUserSupplement.setUserId(userId);
        authUserSupplementService.updateAuthUserSupplement(authUserSupplement);
        return MessageResult.successe(Void.class).message("更新操作成功").build();
    }

}
