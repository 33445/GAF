package com.supermap.gaf.authority.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.commontype.AuthDepartment;
import com.supermap.gaf.authority.commontype.AuthPost;
import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.commontype.AuthUserSupplement;
import com.supermap.gaf.authority.dao.AuthUserSupplementMapper;
import com.supermap.gaf.authority.enums.NodeTypeEnum;
import com.supermap.gaf.authority.service.AuthDepartmentService;
import com.supermap.gaf.authority.service.AuthPostService;
import com.supermap.gaf.authority.service.AuthUserSupplementService;
import com.supermap.gaf.authority.util.TreeConvertUtil;
import com.supermap.gaf.authority.vo.AuthUserSupplementSelectVo;
import com.supermap.gaf.authority.vo.AuthUserSupplementVo;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.platform.client.AuthUserClient;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import com.supermap.gaf.security.SecurityUtilsExt;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.supermap.gaf.authority.util.TreeConvertUtil.ROOT_PARENT_ID;

/**
 * 用户信息补充表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class AuthUserSupplementServiceImpl implements AuthUserSupplementService{

	private static final Logger  log = LoggerFactory.getLogger(AuthUserSupplementServiceImpl.class);

	@Autowired
    private AuthUserSupplementMapper authUserSupplementMapper;


	@Autowired
	private AuthUserClient authUserClient;


	@Autowired
	private AuthDepartmentService authDepartmentService;

	@Autowired
	private AuthPostService authPostService;

	@Override
	public List<TreeNode> getUserTree() {
		List<TreeNode> nodes = new ArrayList<>(8);
		// 当前用户对应租户下的部门
		List<AuthDepartment> authDepartments = authDepartmentService.listDepartment(null);
		if (!CollectionUtils.isEmpty(authDepartments)) {
			authDepartments.forEach(authDepartment -> {
				TreeNode departmentNode = new TreeNode();
				departmentNode.setKey(authDepartment.getDepartmentId());
				departmentNode.setTitle(authDepartment.getDepartmentName());
				departmentNode.setType(NodeTypeEnum.DEPARTMENT.getValue());
				if (org.apache.commons.lang3.StringUtils.isEmpty(authDepartment.getParentId())) {
					departmentNode.setParentId(ROOT_PARENT_ID);
				} else {
					departmentNode.setParentId(authDepartment.getParentId());
				}
				departmentNode.setSortSn(authDepartment.getSortSn());
				nodes.add(departmentNode);

				AuthUserSupplementSelectVo selectVo = new AuthUserSupplementSelectVo();
				selectVo.setDepartmentId(authDepartment.getDepartmentId());
				List<AuthUserSupplement> authUserSupplements = selectList(selectVo);
				List<AuthUserSupplementVo> authUserSupplementVos = listAuthUserSupplementVoBySupplement(authUserSupplements);

				// 部门下用户
				if (!CollectionUtils.isEmpty(authUserSupplementVos)) {
					authUserSupplementVos.forEach(vo -> {
						TreeNode postNode = new TreeNode();
						postNode.setKey(vo.getUserId());
						postNode.setParentId(vo.getDepartmentId());
						postNode.setTitle(vo.getRealName());
						postNode.setType(NodeTypeEnum.USER.getValue());
						postNode.setSortSn(vo.getSortSn());
						nodes.add(postNode);
					});
				}
			});
		}
		// 一级排序 type  二级排序 序号
		return TreeConvertUtil.convertToTree(nodes, Comparator.comparing(n -> ("" + n.getType() + n.getSortSn())));
	}


	@Override
	public List<AuthUserSupplementVo> listUserByPost(String postId) {
		if (org.apache.commons.lang3.StringUtils.isEmpty(postId)) {
			throw new GafException("岗位标识不能为空");
		}
		//用户表通过岗位查询
		List<AuthUserSupplement> authUserSupplementList = selectList(AuthUserSupplementSelectVo.builder().postId(postId).build());
		List<AuthUserSupplementVo> authUserSupplementVoList = listAuthUserSupplementVoBySupplement(authUserSupplementList);

		if (!CollectionUtils.isEmpty(authUserSupplementVoList)) {
			String departmentName = null;
			AuthPost authPost = authPostService.getById(postId);
			if (null != authPost && authPost.getStatus()) {
				AuthDepartment authDepartment = authDepartmentService.getById(authPost.getDepartmentId());
				if (null != authDepartment && authDepartment.getStatus()) {
					departmentName = authDepartment.getDepartmentName();
				}
			}
			for (AuthUserSupplementVo authUserSupplementVo : authUserSupplementVoList) {
				authUserSupplementVo.setDepartmentName(departmentName);
				if (authPost != null) {
					authUserSupplementVo.setPostName(authPost.getPostName());
				}
			}
		}
		return authUserSupplementVoList;
	}

	// @Transactional(rollbackFor = Exception.class)
	@Override
	public List<AuthUserSupplementVo> listUserByDepartmentWithName(String departmentId) {
		AuthDepartment department = authDepartmentService.getById(departmentId);
		if (department == null) {
			throw new GafException("找不到部门信息");
		}
		List<AuthUserSupplement> authUserSupplementList = selectList(AuthUserSupplementSelectVo.builder().departmentId(departmentId).build());
		List<AuthUserSupplementVo> authUserSupplementVoList = listAuthUserSupplementVoBySupplement(authUserSupplementList);
		if (authUserSupplementVoList.size() > 0) {
			for (AuthUserSupplementVo vo : authUserSupplementVoList) {
				vo.setDepartmentName(department.getDepartmentName());
			}
		}
		List<AuthPost> authPosts = authPostService.listPost(null, departmentId);
		if (authUserSupplementVoList.size() > 0 && authPosts != null && authPosts.size() > 0) {
			Map<String, AuthPost> idAndPostMap = authPosts.stream().collect(Collectors.toMap(AuthPost::getPostId, authPost -> authPost));
			for (AuthUserSupplementVo vo : authUserSupplementVoList) {
				AuthPost authPost = idAndPostMap.get(vo.getPostId());
				if (authPost != null) {
					vo.setPostName(authPost.getPostName());
				}
			}
		}
		return authUserSupplementVoList;
	}

	@Override
	public AuthUserSupplementVo getUserInfo() {

		String userId = SecurityUtilsExt.getUserId();
		AuthUserSupplement authUserSupplement = authUserSupplementMapper.select(userId);
		AuthUser user = authUserClient.getByUserId(userId).checkAndGetData();
		AuthUserSupplementVo authUser = new AuthUserSupplementVo(user,authUserSupplement);

		String departmentId = authUser.getDepartmentId();
		if (!org.apache.commons.lang3.StringUtils.isEmpty(departmentId)) {
			AuthDepartment department = authDepartmentService.getById(departmentId);
			authUser.setDepartmentName(department.getDepartmentName());
		}
		String postId = authUser.getPostId();
		if (!org.apache.commons.lang3.StringUtils.isEmpty(postId)) {
			AuthPost post = authPostService.getById(postId);
			authUser.setPostName(post.getPostName());
		}
		authUser.setPassword(null);
		return authUser;
	}

	@Override
    public AuthUserSupplement getById(String userId){
        if(userId == null){
            throw new IllegalArgumentException("userId不能为空");
        }
        return  authUserSupplementMapper.select(userId);
    }

	@Override
    public Page<AuthUserSupplement> listByPageCondition(AuthUserSupplementSelectVo authUserSupplementSelectVo, int pageNum, int pageSize) {
        PageInfo<AuthUserSupplement> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            authUserSupplementMapper.selectList(authUserSupplementSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
	public 	List<AuthUserSupplement> selectList(AuthUserSupplementSelectVo authUserSupplementSelectVo){
		return authUserSupplementMapper.selectList(authUserSupplementSelectVo);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
    public void insertAuthUserSupplement(AuthUserSupplement authUserSupplement){
        if (StringUtils.isEmpty(authUserSupplement.getUserId())) {
        	throw new GafException("用户id不能为空");
		}
		String userName = authUserSupplement.getUserName();
		if (StringUtils.isEmpty(userName)) {
			throw new GafException("用户名不能为空");
		}
		// 先查询用户名是否存在,如果存在则删除,保证用户名唯一
		AuthUserSupplementSelectVo selectVo = new AuthUserSupplementSelectVo();
		selectVo.setUserName(userName);
		List<AuthUserSupplement> authUserSupplements = authUserSupplementMapper.selectList(selectVo);
		if (!authUserSupplements.isEmpty()) {
			authUserSupplementMapper.batchDelete(authUserSupplements.stream().map(AuthUserSupplement::getUserId).collect(Collectors.toList()));
		}
		String name = SecurityUtilsExt.getUserName();
		authUserSupplement.setCreatedBy(name);
		authUserSupplement.setUpdatedBy(name);
        authUserSupplementMapper.insert(authUserSupplement);
    }


	@Override
    public int deleteAuthUserSupplement(String userId){
		if (!StringUtils.isEmpty(userId)) {
			return authUserSupplementMapper.delete(userId);
		}
		return 0;
    }

	@Override
    public int batchDelete(List<String> userIds){
		if(!CollectionUtils.isEmpty(userIds)){
			return authUserSupplementMapper.batchDelete(userIds);
		}
		return 0;
    }

	@Override
    public int updateAuthUserSupplement(AuthUserSupplement authUserSupplement){
		// 无法修改用户名 暂时无需校验
		// checkUniqueness(authUserSupplement);
		authUserSupplement.setUpdatedBy(SecurityUtilsExt.getUserName());
		return authUserSupplementMapper.update(authUserSupplement);
    }

	private void checkUniqueness(AuthUserSupplement authUserSupplement) {
		if (StringUtils.isEmpty(authUserSupplement.getUserName()) ) return;
		AuthUserSupplement oldSupplement = authUserSupplementMapper.select(authUserSupplement.getUserId());
		if (!oldSupplement.getUserName().equals(authUserSupplement.getUserName())) {
			AuthUserSupplementSelectVo selectVo = new AuthUserSupplementSelectVo();
			selectVo.setUserName(authUserSupplement.getUserName());
			List<AuthUserSupplement> authUserSupplements = authUserSupplementMapper.selectList(selectVo);
			if (!CollectionUtils.isEmpty(authUserSupplements)) {
				throw new GafException("用户名已存在");
			}
		}
	}

	@Override
	public AuthUserSupplementVo getAuthUserSupplementVoByUserId(String userId) {
		AuthUser authUser = authUserClient.getByUserId(userId).checkAndGetData();
		if (authUser == null) return null;
		return getAuthUserSupplementVoByAuthUser(authUser);
	}

	@Override
	public AuthUserSupplementVo getAuthUserSupplementVoBySupplement(AuthUserSupplement authUserSupplement) {
		AuthUser authUser = authUserClient.getByUserId(authUserSupplement.getUserId()).checkAndGetData();
		if (authUser == null){
			return null;
		}
		AuthUserSupplementVo vo = new AuthUserSupplementVo();
		BeanUtils.copyProperties(authUserSupplement,vo);
		BeanUtils.copyProperties(authUser,vo);
		return vo;
	}

	@Override
	public AuthUserSupplementVo getAuthUserSupplementVoByAuthUser(AuthUser authUser) {
		AuthUserSupplementVo vo = new AuthUserSupplementVo();
		BeanUtils.copyProperties(authUser,vo);
		AuthUserSupplement authUserSupplement = authUserSupplementMapper.select(authUser.getUserId());
		if (authUserSupplement != null ) {
			BeanUtils.copyProperties(authUserSupplement,vo);
		}
		return vo;
	}

	@Override
	public List<AuthUserSupplementVo> listAuthUserSupplementVoBySupplement(List<AuthUserSupplement> authUserSupplementList) {
		if(authUserSupplementList == null || authUserSupplementList.isEmpty()) return Collections.emptyList();
		List<String> userIds = new ArrayList<>();
		for (AuthUserSupplement authUserSupplement : authUserSupplementList){
			userIds.add(authUserSupplement.getUserId());
		}
		List<AuthUser> authUsers = authUserClient.listByUserIds(userIds).checkAndGetData();
		Map<String,AuthUser> authUsersMap = authUsers.stream().collect(Collectors.toMap(AuthUser::getUserId, item -> item));
		List<AuthUserSupplementVo> voList = new ArrayList<>();
		for (AuthUserSupplement authUserSupplement : authUserSupplementList){
			AuthUser authUser = authUsersMap.get(authUserSupplement.getUserId());
			if (authUser == null){
				continue;
			}
			AuthUserSupplementVo vo = new AuthUserSupplementVo();
			BeanUtils.copyProperties(authUserSupplement,vo);
			BeanUtils.copyProperties(authUser,vo);
			voList.add(vo);
		}
		return voList;
	}

	@Override
	public List<AuthUserSupplementVo> listAuthUserSupplementVoByAuthUser(List<AuthUser> authUserList) {
		if (authUserList == null || authUserList.isEmpty()) return Collections.emptyList();
		List<String> idList = authUserList.stream().map(AuthUser::getUserId).collect(Collectors.toList());
		List<AuthUserSupplement> authUserSupplementList = authUserSupplementMapper.batchSelect(idList);
		Map<String,AuthUserSupplement> authUserSupplementMap = authUserSupplementList.stream().collect(Collectors.toMap(AuthUserSupplement::getUserId, item -> item));
		List<AuthUserSupplementVo> voList = authUserList.stream().map(authUser -> {
			AuthUserSupplementVo vo = new AuthUserSupplementVo();
			BeanUtils.copyProperties(authUser, vo);
			AuthUserSupplement authUserSupplement = authUserSupplementMap.get(authUser.getUserId());
			if (authUserSupplement != null) {
				BeanUtils.copyProperties(authUserSupplement, vo);
			}
			return vo;
		}).collect(Collectors.toList());
		return  voList;
	}

	@GlobalTransactional(rollbackFor = Exception.class)
	@Transactional
	@Override
	public void reomveUser(String userId) {
		authUserClient.deleteAuthUser(userId).throwExIfFail();
		deleteAuthUserSupplement(userId);
	}

	@GlobalTransactional(rollbackFor = Exception.class)
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void reomveUsers(List<String> userIds) {
		if (userIds != null && !userIds.isEmpty()) {
			for (String userId : userIds) {
				reomveUser(userId);
			}
		}
	}


	@Override
	public AuthUserSupplementVo getMemberInfo(String userId) {
		AuthUserSupplementVo vo = getAuthUserSupplementVoByUserId(userId);
		return supplementDepAndPost(vo);
	}

	@Override
	public Page<AuthUserSupplementVo> page(AuthUserSelectVo authUserSearchVo, Integer pageNum, Integer pageSize) {
		Page<AuthUser> page = authUserClient.pageList(authUserSearchVo, pageNum, pageSize).checkAndGetData();
		List<AuthUser> list	 = page.getPageList();
		List<AuthUserSupplementVo> authUserSupplementVos = listAuthUserSupplementVoByAuthUser(list);
		for (AuthUserSupplementVo authUserSupplementVo : authUserSupplementVos) {
			supplementDepAndPost(authUserSupplementVo);
		}
		return Page.create(page.getPageNum(), page.getPageSize(), page.getTotalCount(), page.getTotalPage(), authUserSupplementVos);
	}

	@GlobalTransactional(rollbackFor = Exception.class)
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveUser(AuthUserSupplementVo userSupplementVo) {
		AuthUser user = authUserClient.save(userSupplementVo.extractAuthUser()).checkAndGetData();
		AuthUserSupplement authUserSupplement = userSupplementVo.extractAuthUserSupplement();
		authUserSupplement.setUserId(user.getUserId());
		insertAuthUserSupplement(authUserSupplement);
	}

	@GlobalTransactional(rollbackFor = Exception.class)
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateUser(AuthUserSupplementVo userSupplementVo) {
		AuthUserSupplement authUserSupplement = userSupplementVo.extractAuthUserSupplement();
		updateAuthUserSupplement(authUserSupplement);
		AuthUser authUser = userSupplementVo.extractAuthUser();
		authUserClient.updateAuthUser(authUser, authUser.getUserId()).throwExIfFail();
	}

	private AuthUserSupplementVo supplementDepAndPost(AuthUserSupplementVo authUserSupplementVo) {
		String departmentId = authUserSupplementVo.getDepartmentId();
		if (departmentId != null) {
			AuthDepartment authDepartment = authDepartmentService.getById(departmentId);
			authUserSupplementVo.setDepartmentName(authDepartment.getDepartmentName());
		}
		String postId = authUserSupplementVo.getPostId();
		if (postId != null) {
			AuthPost post = authPostService.getById(postId);
			authUserSupplementVo.setPostName(post.getPostName());
		}
		return authUserSupplementVo;
	}



}
