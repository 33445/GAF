package com.supermap.gaf.storage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.common.storage.entity.MinioConfig;
import com.supermap.gaf.common.storage.spi.TenantInfoI;
import com.supermap.gaf.common.storage.utils.CommonStorageUtils;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.storage.dao.S3ServerMapper;
import com.supermap.gaf.storage.dao.SpaceMapper;
import com.supermap.gaf.storage.dao.StoragePermissionMapper;
import com.supermap.gaf.storage.entity.*;
import com.supermap.gaf.storage.entity.vo.SpaceConfigSelectVo;
import com.supermap.gaf.storage.entity.vo.SpaceSelectVo;
import com.supermap.gaf.storage.entity.vo.StoragePermissionSelectVo;
import com.supermap.gaf.storage.enums.CreatedType;
import com.supermap.gaf.storage.enums.PermissionType;
import com.supermap.gaf.storage.enums.TargetType;
import com.supermap.gaf.storage.events.UpdateRemoteVolumeEvent;
import com.supermap.gaf.storage.service.SpaceConfigService;
import com.supermap.gaf.storage.utils.StorageCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 服务实现类
 *
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class SpaceConfigServiceImpl implements SpaceConfigService {

    private static final Logger log = LoggerFactory.getLogger(SpaceConfigServiceImpl.class);

    @Autowired
    private SpaceMapper spaceMapper;

    @Autowired
    private S3ServerMapper s3ServerMapper;
    @Autowired
    private TenantInfoI tenantInfoI;
    @Autowired
    private StoragePermissionMapper storagePermissionMapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    TargetType getByConfigType(String configType){
        switch (configType){
            case "platform":return TargetType.PLATFORM;
            case "tenant":return TargetType.TENANT;
            case "user":return TargetType.USER;
        }
        return TargetType.PLATFORM;
    }

    @Override
    public SpaceConfig getById(String configType,String id) {
        TargetType targetType = getByConfigType(configType);
        SpaceConfigSelectVo selectVo = SpaceConfigSelectVo.builder().targetType(targetType.getValue()).build();
        if (id == null) {
            throw new IllegalArgumentException("id不能为空");
        }
        selectVo.setId(id);
        List<SpaceConfig> spaceConfigs = spaceMapper.selectSpaceConfig(selectVo);
        if (CollectionUtils.isEmpty(spaceConfigs)) {
            return null;
        } else {
            return spaceConfigs.get(0);
        }
    }

    public List<SpaceConfig> selectList(String configType,SpaceConfigSelectVo spaceConfigSelectVo){
        TargetType targetType = getByConfigType(configType);
        if(targetType == TargetType.PLATFORM){
            spaceConfigSelectVo.setTargetType(targetType.getValue());
            spaceConfigSelectVo.setTarget("");
        }else if(targetType == TargetType.TENANT){
            spaceConfigSelectVo.setTargetType(targetType.getValue());
            spaceConfigSelectVo.setTarget(tenantInfoI.getTenantId());
        }
        return spaceMapper.selectSpaceConfig(spaceConfigSelectVo);
    }

    @Override
    public Page<SpaceConfig> listByPageCondition(String configType,SpaceConfigSelectVo spaceConfigSelectVo, int pageNum, int pageSize) {
        PageInfo<SpaceConfig> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            selectList(configType,spaceConfigSelectVo);
        });
        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
    }

    @Override
    public Collection<ConfigName> getAllNames(String configType,String ower) {
        TargetType targetType = getByConfigType(configType);
        SpaceSelectVo selectVo = new SpaceSelectVo();
        if(targetType == TargetType.PLATFORM){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget("");
        }else if(targetType == TargetType.TENANT){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget(tenantInfoI.getTenantId());
        }
        List<Space> re = spaceMapper.selectList(selectVo);
        Map<String, ConfigName> configMaps = re.stream().collect(Collectors.toMap(item -> item.getName(), item -> new ConfigName(item.getName()), (v1, v2) -> v1));
        if (ower != null) {
            List<Permission> permissions = storagePermissionMapper.selectList(StoragePermissionSelectVo.builder().resource("").ower(ower).build());
            for (Permission permission : permissions) {
                if (configMaps.containsKey(permission.getConfigName())) {
                    ConfigName configName = configMaps.get(permission.getConfigName());
                    Set<PermissionType> permissionTypes = StorageCommonUtils.splitAndConvert(permission.getScope(), item -> PermissionType.valueOf(item), true, true);
                    if (configName != null) {
                        if (configName.containsKey("permissions")) {
                            ((Set<PermissionType>) configName.get("permissions")).addAll(permissionTypes);
                        } else {
                            configName.put("permissions", permissionTypes);
                        }
                    }
                }
            }
        }
        return configMaps.values();
    }

    @Override
    @Transactional
    public void insert(String configType,SpaceConfig spaceConfig) {
        TargetType targetType = getByConfigType(configType);
        String s3ServerId = UUID.randomUUID().toString();
        S3Server s3Server = S3Server.builder()
                .id(s3ServerId)
                .accessKey(spaceConfig.getAccessKey())
                .serviceEndpoint(spaceConfig.getServiceEndpoint())
                .secretKey(spaceConfig.getSecretKey()).build();
        s3ServerMapper.insert(s3Server);
        Space space = new Space();
        space.setId(UUID.randomUUID().toString());
        space.setName(spaceConfig.getName());
        space.setParentSpaceId(s3ServerId);
        space.setStorageName(spaceConfig.getBucketName());
        space.setDescription(spaceConfig.getDescription());
        space.setTotalSize(spaceConfig.getTotalSize());
        if(targetType == TargetType.PLATFORM){
            space.setCreatedType(CreatedType.CREATED.getValue());
            space.setTarget("");
            space.setTargetType(targetType.getValue());
        }else if(targetType == TargetType.TENANT){
            space.setCreatedType(CreatedType.CREATED.getValue());
            space.setTarget(tenantInfoI.getTenantId());
            space.setTargetType(targetType.getValue());
        }
        spaceMapper.insert(space);
        try{
            // 初始化桶
            MinioConfig config = MinioConfig.builder().bucketName(spaceConfig.getBucketName()).serviceEndpoint(spaceConfig.getServiceEndpoint()).accessKey(spaceConfig.getAccessKey()).secretKey(spaceConfig.getSecretKey()).build();
            CommonStorageUtils.initBucket(CommonStorageUtils.createClient(config),config);
        }catch (Exception e){}
        eventPublisher.publishEvent(new UpdateRemoteVolumeEvent(""));
    }


    @Override
    public void delete(String configType,String id) {
        TargetType targetType = getByConfigType(configType);
        SpaceSelectVo selectVo = SpaceSelectVo.builder().id(id).build();
        if(targetType == TargetType.PLATFORM){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget("");
        }else if(targetType == TargetType.TENANT){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget(tenantInfoI.getTenantId());
        }
        List<Space> spaces = spaceMapper.selectList(selectVo);
        if (!CollectionUtils.isEmpty(spaces)) {
            Space space = spaces.get(0);
            s3ServerMapper.delete(space.getParentSpaceId());
            spaceMapper.delete(id);
        }
        eventPublisher.publishEvent(new UpdateRemoteVolumeEvent(""));
    }

    @Override
    @Transactional
    public void batchDelete(String configType,List<String> ids) {
        TargetType targetType = getByConfigType(configType);
        List<Space> spaces = null;
        if(targetType == TargetType.PLATFORM){
            spaces = spaceMapper.selectByIdsAndTargetTypeAndTargetId(ids, targetType.getValue(), "");
        }else if(targetType == TargetType.TENANT){
            spaces = spaceMapper.selectByIdsAndTargetTypeAndTargetId(ids, TargetType.TENANT.getValue(), tenantInfoI.getTenantId());
        }
        if (!CollectionUtils.isEmpty(spaces)) {
            List<String> s3ServerIds = new ArrayList<>();
            List<String> spaceIds = new ArrayList<>();
            for (Space item : spaces) {
                s3ServerIds.add(item.getParentSpaceId());
                spaceIds.add(item.getId());
            }
            s3ServerMapper.batchDelete(s3ServerIds);
            spaceMapper.batchDelete(spaceIds);
        }
        eventPublisher.publishEvent(new UpdateRemoteVolumeEvent(""));
    }


    @Override
    @Transactional
    public void update(String configType,SpaceConfig spaceConfig) {
        TargetType targetType = getByConfigType(configType);
        SpaceSelectVo selectVo = SpaceSelectVo.builder().id(spaceConfig.getId()).build();
        if(targetType == TargetType.PLATFORM){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget("");
        }else if(targetType == TargetType.TENANT){
            selectVo.setTargetType(targetType.getValue());
            selectVo.setTarget(tenantInfoI.getTenantId());
        }
        List<Space> spaces = spaceMapper.selectList(selectVo);
        if (!CollectionUtils.isEmpty(spaces)) {
            Space space = spaces.get(0);
            // 分配类型的存储，只能修改name、description
            if(CreatedType.ALLOCATED.getValue().equals(space.getCreatedType())){
                Space spaceUpdate = Space.builder().id(spaceConfig.getId()).name(spaceConfig.getName()).description(spaceConfig.getDescription()).build();
                spaceMapper.updateSelective(spaceUpdate);
            }else{
                Space spaceUpdate = new Space();
                BeanUtils.copyProperties(spaceConfig, spaceUpdate);
                spaceUpdate.setStorageName(spaceConfig.getBucketName());
                S3Server s3Server = S3Server.builder().id(space.getParentSpaceId()).accessKey(spaceConfig.getAccessKey()).serviceEndpoint(spaceConfig.getServiceEndpoint())
                        .secretKey(spaceConfig.getSecretKey()).build();
                s3ServerMapper.update(s3Server);
                eventPublisher.publishEvent(new UpdateRemoteVolumeEvent(""));
            }
        }
    }

    @Override
    public void allocate(String configType, Space space) {
        Space parent = spaceMapper.select(space.getParentSpaceId());
        if (parent != null) {
            space.setId(UUID.randomUUID().toString());
            space.setCreatedType(CreatedType.ALLOCATED.getValue());
            spaceMapper.insert(space);
        }
    }

}
