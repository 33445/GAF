package com.supermap.gaf.storage.enums;

public enum PermissionType {
    upload, delete, share, download, query;
}
