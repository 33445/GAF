package com.supermap.gaf.storage.events.listeners;

import com.supermap.gaf.storage.config.StorageCustomConfig;
import com.supermap.gaf.storage.events.UpdateRemoteVolumeEvent;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class UpdateRemoteVolumeEventListener implements ApplicationListener<UpdateRemoteVolumeEvent> {
    private static final Logger log = LoggerFactory.getLogger(UpdateRemoteVolumeEventListener.class);
    @Override
    public void onApplicationEvent(UpdateRemoteVolumeEvent event) {
        try {
            // 睡一秒 避免修改事务未提交情况
            log.info("更新挂载，等待1秒事务提交...");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        OkHttpClient okHttpClient = new OkHttpClient();
        List<String> mountReloadUrls =  StorageCustomConfig.MOUNT_RELOAD_URLS;
        if(mountReloadUrls==null){
            return;
        }
        for(String item:mountReloadUrls){
            try{
                log.info("开始 request reload url '{}'",item);
                okHttpClient.newCall(new Request.Builder().url(item).build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        log.error("request reload url '{}' error: {}",item,e.getMessage());
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        log.info("request reload url '{}' 成功",item);
                    }
                });
            }catch (Exception e){
                log.error("request reload url '{}' error: {}",item,e.getMessage());
            }
        }
    }
}
