package com.supermap.gaf.storage.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author wxl
 * @since 2021/11/25
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
