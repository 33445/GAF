package com.supermap.gaf.storage.config;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;

class BaseDataTest {

    @Test
    @DisplayName("system_tenant基础数据测试")
//    @DisabledOnOs(OS.WINDOWS)
    void test() throws LiquibaseException {
        DataSource dataSource = getDatasource("jdbc:postgresql://localhost:5432/test","postgres","root");
        SpringLiquibase springLiquibase = new SpringLiquibase();
        springLiquibase.setDataSource(dataSource);
        springLiquibase.setChangeLog("classpath:"+"com/supermap/gaf/base/data/new/system_tenant/all.xml");
        springLiquibase.setResourceLoader(new DefaultResourceLoader());
        springLiquibase.afterPropertiesSet();
    }

    DataSource getDatasource(String url, String username, String password){
        DataSource dataSource = new SingleConnectionDataSource(url,username,password,true);
        return dataSource;
    }

}