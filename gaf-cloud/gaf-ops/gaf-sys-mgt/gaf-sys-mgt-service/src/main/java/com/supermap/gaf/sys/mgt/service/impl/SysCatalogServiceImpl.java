/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.sys.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.constant.DbFieldNameConstant;
import com.supermap.gaf.authority.enums.NodeTypeEnum;
import com.supermap.gaf.authority.vo.SelectOptionVo;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.Page;
import com.supermap.gaf.data.access.commontypes.ExtendSortSnParam;
import com.supermap.gaf.data.access.dao.BatchSortAndCodeMapper;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
import com.supermap.gaf.sys.mgt.dao.SysCatalogMapper;
import com.supermap.gaf.sys.mgt.enums.BizTypeEnum;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import com.supermap.gaf.sys.mgt.service.SysCatalogService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.supermap.gaf.authority.util.TreeConvertUtil.ROOT_PARENT_ID;

/**
 * 目录服务实现类
 *
 * @author wangxiaolong
 * @date:2021/3/25
 */
@Service
public class SysCatalogServiceImpl implements SysCatalogService {

    private final SysCatalogMapper sysCatalogMapper;

    private final BatchSortAndCodeMapper batchSortAndCodeMapper;

    public SysCatalogServiceImpl(SysCatalogMapper sysCatalogMapper, BatchSortAndCodeMapper batchSortAndCodeMapper) {
        this.sysCatalogMapper = sysCatalogMapper;
        this.batchSortAndCodeMapper = batchSortAndCodeMapper;
    }

    @Override
    public SysCatalog getById(String catalogId) {
        if (catalogId == null) {
            throw new IllegalArgumentException("catalogId不能为空");
        }
        return sysCatalogMapper.select(catalogId);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysCatalog insertSysCatalog(SysCatalog sysCatalog) {
        sysCatalog.setCatalogId(UUID.randomUUID().toString());

        SysCatalog.SysCatalogBuilder queryBuilder = SysCatalog.builder().type(sysCatalog.getType()).parentId(sysCatalog.getParentId());
        Integer sameLevelCount = sysCatalogMapper.countByCombination(queryBuilder.build());
        if (sameLevelCount <= 0) {
            sysCatalog.setSortSn(1);
        } else if (sysCatalog.getSortSn() == null || sysCatalog.getSortSn() > sameLevelCount + 1) {
            sysCatalog.setSortSn(sameLevelCount + 1);
        }
        if (!ROOT_PARENT_ID.equalsIgnoreCase(sysCatalog.getParentId())) {
            SysCatalog parentCatalog = sysCatalogMapper.select(sysCatalog.getParentId());
            if (parentCatalog == null) {
                throw new GafException("未找到上级目录");
            }
            if (!StringUtils.isEmpty(parentCatalog.getBizTypeCode())) {
                sysCatalog.setBizTypeCode(parentCatalog.getBizTypeCode());
            }
        }
        sysCatalogMapper.insert(sysCatalog);
        ExtendSortSnParam baseSortSnParam = getBaseSortSnParam(sysCatalog);
        batchSortAndCodeMapper.revisionSortSnMutiCondition(baseSortSnParam);
        return sysCatalog;
    }

    private ExtendSortSnParam getBaseSortSnParam(SysCatalog sysCatalog) {
        ExtendSortSnParam extendSortSnParam = new ExtendSortSnParam();
        extendSortSnParam.setTableName(DbFieldNameConstant.SYS_CATALOG);
        extendSortSnParam.setIdFieldName(DbFieldNameConstant.CATALOG_ID);
        extendSortSnParam.setParentIdFieldName(DbFieldNameConstant.PARENT_ID);
        extendSortSnParam.setSortSnFieldName(DbFieldNameConstant.SORT_SN);
        extendSortSnParam.setUpdatedTimeFieldName(DbFieldNameConstant.UPDATED_TIME);
        extendSortSnParam.setParentId(sysCatalog.getParentId());
        if (ROOT_PARENT_ID.equalsIgnoreCase(sysCatalog.getParentId())) {
            List<String> conditions = new LinkedList<>();
            conditions.add(DbFieldNameConstant.TYPE + " = '" + sysCatalog.getType() + "'");
            extendSortSnParam.setConditions(conditions);
        }
        return extendSortSnParam;
    }


    /**
     * ；批量插入，可主动设置id。新版资源目录树需要该功能
     * @param sysCatalogs 目录集合
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchInsert(List<SysCatalog> sysCatalogs) {
        if (sysCatalogs != null && sysCatalogs.size() > 0) {
            Set<String> parentIds = new HashSet<>();
            List<ExtendSortSnParam> extendSortSnParamList = new LinkedList<>();
            sysCatalogs.forEach(sysCatalog -> {
                if (!parentIds.contains(sysCatalog.getParentId())) {
                    parentIds.add(sysCatalog.getParentId());
                    extendSortSnParamList.add(getBaseSortSnParam(sysCatalog));
                    // 暂时支持同一种类型 同一租户的目录批量添加后的修复排序
                }
            });
            sysCatalogMapper.batchInsert(sysCatalogs);
            for (ExtendSortSnParam extendSortSnParam : extendSortSnParamList) {
                batchSortAndCodeMapper.revisionSortSnMutiCondition(extendSortSnParam);
            }
        }

    }

    public void batchImport(List<SysCatalog> sysCatalogs){
        if(!CollectionUtils.isEmpty(sysCatalogs)){
            sysCatalogMapper.batchInsert(sysCatalogs);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteSysCatalog(String catalogId) {
        List<SysCatalog> catalogs = getCatalogTreeListByRootId(catalogId);
        List<String> catalogIds = new ArrayList<>();
        for(SysCatalog item:catalogs){
            catalogIds.add(item.getCatalogId());
        }
        batchDelete(catalogIds,false);
        return catalogIds.size();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchDelete(List<String> catalogIds,Boolean recursionAble) {
        if(!CollectionUtils.isEmpty(catalogIds)){
            if(!BooleanUtils.isFalse(recursionAble)){
                for (String catalogId : catalogIds) {
                    deleteSysCatalog(catalogId);
                }
            }else{
                sysCatalogMapper.batchDelete(catalogIds);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysCatalog updateSysCatalog(SysCatalog sysCatalog) {
        SysCatalog current = sysCatalogMapper.select(sysCatalog.getCatalogId());
        if (current == null) {
            throw new GafException("找不到目录");
        }
        sysCatalogMapper.update(sysCatalog);
        if (!Objects.equals(current.getParentId(), sysCatalog.getParentId())) {
            ExtendSortSnParam baseSortSnParam = getBaseSortSnParam(current);
            batchSortAndCodeMapper.revisionSortSnMutiCondition(baseSortSnParam);
            ExtendSortSnParam extendSortSnParam = getBaseSortSnParam(sysCatalog);
            batchSortAndCodeMapper.revisionSortSnMutiCondition(extendSortSnParam);
        } else if (!Objects.equals(sysCatalog.getSortSn(), current.getSortSn())) {
            ExtendSortSnParam baseSortSnParam = getBaseSortSnParam(current);
            baseSortSnParam.setOldSortSn(current.getSortSn());
            baseSortSnParam.setCurSortSn(sysCatalog.getSortSn());
            batchSortAndCodeMapper.revisionSortSnMutiCondition(baseSortSnParam);
        }
        return sysCatalogMapper.select(sysCatalog.getCatalogId());
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<String> getParentPath(String catalogId) {
        List<String> parentPath = new LinkedList<>();
        SysCatalog sysCatalog = this.sysCatalogMapper.select(catalogId);
        if (sysCatalog == null || ROOT_PARENT_ID.equalsIgnoreCase(sysCatalog.getParentId())) {
            parentPath.add(0, ROOT_PARENT_ID);
            return parentPath;
        }
        String parentId = sysCatalog.getParentId();
        while (true) {
            SysCatalog parentCatalog = this.sysCatalogMapper.select(parentId);
            if (parentCatalog == null) {
                throw new RuntimeException("找不到上级目录");
            } else if (ROOT_PARENT_ID.equals(parentCatalog.getParentId())) {
                parentPath.add(0, parentCatalog.getCatalogId());
                break;
            } else {
                parentPath.add(0, parentCatalog.getCatalogId());
                parentId = parentCatalog.getParentId();
            }
        }
        return parentPath;
    }


    @Override
    public List<TreeNode> getNodesByType(String type) {
        // 若没查到或者判断为平台管理员 则认为是平台级 若有租户则查询租户的所有分类目录
        SysCatalog querySysCatalog;
        if (CatalogTypeEnum.ROLE_GROUP_TYPE.getValue().equals(type)) {
            querySysCatalog = SysCatalog.builder().type(type).build();
        } else {
            querySysCatalog = SysCatalog.builder().type(type).build();
        }
        List<SysCatalog> sysCatalogList = sysCatalogMapper.selectByCombination(querySysCatalog);
        return sysCatalogList.stream().map(sysCatalog -> {
            TreeNode node = new TreeNode();
            node.setTitle(sysCatalog.getName());
            node.setSortSn(sysCatalog.getSortSn());
            node.setParentId(sysCatalog.getParentId());
            node.setType(NodeTypeEnum.CATALOG.getValue());
            node.setKey(sysCatalog.getCatalogId());
            return node;
        }).collect(Collectors.toList());
    }

    @Override
    public List<SysCatalog> getByCombination(SysCatalog queryCatalog) {
        return this.sysCatalogMapper.selectByCombination(queryCatalog);
    }

    @Override
    public List<SysCatalog> getByType(String type) {
        SysCatalog querySysCatalog = SysCatalog.builder().type(type).build();
        return sysCatalogMapper.selectByCombination(querySysCatalog);
    }


    @Override
    public Page<SysCatalog> listByPageCondition(String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Integer pageNum, Integer pageSize, String catalogType) {
        SysCatalog.SysCatalogBuilder builder = SysCatalog.builder().type(catalogType).parentId(ROOT_PARENT_ID);
        PageInfo<SysCatalog> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> sysCatalogMapper.selectByCondition(searchFieldName, searchFieldValue, orderFieldName, orderMethod, builder.build()));
        Page<SysCatalog> page = new Page<>();
        page.setPageIndex(pageInfo.getPageNum());
        page.setPageSize(pageInfo.getPageSize());
        page.setTotal((int) pageInfo.getTotal());
        page.setContent(pageInfo.getList());
        page.calculateTotalPage();
        return page;
    }

    @Override
    public List<TreeNode> getTreeByCatalogId(String catalogId) {
        List<TreeNode> all = new LinkedList<>();
        SysCatalog sysCatalog = this.sysCatalogMapper.select(catalogId);
        if (sysCatalog != null) {
            TreeNode node = new TreeNode();
            node.setKey(sysCatalog.getCatalogId());
            node.setTitle(sysCatalog.getName());
            node.setType(NodeTypeEnum.CATALOG.getValue());
            node.setSortSn(1);
            node.setParentId(ROOT_PARENT_ID);
            all.add(node);
            Set<String> parentIds = new HashSet<>();
            parentIds.add(sysCatalog.getCatalogId());
            while (true) {
                List<SysCatalog> sysCatalogs = this.sysCatalogMapper.selectByParentIds(parentIds);
                if (sysCatalogs.isEmpty()) {
                    break;
                }
                Set<String> catalogIds = new HashSet<>(sysCatalogs.size());
                sysCatalogs.forEach(catalog -> {
                    catalogIds.add(catalog.getCatalogId());
                    TreeNode treeNode = new TreeNode();
                    treeNode.setKey(catalog.getCatalogId());
                    treeNode.setTitle(catalog.getName());
                    treeNode.setType(NodeTypeEnum.CATALOG.getValue());
                    treeNode.setSortSn(catalog.getSortSn());
                    treeNode.setParentId(catalog.getParentId());
                    all.add(treeNode);
                });
                parentIds = catalogIds;
            }
        }
        return all;
    }

    @Override
    public List<SelectOptionVo> getBizTypes() {
        BizTypeEnum[] values = BizTypeEnum.values();
        List<SelectOptionVo> optionVos = new LinkedList<>();
        for (BizTypeEnum value : values) {
            SelectOptionVo selectOptionVo = new SelectOptionVo();
            selectOptionVo.setKey(value.getValue());
            selectOptionVo.setValue(value.getValue());
            selectOptionVo.setLabel(value.getName());
            optionVos.add(selectOptionVo);
        }
        return optionVos;
    }

    @Override
    public List<SysCatalog> getCatalogTreeListByRootId(String rootCatalogId) {
        return sysCatalogMapper.getCatalogTreeListByRootId(rootCatalogId);
    }


}
