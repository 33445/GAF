package com.supermap.gaf.portal.configuration;

import com.supermap.gaf.security.registry.UserAuthoritiesServiceSpiRegistry;
import com.supermap.gaf.security.registry.UserResourcesServiceSpiRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author wxl
 * @since 2022/4/9
 */
@Configuration
@Slf4j
public class UserAuthoritiesAndResourcesConfigration {

    @Bean
    public UserAuthoritiesServiceSpiRegistry userAuthoritiesSpiRegistry() {
        return new UserAuthoritiesServiceSpiRegistry();
    }


    @Bean
    public UserResourcesServiceSpiRegistry userResourcesSpiRegistry() {
        return new UserResourcesServiceSpiRegistry();
    }

}
