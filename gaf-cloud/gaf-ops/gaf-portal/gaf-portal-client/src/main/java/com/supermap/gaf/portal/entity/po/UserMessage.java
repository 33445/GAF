package com.supermap.gaf.portal.entity.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户消息通知表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户消息通知表")
public class UserMessage implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String userMessageId;
    @NotNull
    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("类型")
    private String type;
    @ApiModelProperty("类型链接")
    private String typeLink;
    @ApiModelProperty("消息")
    private String userMessageContent;
    @NotNull
    @ApiModelProperty("未读已读状态")
    private Boolean userMessageStatus;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
}