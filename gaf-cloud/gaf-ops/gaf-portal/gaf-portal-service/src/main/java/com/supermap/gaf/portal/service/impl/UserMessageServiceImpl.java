package com.supermap.gaf.portal.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.portal.dao.UserMessageMapper;
import com.supermap.gaf.portal.entity.po.UserMessage;
import com.supermap.gaf.portal.entity.vo.UserMessageSelectVo;
import com.supermap.gaf.portal.service.StompService;
import com.supermap.gaf.portal.service.UserMessageService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * 用户消息通知表服务实现类
 * @author dqc
 * @date yyyy-mm-dd
 */
@Slf4j
@Service
public class UserMessageServiceImpl implements UserMessageService {
    
	private static final Logger  log = LoggerFactory.getLogger(UserMessageServiceImpl.class);
	
	@Autowired
    private UserMessageMapper userMessageMapper;
	@Autowired
    private StompService stompService;
	

	@Override
    public UserMessage getById(String userMessageId){
        if(userMessageId == null){
            throw new IllegalArgumentException("userMessageId不能为空");
        }
        return userMessageMapper.select(userMessageId);
    }
	
	@Override
    public Page<UserMessage> listByPageCondition(UserMessageSelectVo userMessageSelectVo, int pageNum, int pageSize) {
        PageInfo<UserMessage> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            userMessageMapper.selectList(userMessageSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

    @Override
    public List<UserMessage> listByCondition(UserMessageSelectVo userMessageSelectVo) {
        return userMessageMapper.selectList(userMessageSelectVo);
    }


    @Override
    public UserMessage insertUserMessage(UserMessage userMessage){
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		userMessage.setUserMessageId(UUID.randomUUID().toString());
        log.info("生成即时消息通知");
        userMessageMapper.insert(userMessage);
        //发送stomp消息
        sendBellMsg(userMessage);
        return userMessage;
    }
	
	@Override
    public void batchInsert(List<UserMessage> userMessages){
		if (userMessages != null && userMessages.size() > 0) {
	        userMessages.forEach(userMessage -> {
				userMessage.setUserMessageId(UUID.randomUUID().toString());
            });
            userMessageMapper.batchInsert(userMessages);
            //发送stomp消息
            sendBellMsg(userMessages.get(0));
        }
        
    }
	

	@Override
    public void deleteUserMessage(String userMessageId){
        userMessageMapper.delete(userMessageId);
        //发送stomp消息
        sendBellMsg(userMessageId);
    }

	@Override
    public void batchDelete(List<String> userMessageIds){
        userMessageMapper.batchDelete(userMessageIds);
        //发送stomp消息
        for (String userMessageId : userMessageIds){
            sendBellMsg(userMessageId);
        }
    }
	

	@Override
    public UserMessage updateUserMessage(UserMessage userMessage){
        userMessageMapper.update(userMessage);
        //发送stomp消息
        sendBellMsg(userMessage);
        return userMessage;
    }

    /**
     * 发送stomp消息
     * @param userMessage
     */
    private void sendBellMsg(UserMessage userMessage){
        stompService.sendBellMsg(userMessage.getUserName(), listByCondition(UserMessageSelectVo.builder().userName(userMessage.getUserName()).userMessageStatus(false).build()));
    }
    /**
     * 发送stomp消息
     * @param userMessageId
     */
    private void sendBellMsg(String userMessageId){
        UserMessage userMessage = getById(userMessageId);
        sendBellMsg(userMessage);
    }
    
}
