/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.portal.menu.service.impl;

import com.alibaba.fastjson.JSON;
import com.supermap.gaf.portal.menu.commontypes.CustomationInfo;
import com.supermap.gaf.portal.menu.commontypes.MenuInfo;
import com.supermap.gaf.portal.menu.dao.CustomationDao;
import com.supermap.gaf.portal.menu.dao.MenuDao;
import com.supermap.gaf.portal.menu.service.CustomationServices;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Yw
 * @date:2021/3/25
 */
@Service
@Slf4j
public class CustomationServicesImpl implements CustomationServices {
    @Autowired
    private CustomationDao customationDao;
    private static Logger logger = LogUtil.getLocLogger(CustomationServicesImpl.class);
    @Autowired
    private MenuDao menuDao;

    @Override
    public String queryCustomation() {
        String msg;
        boolean success = true;

        CustomationInfo customation = getCustomizedPortalConfig();

        Map<String, Object> res = new HashMap<String, Object>(16);
        res.put("customization", customation);
        msg = customation != null ? "查询定制信息成功;" : "查询不到定制信息;0";

        List<MenuInfo> menuArr = menuDao.queryMenus();
        String userName = SecurityUtilsExt.getUserName();
        log.info("customation:{}", customation);

        if (StringUtils.isEmpty(userName)) {
            msg += menuArr.size() > 0 ? "查询菜单成功" : "查询不到菜单信息1";
            res.put("menus", menuArr);
            res.put("success", success);
            res.put("msg", msg);
            return JSON.toJSONString(res);
        }

        //获取授权菜单
        res.put("success", success);
        res.put("msg", msg);
        return JSON.toJSONString(res);
    }

    @Override
    public CustomationInfo queryConfig() {
        return getCustomizedPortalConfig();
    }
    @Override
    public CustomationInfo queryDefaultConfig() {
        CustomationInfo customizedPortalConfig = getCustomizedPortalConfig();
        customizedPortalConfig.setUser(null);
        return customizedPortalConfig;
    }

    private CustomationInfo getCustomizedPortalConfig() {
        return customationDao.queryCustomation();
    }
}
