package com.supermap.gaf.portal.service;
import com.supermap.gaf.portal.entity.po.MicroFrontend;
import com.supermap.gaf.portal.entity.vo.MicroFrontendSelectVo;
import com.supermap.gaf.commontypes.pagination.Page;
import java.util.*;

/**
 * 微前端应用服务类
 * @author dqc 
 * @date yyyy-mm-dd
 */
public interface MicroFrontendService {
	
	/**
    * id查询微前端应用
    * @return
    */
	MicroFrontend getById(String microFrontendId);
	
	/**
     * 分页条件查询
     * @param microFrontendSelectVo 查询条件
     * @param pageNum 当前页数
     * @param pageSize 页面大小
     * @return 分页对象
     */
	Page<MicroFrontend> listByPageCondition(MicroFrontendSelectVo microFrontendSelectVo, int pageNum, int pageSize);

	/**
	 * 多条件查询
	 * @param microFrontendSelectVo 查询条件
	 * @return 若未查询到则返回空集合
	 */
	List<MicroFrontend> selectList(MicroFrontendSelectVo microFrontendSelectVo);


		/**
         * 新增微前端应用
         * @return
         */
    void insertMicroFrontend(MicroFrontend microFrontend);

    /**
     * 删除微前端应用
     * 
     */
    int deleteMicroFrontend(String microFrontendId);

    /**
     * 批量删除
     * 
	 */
    int batchDelete(List<String> microFrontendIds);

    /**
     * 更新微前端应用
     * @return 
     */
    int updateMicroFrontend(MicroFrontend microFrontend);
    
}
