package com.supermap.gaf.platform.client;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.commontypes.MessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;


@Component
@FeignClient(name = "GAF-PLATFORM", contextId = "AuthUserCurrentClient")
public interface AuthUserCurrentClient {

    @GetMapping("/platform/auth-users/current/info")
    MessageResult<AuthUser> getUserInfo();

}
