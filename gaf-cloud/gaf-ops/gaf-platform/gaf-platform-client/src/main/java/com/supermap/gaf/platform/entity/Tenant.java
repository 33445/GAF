package com.supermap.gaf.platform.entity;

import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 租户表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("租户表")
public class Tenant implements Serializable{
    private static final long serialVersionUID = -1L;
    @ApiModelProperty("主键")
    private String tenantId;
    @NotNull(groups={AddGroup.class,UpdateGroup.class})
    @ApiModelProperty("租户名称")
    private String tenantName;
    @ApiModelProperty("租户初始化数据库资源id")
    private String tenantResourceId;
    /**
     * 默认值1：0
     */
    @ApiModelProperty("0未初始化 1 初始化中 2 使用中 3已冻结")
    private Integer status;
    @ApiModelProperty("租户类型")
    private String tenantType;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("imgr用户名")
    private String imgrUsername;
    @ApiModelProperty("imgr密码")
    private String imgrPassword;
    @ApiModelProperty("imgr站点")
    private String imgrAppsetId;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;

    public static enum TenantStatus{
        NOT_READY(0),READYING(1), UP(2),DOWN(3);
        private int value;

        TenantStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}