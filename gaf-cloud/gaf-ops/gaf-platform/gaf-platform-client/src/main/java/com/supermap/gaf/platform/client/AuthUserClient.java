package com.supermap.gaf.platform.client;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.util.List;

@Component
@FeignClient(name = "GAF-PLATFORM", contextId = "AuthUserClient")
public interface AuthUserClient {


    @GetMapping("/platform/auth-users/{userId}")
    MessageResult<AuthUser> getByUserId(@PathVariable("userId") String userId);


    @GetMapping("/platform/auth-users/name/{username}")

    MessageResult<AuthUser> getByUsername(@PathVariable("username") String username);

    @GetMapping("/platform/auth-users/email/{email}")
    MessageResult<AuthUser> getByEmail(@PathVariable("email") String email);

    @GetMapping("/platform/auth-users")
    MessageResult<Page<AuthUser>> pageList(@Valid @SpringQueryMap AuthUserSelectVo authUserSelectVo,
                                                  @RequestParam ("pageNum") Integer pageNum,
                                                  @RequestParam ("pageSize") Integer pageSize);
    @PostMapping("/platform/auth-users/batch")
    MessageResult<List<AuthUser>> listByUserIds(@NotEmpty @RequestBody List<String>  userIds);


    @PostMapping("/platform/auth-users")
    MessageResult<AuthUser> save(@Valid @RequestBody AuthUser authUser);

    @PutMapping("/platform/auth-users/{userId}")
    MessageResult<AuthUser> updateAuthUser(@Valid @RequestBody AuthUser authUser, @PathVariable("userId") String userId);

    @DeleteMapping("/platform/auth-users/{userId}")
    MessageResult<AuthUser> deleteAuthUser(@PathVariable("userId") String userId);

}
