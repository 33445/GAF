/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.vo.EmailChangeVo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.constant.CacheGroupConstant;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.extend.commontypes.User;
import com.supermap.gaf.platform.dao.AuthUserMapper;
import com.supermap.gaf.platform.email.EmailConstant;
import com.supermap.gaf.platform.email.EmailService2;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import com.supermap.gaf.platform.service.AuthUserService;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.LogUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 用户服务实现类
 *
 * @author dqc
 * @date:2021/3/25
 */
@Service
public class AuthUserServiceImpl implements AuthUserService {
    private static final Logger logger = LogUtil.getLocLogger(AuthUserServiceImpl.class);
    @Value("${gaf.authority.user.initPassword:}")
    private String initPassword;
    @Value("${spring.mail.enable:true}")
    private Boolean mailEnable;
    @Lazy
    @Autowired
    private AuthUserMapper authUserMapper;

    @Autowired
    private EmailService2 emailService2;

    @Autowired
    public RedisTemplate<String, Object> redisTemplate;


    /**
     * 用户密码长度
     */
    private static final int USER_PASSWORD_LENGTH = 8;

    @Override
    public Page<AuthUser> page(AuthUserSelectVo authUserSelectVo, Integer pageNum, Integer pageSize) {
        PageInfo<AuthUser> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            authUserMapper.selectList(authUserSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

    @Override
    public AuthUser getByUsername(String username) {
        List<AuthUser> authUsers = authUserMapper.selectList(AuthUserSelectVo.builder().name(username).build());
        return CollectionUtils.isEmpty(authUsers)?null:authUsers.get(0);
    }

    @Override
    public AuthUser getByEmail(String email) {
        List<AuthUser> authUsers = authUserMapper.selectList(AuthUserSelectVo.builder().email(email).build());
        return CollectionUtils.isEmpty(authUsers)?null:authUsers.get(0);
    }


    @Override
    public AuthUser getByUserId(String userId) {
        if (userId == null) {
            throw new IllegalArgumentException("userId不能为空");
        }
        return authUserMapper.select(userId);
    }

    @Override
    public List<AuthUser> listByUserIds(List<String> userIds) {
        if (userIds == null || userIds.isEmpty()) return Collections.emptyList();
        return authUserMapper.batchSelect(userIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AuthUser save(AuthUser authUser) {
        // 唯一性校验
        checkUniqueness(authUser, false);
        String email = authUser.getEmail();

        Boolean canConnect = emailService2.getCanConnect();
        String password;
        if (mailEnable && !StringUtils.isEmpty(email) && canConnect) {
            // 生成初始密码
            password = generatePassword();
        } else {
            // 默认123456
            password = "123456";
        }

        String userId = UUID.randomUUID().toString();
        authUser.setUserId(userId);
        // 加密后的密码，存入数据库
        String bCryptPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        authUser.setPassword(bCryptPassword);

        authUserMapper.insert(authUser);

        AuthUser insertedAuthUser = authUserMapper.select(userId);

        // 将初始密码发送至邮箱中
        if (mailEnable && !StringUtils.isEmpty(email) && canConnect) {
            emailService2.sendPassword(authUser.getEmail(), password);
        }

        return insertedAuthUser;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<AuthUser> saveBatch(List<AuthUser> authUsers) {
        if (!CollectionUtils.isEmpty(authUsers)) {
            List<AuthUser> result = new LinkedList<>();
            for (AuthUser authUser : authUsers) {
                result.add(save(authUser));
            }
            return  result;
        }
        return Collections.emptyList();
    }

    // todo: 消息队列 异步删除其他租户数据库的脏数据
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AuthUser deleteAuthUser(String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw new GafException("删除异常，用户标识缺失");
        }
        AuthUser oldAuthUser = authUserMapper.select(userId);
        if (oldAuthUser == null) {
            throw new GafException("找不到用户");
        }
        // 查询用户的所有角色 若有管理员的角色则不允许删除
        //Set<String> roleIdSet = listRole(oldAuthUser);
        //if (roleIdSet.contains(AuthUser.TENANT_ADMIN_ROLE_ID)) {
        //    throw new GafException("该用户有租户管理员的角色，不允许删除");
        //}
        // 删除用户
        authUserMapper.delete(userId);
        //authUserRoleService.deleteByUserId(userId);
        return oldAuthUser;
    }




    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchDelete(List<String> userIds) {
        if (!CollectionUtils.isEmpty(userIds)) {
            userIds.forEach(this::deleteAuthUser);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AuthUser updateAuthUser(AuthUser authUser) {
        AuthUser authUserExist = getByUserId(authUser.getUserId());
        if (null == authUserExist || !authUserExist.getStatus()) {
            throw new GafException("更新失败，不是有效的用户");
        }

        checkUniqueness(authUser, true);
        authUserMapper.update(authUser);

        return authUser;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AuthUser resetPassword(String userId) {
        AuthUser authUser = getByUserId(userId);
        if (authUser == null || !authUser.getStatus()) {
            throw new GafException("用户不存在");
        }
        String email = authUser.getEmail();
        Boolean canConnect = emailService2.getCanConnect();
        String password;
        if (mailEnable && !StringUtils.isEmpty(email) && canConnect) {
            // 生成初始密码
            password = generatePassword();
        } else {
            // 默认123456
            password = "123456";
        }

        String newPassword = password;
        // 加密后的密码，存入数据库
        String bCryptPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
        authUser.setPassword(bCryptPassword);
        authUserMapper.update(authUser);

        // 将新密码发送至邮箱中
        if (mailEnable && !StringUtils.isEmpty(email) && canConnect) {
            emailService2.sendPassword(authUser.getEmail(), newPassword);
        }
        return authUser;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public AuthUser active(String userId) {
        List<AuthUser> authUsers = authUserMapper.selectByCombination(AuthUser.builder().userId(userId).build());
        if (authUsers.size() != 1) {
            throw new GafException("未找到该用户");
        }
        AuthUser authUser = authUsers.get(0);
        AuthUser authUser1 = new AuthUser();
        authUser1.setState(true);
        authUser1.setUserId(userId);
        authUserMapper.update(authUser1);
        authUser.setState(true);
        return authUser;
    }

    @Override
    public AuthUser inActive(String userId) {
        List<AuthUser> authUsers = authUserMapper.selectByCombination(AuthUser.builder().userId(userId).build());
        if (authUsers.size() != 1) {
            throw new GafException("未找到该用户");
        }
        AuthUser authUser = authUsers.get(0);
        AuthUser authUser1 = new AuthUser();
        authUser1.setState(false);
        authUser1.setUserId(userId);
        authUserMapper.update(authUser1);
        authUser.setState(false);
        return authUser;
    }





    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changePassword(String userId, String oldPassword, String newPassword) {
        AuthUser user = authUserMapper.selectWithPassword(userId);
        boolean isSame = BCrypt.checkpw(oldPassword, user.getPassword());
        if (isSame) {
            String bCryptPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
            authUserMapper.update(AuthUser.builder().userId(user.getUserId()).password(bCryptPassword).build());
        } else {
            throw new GafException("旧密码不正确");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String changeEmail(EmailChangeVo emailChangeVo) {
        User user = SecurityUtilsExt.getUser();
        if (Objects.equals(user.getEmail(), emailChangeVo.getNewEmail())) {
            return "新旧邮箱相同";
        }


        if (!StringUtils.isEmpty(user.getEmail())) {
            // 校验
            Object oldEmailCheckCode = redisTemplate.opsForValue().get(CacheGroupConstant.CHECK_CODE + ":" + user.getEmail());
            if (Objects.isNull(oldEmailCheckCode)) {
                return "原邮箱校验码已过时";
            }
            if (!Objects.equals(emailChangeVo.getOldEmailCheckCode(), oldEmailCheckCode)) {
                return "校验码:" + emailChangeVo.getOldEmailCheckCode() + "错误";
            }
        }


        Object newEmailCheckCode = redisTemplate.opsForValue().get(CacheGroupConstant.CHECK_CODE + ":" + emailChangeVo.getNewEmail());
        if (Objects.isNull(newEmailCheckCode)) {
            return "新邮箱校验码已过时";
        }
        if (!Objects.equals(emailChangeVo.getNewEmailCheckCode(), newEmailCheckCode)) {
            return "校验码:" + emailChangeVo.getNewEmailCheckCode() + "错误";
        }
        AuthUser authUser = getByUserId(user.getUserId());
        authUser.setPassword(null);
        authUser.setEmail(emailChangeVo.getNewEmail());
        authUserMapper.update(authUser);
        return null;
    }

    @Override
    public void sendCheckCode(String email) {
        User user = SecurityUtilsExt.getUser();
        if (StringUtils.isEmpty(email)) {
            email = user.getEmail();
        }
        if (StringUtils.isEmpty(email)) {
            throw new GafException("邮箱为空");
        }
        String checkCode = generateRandomStr(6);
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime after10Min = now.plusMinutes(10);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = dtf.format(after10Min);
        redisTemplate.opsForValue().set(CacheGroupConstant.CHECK_CODE + ":" + email, checkCode, Duration.ofMinutes(10));
        String context = String.format(EmailConstant.CHECK_CODE_TEXT_TEMPLATE, user.getUserName(), checkCode, "10", format);
        emailService2.sendText(email, EmailConstant.CAHANGE_EMAIL, context);
    }


    // 生成几位随机数字 不足以0填充
    private String generateRandomStr(int bit) {
        int randomNum = generateRandomNum(bit);
        return String.format("%0" + bit + "d", randomNum);
    }

    // 生成几位随机数字
    private int generateRandomNum(int bit) {
        Random random = new Random();
        return random.nextInt((int) Math.pow(10, bit));
    }


    /**
     * 随机生成密码
     *
     * @return 密码
     */
    private String generatePassword() {
        if (!StringUtils.isBlank(this.initPassword)) {
            return this.initPassword;
        }
        return UUID.randomUUID().toString().replace("-", "").substring(0, USER_PASSWORD_LENGTH);
    }


    /**
     * 唯一性校验
     *
     * @param authUser 用户
     * @param isUpdate 是否为更新，更新时需要排除当前用户
     */
    private void checkUniqueness(AuthUser authUser, boolean isUpdate) {
        if (isUpdate) {
            AuthUser oldUser = authUserMapper.select(authUser.getUserId());

            if (!StringUtils.isEmpty(authUser.getEmail()) &&  !authUser.getEmail().equals(oldUser.getEmail())) {
                List<AuthUser> emailAuthUsers = authUserMapper.selectByCombination(AuthUser.builder().status(true).email(authUser.getEmail()).build());
                if (!CollectionUtils.isEmpty(emailAuthUsers)) {
                    throw new GafException("邮箱已存在");
                }
            }

            if (!StringUtils.isEmpty(authUser.getName()) &&  !authUser.getName().equals(oldUser.getName()) ) {
                List<AuthUser> nameAuthUsers = authUserMapper.selectByCombination(AuthUser.builder().status(true).name(authUser.getName()).build());
                if (!CollectionUtils.isEmpty(nameAuthUsers)) {
                    throw new GafException("用户名已存在");
                }
            }
        } else {
            // email，全局不重复

            String email = authUser.getEmail();
            if (!StringUtils.isEmpty(email)) {
                List<AuthUser> emailAuthUsers = authUserMapper.selectByCombination(AuthUser.builder().status(true).email(email).build());
                if (!CollectionUtils.isEmpty(emailAuthUsers)) {
                    throw new GafException("邮箱已存在");
                }
            }

            String name = authUser.getName();
            if (StringUtils.isEmpty(name)) {
                throw new IllegalArgumentException("用户名不能为空");
            }

            List<AuthUser> nameAuthUsers = authUserMapper.selectByCombination(AuthUser.builder().status(true).name(name).build());
            if (!CollectionUtils.isEmpty(nameAuthUsers)) {
                throw new GafException("用户名已存在");
            }
        }
    }
}
