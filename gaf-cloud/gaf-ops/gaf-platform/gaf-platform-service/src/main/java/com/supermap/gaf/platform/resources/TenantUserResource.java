package com.supermap.gaf.platform.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.platform.entity.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;



/**
 * 租户用户接口
 * @author zrc
 * @date yyyy-mm-dd
 * /tenant-users/
 */
@Component
@Api(value = "租户用户接口")
public class TenantUserResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "获取当前用户租户列表", notes = "获取当前用户租户列表")
    @Path("/tenantList")
    public MessageResult<List<Tenant>> getTenantList(){
        Tenant tenant = new Tenant();
        tenant.setTenantId("system");
        tenant.setTenantName("系统租户");
        tenant.setStatus(2);
        tenant.setTenantResourceId("f1d794ce-bd47-44ab-90a5-de85a9f2bc22");
        return MessageResult.data(Collections.singletonList(tenant)).message("查询成功").build();
    }


}
