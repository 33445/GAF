package com.supermap.gaf.platform.resources;

import com.supermap.gaf.commontypes.GafCommonConstant;
import com.supermap.gaf.commontypes.MessageResult;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.platform.client.TenantClient;
//import com.supermap.gaf.platform.entity.Tenant;
//import com.supermap.gaf.platform.entity.vo.TenantAdminVo;
//import com.supermap.gaf.platform.entity.vo.TenantSelectVo;
//import com.supermap.gaf.platform.service.TenantService;
//import com.supermap.gaf.validGroup.AddGroup;
//import com.supermap.gaf.validGroup.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.groups.ConvertGroup;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * 租户接口
 * @author zrc 
 * @date yyyy-mm-dd
 * /platform/tenants
 */
@Component
@Api(value = "租户接口")
public class TenantResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "切换租户时设置浏览器cookie", notes = "切换租户时设置浏览器cookie")
    @Path("/switch")
    public MessageResult setTenantCookie(@QueryParam("tenantId") String tenantId, @Context HttpServletResponse response){
        Cookie cookie = new Cookie(GafCommonConstant.TENANT_HEADER, tenantId);
        cookie.setPath("/");
        response.addCookie(cookie);
        MessageResult result = new MessageResult();
        result.setSuccessed(true);
        return result;
    }


}
