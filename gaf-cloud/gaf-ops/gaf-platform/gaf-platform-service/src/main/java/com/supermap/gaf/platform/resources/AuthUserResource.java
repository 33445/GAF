/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform.resources;

import com.supermap.gaf.authority.commontype.AuthUser;
import com.supermap.gaf.authority.vo.EmailChangeVo;
import com.supermap.gaf.authority.vo.PasswordVo;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.platform.client.AuthUserClient;
import com.supermap.gaf.platform.client.AuthUserCurrentClient;
import com.supermap.gaf.platform.entity.vo.AuthUserSelectVo;
import com.supermap.gaf.platform.service.AuthUserService;
import com.supermap.gaf.security.RequestUtil;
import com.supermap.gaf.security.SecurityUtilsExt;
import io.swagger.annotations.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;


/**
 * 用户接口
 *
 * @author dqc
 * @date:2021/3/25
 */
@Component
@Api(value = "用户接口")
public class AuthUserResource implements AuthUserClient, AuthUserCurrentClient {

    private final AuthUserService authUserService;

    public AuthUserResource(AuthUserService authUserService) {
        this.authUserService = authUserService;
    }


    @ApiOperation(value = "根据用户id集合查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userIds", value = "用户id集合", paramType = "body", dataTypeClass = List.class)
    })
    @POST
    @Path("/batch")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<List<AuthUser>> listByUserIds(@NotEmpty List<String>  userIds){
        return  MessageResult.data(authUserService.listByUserIds(userIds)).message("查询成功").build();
    }

    @ApiOperation(value = "根据id查询用户", notes = "根据id查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{userId}")
    public MessageResult<AuthUser> getByUserId(@PathParam("userId") String userId) {
        return MessageResult.data(authUserService.getByUserId(userId)).message("查询成功").build();
    }

    @ApiOperation(value = "根据用户名查询用户", notes = "根据用户名查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名称", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/name/{username}")
    public MessageResult<AuthUser> getByUsername(@PathParam("username") String username) {
        return MessageResult.data(authUserService.getByUsername(username)).message("查询成功").build();
    }

    @ApiOperation(value = "根据邮箱查询用户", notes = "根据邮箱查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "用户名称", paramType = "path", dataType = "string", required = true)
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/email/{email}")
    public MessageResult<AuthUser> getByEmail(@PathParam("email") String email) {
        return MessageResult.data(authUserService.getByEmail(email)).message("查询成功").build();
    }

    @ApiOperation(value = "分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1", defaultValue = "1", allowableValues = "range[1,infinity]", paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10", allowableValues = "range(0,infinity]", paramType = "query", dataType = "integer"),
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<Page<AuthUser>> pageList( @ApiParam @Valid @BeanParam AuthUserSelectVo authUserSelectVo,
                                                    @DefaultValue("1") @QueryParam("pageNum") Integer pageNum,
                                                    @DefaultValue("10") @QueryParam("pageSize") Integer pageSize) {
        Page<AuthUser>  page = authUserService.page(authUserSelectVo,pageNum,pageSize);
        return MessageResult.data(page).message("查询成功").build();
    }


    @ApiOperation(value = "新增用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authUser", value = "用户", dataTypeClass = AuthUser.class, paramType = "body", required = true)
    })
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<AuthUser> save(@Valid AuthUser authUser) {
        AuthUser insertedAuthUser = authUserService.save(authUser);
        return MessageResult.data(insertedAuthUser).message("新增操作成功").build();
    }

    @ApiOperation(value = "启用用户", notes = "用户状态有启用和禁用。该接口将用户状态改为启动。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/active/{userId}")
    public MessageResult<AuthUser> active(@NotEmpty @PathParam("userId") String userId) {
        AuthUser user = authUserService.active(userId);
        return MessageResult.data(user).message("新增操作成功").build();
    }

    @ApiOperation(value = "禁用用户", notes = "禁用用户即删除用户，给用户的分配的角色、岗位、挂职都会被清空")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/inactive/{userId}")
    public MessageResult<AuthUser> inactiveAuthUser(@PathParam("userId") String userId) {
        AuthUser authUser = authUserService.inActive(userId);;
        return MessageResult.data(authUser).message("删除操作成功").build();
    }

    @ApiOperation(value = "删除用户", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{userId}")
    public MessageResult<AuthUser> deleteAuthUser(@PathParam("userId") String userId) {
        AuthUser authUser = authUserService.deleteAuthUser(userId);;
        return MessageResult.data(authUser).message("删除操作成功").build();
    }

    @ApiOperation(value = "批量删除用户", notes = "根据id批量删除用户。注意：给用户的分配的角色、岗位、挂职都会被清空")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    public MessageResult<Void> batchDelete(List<String> userIds) {
        authUserService.batchDelete(userIds);
        return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "更新用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authUser", value = "用户", paramType = "body", dataTypeClass = AuthUser.class, required = true),
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @Path("/{userId}")
    public MessageResult<AuthUser> updateAuthUser(@Valid AuthUser authUser, @PathParam("userId") String userId) {
        authUser.setUserId(userId);
        AuthUser updatedAuthUser = authUserService.updateAuthUser(authUser);
        return MessageResult.data(updatedAuthUser).message("更新操作成功").build();
    }

    @ApiOperation(value = "变更密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "passwordVo", value = "新旧密码", dataTypeClass = PasswordVo.class, paramType = "body", required = true)
    })
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/password-change")
    public MessageResult<Void> changePassword(PasswordVo passwordVo) {
        String userId = SecurityUtilsExt.getUserId();
        authUserService.changePassword(userId, passwordVo.getOldPassword(), passwordVo.getNewPassword());
        return MessageResult.successe(Void.class).status(200).message("变更密码成功").build();
    }

    @ApiOperation(value = "重置密码", notes = "根据用户id重置该用户的密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", dataType = "string", required = true)
    })
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{userId}/reset-password")
    public MessageResult<Void> resetPassword(@PathParam("userId") String userId) {
        authUserService.resetPassword(userId);
        return MessageResult.successe(Void.class).status(200).message("重置密码成功").build();
    }

    @ApiOperation(value = "修改邮箱", notes = "修改邮箱")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "emailChangeVo", value = "修改邮箱所需参数", paramType = "path", dataType = "string", required = true)
    })
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/email-change")
    public MessageResult<Void> changeEmail(EmailChangeVo emailChangeVo) {
        String msg = authUserService.changeEmail(emailChangeVo);
        if (StringUtils.isEmpty(msg)) {
            return MessageResult.successe(Void.class).message("修改成功").build();
        } else {
            return MessageResult.failed(Void.class).message(msg).build();
        }
    }


    @ApiOperation(value = "发送校验码", notes = "若不指定邮箱则通过用户的邮箱发送校验码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "邮箱。非必须，默认使用当前用户邮箱", paramType = "query", dataType = "string")
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/checkcode")
    public MessageResult<Void> sendCheckCode(@QueryParam("email") String email) {
        authUserService.sendCheckCode(email);
        return MessageResult.successe(Void.class).status(200).message("发送验证码成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "获取当前用户信息", notes = "获取当前用户信息")
    @Path("/current/info")
    @Override
    public MessageResult<AuthUser> getUserInfo() {
        String userName = RequestUtil.getUserName();
        if (!StringUtils.isEmpty(userName)){
            AuthUser authUser = authUserService.getByUsername(userName);
            authUser.setPassword(null);
            return MessageResult.successe(AuthUser.class).data(authUser).build();
        }
        return MessageResult.failed(AuthUser.class).build();
    }
}
