/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.platform;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;


/**
 * @author wxl
 * @date:2021/3/25
 */
@SpringBootApplication(exclude = {LiquibaseAutoConfiguration.class})
@ComponentScan(basePackages = {"com.supermap.gaf"})
@MapperScan(basePackages = {"com.supermap.gaf.**.dao", "com.supermap.gaf.dao", "com.supermap.gaf.**.mapper"})
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableFeignClients(basePackages = "com.supermap.gaf")
@EnableAsync
public class Application {
    @Bean("balancedRestTemplate")
    @LoadBalanced
    public RestTemplate balancedRestTemplate() {
        return new RestTemplate();
    }
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
