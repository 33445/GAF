package com.supermap.gaf.gateway;

import com.supermap.gaf.extend.spi.ValidateAuthentication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.config.EnableWebFlux;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author wxl
 * @since 2022/4/8
 */
@SpringBootTest(
        classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "spring.main.web-application-type=reactive")
@ActiveProfiles("local")
@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@EnableWebFlux
public class SpiDynamicBeanTest  {

    @Autowired
    ApplicationContext context;

    @Autowired
    private WebTestClient webClient;

    @Test
    public void testSpiDefaultBean() throws MalformedURLException {
        System.out.println("true = " + true);
        String pluginUrlStr = "https://gaf.net.cn/download/gaf/pro/gaf-extend-spi-impl-3.0.0.pro-alpha-SNAPSHOT-jar-with-dependencies.jar";
        URL[] pluginUrl = new URL[]{new URL(pluginUrlStr)};
        URLClassLoader ucl = new URLClassLoader(pluginUrl, Thread.currentThread().getContextClassLoader());

        ServiceLoader<ValidateAuthentication> authentications = ServiceLoader.load(ValidateAuthentication.class,ucl);
        Map<String,Object> map = new HashMap<>();
        for (ValidateAuthentication validateAuthentication : authentications) {
            System.out.println("authentication.geStClass().getName() = " + validateAuthentication.getClass().getName());
            System.out.println("object:"+ validateAuthentication);
            map.putIfAbsent(validateAuthentication.getClass().getName(), validateAuthentication);
        }

        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext)  context;
        //获取BeanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) configurableApplicationContext.getBeanFactory();
        //创建bean信息.
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(map.get("com.supermap.gaf.extend.spi.impl.DefaultAuthenticationImpl").getClass());

        //判断Bean是否已经注册
        String beanName ="com.supermap.gaf.extend.spi.impl.DefaultAuthenticationImpl";
        boolean b = context.containsBean(beanName);
        if (b) {
            System.out.println(String.format("Bean %s 已注册", beanName));
        } else {
            //动态注册bean.
            defaultListableBeanFactory.registerBeanDefinition(beanName, beanDefinitionBuilder.getBeanDefinition());
            //获取动态注册的bean.
            boolean registry = context.containsBean(beanName);
            if (registry) {
                Object bean = context.getBean(beanName);
                System.out.println(String.format("Bean %s 注册成功", beanName));
                ValidateAuthentication validateAuthentication = (ValidateAuthentication) bean;
                validateAuthentication.doValidateAuthentication(null);
                System.out.println("spring bean"+ bean);
            } else {
                System.out.println( "register Bean Error");
            }
        }
        System.out.println("register Bean Success");
    }

    @Test
    public void testSpiBean() {
        System.out.println("true = " + true);
        ServiceLoader<ValidateAuthentication> authentications = ServiceLoader.load(ValidateAuthentication.class);
        Map<String,Object> map = new HashMap<>();
        for (ValidateAuthentication validateAuthentication : authentications) {
            System.out.println("authentication.geStClass().getName() = " + validateAuthentication.getClass().getName());
            System.out.println("object:"+ validateAuthentication);
            map.putIfAbsent(validateAuthentication.getClass().getName(), validateAuthentication);
        }

        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext)  context;
        //获取BeanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) configurableApplicationContext.getBeanFactory();
        //创建bean信息.
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(map.get("com.supermap.gaf.extend.spi.impl.SimpleAuthenticationImpl").getClass());
        //判断Bean是否已经注册
        String beanName ="com.supermap.gaf.extend.spi.impl.SimpleAuthenticationImpl";
        boolean b = context.containsBean(beanName);
        if (b) {
            System.out.println(String.format("Bean %s 已注册", beanName));
        } else {
            //动态注册bean.
            defaultListableBeanFactory.registerBeanDefinition(beanName, beanDefinitionBuilder.getBeanDefinition());
            //获取动态注册的bean.
            boolean registry = context.containsBean(beanName);
            if (registry) {
                Object bean = context.getBean(beanName);
                System.out.println(String.format("Bean %s 注册成功", beanName));
                System.out.println("spring bean"+ bean);
            } else {
                System.out.println( "register Bean Error");
            }
        }
        System.out.println("register Bean Success");
    }
}
