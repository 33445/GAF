/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.gateway.resources;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.gateway.GatewayRouteDefinition;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: gaf-service-manager-modules
 * @description:
 * @author: lidong
 * @date:2021/3/25
 * @create: 2019/07/18
 */
@RestController
@RequestMapping("/routes")
public class RouteMgtResource {

   @Autowired
   private GatewayRouteRouteResource gatewayRouteResource;

    @ApiOperation(value = "查询自定义网关路由", notes = "查询自定义网关路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "routeSearchParam", value = "路由搜索参数json字符串", paramType = "query", dataType = "string")
    })
    @GetMapping
    public MessageResult<List<GatewayRouteDefinition>> queryGatewayRoutes(@RequestParam(name = "routeSearchParam", required = false) String routeSearchParam) {
        return gatewayRouteResource.queryGatewayRoutes(routeSearchParam);
    }

    @ApiOperation(value = "Id为参数查询自定义网关路由", notes = "Id为参数查询自定义网关路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "自定义路由id", paramType = "path", dataType = "string")
    })
    @GetMapping(value = "/{id}", produces = "application/json")
    public MessageResult<GatewayRouteDefinition> queryGatewayRoute(@PathVariable("id") String id) {
        return gatewayRouteResource.queryGatewayRoute(id);
    }

    @ApiOperation(value = "新增自定义网关路由", notes = "添加动态网关路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "route", value = "动态网关路由对象", paramType = "body", dataTypeClass = GatewayRouteDefinition.class)
    })
    @PostMapping(produces = "application/json;charset=UTF-8")
    public MessageResult<String> addRoute(@RequestBody GatewayRouteDefinition route) {
        return gatewayRouteResource.addRoute(route);
    }

    @ApiOperation(value = "批量删除路由", notes = "传入json字符串参数，批量删除路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idsJsonStr", value = "json字符串参数", paramType = "body")
    })
    @DeleteMapping(produces = "application/json")
    public MessageResult<String> batchDeleteRoute(@RequestBody String idsJsonStr) {
        return gatewayRouteResource.batchDeleteRoute(idsJsonStr);
    }

    @ApiOperation(value = "ID删除路由", notes = "传入ID参数，删除路由")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "路由id", paramType = "path", dataType = "string")
    })
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public MessageResult<String> deleteRoute(@PathVariable("id") String id) {
     return gatewayRouteResource.deleteRoute(id);
    }

    @ApiOperation(value = "编辑路由", notes = "编辑路由信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "route", value = "路由对象", paramType = "body", dataTypeClass = GatewayRouteDefinition.class)
    })
    @PutMapping(produces = "application/json")
    public MessageResult<String> updateRoute(@RequestBody GatewayRouteDefinition route) {
        return gatewayRouteResource.updateRoute(route);
    }

}
