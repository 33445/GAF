/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.work;

import com.supermap.desktop.develop.entity.PresignUploadRequest;
import com.supermap.desktop.develop.exception.FileUploadException;
import com.supermap.desktop.develop.utils.CommonUtils;

import javax.swing.*;

/**
 * @author heykb
 * @date:2021/3/25
 */
public class FileUploadWork extends SwingWorker<Object, Object> {
    private PresignUploadRequest uploadRequest;

    public FileUploadWork(PresignUploadRequest uploadRequest) {
        this.uploadRequest = uploadRequest;
        uploadRequest.setProgressListener(progress->{
            setProgress(progress);
            return null;
        });
    }

    @Override
    protected Object doInBackground() throws FileUploadException {
        CommonUtils.uploadByPreSignedUrl(uploadRequest);
        return null;
    }
}
