package com.supermap.desktop.develop.histroy;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class DownloadHistory implements UniqueHistoryNode{
    private String sourceId;
    private String path;
    private String server;
    public DownloadHistory(String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        this.setSourceId(jsonObject.getString("sourceId"));
        this.setPath(jsonObject.getString("path"));
        this.setServer(jsonObject.getString("server"));
    }

    public DownloadHistory(String server, String sourceId, String path) {
        this.sourceId = sourceId;
        this.path = path;
        this.server = server;
    }

    public String toFileString(){
        return JSON.toJSONString(this);
    }
    public String getSourceId() {
        return sourceId;
    }

    @Override
    public String toString() {
        return toFileString();
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean isConflict(UniqueHistoryNode another) {

        DownloadHistory ano = (DownloadHistory)another;
        return sourceId.equals(ano.getSourceId()) && server.equals(ano.getServer());
//        return this.getPath().equals(ano.getPath());
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
}
