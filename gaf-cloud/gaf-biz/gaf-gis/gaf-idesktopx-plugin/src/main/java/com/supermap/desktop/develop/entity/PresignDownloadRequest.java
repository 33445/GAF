package com.supermap.desktop.develop.entity;

import java.nio.file.Path;
import java.util.function.Function;

/**
 * @author heykb
 */
public class PresignDownloadRequest {
    private String presignUrl;
    private Path path;
    private boolean async;
    private Function<Integer,Void> progressListener;

    public PresignDownloadRequest(String presignUrl, Path path, boolean async) {
        this.presignUrl = presignUrl;
        this.path = path;
        this.async = async;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public Function<Integer, Void> getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(Function<Integer, Void> progressListener) {
        this.progressListener = progressListener;
    }

    
    public String getPresignUrl() {
        return presignUrl;
    }

    public void setPresignUrl(String presignUrl) {
        this.presignUrl = presignUrl;
    }
}
