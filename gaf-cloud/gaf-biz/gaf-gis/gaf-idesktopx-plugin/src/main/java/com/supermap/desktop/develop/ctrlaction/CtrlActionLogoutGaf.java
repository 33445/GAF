/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ctrlaction;

import com.supermap.desktop.controls.ui.controls.DockbarManager;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.Interface.IDockbar;
import com.supermap.desktop.core.implement.CtrlAction;
import com.supermap.desktop.develop.ui.*;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;

/**
 * @author SuperMap
 * @date:2021/3/25
 */
public class CtrlActionLogoutGaf extends CtrlAction {
    public static boolean enable = false;

    public CtrlActionLogoutGaf(IBaseItem caller) {
        super(caller);
    }

    @Override
    public boolean enable() {
        return enable;
    }

    @Override
    public void run() {
        if (enable()) {
            CommonUtils.checkGafStatus(false);
            DockbarManager dockbarManager = ApplicationContextUtils.getDockbarManager();

            // 删除树数据
            IDockbar datasource = dockbarManager.get(GafDatasourceManager.class);
            datasource.setVisible(false);
            ((GafDatasourceManager) datasource.getInnerComponent()).updateTree(new GAFDatasourceTreeNew());
            // 删除树数据
            IDockbar workspace = dockbarManager.get(GafWorkspaceManager.class);
            workspace.setVisible(false);
            ((GafWorkspaceManager) workspace.getInnerComponent()).updateTree(new GAFWorkspaceTree());

            // 删除树数据
            IDockbar tileManager = dockbarManager.get(GafTileManager.class);
            tileManager.setVisible(false);
            ((GafTileManager) tileManager.getInnerComponent()).updateTree(new GAFTileTree());
        }
    }
}
