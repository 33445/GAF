package com.supermap.desktop.develop.ui;

import com.supermap.desktop.core.enums.WindowType;
import com.supermap.desktop.core.event.FormClosedEvent;
import com.supermap.desktop.core.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.develop.GAFProperties;
import com.supermap.desktop.mapview.FormMap;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.mapview.map.propertycontrols.GAFControlMapMongoDBCacheManage;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;

public class GAFFormCacheManager extends FormMap {
    private JPanel panelLeft;
    private JPanel panelRight;
    private GAFControlMapMongoDBCacheManage controlMapMongoDBCacheManage;
    private JSplitPane splitPane;

    public GAFControlMapMongoDBCacheManage getControlMapMongoDBCacheManage() {
        return controlMapMongoDBCacheManage;
    }

    public GAFFormCacheManager() {
        super(MapViewProperties.getString("String_MapCacheManager"), (Icon)null);
    }


    public void initializeLayout() {
        this.panelLeft = new JPanel();
        this.panelRight = new JPanel();
        this.controlMapMongoDBCacheManage = new GAFControlMapMongoDBCacheManage();
        this.controlMapMongoDBCacheManage.setMapControl(this.mapControl);
        this.splitPane = new JSplitPane(1, this.childWindow, this.panelRight);
        this.splitPane.setDividerLocation(1000);
        this.splitPane.setContinuousLayout(true);
        this.splitPane.setBorder((Border)null);
        this.splitPane.setResizeWeight(1.0D);
        this.panelRight.setLayout(new GridBagLayout());
        this.panelRight.add(this.controlMapMongoDBCacheManage, (new GridBagConstraintsHelper(0, 0, 1, 1)).setWeight(1.0D, 1.0D).setFill(1));
        this.add(this.splitPane, "Center");
        setTitle(GAFProperties.getString("String_Alias")+"-"+getTitle());

    }

    public WindowType getWindowType() {
        return WindowType.CACHE_MANAGER;
    }


    @Override
    public void formClosed(FormClosedEvent e) {
        super.formClosed(e);
        GAFControlMapMongoDBCacheManage.CURRENT_LIST_CACHE = new ArrayList<>();
        GafTileManager.gafTilesManagerTree.reDrawTree(null);

    }
}