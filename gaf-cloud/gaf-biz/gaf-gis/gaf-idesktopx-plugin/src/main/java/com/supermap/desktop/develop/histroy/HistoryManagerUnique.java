package com.supermap.desktop.develop.histroy;

import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.utilties.StringUtilities;

public class HistoryManagerUnique<T extends UniqueHistoryNode> extends HistoryManager<T> {
    public HistoryManagerUnique() {
    }

    public HistoryManagerUnique(String filePath, Class<T> eClass) {
        super(filePath, eClass);
    }


    protected boolean append(T history) {
        if (StringUtilities.isNullOrEmptyString(history)) {
            return false;
        } else {
            boolean isAppend = true;
            for(UniqueHistoryNode item:this.commandList){
                if(item.isConflict(history)){
                    this.commandList.remove(item);
                    isAppend = false;
                    break;
                }
            }
            this.commandList.add(history);
            return isAppend;
        }
    }

    protected boolean remove(T history) {
        return this.commandList.remove(history);
    }
}
