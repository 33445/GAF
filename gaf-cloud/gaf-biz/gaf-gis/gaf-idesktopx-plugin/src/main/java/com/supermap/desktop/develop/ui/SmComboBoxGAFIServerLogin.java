//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
package com.supermap.desktop.develop.ui;

import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;

public class SmComboBoxGAFIServerLogin{
    private IServerLoginInfo iServerLoginInfo;
    public SmComboBoxGAFIServerLogin(IServerLoginInfo iServerLoginInfo) {
        this.iServerLoginInfo = iServerLoginInfo;
    }

    public IServerLoginInfo getIServerLoginInfo() {
        return iServerLoginInfo;
    }

    public void setIServerLoginInfo(IServerLoginInfo iServerLoginInfo) {
        this.iServerLoginInfo = iServerLoginInfo;
    }
}

