package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.EngineType;
import com.supermap.desktop.core.implement.SmPopupMenu;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.develop.entity.GAFTileStorageConnection;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.develop.work.FileDownloadWork;
import com.supermap.desktop.mapview.MapViewResources;
import com.supermap.desktop.mapview.map.propertycontrols.GAFControlMapMongoDBCacheManage;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageType;
import org.apache.arrow.vector.util.CallBack;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class GAFTileTree extends AbstratGafTree {

    public GAFTileTree() {
    }

    public GAFTileTree(HistoryManager historyManager, JSONArray list) {
        super(historyManager, list);
    }

    @Override
    public List getCurrentOpened() {
        return GAFControlMapMongoDBCacheManage.CURRENT_LIST_CACHE;
    }

    @Override
    protected void buildTree(DefaultMutableTreeNode root, JSONArray list) {
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                JSONObject item = list.getJSONObject(i);
                JSONObject sourceInfo = item.getJSONObject("sourceInfo");
                GAFTileStorageConnection connectionInfo = new GAFTileStorageConnection();
                connectionInfo.setSourceType(item.getString("sourceType"));
                connectionInfo.setId(item.getString("tileId"));
                connectionInfo.setAlias(item.getString("tileAlias"));
                connectionInfo.setServer(sourceInfo.getString("addr"));
                connectionInfo.setUser(sourceInfo.getString("userName"));
                connectionInfo.setPassword(sourceInfo.getString("password"));
                connectionInfo.setDatabase(sourceInfo.getString("dbName"));
                connectionInfo.setName(item.getString("tileName"));
                if (CommonUtils.isFileTypeSource(connectionInfo)) {
                    connectionInfo.setServer(sourceInfo.getString("path"));
                }else{
                    connectionInfo.setStorageType(TileStorageType.MONGO);
                }
                root.add(new DefaultMutableTreeNode(connectionInfo));
            }
        }
    }

    @Override
    void doubleClickHandler(DefaultMutableTreeNode node, CallBack cb) {
        GAFTileStorageConnection conn = (GAFTileStorageConnection) node.getUserObject();
        preOpAndCheck(conn,false, connection->{
            if (CommonUtils.isFileTypeSource(conn)) {
                // 记录下载记录
                conn.setLocalPath(connection.getServer());
                CommonUtils.appendDownloadHistory(CommonUtils.TILE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                updateNode(node);
            }
            CommonUtils.openTile(connection);
            if (cb != null) cb.doWork();
            return null;

        });
    }

    public void preOpAndCheck(GAFTileStorageConnection connectionInfo, boolean reDownload ,Function<TileStorageConnection, Object> continueOp) {
        try {
            TileStorageConnection canOpenConn = CommonUtils.copyTileStorageConnection(connectionInfo);
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                String localPath = CommonUtils.getDownloadedPath(CommonUtils.TILE_DOWNLOAD_HISTORY, connectionInfo.getId());
                if (localPath != null && !reDownload) {
                    // 有下载记录直接打开
                    if (!Files.exists(Paths.get(localPath))) {
                        JOptionPaneUtilities.showErrorMessageDialog(String.format("%s不存在", localPath));
                        return;
                    }
                    canOpenConn.setServer(localPath);
                    if(continueOp!=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
                Optional<String> fileName = CommonUtils.getFileName(connectionInfo);
                if (!fileName.isPresent()) {
                    JOptionPaneUtilities.showErrorMessageDialog("无法下载该文件");
                    return;
                }
                if (CommonUtils.tipIfNotExist("/"+connectionInfo.getServer())) {
                    return;
                }
                String dirName = Paths.get(connectionInfo.getServer()).getParent().getFileName().toString();
                int re = CommonUtils.dirChoose(smFileChoose, dirName + "/" + fileName.get());
                if (re == -1) {
                    return;
                } else if (re == 0) {
                    Path localDir = Paths.get(smFileChoose.getFilePath() + "/" + dirName);

                    // todo: 执行下载
                    String serverDir = connectionInfo.getServer().replace(fileName.get(),"");
                    CommonUtils.downloadAsync(serverDir, localDir, () -> {
                                ApplicationContextUtils.getOutput().output("全部下载结束");
                                canOpenConn.setServer(localDir.resolve(fileName.get()).toString());
                                if(continueOp!=null){
                                    continueOp.apply(canOpenConn);
                                }
                            },
                            evt -> {
                                FileDownloadWork downloadWork = (FileDownloadWork) evt.getSource();
                                String file = downloadWork.getServerPath();
                                Integer progress = (Integer) evt.getNewValue();
                                if (progress % 10 == 0) {
                                    ApplicationContextUtils.getOutput().output("已下载" + file + ":" + progress + "%");
                                }
                                if (progress == 100) {
                                    ApplicationContextUtils.getOutput().output(file + "下载成功");
                                }
                                return null;
                            });
                    return;
                } else {
                    Path downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + dirName);
                    canOpenConn.setServer(downloadPath.resolve(fileName.get()).toString());
                    if(continueOp!=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
            } else {
                if(continueOp!=null){
                    continueOp.apply(canOpenConn);
                }
            }
        } catch (FileDownloadException e) {
            ApplicationContextUtils.getOutput().output(e.getMessage() + "下载失败");
        } catch (Exception e) {
            ApplicationContextUtils.getOutput().output(e.getMessage());
        }
    }


    @Override
    protected boolean faceLeafAndCheckOpened(TreeCellRender render, List opened, JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) value;
        GAFTileStorageConnection conn = (GAFTileStorageConnection) defaultMutableTreeNode.getUserObject();
        if (CommonUtils.isFileTypeSource(conn)) {
            Optional<String> fileName = CommonUtils.getFileName(conn);
            if (fileName.isPresent()) {
                render.setText(String.format("%s（%s）", fileName.get(),conn.getAlias()));
            } else {
                render.setText(String.format("❌%s（%s）", "格式错误"));
            }
            render.setIcon(MapViewResources.getIcon("/mapviewresources/Image_SingleVersion.png"));
        } else {
            render.setText(String.format("%s（%s）", conn.getName(),conn.getAlias()));
            render.setIcon(CommonUtils.getICon(EngineType.MONGODB));
        }

        if (opened == null) {
            return false;
        }
        for (Object item : opened) {
            TileStorageConnection connection = (TileStorageConnection) item;
            if (CommonUtils.sameTile(conn, connection)) {
                return true;
            }
        }
        return false;
    }

    @Override
    SmPopupMenu buildLeafPopMenu(DefaultMutableTreeNode treeNode) {
        return ApplicationContextUtils.getContextMenuManager().get("SuperMap.Desktop.UI.GafTileManager.ContextMenuTile");
    }

    @Override
    SmPopupMenu buildCatalogPopMenu(DefaultMutableTreeNode treeNode) {
        return null;
    }
}
