/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.exception.FileUploadException;
import com.supermap.desktop.develop.ui.DialogLoginGaf;
import com.supermap.desktop.develop.ui.GAFDatasourceTreeNew;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.develop.utils.HttpUtils;
import monocle.std.list;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import javax.security.sasl.AuthenticationException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author heykb
 * @date:2021/3/25
 */
public class GafClient {
    private static final String TOKEN_ENDPOINT = "%s/authentication/token";
    private static final String REFRESH_TOKEN_ENDPOINT = "%s/authentication/token/refresh";
    private static final String WORKSPACE_ENDPOINT = "%s/data-mgt/data-workspaces";
    private static final String I_SERVER_SETTING_ENDPOINT = "%s/data-mgt/iserver/setting";
    private static final String DATASOURCE_ENDPOINT = "%s/data-mgt/sys-resource-datasources";
    private static final String FILE_UPLOAD_ENDPOINT = "%s/storage/api/tenant-created-first/default/upload%s";
    private static final String FILE_DOWNLOAD_ENDPOINT = "%s/storage/api/tenant-created-first/default/download%s";
    private static final String FILE_METADATA_ENDPOINT = "%s/storage/api/tenant-created-first/default/metadata%s";
    private static final String FILE_LIST_OBJECTS_ENDPOINT = "%s/storage/api/tenant-created-first/default/list-objects/%s";
    private static final String SERVICE_ENDPOINT = "%s/map/webgis-services";
    private static final String TILE_ENDPOINT = "%s/data-mgt/tiles";
    private static final String SERVICE_RESOURCE_ENDPOINT = "%s/map/service-sources";
    private String username;
    private String password;
    private String host;
    private String token;
    private String refreshToken;
    private static GafClient INSTANCE;

    private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();


    public String getEndpoint(){
        try {
            URL server = new URL(this.host);
            return server.getHost()+":"+server.getPath();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

    }

    public static GafClient instance() {
        if (INSTANCE == null) {
//            JOptionPaneUtilities.showErrorMessageDialog("请登录");
            new DialogLoginGaf().showDialog();
        }
        return INSTANCE;
    }

    public static void logout() {
        if (INSTANCE != null) {
            INSTANCE = null;
        }
    }

    public GafClient(String username, String password, String host) {
        this.username = username;
        this.password = password;
        this.host = host;
        try {
            JSONObject re = getToken(username, password, host);
            this.token = re.getString("access_token");
            this.refreshToken = re.getString("refresh_token");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("登录失败：" + e.getMessage());
        }
        INSTANCE = this;
    }

    public JSONObject getToken(String username, String password, String host) throws Exception {
        String url = String.format(TOKEN_ENDPOINT, host);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("username", username);
        urlParams.put("password", password);
        JSONObject re = (JSONObject) HttpUtils.doPostJson(url, null, urlParams, JSONObject.class);
        checkResult(re,true);
        JSONObject data = re.getJSONObject("data");
        return data;
    }

    public JSONObject refreshToken(String refreshToken, String host) throws Exception {
//        String url = String.format(REFRESH_TOKEN_ENDPOINT, host);
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        NameValuePair pair = new BasicNameValuePair("refresh_token", refreshToken);
//        params.add(pair);
//        JSONObject re = (JSONObject) HttpUtils.doPostJson(url, params, JSONObject.class);
//        checkResult(re,true,"刷新token");
//        ApplicationContextUtils.getOutput().output("刷新token");
//        return re.getJSONObject("data");
        return getToken(this.username,this.password,host);
    }

    public JSONArray workspaceList() throws Exception {
        String url = String.format(WORKSPACE_ENDPOINT, host);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("pageNum", "0");
        urlParams.put("pageSize", "0");
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, urlParams, headers, JSONObject.class);
        });
//        JSONObject re = (JSONObject) HttpUtils.doGet(url,urlParams,headers,JSONObject.class);
        checkResult(re,true,"查询工作空间");
        return re.getJSONObject("data").getJSONArray("pageList");
    }
    public JSONObject iserverSetting() throws Exception {
        String url = String.format(I_SERVER_SETTING_ENDPOINT, host);
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, null, headers, JSONObject.class);
        });
//        JSONObject re = (JSONObject) HttpUtils.doGet(url,urlParams,headers,JSONObject.class);
        checkResult(re,true,"获取iserver配置",false);
        return re.getJSONObject("data");
    }



    public JSONArray tileList() throws Exception {
        String url = String.format(TILE_ENDPOINT, host);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("pageNum", "0");
        urlParams.put("pageSize", "0");
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, urlParams, headers, JSONObject.class);
        });
//        JSONObject re = (JSONObject) HttpUtils.doGet(url,urlParams,headers,JSONObject.class);
        checkResult(re,true,"查询瓦片");
        return re.getJSONObject("data").getJSONArray("pageList");
        /*String s = "[{\n" +
                "    \"sourceType\":\"mongodb\",\n" +
                "    \"tileId\":\"123456\",\n" +
                "    \"tileName\":\"CJDCQ_chongqing223\",\n" +
                "    \"tileAlias\":\"XZQmytmp\",\n" +
                "    \"sourceInfo\":{\n" +
                "        \"addr\":\"192.168.192.100:30061\",\n" +
                "        \"userName\":\"testadmin\",\n" +
                "        \"password\":\"123456\",\n" +
                "        \"dbName\":\"test\"\n" +
                "    }\n" +
                "},\n" +
                "{\n" +
                "    \"sourceType\":\"file\",\n" +
                "    \"tileId\":\"2222\",\n" +
                "    \"tileName\":\"CJDCQ_chongqing22\",\n" +
                "    \"tileAlias\":\"filtest\",\n" +
                "    \"sourceInfo\":{\n" +
                "        \"path\":\"tiles-manage/XZQ_192.168.11.118_32224_space_tile/XZQ_192.168.11.118_32224_space_tile/XZQ_192.168.11.118_32224_space_tile.sci\"\n" +
                "    }\n" +
                "}]";
        return JSON.parseArray(s);*/
    }

    public JSONArray datasourceTree() throws Exception {
        String url = String.format(DATASOURCE_ENDPOINT + "/tree", host);
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, null, headers, JSONObject.class);
        });
        checkResult(re,"查询数据源");
        return re.getJSONArray("data");
    }
    public JSONArray datasources() throws Exception{
        String url = String.format(DATASOURCE_ENDPOINT, host);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("pageNum", "0");
        urlParams.put("pageSize", "0");
        urlParams.put("isSdx", "true");
        urlParams.put("isTemplate", "false");
        urlParams.put("isStandard", "false");
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, urlParams, headers, JSONObject.class);
        });
        checkResult(re,true,"查询数据源");
        JSONArray datasources = re.getJSONObject("data").getJSONArray("content");
        urlParams.put("isStandard", "true");
        re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, urlParams, headers, JSONObject.class);
        });
        checkResult(re,true,"查询数据源");
        JSONArray standardDatasources = re.getJSONObject("data").getJSONArray("content");
        if(standardDatasources!=null){
            datasources.addAll(re.getJSONObject("data").getJSONArray("content"));
        }
        return datasources;
    }

    public String registryWorkspace(WorkspaceConnectionInfo connectionInfo, String serverDir) throws Exception {
        String server = connectionInfo.getServer();
        if (CommonUtils.isFileTypeSource(connectionInfo)) {
            server = serverDir + Paths.get(server).getFileName().toString();
        }
        String url = String.format(WORKSPACE_ENDPOINT, host);
        Map<String, String> body = new HashMap<>();
        body.put("wsName", connectionInfo.getName());
        body.put("typeCode", connectionInfo.getType().name());
        body.put("server", server);
        body.put("database", connectionInfo.getDatabase());
        body.put("userName", connectionInfo.getUser());
        body.put("password", connectionInfo.getPassword());
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doPostJson(url, JSON.toJSONString(body), null, headers, JSONObject.class);
        });
        checkResult(re,true,"注册工作空间");
        return re.getJSONObject("data").getString("workspaceId");
    }

    public String registryDatasource(DatasourceConnectionInfo connectionInfo, String catalogCode, String path) throws Exception {
        String server = connectionInfo.getServer();
        if (CommonUtils.isFileTypeSource(connectionInfo)) {
            server = path + Paths.get(server).getFileName().toString();
        }
        String url = String.format(DATASOURCE_ENDPOINT, host);
        Map<String, Object> body = new HashMap<>();
        body.put("addr", server);
        body.put("typeCode", connectionInfo.getEngineType().name());
        body.put("dbName", connectionInfo.getDatabase());
        body.put("dsName", connectionInfo.getAlias());
        body.put("userName", connectionInfo.getUser());
        body.put("password", connectionInfo.getPassword());
//        body.put("catalogCode", catalogCode);
        body.put("isSdx", true);
        body.put("isStandard", GAFDatasourceTreeNew.isStandard.equals(catalogCode));
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doPostJson(url, JSON.toJSONString(body), null, headers, JSONObject.class);
        });
        checkResult(re,true,"注册数据源");
        return re.getJSONObject("data").getString("datasourceId");
    }

    public String batchRegistryService(String address, String typeCode, String name,String sourceId,int sourceType) throws Exception {
        String url = String.format(SERVICE_ENDPOINT, host);
        Map<String, Object> body = new HashMap<>();
        body.put("address", address);
        body.put("typeCode", typeCode);
        body.put("name", name);
        body.put("description", "来自桌面插件");
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("registryType", "batch");
        urlParams.put("sourceId", sourceId);
        urlParams.put("sourceType", sourceType+"");
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doPostJson(url, JSON.toJSONString(body), urlParams, headers, JSONObject.class);
        });
        checkResult(re,true,"注册服务");
        return re.getString("data");
    }


    public void addServiceResource(String serviceId,String sourceId,int sourceType) throws Exception {
        String url = String.format(SERVICE_RESOURCE_ENDPOINT, host);
        Map<String, Object> body = new HashMap<>();
        body.put("serviceId", serviceId);
        body.put("sourceId", sourceId);
        body.put("sourceType", sourceType);
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doPostJson(url, JSON.toJSONString(body), null, headers, JSONObject.class);
        });
        checkResult(re,false,"自动添加关联关系");
    }

    public String uploadPresignUrl(String keyName, String contentMd5) throws FileUploadException {
        try {
            String url = String.format(FILE_UPLOAD_ENDPOINT, host, keyName);
            JSONObject re = (JSONObject) apiWork(token -> {
                Map<String, String> headers = new HashMap<>();
                if (!StringUtils.isEmpty(contentMd5)) {
                    headers.put("contentMd5", contentMd5);
                }
                headers.put("Authorization", "Bearer " + token);
                return HttpUtils.doPutJson(url, null, null, headers, JSONObject.class);
            });
            checkResult(re,true,"获取文件上传签名地址");
            return re.getString("data");
        } catch (Exception e) {
            throw new FileUploadException(Paths.get(keyName).getFileName().toString());
        }
    }

    public String uploadPresignUrl(String keyName) throws FileUploadException {
        return uploadPresignUrl(keyName, null);
    }

    public String downloadPresignUrl(String keyName) throws FileDownloadException {
        try {
            String url = String.format(FILE_DOWNLOAD_ENDPOINT, host, keyName);

            JSONObject re = (JSONObject) apiWork(token -> {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return HttpUtils.doGet(url, null, headers, JSONObject.class);
            });
            //        JSONObject re = (JSONObject) HttpUtils.doGet(url,null,headers,JSONObject.class);
            checkResult(re,true,"获取文件下载签名地址");
            return re.getString("data");
        } catch (Exception e) {
            throw new FileDownloadException(Paths.get(keyName).getFileName().toString());
        }
    }

    public JSONObject objectMetadata(String keyName) throws Exception {
        String url = String.format(FILE_METADATA_ENDPOINT, host, keyName);
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            return HttpUtils.doGet(url, null, headers, JSONObject.class);
        });
        return re;

    }
    public JSONArray listObjects(String dir) throws Exception {
        String url = String.format(FILE_LIST_OBJECTS_ENDPOINT, host, dir);
        JSONObject re = (JSONObject) apiWork(token -> {
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token);
            Map<String, String> urlParams = new HashMap<>();
            urlParams.put("recursion", "true");
            return HttpUtils.doGet(url, urlParams, headers, JSONObject.class);
        });
        checkResult(re,true);
        return re.getJSONArray("data");
    }
    private Object apiWork(ApiWork work) throws Exception {
        Object re = null;
        readWriteLock.readLock().lock();
        readWriteLock.readLock().unlock();
        try {
            re = work.run(this.token);
        } catch (AuthenticationException e) {
            if(readWriteLock.writeLock().tryLock()){
                try{
                    if (StringUtils.isEmpty(this.refreshToken)) {
                        JSONObject result = getToken(this.username, this.password, this.host);
                        this.token = result.getString("access_token");
                        this.refreshToken = result.getString("refresh_token");
                    } else {
                        JSONObject result = refreshToken(this.refreshToken, this.host);
                        this.token = result.getString("access_token");
                        this.refreshToken = result.getString("refresh_token");
                    }
                }catch (AuthenticationException ex){
                    INSTANCE = null;
                    throw ex;
                }finally {
                    readWriteLock.writeLock().unlock();
                }
            }
            readWriteLock.readLock().lock();
            readWriteLock.readLock().unlock();
            re = work.run(this.token);
        }catch (Exception e){
            ApplicationContextUtils.getOutput().output(e.getMessage());
            throw new RuntimeException(e);
        }
        return re;
    }


    private void checkResult(JSONObject re) {
        checkResult(re,false);
    }
    private void checkResult(JSONObject re,String opName) {
        checkResult(re,false,opName,true);
    }
    private void checkResult(JSONObject re,boolean requiredData){
        checkResult(re,requiredData,"请求",true);
    }
    private void checkResult(JSONObject re,boolean requiredData,String opName){
        checkResult(re,requiredData,opName,true);
    }
    private void checkResult(JSONObject re,boolean requiredData,String opName,boolean outLog){

        if (!re.getBoolean("successed")) {
            if(outLog){
                ApplicationContextUtils.getOutput().output(JSON.toJSONString(re));
            }
            throw new RuntimeException(opName+"失败：" + re.getString("message"));
        }else if((requiredData && re.get("data")==null)){
            if(outLog){
                ApplicationContextUtils.getOutput().output(JSON.toJSONString(re));
            }
            throw new RuntimeException(opName+"要求响应必须包含data");
        }
    }



    static interface ApiWork {
        Object run(String token) throws Exception;
    }
}
