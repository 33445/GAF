/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.work;

import com.supermap.desktop.develop.client.GafClient;
import com.supermap.desktop.develop.entity.PresignDownloadRequest;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.utils.CommonUtils;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.nio.file.Path;

/**
 * @author heykb
 * @date:2021/3/25
 */
public class FileDownloadWork extends SwingWorker<Object, Object> {
    private String serverPath;
    private Path localPath;

    public FileDownloadWork(String serverPath, Path path) {
        this.serverPath = serverPath;
        this.localPath = path;
    }



    @Override
    protected Object doInBackground() throws FileDownloadException, FileNotFoundException {

        String preSignedUrl = GafClient.instance().downloadPresignUrl(serverPath);
        PresignDownloadRequest downloadRequest = new PresignDownloadRequest(preSignedUrl,localPath,false);
        downloadRequest.setProgressListener(progress->{
            setProgress(progress);
            return null;
        });
        CommonUtils.downloadByPreSignedUrl(downloadRequest);
        return null;
    }

    public String getServerPath() {
        return serverPath;
    }
}
