package com.supermap.desktop.develop.entity;

import com.supermap.tilestorage.TileStorageConnection;

public class GAFTileStorageConnection extends TileStorageConnection implements GAFSource {
    private String id;

    private String localPath;

    private String sourceType;

    private String alias;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
