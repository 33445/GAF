/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ui;


import com.supermap.desktop.controls.ui.controls.button.SmButton;
import com.supermap.desktop.core.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.dataview.DataViewResources;
import com.supermap.desktop.develop.utils.CommonUtils;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.BOTH;

/**
 * @author heykb
 * @date:2021/3/25
 */
public class GafTileManager extends JPanel {
    public static GAFTileTree gafTilesManagerTree;
    private JScrollPane jScrollPane;
    private SmButton refreshButton;

    //    AttributeMan
    public void updateTree(GAFTileTree tree) {
        remove(jScrollPane);
        remove(refreshButton);
        repaint();
        initComponent(tree);
        revalidate();

    }

    void initComponent(GAFTileTree tree) {
        gafTilesManagerTree = tree;
        this.jScrollPane = new JScrollPane(tree);
        this.setLayout(new GridBagLayout());
        this.refreshButton = new SmButton();
        this.refreshButton.setIcon(DataViewResources.getIcon("/dataviewresources/Catalog/Image_Refresh.png"));
        this.add(this.refreshButton, (new GridBagConstraintsHelper(0, 1)).setFill(BOTH));
        this.add(jScrollPane, new GridBagConstraintsHelper(0, 0).setWeight(1, 1).setFill(BOTH));
        this.refreshButton.addActionListener(e -> {
            try {
                CommonUtils.refreshTileTree();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });
    }

    public GafTileManager() {
        initComponent(new GAFTileTree());
    }


}
