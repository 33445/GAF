/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.ctrlaction;

import com.supermap.ProductType;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.implement.CtrlAction;
import com.supermap.desktop.core.license.LicenseManager;
import com.supermap.desktop.develop.GAFProperties;
import com.supermap.desktop.develop.entity.GAFTileStorageConnection;
import com.supermap.desktop.develop.ui.DialogGAFReleaseLocalCache;
import com.supermap.desktop.develop.ui.DialogGAFReleaseMongoCache;
import com.supermap.desktop.develop.utils.CommonUtils;
import com.supermap.desktop.mapview.map.propertycontrols.GAFControlMapMongoDBCacheManage;
import com.supermap.tilestorage.TileStorageConnection;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;

/**
 * @author SuperMap
 * @date:2021/3/25
 */
public class CtrlActionReleaseTile extends CtrlAction implements GAFTreeNodeCtrlAction {
    private DefaultMutableTreeNode node;

    @Override
    public void setNode(DefaultMutableTreeNode node) {
        this.node = node;
    }

    public CtrlActionReleaseTile(IBaseItem caller) {
        super(caller);
    }

    @Override
    public void run() {
        try {
            GAFTileStorageConnection connection = (GAFTileStorageConnection) node.getUserObject();
            if(CommonUtils.isFileTypeSource(connection)){
                new DialogGAFReleaseLocalCache(connection).showDialog();
            }else{
                (new DialogGAFReleaseMongoCache(connection, GAFProperties.getString("String_Alias"))).showDialog();
            }
        } catch (Exception var2) {
            Application.getActiveApplication().getOutput().output(var2);
        }

    }

    public boolean enable() {
        GAFTileStorageConnection conn = (GAFTileStorageConnection) node.getUserObject();
        boolean re = false;
        List opened = GAFControlMapMongoDBCacheManage.CURRENT_LIST_CACHE;
        for(Object item:opened){
            TileStorageConnection connection = (TileStorageConnection) item;
            if(CommonUtils.sameTile(conn,connection)){
                re = true;
                break;
            }
        }
        return re && (LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_ENTERPRISE) || LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_STANDARD) && LicenseManager.INSTANCE.isGranted(ProductType.IDESKTOPJAVA_ONLINESHARE));
    }

}
