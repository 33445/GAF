package com.supermap.desktop.develop.ui;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.WorkspaceType;
import com.supermap.desktop.core.implement.SmPopupMenu;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.utilties.JOptionPaneUtilities;
import com.supermap.desktop.core.utilties.WorkspaceUtilities;
import com.supermap.desktop.develop.entity.GAFWorkspaceConnectionInfo;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.utils.ApplicationContextUtils;
import com.supermap.desktop.develop.utils.CommonUtils;
import org.apache.arrow.vector.util.CallBack;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class GAFWorkspaceTree extends AbstratGafTree{

    public GAFWorkspaceTree() {
    }

    public GAFWorkspaceTree(HistoryManager historyManager, JSONArray list) {
        super(historyManager, list);
    }

    @Override
    public List getCurrentOpened() {
       return Arrays.asList(CommonUtils.copyWorkspaceConn(ApplicationContextUtils.getWorkspace().getConnectionInfo()));
    }

    @Override
    protected void buildTree(DefaultMutableTreeNode root, JSONArray list) {
        DefaultMutableTreeNode ConnTypeNode = new DefaultMutableTreeNode("数据连接型");
        DefaultMutableTreeNode fileTypeNode = new DefaultMutableTreeNode("文件型");
        root.add(ConnTypeNode);
        root.add(fileTypeNode);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                JSONObject workspaceConn = list.getJSONObject(i);
                GAFWorkspaceConnectionInfo connectionInfo = new GAFWorkspaceConnectionInfo();
                connectionInfo.setId(workspaceConn.getString("workspaceId"));
                connectionInfo.setServer(workspaceConn.getString("server"));
                connectionInfo.setUser(workspaceConn.getString("userName"));
                connectionInfo.setPassword(workspaceConn.getString("password"));
                connectionInfo.setDatabase(workspaceConn.getString("database"));
                connectionInfo.setName(workspaceConn.getString("wsName"));
                try {
                    connectionInfo.setType((WorkspaceType) WorkspaceType.parse(WorkspaceType.class, workspaceConn.getString("typeCode")));
                } catch (RuntimeException e) {
                    connectionInfo.setName("❌" + connectionInfo.getName());
                    ApplicationContextUtils.getOutput().output(String.format("%s工作空间连接信息解析失败，不存在类型%s", workspaceConn.getString("wsName"), workspaceConn.getString("typeCode")));
                }
                if (WorkspaceType.SQL.equals(connectionInfo.getType())) {
                    connectionInfo.setDriver("SQL SERVER");
                }
                if (WorkspaceType.SMWU.equals(connectionInfo.getType())
                        || WorkspaceType.SXWU.equals(connectionInfo.getType())
                        || WorkspaceType.SMW.equals(connectionInfo.getType())
                        || WorkspaceType.SXW.equals(connectionInfo.getType())
                ) {
                    fileTypeNode.add(new DefaultMutableTreeNode(connectionInfo));
                } else {
                    ConnTypeNode.add(new DefaultMutableTreeNode(connectionInfo));
                }
            }
        }
    }

    @Override
    public void doubleClickHandler(DefaultMutableTreeNode node, CallBack cb) {
        GAFWorkspaceConnectionInfo conn = (GAFWorkspaceConnectionInfo) node.getUserObject();
        preOpAndCheck(conn, false, connectionInfo -> {
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                // 记录下载记录
                conn.setLocalPath(connectionInfo.getServer());
                CommonUtils.appendDownloadHistory(CommonUtils.WORKSPACE_DOWNLOAD_HISTORY, conn.getId(), conn.getLocalPath());
                updateNode(node);
            }
            if (WorkspaceType.SQL.equals(connectionInfo.getType())) {
                connectionInfo.setDriver("SQL SERVER");
            }
            if (!CommonUtils.sameWorkspace(ApplicationContextUtils.getWorkspace().getConnectionInfo(), connectionInfo)) {
                Workspace workspaceTemp = new Workspace();
                boolean openResult = workspaceTemp.open(connectionInfo);
                if (openResult) {
                    workspaceTemp.close();
                    if (WorkspaceUtilities.closeWorkspace()) {
                        CommonUtils.openWorkspace(connectionInfo);
                        if (cb != null) cb.doWork();
                    }
                } else {
                    ApplicationContextUtils.getOutput().output("工作空间打开失败！");
                }
            } else {
                if (cb != null) cb.doWork();
            }
            return null;
        });
    }
    public void preOpAndCheck(GAFWorkspaceConnectionInfo connectionInfo, boolean reDownload, Function<WorkspaceConnectionInfo, Object> continueOp) {
        try {
            WorkspaceConnectionInfo canOpenConn = CommonUtils.copyWorkspaceConn(connectionInfo);
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                String localPath = CommonUtils.getDownloadedPath(CommonUtils.WORKSPACE_DOWNLOAD_HISTORY, connectionInfo.getId());
                if (localPath != null && !reDownload) {
                    // 有下载记录直接打开
                    if (!Files.exists(Paths.get(localPath))) {
                        JOptionPaneUtilities.showErrorMessageDialog(String.format("%s不存在", localPath));
                        return;
                    }
                    canOpenConn.setServer(localPath);
                    if(continueOp!=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
                Optional<String> fileName = CommonUtils.getFileName(connectionInfo);
                if (!fileName.isPresent()) {
                    JOptionPaneUtilities.showErrorMessageDialog("无法下载该文件");
                    return;
                }
                if (CommonUtils.tipIfNotExist("/"+connectionInfo.getServer())) {
                    return;
                }
                int re = CommonUtils.dirChoose(smFileChoose, fileName.get());
                if (re == -1) {
                    return;
                } else if (re == 0) {
                    Path downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + fileName.get());

                    // todo: 执行下载
                    CommonUtils.downloadAsync("/"+connectionInfo.getServer(), downloadPath, evt -> {
                        if (evt.getPropertyName().equals("progress")) {
                            Integer progress = (Integer) evt.getNewValue();
                            if (progress % 10 == 0) {
                                ApplicationContextUtils.getOutput().output("已下载" + downloadPath.getFileName().toString() + ":" + progress + "%");
                            }
                            if (progress == 100) {
                                ApplicationContextUtils.getOutput().output(downloadPath.getFileName().toString() + "下载成功");
                                canOpenConn.setServer(downloadPath.toString());
                                if(continueOp!=null){
                                    continueOp.apply(canOpenConn);
                                }
                            }
                        }
                    });
                    return;
                } else {
                    Path downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + fileName.get());
                    canOpenConn.setServer(downloadPath.toString());
                    if(continueOp!=null){
                        continueOp.apply(canOpenConn);
                    }
                    return;
                }
            } else {
                continueOp.apply(canOpenConn);
            }
        } catch (FileDownloadException e) {
            ApplicationContextUtils.getOutput().output(e.getMessage() + "下载失败");
        } catch (Exception e) {
            ApplicationContextUtils.getOutput().output(e.getMessage());
        }
    }


    @Override
    protected boolean faceLeafAndCheckOpened(TreeCellRender render, List opened, JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        GAFWorkspaceConnectionInfo connectionInfo = (GAFWorkspaceConnectionInfo) ((DefaultMutableTreeNode) value).getUserObject();
        render.setIcon(CommonUtils.getICon(connectionInfo.getType()));
        if (CommonUtils.isFileTypeSource(connectionInfo)) {
            Optional<String> fileName = CommonUtils.getFileName(connectionInfo);
            if (fileName.isPresent()) {
                render.setText(String.format("%s", fileName.get()));
            } else {
                render.setText(String.format("❌%s", "格式错误"));
            }
        } else {
            render.setText(connectionInfo.getName());
        }
        if(opened == null){
            return false;
        }
        for(Object item:opened){
            if (item != null && item instanceof WorkspaceConnectionInfo) {
                // 已打开状态
                WorkspaceConnectionInfo openedConn = (WorkspaceConnectionInfo) item;
                if (openedConn.getType() != WorkspaceType.DEFAULT) {
                    if (CommonUtils.sameWorkspace(connectionInfo, openedConn)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    SmPopupMenu buildLeafPopMenu(DefaultMutableTreeNode treeNode) {
        return ApplicationContextUtils.getContextMenuManager().get("SuperMap.Desktop.UI.GafWorkspaceListManager.ContextMenuWorkspace");
    }

    @Override
    SmPopupMenu buildCatalogPopMenu(DefaultMutableTreeNode treeNode) {
        return null;
    }
}
