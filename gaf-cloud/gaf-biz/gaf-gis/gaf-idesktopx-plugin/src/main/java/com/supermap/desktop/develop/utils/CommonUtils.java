/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.desktop.develop.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.data.*;
import com.supermap.data.processing.CacheWriter;
import com.supermap.desktop.controls.ui.UICommonToolkit;
import com.supermap.desktop.controls.ui.controls.DockbarManager;
import com.supermap.desktop.controls.ui.controls.SmFileChoose;
import com.supermap.desktop.core.Application;
import com.supermap.desktop.core.Interface.IBaseItem;
import com.supermap.desktop.core.Interface.IContextMenuManager;
import com.supermap.desktop.core.Interface.IDockbar;
import com.supermap.desktop.core.tools.historyManager.HistoryManager;
import com.supermap.desktop.core.tools.iserverInfo.IServerLoginInfo;
import com.supermap.desktop.core.utilties.*;
import com.supermap.desktop.develop.client.GafClient;
import com.supermap.desktop.develop.client.GlobalOkHttpClient;
import com.supermap.desktop.develop.ctrlaction.CtrlActionLoginGaf;
import com.supermap.desktop.develop.ctrlaction.CtrlActionLogoutGaf;
import com.supermap.desktop.develop.ctrlaction.CtrlActionRegistryWorkspace;
import com.supermap.desktop.develop.ctrlaction.CtrlActionSynchronizeWorkspace;
import com.supermap.desktop.develop.entity.*;
import com.supermap.desktop.develop.exception.FileDownloadException;
import com.supermap.desktop.develop.exception.FileUploadException;
import com.supermap.desktop.develop.histroy.DownloadHistory;
import com.supermap.desktop.develop.histroy.HistoryManagerUnique;
import com.supermap.desktop.develop.ui.*;
import com.supermap.desktop.develop.work.FileDownloadWork;
import com.supermap.desktop.develop.work.FileUploadWork;
import com.supermap.desktop.mapview.mapCache.LayerCacheData;
import com.supermap.desktop.mapview.mapCache.LayerCacheDataLocal;
import com.supermap.desktop.mapview.mapCache.LayerCacheDataMongo;
import com.supermap.iobjects.processes.publish.processes.ServiceType;
import com.supermap.mapping.LayerCache;
import com.supermap.tilestorage.TileStorageConnection;
import com.supermap.tilestorage.TileStorageManager;
import com.supermap.tilestorage.TileStorageType;
import okhttp3.*;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;
import org.apache.arrow.vector.util.CallBack;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.io.Util;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Semaphore;
import java.util.function.Function;

/**
 * @author heykb
 * @date:2021/3/25
 */
public class CommonUtils {
    public static boolean online = false;
    public static final String WORKSPACE_DIR = "datas/";
    public static final String DATASOURCE_DIR = "datas/";
    public static final HistoryManagerUnique<DownloadHistory> WORKSPACE_DOWNLOAD_HISTORY = new HistoryManagerUnique<DownloadHistory>(HistoryManager.HISTORY_DEFAULT_DIR + "WorkspaceDownload", DownloadHistory.class);
    public static final HistoryManagerUnique<DownloadHistory> DATASOURCE_DOWNLOAD_HISTORY = new HistoryManagerUnique<DownloadHistory>(HistoryManager.HISTORY_DEFAULT_DIR + "DatasourceDownload", DownloadHistory.class);
    public static final HistoryManagerUnique<DownloadHistory> TILE_DOWNLOAD_HISTORY = new HistoryManagerUnique<DownloadHistory>(HistoryManager.HISTORY_DEFAULT_DIR + "TileDownload", DownloadHistory.class);

    public static void checkGafPopMenuStatus() {
        Workspace workspace = ApplicationContextUtils.getWorkspace();
        IContextMenuManager manager = ApplicationContextUtils.getContextMenuManager();
        for (IBaseItem iBaseItem : manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuWorkspace").items()) {
            if (iBaseItem != null && "gafWorkspaceUpload".equals(iBaseItem.getID())) {
                boolean enable = online; /*&& (workspace.getType().equals(WorkspaceType.SMWU)||workspace.getType().equals(WorkspaceType.SXWU));*/
                iBaseItem.setEnabled(enable);
                iBaseItem.setVisible(true);
//                CtrlActionReleaseWorkspace
            }
        }
//        for(IBaseItem iBaseItem: manager.get("SuperMap.Desktop.UI.WorkspaceControlManager.ContextMenuDatasource").items()){
//            if(iBaseItem!=null && "gafDatasourceUpload".equals(iBaseItem.getID())){
//                boolean enable = online;
//                iBaseItem.setEnabled(enable);
//                iBaseItem.setVisible(true);
//            }
//
//        }

    }

    public static void checkGafStatus(boolean online) {
        CommonUtils.online = online;
        if (!online) {
            // 注销
            checkGafPopMenuStatus();
            CtrlActionLogoutGaf.enable = false;
            CtrlActionLoginGaf.enable = true;
            CtrlActionRegistryWorkspace.enable = false;
            CtrlActionSynchronizeWorkspace.enable = false;
            GafClient.logout();
        } else {
            //登录
            checkGafPopMenuStatus();
            CtrlActionLogoutGaf.enable = true;
            CtrlActionLoginGaf.enable = false;
            CtrlActionRegistryWorkspace.enable = true;
            CtrlActionSynchronizeWorkspace.enable = true;
        }

    }

    public static boolean isFileTypeSource(Object source) {
        if (source == null) return false;
        if (source instanceof WorkspaceConnectionInfo) {
            WorkspaceConnectionInfo connectionInfo = (WorkspaceConnectionInfo) source;
            return WorkspaceType.SMWU.equals(connectionInfo.getType())
                    || WorkspaceType.SXWU.equals(connectionInfo.getType())
                    || WorkspaceType.SMW.equals(connectionInfo.getType())
                    || WorkspaceType.SXW.equals(connectionInfo.getType());
        }
        if (source instanceof DatasourceConnectionInfo) {
            DatasourceConnectionInfo connectionInfo = (DatasourceConnectionInfo) source;
            return EngineType.UDB.equals(connectionInfo.getEngineType()) || EngineType.UDBX.equals(connectionInfo.getEngineType());
        }
        if(source instanceof GAFTileStorageConnection){
            return ((GAFTileStorageConnection) source).getSourceType().toLowerCase().indexOf("mongodb") == -1;
        }
        if(source instanceof TileStorageConnection){
            return ((TileStorageConnection) source).getStorageType().name().toLowerCase().indexOf("mongo") == -1;
        }
        return false;
    }

    public static WorkspaceConnectionInfo copyWorkspaceConn(WorkspaceConnectionInfo source) {
        WorkspaceConnectionInfo wConn = new WorkspaceConnectionInfo();
        wConn.setType(source.getType());
        wConn.setUser(source.getUser());
        wConn.setServer(source.getServer());
        wConn.setPassword(source.getPassword());
        wConn.setDatabase(source.getDatabase());
        wConn.setDriver(source.getDriver());
        wConn.setName(source.getName());
        return wConn;
    }
    public static TileStorageConnection copyTileStorageConnection(TileStorageConnection connectionInfo) {
        TileStorageConnection re = new TileStorageConnection();
        re.setStorageType(connectionInfo.getStorageType());
        re.setServer(connectionInfo.getServer());
        re.setUser(connectionInfo.getUser());
        re.setPassword(connectionInfo.getPassword());
        re.setName(connectionInfo.getName());
        re.setDatabase(connectionInfo.getDatabase());
        return re;
    }


    public static String getBase64Md5(Path path) throws Exception {

        MessageDigest md5 = MessageDigest.getInstance("MD5");
        try (InputStream in = Files.newInputStream(path)) {
            byte[] buffer = new byte[8192];
            int length;
            while ((length = in.read(buffer)) != -1) {
                //md5计算
                md5.update(buffer, 0, length);
            }

        }
        String base64Md5 = Base64.getEncoder().encodeToString(md5.digest());
        return base64Md5;
    }

    public static void uploadAsync(String targetPath, Path source, PropertyChangeListener listener) throws FileUploadException {
        ApplicationContextUtils.getOutput().output("开始上传" + source.getFileName().toString());
        uploadAsync(targetPath, source, true, listener);
    }

    public static void uploadAsync(String targetPath, Path source, boolean quickUploadEnable, PropertyChangeListener listener) throws FileUploadException {
        try {
            String base64Md5 = null;
                try {
                    JSONObject re = GafClient.instance().objectMetadata(targetPath);
                    JSONObject metadata = re.getJSONObject("data");

                    if (404 != re.getInteger("status") && metadata != null) {
                        // 修改判断
                        Date lastModified = re.getDate("lastModified");
                        // 快速上傳
                        if (quickUploadEnable) {
                            base64Md5 = getBase64Md5(source);
                            if (404 != re.getInteger("status") && metadata.getJSONObject("userMetadata") != null) {
                                String md5 = metadata.getJSONObject("userMetadata").getString("base64md5");
                                if (base64Md5.equals(md5)) {
                                    ApplicationContextUtils.getOutput().output(source.getFileName().toString() + "快速上传");
                                    return;
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                }
            String presignUrl = GafClient.instance().uploadPresignUrl(targetPath, base64Md5);
            PresignUploadRequest uploadRequest = new PresignUploadRequest(presignUrl,source.toFile(),base64Md5,false);
            FileUploadWork workspaceUpload = new FileUploadWork(uploadRequest);
            workspaceUpload.addPropertyChangeListener(listener);
            workspaceUpload.execute();
        } catch (Exception e) {
            throw new FileUploadException(source.getFileName().toString());
        }
    }



    public static void downloadAsync(String serverPath, Path localPath, PropertyChangeListener listener) throws FileDownloadException, FileNotFoundException {
        FileDownloadWork downloadWork = new FileDownloadWork(serverPath, localPath);
        downloadWork.addPropertyChangeListener(listener);
        ApplicationContextUtils.getOutput().output("开始下载" + localPath.getFileName().toString());
        downloadWork.execute();
    }

    public static void downloadAsync(String dir, Path localDir, CallBack finished, Function<PropertyChangeEvent,Object> listenerForEvery) throws FileDownloadException, FileNotFoundException, InterruptedException, BrokenBarrierException {
        JSONArray array = null;
        try {
            array = GafClient.instance().listObjects(dir);
        } catch (Exception e) {
            throw new FileNotFoundException(dir);
        }
        int fileCount = array.size();
        final JSONArray files = array;
        if(fileCount > 0){
            new Thread(()-> {
                try {
//                    CyclicBarrier cyclicBarrier = new CyclicBarrier(fileCount + 1);
                    //同时下载数目控制
                    Semaphore semaphore = new Semaphore(3);
                    Semaphore waitSem = new Semaphore(0-fileCount+1);
                    for (int i = 0; i < fileCount; ++i) {
                        semaphore.acquire();
                        String serverPath = files.getJSONObject(i).getString("name");
                        String serverRelativePath = serverPath.replace(dir,"");
                        FileDownloadWork downloadWork = new FileDownloadWork("/" + serverPath, localDir.resolve(serverRelativePath));
                        downloadWork.addPropertyChangeListener(evt -> {
                            if (evt.getPropertyName().equals("progress")) {
                                listenerForEvery.apply(evt);
                                Integer newProgress = (Integer) evt.getNewValue();
                                if (newProgress == 100 || newProgress == -1) {
                                    semaphore.release();
                                    waitSem.release();
                                }
                            }

                        });
                        ApplicationContextUtils.getOutput().output("开始下载" + serverPath);
                        downloadWork.execute();
                    }

                    waitSem.acquire();

                    finished.doWork();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }).start();

        }
    }

    public static void test(String[] args) {
        try {
            downloadAsync("tiles-manage/XZQ_192.168.11.118_32224_space_tile/XZQ_192.168.11.118_32224_space_tile/",Paths.get("E:\\tmp\\tm2\\test"),
                    ()->{
                        System.out.println("下载结束");
                    },evt->{
                        FileDownloadWork downloadWork = (FileDownloadWork) evt.getSource();
                        String serverPath = downloadWork.getServerPath();
                        Integer progress = (Integer) evt.getNewValue();
                        if (progress % 10 == 0) {
                            ApplicationContextUtils.getOutput().output("已下载" + serverPath + ":" + progress + "%");
                        }
                        if (progress == 100) {
                            ApplicationContextUtils.getOutput().output(serverPath + "下载成功");
                        }
                        return null;
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public static void deletePath(Path path) throws IOException {
        if (!Files.isDirectory(path)) {
            Files.deleteIfExists(path);
        } else {
            Files.walkFileTree(path,
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file,
                                                         BasicFileAttributes attrs) throws IOException {
                            Files.deleteIfExists(file);
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir,
                                                                  IOException exc) throws IOException {
                            Files.deleteIfExists(dir);
                            return FileVisitResult.CONTINUE;
                        }

                    }
            );
        }
    }

    public static void refreshWorkspaceTree() throws Exception {
        DockbarManager dockbarManager = ApplicationContextUtils.getDockbarManager();
        IDockbar workspace = dockbarManager.get(GafWorkspaceManager.class);
        workspace.setVisible(true);
        ((GafWorkspaceManager) workspace.getInnerComponent()).updateTree(new GAFWorkspaceTree(CommonUtils.WORKSPACE_DOWNLOAD_HISTORY,GafClient.instance().workspaceList()));
    }

    public static void refreshDatasourceTree() throws Exception {
        DockbarManager dockbarManager = ApplicationContextUtils.getDockbarManager();
        IDockbar datasource = dockbarManager.get(GafDatasourceManager.class);
        datasource.setVisible(true);
        ((GafDatasourceManager) datasource.getInnerComponent()).updateTree(new GAFDatasourceTreeNew(CommonUtils.DATASOURCE_DOWNLOAD_HISTORY,GafClient.instance().datasources()));
    }
    public static void refreshTileTree() throws Exception {
        DockbarManager dockbarManager = ApplicationContextUtils.getDockbarManager();
        IDockbar datasource = dockbarManager.get(GafTileManager.class);
        datasource.setVisible(true);
        ((GafTileManager) datasource.getInnerComponent()).updateTree(new GAFTileTree(CommonUtils.TILE_DOWNLOAD_HISTORY,GafClient.instance().tileList()));
    }


    public static DefaultMutableTreeNode searchTree(TreeModel treeModel, TreeSearchVisitor visitor) {
        DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treeModel.getRoot();
        List<DefaultMutableTreeNode> needFind = new ArrayList<>();
        needFind.add(treeNode);
        while (true) {
            if (needFind.isEmpty()) {
                return null;
            }
            List<DefaultMutableTreeNode> tmp = new ArrayList<>();
            for (DefaultMutableTreeNode parent : needFind) {
                if (visitor.search(parent)) {
                    return parent;
                }
                for (int i = 0; i < parent.getChildCount(); ++i) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent.getChildAt(i);
                    tmp.add(node);
                }
            }
            needFind = tmp;
        }
    }

    public static ResizableIcon getICon(WorkspaceType workspaceType) {
        String iconPath = "";
        iconPath = "/coreresources/WorkspaceManager/Image_RecentUseWorkspace.png";
        return CoreResources.getIcon(iconPath);
    }

    public static ResizableIcon getICon(EngineType engineType) {
        return DatasourceImageUtilities.getEngineIcon(engineType);
    }

    public static int dirChoose(SmFileChoose smFileChoose, String conflictFileName) {
        if (smFileChoose.showOpenDialog(null) != 0) {
            return -1;
        }
        Path downloadPath = null;
        if (conflictFileName != null) {
            downloadPath = Paths.get(smFileChoose.getFilePath() + "/" + conflictFileName);
            if (Files.exists(downloadPath)) {
                if (UICommonToolkit.showConfirmDialogYesNo("目录下已存在同名文件，是否覆盖？") != 0) {
                    return 1;
                }
            }
        }
        return 0;
    }

    public static Optional<String> getFileName(Object source) {
        if (source == null) {
            return Optional.empty();
        }
        try {
            if (isFileTypeSource(source)) {
                if (source instanceof WorkspaceConnectionInfo) {
                    WorkspaceConnectionInfo connectionInfo = (WorkspaceConnectionInfo) source;
                    return Optional.of(Paths.get(connectionInfo.getServer()).getFileName().toString());
                } else if (source instanceof DatasourceConnectionInfo) {
                    DatasourceConnectionInfo connectionInfo = (DatasourceConnectionInfo) source;
                    return Optional.of(Paths.get(connectionInfo.getServer()).getFileName().toString());
                }else if (source instanceof GAFTileStorageConnection) {
                    GAFTileStorageConnection connectionInfo = (GAFTileStorageConnection) source;
                    return Optional.of(Paths.get(connectionInfo.getServer()).getFileName().toString());
                }
            }
        } catch (Exception e) {

        }
        return Optional.empty();
    }



    public static interface TreeSearchVisitor {
        boolean search(DefaultMutableTreeNode node);
    }
    public static void uploadWorkspace(Workspace oldWorkspace){
        uploadWorkspace(oldWorkspace,null);
    }
    public static void uploadWorkspace(Workspace oldWorkspace, Function<String,Object> callback) {
        try {
            WorkspaceConnectionInfo connectionInfo = oldWorkspace.getConnectionInfo();
            if (CommonUtils.isFileTypeSource(connectionInfo)) {
                Path wPath = CommonUtils.handlerWorkspaceForUpload(oldWorkspace);
                // todo: 上传工作空间
                final Path tmp = wPath;
                String serverPath = "/datas/" + wPath.getFileName().toString();
                CommonUtils.uploadAsync(serverPath, wPath, false, evt -> {
                    if (evt.getPropertyName().equals("progress")) {
                        Integer progress = (Integer) evt.getNewValue();
                        if (progress % 10 == 0) {
                            ApplicationContextUtils.getOutput().output("已上传" + tmp.getFileName().toString() + ":" + progress + "%");
                        }
                        if (progress == 100) {
                            try {
                                ApplicationContextUtils.getOutput().output(tmp.getFileName().toString() + "上传完成");
                                CommonUtils.deletePath(tmp.getParent());
                                if(callback!=null){
                                    callback.apply("/datas/");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        } catch (FileUploadException e) {
            ApplicationContextUtils.getOutput().output(e.getMessage() + "上传失败");
        } catch (Exception e) {
            ApplicationContextUtils.getOutput().output(e.getMessage());
        }

    }

    public static Path handlerWorkspaceForUpload(Workspace oldWorkspace) throws IOException {
        // 复制工作空间修改数据源连接
        Workspace workspace = new Workspace();
        Datasources oldDatasources = oldWorkspace.getDatasources();
        WorkspaceConnectionInfo wOldConn = oldWorkspace.getConnectionInfo();
        Path wOldPath = Paths.get(wOldConn.getServer());
        Path tmpPath = Files.createTempDirectory("gaf_");
        Path wPath = tmpPath.resolve(wOldPath.getFileName().toString());
        Files.createDirectories(wPath.getParent());
        Files.copy(wOldPath, wPath, StandardCopyOption.REPLACE_EXISTING);
        WorkspaceConnectionInfo wConn = CommonUtils.copyWorkspaceConn(wOldConn);
        wConn.setServer(wPath.toString());
        workspace.open(wConn);
        Datasources datasources = workspace.getDatasources();
        Map<Integer, Datasource> fileSources = new HashMap<>();
        for (int i = 0; i < oldDatasources.getCount(); ++i) {
            Datasource datasource = oldDatasources.get(i);
            DatasourceConnectionInfo dConn = datasource.getConnectionInfo();
            if (CommonUtils.isFileTypeSource(dConn)) {
                fileSources.put(i, datasources.get(i));
            }
        }
        // 关闭文件型数据源
        fileSources.values().stream().forEach(datasource -> datasource.close());
        // 重新指定路径打开
        for (Integer i : fileSources.keySet()) {
            Datasource datasource = oldDatasources.get(i);
            DatasourceConnectionInfo dConn = datasource.getConnectionInfo();
            Path dPath = Paths.get(dConn.getServer());
            Path dNewPath = wPath.resolveSibling(dPath.getFileName().toString());
            DatasourceConnectionInfo dNewConn = DatasourceUtilities.copyDatasourceConnectionInfo(dConn);
            dNewConn.setServer(dNewPath.toString());
            datasources.create(dNewConn);
        }
        // 保存
        workspace.save();
        workspace.close();
        // 上传工作空间
        return wPath;
    }

    public static void uploadDatasource(Datasource datasource) {
        DatasourceConnectionInfo connectionInfo = datasource.getConnectionInfo();
        if (isFileTypeSource(connectionInfo)) {
            Path path = Paths.get(connectionInfo.getServer());
            if (!Files.exists(path)) {
                return;
            }

            CommonUtils.uploadAsync("/datas/" + path.getFileName().toString(), path, evt -> {
                if (evt.getPropertyName().equals("progress")) {
                    Integer progress = (Integer) evt.getNewValue();
                    if (progress % 10 == 0) {
                        ApplicationContextUtils.getOutput().output("已上传" + path.getFileName().toString() + ":" + progress + "%");
                    }
                    if (progress == 100) {
                        ApplicationContextUtils.getOutput().output(path.getFileName().toString() + "上传完成");
                    }
                }
            });
            if (connectionInfo.getServer().endsWith(".udb")) {
                String uddPathStr = connectionInfo.getServer();
                uddPathStr = uddPathStr.substring(0, uddPathStr.length() - 1) + "d";
                Path uddPath = Paths.get(uddPathStr);
                if (Files.exists(uddPath)) {
                    CommonUtils.uploadAsync("/datas/" + uddPath.getFileName().toString(), uddPath, evt -> {
                        if (evt.getPropertyName().equals("progress")) {
                            Integer progress = (Integer) evt.getNewValue();
                            if (progress % 10 == 0) {
                                ApplicationContextUtils.getOutput().output("已上传" + uddPath.getFileName().toString() + ":" + progress + "%");
                            }
                            if (progress == 100) {
                                ApplicationContextUtils.getOutput().output(uddPath.getFileName().toString() + "上传完成");
                            }
                        }
                    });
                }
            }
        }
    }

    public static boolean sameDatasource(DatasourceConnectionInfo o1,DatasourceConnectionInfo o2){
        boolean re = false;
        if(o1 == o2) return true;
        if(CommonUtils.isFileTypeSource(o1)){
            try{
                String path1 = o1.getServer();
                if(o1 instanceof GAFDatasourceConnectionInfo && ((GAFDatasourceConnectionInfo) o1).getLocalPath()!=null){
                    path1 = ((GAFDatasourceConnectionInfo) o1).getLocalPath();
                }
                String path2 = o2.getServer();
                if(o2 instanceof GAFDatasourceConnectionInfo && ((GAFDatasourceConnectionInfo) o2).getLocalPath()!=null){
                    path2 = ((GAFDatasourceConnectionInfo) o2).getLocalPath();
                }
                re = Files.isSameFile(Paths.get(path1),Paths.get(path2));
            }catch (Exception e){}
        }else{
            re = (o1.getEngineType()==o2.getEngineType() && StringUtils.equals(o1.getServer(),o2.getServer())
                    && StringUtils.equals(o1.getDatabase(),o2.getDatabase()));
        }
        return re;

    }
    public static boolean sameTile(TileStorageConnection o1, TileStorageConnection o2) {
        boolean re = false;
        if(o1 == o2) return true;
        if(CommonUtils.isFileTypeSource(o1)){
            try{
                String path1 = o1.getServer();
                if(o1 instanceof GAFTileStorageConnection && ((GAFTileStorageConnection) o1).getLocalPath()!=null){
                    path1 = ((GAFTileStorageConnection) o1).getLocalPath();
                }
                String path2 = o2.getServer();
                if(o2 instanceof GAFTileStorageConnection && ((GAFTileStorageConnection) o2).getLocalPath()!=null){
                    path2 = ((GAFTileStorageConnection) o2).getLocalPath();
                }
                re = Files.isSameFile(Paths.get(path1),Paths.get(path2));
            }catch (Exception e){}
        }else{
            re = (StringUtils.equals(o1.getServer(),o2.getServer())
                    && StringUtils.equals(o1.getDatabase(),o2.getDatabase())
                    && StringUtils.equals(o1.getName(),o2.getName()));
        }
        return re;
    }
    public static boolean sameWorkspace(WorkspaceConnectionInfo o1,WorkspaceConnectionInfo o2){
        boolean re = false;
        if(o1 == o2) return true;
        if(CommonUtils.isFileTypeSource(o1)){
            try{
                String path1 = o1.getServer();
                if(o1 instanceof GAFWorkspaceConnectionInfo && ((GAFWorkspaceConnectionInfo) o1).getLocalPath()!=null){
                    path1 = ((GAFWorkspaceConnectionInfo) o1).getLocalPath();
                }
                String path2 = o2.getServer();
                if(o2 instanceof GAFWorkspaceConnectionInfo && ((GAFWorkspaceConnectionInfo) o2).getLocalPath()!=null){
                    path2 = ((GAFWorkspaceConnectionInfo) o2).getLocalPath();
                }
                re = Files.isSameFile(Paths.get(path1),Paths.get(path2));
            }catch (Exception e){}
        }else{
            re = (o1.getType()==o2.getType() && StringUtils.equals(o1.getServer(),o2.getServer())
                    && StringUtils.equals(o1.getDatabase(),o2.getDatabase()));
        }
        return re;

    }

    public static boolean tipIfNotExist(String path) {
        try {
            JSONObject reJson = GafClient.instance().objectMetadata(path);
            if (404 == reJson.getInteger("status")) {
                JOptionPaneUtilities.showErrorMessageDialog("文件不存在");
                return true;
            }
            return false;
        } catch (Exception e) {
            JOptionPaneUtilities.showErrorMessageDialog("服务不可用");
            return true;
        }
    }

    public static void openWorkspace(WorkspaceConnectionInfo connectionInfo){
        WorkspaceUtilities.openWorkspace(connectionInfo, true);
        ApplicationContextUtils.getOutput().output("工作空间打开成功！");
        if(!StringUtils.isEmpty(connectionInfo.getName())){
            ApplicationContextUtils.getWorkspace().setCaption(connectionInfo.getName());
        }
        ApplicationContextUtils.getWorkspace().save();

    }

    public static SmComboBoxGAFIServerLogin getGAFIserverLogin(){
        try {
            JSONObject re = GafClient.instance().iserverSetting();
            IServerLoginInfo iServerLoginInfo = new IServerLoginInfo();
            String domainUrl = re.getString("domainUrl");
            String hostServerUrl = re.getString("hostServerUrl");
            URL url = new URL(!StringUtils.isEmpty(domainUrl)?domainUrl:hostServerUrl);
            iServerLoginInfo.setServer(url.getProtocol()+"://"+url.getHost()+(url.getPort()==-1?"":":"+url.getPort())+"/iserver");
            iServerLoginInfo.setPassword(re.getString("password"));
            iServerLoginInfo.setUserName(re.getString("username"));
            return new SmComboBoxGAFIServerLogin(iServerLoginInfo);
        } catch (Exception e) {
            JOptionPaneUtilities.showMessageDialog("没有获取到有用的iserver连接");
            throw new RuntimeException(e);
        }
    }

    public static void appendDownloadHistory(HistoryManager<DownloadHistory> historyManager,String workspaceId,String path){
        if(workspaceId!=null && path!=null){
            DownloadHistory item = new DownloadHistory(GafClient.instance().getEndpoint(),workspaceId,path);
            historyManager.appendHistory(item);

        }
    }
    public static String  getDownloadedPath(HistoryManager<DownloadHistory> historyManager,String workspaceId){
        return getSourceId2PathMap(historyManager).get(workspaceId);
    }
    public static Map<String, String> getSourceId2PathMap(HistoryManager<DownloadHistory> historyManager) {
        Map<String, String> re = new HashMap<>();
        List<DownloadHistory> list = historyManager.getAllHistory();
        for(DownloadHistory item:list){
            if(GafClient.instance().getEndpoint().equals(item.getServer())){
                re.put(item.getSourceId(),item.getPath());
            }
        }
        return re;
    }


    public static GAFFormCacheManager openCacheManager(){
        GAFFormCacheManager formCacheManager = new GAFFormCacheManager();
        Application.getActiveApplication().setActiveForm(formCacheManager);
        return formCacheManager;
    }

    public static void openTile(TileStorageConnection tileStorageConnection){
        if(!(Application.getActiveApplication().getActiveForm() instanceof GAFFormCacheManager)){
            openCacheManager().getControlMapMongoDBCacheManage().addData(tileStorageConnection);
        }else{
            ((GAFFormCacheManager) Application.getActiveApplication().getActiveForm()).getControlMapMongoDBCacheManage().addData(tileStorageConnection);
        }
    }

    public static TileStorageConnection getTileStorageConnection(LayerCache layerCache){
        String connectionInfo = layerCache.getConnectionInfo();
        String[] split = connectionInfo.split(";");
        TileStorageConnection re = new TileStorageConnection();
        if (split.length == 1) {
            re.setServer(connectionInfo);
        } else {
            re.setServer(split[0]);
            if (split.length >= 3) {
                re.setStorageType(TileStorageType.MONGO);
                re.setDatabase(split[1]);
                re.setName(split[2]);
            }
            if (split.length >= 5) {
                re.setUser(split[3]);
                re.setPassword(split[4]);
            }
        }
        return re;
    }
    public static LayerCacheData createLayerCacheData(LayerCache layerCache) {
        LayerCacheData layerCacheData = null;
        String connectionInfo = layerCache.getConnectionInfo();
        String[] split = connectionInfo.split(";");
        if (split.length == 1) {
            if (FileUtilities.exists(connectionInfo)) {
                CacheWriter cacheWriter = new CacheWriter();
                cacheWriter.FromConfigFile(connectionInfo);
                List scales = new ArrayList();
                HashMap outputScaleCaptions = cacheWriter.getCacheScaleCaptions();
                Iterator var7 = outputScaleCaptions.keySet().iterator();

                while(var7.hasNext()) {
                    Double scale = (Double)var7.next();
                    scales.add(scale);
                }

                layerCacheData = new LayerCacheDataLocal(layerCache, scales, new PrjCoordSys(cacheWriter.getPrjCoordSys()));
            }
        } else {
            TileStorageConnection tileStorageConnection = getTileStorageConnection(layerCache);

            TileStorageManager tileStorageManager = new TileStorageManager();
            if (tileStorageManager.open(tileStorageConnection)) {
                List scales = new ArrayList();
                double[] cacheScales = tileStorageManager.getVersions()[0].getScales();
                double[] var17 = cacheScales;
                int var9 = cacheScales.length;

                for(int var10 = 0; var10 < var9; ++var10) {
                    double cacheScale = var17[var10];
                    scales.add(1.0D / cacheScale);
                }

                layerCacheData = new LayerCacheDataMongo(layerCache, scales, new PrjCoordSys(tileStorageManager.getInfo().getPrjCoordSys()), tileStorageManager.getInfo(), tileStorageManager.getVersions(), tileStorageConnection);
            }
        }

        return (LayerCacheData)layerCacheData;
    }

    public static void registerService(String links,String sourceId,int sourceType){
        try {
            Application.getActiveApplication().getOutput().output("开始注册服务...");
            Object obj = JSON.parse(links);
            if (obj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray)obj;

                for(int i = 0; i < jsonArray.size(); ++i) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.containsKey("serviceType") && jsonObject.containsKey("serviceAddress")) {
                        String serviceType = jsonObject.get("serviceType").toString();
                        String url = jsonObject.getString("serviceAddress");

                        String re = GafClient.instance().batchRegistryService(url,serviceType,url.substring(url.lastIndexOf("/")+1)+"-"+ RandomStringUtils.random(4),sourceId,sourceType);
                        List<JSONObject> array = JSON.parseArray(re,JSONObject.class);
                        for(JSONObject item:array){
                            if(item.getBoolean("isSuccessed")){
                                Application.getActiveApplication().getOutput().output(item.getString("data")+" 注册成功");

                            }else{
                                Application.getActiveApplication().getOutput().output(item.getString("data")+" 注册失败："+item.getString("message"));
                            }
                        }
                    }
                }
            }
        } catch (Exception var7) {
            Application.getActiveApplication().getOutput().output(var7);
        }

    }

    static RequestBody createUploadRequestBody(File file, Function<Integer,Void> progressListener){
        MediaType mediaType = MediaType.parse("application/octet-stream");
        if(progressListener == null){
            return RequestBody.create(mediaType, file);
        }
        return  new RequestBody() {
            public MediaType contentType() {
                return mediaType;
            }
            public long contentLength() {
                return file.length();
            }
            public void writeTo(BufferedSink sink) throws IOException {
                Source source = null;
                try {
                    source = Okio.source(file);
                    if (source == null) throw new IllegalArgumentException("source == null");
                    long totalBytesRead = 0;

                    for (long readCount; (readCount = source.read(sink.buffer(), 8192)) != -1; ) {
                        totalBytesRead += readCount;
                        int progress = (int) Math.round(((double) totalBytesRead / (double) contentLength()) * 100d);
                        progressListener.apply(progress == 100 ? 99 : progress);
                    }
                } finally {
                    Util.closeQuietly(source);
                }
            }
        };
    }

    public static void uploadByPreSignedUrl(PresignUploadRequest uploadRequest) {
        OkHttpClient client = GlobalOkHttpClient.INSTANCE.getClient();
        Function<Integer, Void> progressListener = uploadRequest.getProgressListener();
        RequestBody body = createUploadRequestBody(uploadRequest.getFile(),progressListener);
        Request.Builder builder = new Request.Builder()
                .url(uploadRequest.getPresignUrl())
                .method("PUT", body)
                .addHeader("Content-Type", "application/octet-stream");

        if (!StringUtils.isEmpty(uploadRequest.getContentMd5())) {
            builder.addHeader("Content-MD5", uploadRequest.getContentMd5())
                    .addHeader("x-amz-meta-base64md5", uploadRequest.getContentMd5());
        }
        Request request = builder.build();
        try {
            Call call = client.newCall(request);
            if(uploadRequest.isAsync()){
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        progressListener.apply(-1);
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            progressListener.apply(-1);
                            throw new FileUploadException(response.code() + " " + response.body().string());
                        }
                        if(progressListener!=null){
                            progressListener.apply(100);
                        }
                    }
                });
            }else{
                Response response = call.execute();
                if (!response.isSuccessful()) {
                    progressListener.apply(-1);
                    throw new FileUploadException(response.code() + " " + response.body().string());
                }
                if(progressListener!=null){
                    progressListener.apply(100);
                }
            }

        } catch (IOException e) {
            throw new FileUploadException(e);
        }

    }

    public static void downloadByPreSignedUrl(PresignDownloadRequest downloadRequest) {

        OkHttpClient client = GlobalOkHttpClient.INSTANCE.getClient();
        Request request = new Request.Builder()
                .url(downloadRequest.getPresignUrl())
                .method("GET", null)
                .build();
        Path path = downloadRequest.getPath();
        Function<Integer, Void> progressListener = downloadRequest.getProgressListener();
        try(Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new FileDownloadException(response.code() + " " + response.body().string());
            }

            if(!Files.exists(path.getParent())){
                Files.createDirectories(path.getParent());
            }
            Files.deleteIfExists(path);
            long contentLength = response.body().contentLength();
            if(response.isSuccessful()){
                if (contentLength > 0) {
                    try (FileOutputStream out = new FileOutputStream(path.toFile());
                         InputStream in = new BufferedInputStream(response.body().byteStream())) {
                        byte[] buf = new byte[8192];
                        int n;
                        long nread = 0L;
                        long lastBytes;
                        while ((lastBytes = contentLength - nread) > 0 && (n = in.read(buf, 0, lastBytes < buf.length ? (int) lastBytes : buf.length)) > 0) {
                            out.write(buf, 0, n);

                            if(progressListener!=null){
                                int progress = (int) Math.round(((double) nread / (double) contentLength) * 100d);
                                progress = progress == 100 ? 99 : progress;
                                progressListener.apply(progress);
                            }
                            nread += n;
                        }
                    }
                }else{
                    Files.createFile(path);
                }
                if(progressListener!=null){
                    progressListener.apply(100);
                }
            }else{
                if(progressListener!=null){
                    progressListener.apply(-1);
                }
                if(response.code()==404){
                    JOptionPaneUtilities.showErrorMessageDialog("文件不存在");
                    throw new FileDownloadException();
                }else{
                    throw new IOException(response.code()+";"+response.toString());
                }
            }
        } catch (IOException e) {
            ApplicationContextUtils.getOutput().output(path.getFileName().toString() + "下载失败："+e.getMessage());
            throw new FileDownloadException(e.getMessage());
        }
    }

    /*public static void downloadByPreSignedUrl(String preSignedUrl, Path path) throws FileDownloadException, FileNotFoundException {
        downloadByPreSignedUrl(preSignedUrl, path, null);
    }*/
    /*public static void uploadByPreSignedUrl(PresignUploadRequest uploadRequest, Path path) throws FileUploadException {
        uploadByPreSignedUrl(uploadRequest, path, null);
    }*/

  /*  public static void uploadByPreSignedUrl(PresignUploadRequest uploadRequest, Path path, ProgressListener listener) throws FileUploadException {
        HttpURLConnection connection = null;
        OutputStream out = null;
        BufferedReader reader = null;
        try {
            String preSignedUrl = uploadRequest.getPresignUrl();
            String base64Md5 = uploadRequest.getContentMd5();
            URL url = new URL(preSignedUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "application/octet-stream");
            if (!StringUtils.isEmpty(base64Md5)) {
                connection.setRequestProperty("Content-MD5", base64Md5);
                connection.setRequestProperty("x-amz-meta-base64md5", base64Md5);
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("PUT");
            connection.connect();
            out = new DataOutputStream(connection.getOutputStream());
            long fileSize = path.toFile().length();

            try (InputStream in = Files.newInputStream(path)) {
                long nread = 0L;
                byte[] buf = new byte[8192];
                int n;
                while ((n = in.read(buf)) > 0) {
                    out.write(buf, 0, n);
                    nread += n;
                    if (listener != null) {
                        int progress = (int) Math.round(((double) nread / (double) fileSize) * 100d);
                        progress = progress == 100 ? 99 : progress;
                        listener.progress(progress, connection);
                    }
                }
            }
            int code = connection.getResponseCode();
            if (code != 200) {
                throw new RuntimeException(code + " 检查MD5");
            }
            if (listener != null) {
                listener.progress(100, connection);
            }

        } catch (Exception e) {
            throw new FileUploadException(path.getFileName().toString());
        } finally {
            try {
                reader.close();
                out.flush();
                out.close();
                connection.disconnect();
            } catch (IOException e) {
            }
        }
    }*/
   /* public static void downloadByPreSignedUrl(String preSignedUrl, Path path, ProgressListener listener) throws FileDownloadException, FileNotFoundException {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(preSignedUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.connect();
//        connection.getContentLength();
            DataInputStream in = new DataInputStream(connection.getInputStream());
            long fileSize = connection.getContentLength();
            if(!Files.exists(path.getParent())){
                Files.createDirectories(path.getParent());
            }
            // 删除旧文件
            Files.deleteIfExists(path);
            try (OutputStream out = Files.newOutputStream(path)) {
                long nread = 0L;
                byte[] buf = new byte[8192];
                int n;
                while ((n = in.read(buf)) > 0) {
                    out.write(buf, 0, n);
                    nread += n;
                    if (listener != null) {
                        int progress = (int) Math.round(((double) nread / (double) fileSize) * 100d);
                        progress = progress == 100 ? 99 : progress;
                        listener.progress(progress, connection);
                    }
                }
            }

            int code = connection.getResponseCode();
            if (code != 200) {
                throw new RuntimeException("下载失败");
            }
            if (listener != null) {
                listener.progress(100, connection);
            }
        } catch (FileNotFoundException e) {
            JOptionPaneUtilities.showErrorMessageDialog("文件不存在");
            throw e;
        } catch (Exception e) {
            ApplicationContextUtils.getOutput().output(path.getFileName().toString() + "下载失败："+e.getMessage());
            throw new FileDownloadException(path.getFileName().toString());
        } finally {
            connection.disconnect();

        }
    }*/

}
