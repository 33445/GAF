package com.supermap.gaf.data.mgt.test;

import com.supermap.gaf.data.mgt.Application;
import com.supermap.gaf.data.mgt.entity.CacheBuildPublishParam;
import com.supermap.gaf.data.mgt.entity.CacheBuildPublishParam2;
import com.supermap.gaf.data.mgt.entity.DatasetCacheBuildSetting;
import com.supermap.gaf.data.mgt.entity.PublishParamemter;
import com.supermap.gaf.data.mgt.entity.iserver.MongoDBMVTTileProviderConfig;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;
import com.supermap.gaf.data.mgt.enums.CacheTypeEnum;
import com.supermap.gaf.data.mgt.enums.StorageTypeEnum;
import com.supermap.gaf.data.mgt.service.CacheBuildHistoryService;
import com.supermap.gaf.data.mgt.service.CacheBuildService;
import com.supermap.gaf.data.mgt.service.PublishService;
import com.supermap.gaf.data.mgt.service.TileService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("local")
public class PublishResourceTest2 {

    @Autowired
    private PublishService publishService;

    @Autowired
    private CacheBuildService cacheBuildService;
    
    @Autowired
    private TileService tileService;

    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @Test
    public void testPublishNewCacheBuildInPublishHistory() {
        CacheBuildPublishParam2 param2 = new CacheBuildPublishParam2();
        param2.setPublishHistoryId("666be0f2-cb3b-449a-8704-16147e4bd150");
        List<DatasetCacheBuildSetting> settings = new ArrayList<>();

        DatasetCacheBuildSetting setting = new DatasetCacheBuildSetting();
        setting.setDatasourceId("38ad1f21-fc3f-417f-ac4c-79865cdf1c1b");
        setting.setDatasetName("GV01内部结构");
        setting.setIsSimp(false);
        setting.setCacheType(CacheTypeEnum.FULLVOLUME.getCode());
        setting.setTileType("MODEL");
        settings.add(setting);

        param2.setDatasetCacheBuildSettings(settings);

        param2.setIsPublishRestData(false);
        param2.setIsPublishAfterSlide(true);

        PublishCacheBuildHistorVo vo = publishService.publishNewCacheBuildInPublishHistory(param2);

        CompletableFuture<Void> allOf = vo.getAllOf();
        if (allOf != null) {
            System.out.println("等待发布结束");
            allOf.join();
            System.out.println("发布结束");

        }

        System.out.println("结束");
    }


    @Test
    public void testPublishMongodbOrWorkspace() {
        PublishParamemter publishParamemter = new PublishParamemter();
        publishParamemter.setPublishHistoryId("a9250257-bbbe-4f98-aaec-aa7661a8c01d");
        publishParamemter.setIsPublishRestData(false);
        publishService.publishMongodbOrWorkspace(publishParamemter);
        System.out.println("publishParamemter = " + publishParamemter);
    }

    @Test
    public void testPublishMongo() {

        MongoDBMVTTileProviderConfig param = new MongoDBMVTTileProviderConfig();
        param.setDatabase("test");
        param.setPassword("123456");

        String s = RandomStringUtils.random(4, true, true);
        param.setName("my_mongo_"+ s);
        param.setServerAdresses(new String[]{"192.168.192.100:30061"});
        param.setUsername("testadmin");
        param.setTilesetNames(new String[]{"GV01内部结构_RYOR"});
        List<PublishResult> re = tileService.publish3D(param);
        System.out.println("re = " + re);
    }


    @Test
    public void testPublishCacheBuild() {
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.MONGODB.value());
        String storageInfo = "{\"connectionName\":\"my_mongo\",\"username\":\"testadmin\",\"password\":\"123456\",\"port\":30061,\"host\":\"192.168.192.100\",\"database\":\"test\"}";
        cacheBuildPublishParam.setStorageInfo(storageInfo);
        cacheBuildPublishParam.setIsPublishRestData(false);
        cacheBuildPublishParam.setIsPublishAfterSlide(false);

        PublishCacheBuildHistorVo vo = publishService.publishCacheBuild(cacheBuildPublishParam);

        CompletableFuture<Void> allOf = vo.getAllOf();
        if (allOf != null) {
            System.out.println("等待发布任务完成");
            allOf.join();
            System.out.println("发布任务完成");
        }
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(System.currentTimeMillis() + "结束");

    }


    private CacheBuildPublishParam getCacheBuildPublishParam() {
        CacheBuildPublishParam cacheBuildPublishParam = new CacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.WORKSPACE.value());
        String workspaceName = "road";
        cacheBuildPublishParam.setStorageInfo("{\"workspaceName\": \""+ workspaceName +"\"}");
        cacheBuildPublishParam.setIsPublishRestData(false);
        cacheBuildPublishParam.setIsPublishAfterSlide(false);
        List<DatasetCacheBuildSetting> settings = new ArrayList<>();
        //DatasetCacheBuildSetting setting = new DatasetCacheBuildSetting();
        //setting.setDatasourceId("38ad1f21-fc3f-417f-ac4c-79865cdf1c1b");
        //setting.setDatasetName("GV01内部结构");
        //setting.setIsSimp(false);
        //setting.setCacheType(CacheTypeEnum.FULLVOLUME.getCode());
        //setting.setTileType("MODEL");
        //settings.add(setting);

        DatasetCacheBuildSetting setting = new DatasetCacheBuildSetting();
        setting.setDatasourceId("38ad1f21-fc3f-417f-ac4c-79865cdf1c1b");
        setting.setDatasetName("GV01_part1");
        setting.setIsSimp(false);
        setting.setCacheType(CacheTypeEnum.FULLVOLUME.getCode());
        setting.setTileType("MODEL");
        settings.add(setting);

        DatasetCacheBuildSetting setting2 = new DatasetCacheBuildSetting();
        setting2.setDatasourceId("38ad1f21-fc3f-417f-ac4c-79865cdf1c1b");
        setting2.setDatasetName("GV01_part2");
        setting2.setIsSimp(false);
        setting2.setCacheType(CacheTypeEnum.FULLVOLUME.getCode());
        setting2.setTileType("MODEL");
        settings.add(setting2);

        cacheBuildPublishParam.setDatasetCacheBuildSettings(settings);
        return cacheBuildPublishParam;
    }






}