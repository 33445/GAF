package com.supermap.gaf.data.mgt.test;

import com.supermap.gaf.data.mgt.Application;
import com.supermap.gaf.data.mgt.entity.*;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryLatestVO;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorTaskVo;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;
import com.supermap.gaf.data.mgt.enums.CacheTypeEnum;
import com.supermap.gaf.data.mgt.enums.StorageTypeEnum;
import com.supermap.gaf.data.mgt.service.CacheBuildHistoryService;
import com.supermap.gaf.data.mgt.service.CacheBuildService;
import com.supermap.gaf.data.mgt.service.PublishService;
import com.supermap.services.rest.management.ServiceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("local")
public class PublishResourceTest {

    @Autowired
    private PublishService publishService;

    @Autowired
    private CacheBuildService cacheBuildService;

    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @Test
    public void testGetLatestCacheStatus() {
        List<CacheBuildStatus> latestCacheStatus = publishService.getLatestCacheStatus("52d47155-a706-4e84-a9cd-22f3237a5dfe");

        System.out.println("latestCacheStatus = " + latestCacheStatus);

    }

    @Test
    public void testListLastest() {

        List<CacheBuildHistoryLatestVO> cacheBuildHistoryLatestVOS = cacheBuildHistoryService.listLastest("52d47155-a706-4e84-a9cd-22f3237a5dfe");
        System.out.println("cacheBuildHistoryLatestVOS = " + cacheBuildHistoryLatestVOS);
    }

    @Test
    public void publishDataWorkspace() {
        PublishParam publishParam  = new PublishParam();
        publishParam.setServicesTypes(new ServiceType[]{ServiceType.RESTMAP,ServiceType.RESTDATA});
        PublishHistory publishHistory = publishService.publishDataWorkspace("1bf7a7ed-e60c-457a-84eb-79dabd9ba596", publishParam);
        System.out.println("publishHistory = " + publishHistory);
    }

    @Test
    public void testCacheBuild2() {
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.MONGODB.value());
        String storageInfo = "{\"connectionName\":\"road\",\"username\":\"testadmin\",\"password\":\"123456\",\"port\":30061,\"host\":\"192.168.192.100\",\"database\":\"test\"}";
        cacheBuildPublishParam.setStorageInfo(storageInfo);
        PublishCacheBuildHistorTaskVo vo = cacheBuildService.cacheBuild(cacheBuildPublishParam);
        List<CompletableFuture<CacheBuildResult>> allFutures = vo.getAllFutures();
        // 等待切片任务结束
        System.out.println("vo = " + vo);
        System.out.println("等待所有切片任务结束");
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size()])).join();
        System.out.println("所有任务结束");
    }

    @Test
    public void testCacheBuild() {
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        PublishCacheBuildHistorTaskVo vo = cacheBuildService.cacheBuild(cacheBuildPublishParam);
        List<CompletableFuture<CacheBuildResult>> allFutures = vo.getAllFutures();
        // 等待切片任务结束
        System.out.println("vo = " + vo);
        System.out.println("等待所有切片任务结束");
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size()])).join();
        System.out.println("所有任务结束");
    }

    @Test
    public void publishScenceWithData3() {
        // 使用mongoDB 存储切片 并发布三维服务，但是数据服务,但是由于通过文件型工作空间发布,由于文件路径问题 会导致iserver找不到文件
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.MONGODB.value());
        String storageInfo = "{\"connectionName\":\"my_mongo_pulish\",\"username\":\"testadmin\",\"password\":\"123456\",\"port\":30061,\"host\":\"192.168.192.100\",\"database\":\"test\"}";
        cacheBuildPublishParam.setStorageInfo(storageInfo);
        cacheBuildPublishParam.setIsPublishRestData(true);
        cacheBuildPublishParam.setIsPublishAfterSlide(true);
        PublishCacheBuildHistorVo vo = publishService.publishCacheBuild(cacheBuildPublishParam);
        CompletableFuture<Void> allOf = vo.getAllOf();
        if (allOf != null) {
            System.out.println("等待发布任务完成");
            allOf.join();
            System.out.println("发布任务完成");
        }


    }

    @Test
    public void publishScenceWithData2() {
        // 使用mongoDB 存储切片 并发布三维服务，但是不发数据服务,
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.MONGODB.value());
        String storageInfo = "{\"connectionName\":\"my_mongo\",\"username\":\"testadmin\",\"password\":\"123456\",\"port\":30061,\"host\":\"192.168.192.100\",\"database\":\"test\"}";
        cacheBuildPublishParam.setStorageInfo(storageInfo);
        cacheBuildPublishParam.setIsPublishRestData(false);
        cacheBuildPublishParam.setIsPublishAfterSlide(true);
        PublishCacheBuildHistorVo vo = publishService.publishCacheBuild(cacheBuildPublishParam);
        CompletableFuture<Void> allOf = vo.getAllOf();
        if (allOf != null) {
            System.out.println("等待发布任务完成");
            allOf.join();
            System.out.println("发布任务完成");
        }


    }

    @Test
    public void publishScenceWithData() {
        // 由于文件路径原因导致发布失败，但是切片成功
        CacheBuildPublishParam cacheBuildPublishParam = getCacheBuildPublishParam();
        cacheBuildPublishParam.setIsPublishRestData(true);
        cacheBuildPublishParam.setIsPublishAfterSlide(true);
        PublishCacheBuildHistorVo vo = publishService.publishCacheBuild(cacheBuildPublishParam);
        CompletableFuture<Void> allOf = vo.getAllOf();
        if (allOf != null) {
            System.out.println("等待发布任务完成");
            allOf.join();
            System.out.println("发布任务完成");
        }


    }

    private CacheBuildPublishParam getCacheBuildPublishParam() {
        CacheBuildPublishParam cacheBuildPublishParam = new CacheBuildPublishParam();
        cacheBuildPublishParam.setStorageType(StorageTypeEnum.WORKSPACE.value());
        String workspaceName = "road";
        cacheBuildPublishParam.setStorageInfo("{\"workspaceName\": \""+ workspaceName +"\"}");
        cacheBuildPublishParam.setIsPublishRestData(false);
        cacheBuildPublishParam.setIsPublishAfterSlide(false);
        List<DatasetCacheBuildSetting> settings = new ArrayList<>();
        DatasetCacheBuildSetting setting = new DatasetCacheBuildSetting();
        //setting.setDatasourceId("1996db0a-689a-45e1-bd34-35c910ed4aff");
        setting.setDatasourceId("38ad1f21-fc3f-417f-ac4c-79865cdf1c1b");
        setting.setDatasetName("GV01内部结构");
        setting.setIsSimp(false);
        setting.setCacheType(CacheTypeEnum.FULLVOLUME.getCode());
        setting.setTileType("MODEL");
        settings.add(setting);

        cacheBuildPublishParam.setDatasetCacheBuildSettings(settings);
        return cacheBuildPublishParam;
    }

    @Test
    public void publishTileMap() {
        // 有问题
        String tileId = "b1133d1d-1933-4882-b67d-d02b8c7dfe80";

        PublishTileParam param = new PublishTileParam();
        param.setServiceTypes(new ServiceType[]{ServiceType.RESTMAP,ServiceType.RESTVECTORTILE});
        PublishHistory publishHistory = publishService.publishTileMap(tileId, param);
        System.out.println("publishHistory = " + publishHistory);

    }
}