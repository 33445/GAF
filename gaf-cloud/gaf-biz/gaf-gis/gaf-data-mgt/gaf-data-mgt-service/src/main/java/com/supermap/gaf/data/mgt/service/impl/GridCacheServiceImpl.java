package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.data.*;
import com.supermap.data.processing.CacheBuilderGridTerrain;
import com.supermap.data.processing.CacheProcessTools;
import com.supermap.data.processing.StorageType;
import com.supermap.gaf.data.mgt.service.DatasetTypeCacheService;
import com.supermap.gaf.data.mgt.util.ScenesUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import com.supermap.tilestorage.TileStorageConnection;
import org.slf4j.Logger;

import java.util.Map;
import java.util.function.Consumer;

/**
 * ClassName:GridCacheServiceImpl
 * Package:com.supermap.cim.data.service.impl
 * @description 地形数据集
 * @date:2022/1/24 9:07
 * @author:Yw
 */
public class GridCacheServiceImpl implements DatasetTypeCacheService {
    private static final Logger logger = LogUtil.getLocLogger(GridCacheServiceImpl.class);
    private String fileSuffix = ".sct";


    @Override
    public boolean executeS3MBuilder(Dataset dataset, String resultOutputFolder, String fileName, Map<String,Object> otherParam, Consumer<Integer> stepPercentHandler) {
        boolean isBuild = false;
        DatasetGrid gridDataset = (DatasetGrid)dataset;
        CacheBuilderGridTerrain builder = new CacheBuilderGridTerrain();
        SteppedListener steppedListener = steppedEvent -> {
            int percent = steppedEvent.getPercent();
            logger.info(String.format("切片完成百分比： %d%%", percent));
            try {
                stepPercentHandler.accept(percent);
            } catch (Exception e) {
                logger.error("切片完成百分比后置处理失败",e);
            }
        };
        try {
            builder.setStorageType(StorageType.Compact);
            builder.setProcessThreadsCount(16);
            builder.setEncodeType(EncodeType.NONE);
            builder.setDataset(gridDataset);
            builder.setBounds(gridDataset.getBounds());
            builder.setOutputFolder(resultOutputFolder);
            builder.setCacheName(fileName);
            builder.addSteppedListener(steppedListener);
            isBuild = builder.build();
            Object obj = otherParam.get("workspaceConnectionInfo");
            if (null != obj) {
                String datasetPath = new StringBuilder()
                        .append(resultOutputFolder)
                        .append("/")
                        .append(fileName)
                        .append("/")
                        .append(fileName)
                        .append(fileSuffix)
                        .toString();
                WorkspaceConnectionInfo  workspaceConnectionInfo = (WorkspaceConnectionInfo)obj;
                ScenesUtils.addGridLayer(fileName,workspaceConnectionInfo,datasetPath);
            }
            return isBuild;
        }catch (Exception e) {
            logger.info("地形切片缓存失败，原因：" + e.getMessage());
            throw new GafException(e);
        } finally {
            builder.removeSteppedListener(steppedListener);
            builder.dispose();
        }
    }


    @Override
    public boolean writeToMongo(String configFile, TileStorageConnection ts, String datasetName) {
        String realConfigFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        String configMongoFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append("new_")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        return CacheProcessTools.terrainCacheToMongoDB(realConfigFile, ts, 3, configMongoFile);
    }
}
