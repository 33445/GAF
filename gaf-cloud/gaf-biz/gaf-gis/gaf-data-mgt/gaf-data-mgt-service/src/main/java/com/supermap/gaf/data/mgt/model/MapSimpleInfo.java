package com.supermap.gaf.data.mgt.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 工作空间下地图简略信息
 * @author wxl
 * @since 2021/9/14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapSimpleInfo {
    /**
     * 地图名
     */
    @ApiModelProperty("地图名")
    private String name;
    /**
     * 当前比例尺
     */
    @ApiModelProperty("当前比例尺")
    private double scale;

    /**
     * 坐标系名
     */
    @ApiModelProperty("坐标系名")
    private String coordSysName;
    /**
     * 坐标单位
     */
    @ApiModelProperty("坐标单位")
    private String unit;
}
