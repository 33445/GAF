package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;

/**
 * ClassName:ServerInfo
 * Package:com.supermap.cim.data.commontype
 *
 * @date:2022/1/21 13:42
 * @author:Yw
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("切片缓存连接信息")
public class ServerInfo {
    @QueryParam("username")
    @ApiModelProperty("用户名")
    private String username;
    @QueryParam("password")
    @ApiModelProperty("密码")
    private String password;
    @QueryParam("host")
    @ApiModelProperty("host")
    private String host;
    @QueryParam("port")
    @ApiModelProperty("端口")
    private Integer port;
    @QueryParam("database")
    @ApiModelProperty("数据库名称")
    private String database;
    @QueryParam("filePath")
    @ApiModelProperty("文件型工作空间路径")
    private String filePath;
}
