package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DqProgramRule;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.service.DqProgramRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 数据质量管理-方案-规则联系表接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据质量管理-方案-规则联系表接口")
public class DqProgramRuleResource{
    @Autowired
    private DqProgramRuleService dqProgramRuleService;
	

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "通过id查询数据质量管理-方案-规则联系表", notes = "通过id查询数据质量管理-方案-规则联系表")
	@Path("/{dqProgramRuleId}")
    public MessageResult<DqProgramRule> getById(@PathParam("dqProgramRuleId")String dqProgramRuleId){
        DqProgramRule dqProgramRule = dqProgramRuleService.getById(dqProgramRuleId);
		return MessageResult.successe(DqProgramRule.class).data(dqProgramRule).status(200).message("查询成功").build();
    }
	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据质量管理-方案-规则联系表", notes = "分页条件查询数据质量管理-方案-规则联系表")
    public MessageResult<Page> pageList(@BeanParam DqProgramRuleSelectVo dqProgramRuleSelectVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DqProgramRuleVo> page = dqProgramRuleService.listByPageCondition(dqProgramRuleSelectVo, pageNum, pageSize);
		return MessageResult.successe(Page.class).data(page).status(200).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据质量管理-方案-规则联系表", notes = "新增数据质量管理-方案-规则联系表")
    public MessageResult<Void> insertDqProgramRule(DqProgramRule dqProgramRule){
        dqProgramRuleService.insertDqProgramRule(dqProgramRule);
		return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/batch")
    @ApiOperation(value = "批量新增数据质量管理-方案-规则联系表", notes = "批量新增数据质量管理-方案-规则联系表")
    public MessageResult<Void> batchInsert(List<DqProgramRule> DqProgramRules){
        dqProgramRuleService.batchInsert(DqProgramRules);
		return MessageResult.successe(Void.class).status(200).message("批量新增操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据质量管理-方案-规则联系表", notes = "根据id删除数据质量管理-方案-规则联系表")
	@Path("/{dqProgramRuleId}")
    public MessageResult<Void> deleteDqProgramRule(@PathParam("dqProgramRuleId")String dqProgramRuleId){
        dqProgramRuleService.deleteDqProgramRule(dqProgramRuleId);
		return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据质量管理-方案-规则联系表", notes = "批量删除数据质量管理-方案-规则联系表")
    public MessageResult<Void> batchDelete(List<String> dqProgramRuleIds){
        dqProgramRuleService.batchDelete(dqProgramRuleIds);
		return MessageResult.successe(Void.class).status(200).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据质量管理-方案-规则联系表", notes = "根据id更新数据质量管理-方案-规则联系表")
	@Path("/{dqProgramRuleId}")
    public MessageResult<Void> updateDqProgramRule(DqProgramRule dqProgramRule, @PathParam("dqProgramRuleId")String dqProgramRuleId){
        dqProgramRule.setDqProgramRuleId(dqProgramRuleId);
        dqProgramRuleService.updateDqProgramRule(dqProgramRule);
		return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

}
