package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.Tile;
import com.supermap.gaf.data.mgt.entity.iserver.MongoDBMVTTileProviderConfig;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.TileSelectVo;
import com.supermap.gaf.data.mgt.model.MongoDBTileSourceInfo;
import com.supermap.gaf.data.mgt.model.TileDetailInfo;

import java.util.List;

/**
 * 瓦片服务类
 * @author wxl 
 * @date yyyy-mm-dd
 */
public interface TileService {
	
	/**
    * 根据id查询瓦片
    * @return
    */
    Tile getById(String tileId);

    /**
     * 多条件查询
     * @param tileSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
    List<Tile> selectList(TileSelectVo tileSelectVo);
	
	/**
     * 分页条件查询
     * @param tileSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<Tile> listByPageCondition(TileSelectVo tileSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增瓦片
    * @return 新增的tile
    */
    Tile insertTile(Tile tile);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<Tile> tiles);

    /**
    * 删除瓦片
    * 
    */
    Tile deleteTile(String tileId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> tileIds);
    /**
    * 更新瓦片
    * @return 更新后的tile
    */
    Tile updateTile(Tile tile);

    /**
     * 查询UGCV5瓦片列表
     * @param path
     * @return
     */
    List<String> listUgcv5TilesetNames(String path);

    /**
     * 查询来源于mongodb的所有瓦片名
     * @param info mongodb连接信息
     * @return 所有瓦片名
     */
    List<String> listMongoTilesetNames(MongoDBTileSourceInfo info);



    /**
     * 根据瓦片id获取瓦片详细
     * @param tileId 瓦片id
     */
    TileDetailInfo getDetail(String tileId);

    List<PublishResult> publishMap(String tileId, PublishTileParam param);

    List<PublishResult> publish3D(MongoDBMVTTileProviderConfig  param);

    List<PublishResult> publish3D(String tileId);
}
