package com.supermap.gaf.data.mgt.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.supermap.gaf.data.mgt.jackson.CustomDurationSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-执行统计结果")
public class DqExecuteResult {

    @ApiModelProperty("执行结果id")
    private String dqExecuteResultId;
    @ApiModelProperty("数据总量")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long totalAmount;
    @ApiModelProperty("失败数据总量")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long failedAmount;
    @NotNull
    @ApiModelProperty("耗费时间-秒")
    @JsonSerialize(using= CustomDurationSerializer.class)
    private Long costTime;
    @ApiModelProperty("执行状态。0代表成功，1代表失败。")
    private Integer resultStatus;
    @ApiModelProperty("创建时间")
    private Date createdTime;
}
