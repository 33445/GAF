package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据质量管理-模型表数据访问类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface DataQualityModelMapper{
	/**
     * 根据主键 dataQualityModelId查询
     * 
	 */
    DataQualityModel select(@Param("dataQualityModelId") String dataQualityModelId);
	
	/**
     * 多条件查询
     * @param dataQualityModelSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<DataQualityModel> selectList(DataQualityModelSelectVo dataQualityModelSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(DataQualityModel dataQualityModel);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<DataQualityModel> dataQualityModels);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> dataQualityModelIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("dataQualityModelId") String dataQualityModelId);
    /**
     * 更新
     *
	 */
    int update(DataQualityModel dataQualityModel);
}
