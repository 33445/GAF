package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelVo;

import java.util.List;

/**
 * 数据质量管理-模型表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataQualityModelService {
	
	/**
    * 根据id查询数据质量管理-模型表
    * @return
    */
    DataQualityModelVo getById(String dataQualityModelId);
	
	/**
     * 分页条件查询
     * @param dataQualityModelSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
    Page<DataQualityModelVo> listByPageCondition(DataQualityModelSelectVo dataQualityModelSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增数据质量管理-模型表
    * @return 新增的dataQualityModel
    */
    DataQualityModel insertDataQualityModel(DataQualityModel dataQualityModel);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<DataQualityModel> dataQualityModels);

    /**
    * 删除数据质量管理-模型表
    * 
    */
    void deleteDataQualityModel(String dataQualityModelId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dataQualityModelIds);
    /**
    * 更新数据质量管理-模型表
    * @return 更新后的dataQualityModel
    */
    DataQualityModel updateDataQualityModel(DataQualityModel dataQualityModel);

    List<DataQualityRule> rules(String dataQualityModelId);
}
