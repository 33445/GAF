package com.supermap.gaf.data.mgt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 瓦片
 * @author wxl 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("瓦片")
public class Tile implements Serializable{
    @NotNull
    @ApiModelProperty("瓦片id")
    private String tileId;
    @NotNull
    @ApiModelProperty("瓦片名")
    private String tileName;
    @NotNull
    @ApiModelProperty("瓦片别名")
    private String tileAlias;
    @NotNull
    @ApiModelProperty("瓦片来源类型")
    private String sourceType;
    @NotNull
    @ApiModelProperty("瓦片来源信息")
    private String sourceInfo;
    @ApiModelProperty("描述")
    private String description;
    /**
    * 默认值1：CURRENT_TIMESTAMP
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("修改时间")
    private Date updatedTime;
    @ApiModelProperty("修改人")
    private String updatedBy;
}