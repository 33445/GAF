package com.supermap.gaf.data.mgt.model;

import lombok.Data;

@Data
public class TileSourceInfo extends MongoDBTileSourceInfo {
    private String path;
}
