package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataRange;
import com.supermap.gaf.data.mgt.entity.vo.DataRangeSelectVo;
import com.supermap.gaf.data.mgt.mapper.DataRangeMapper;
import com.supermap.gaf.data.mgt.service.DataRangeService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * 数据标准值域范围服务实现类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Service
public class DataRangeServiceImpl implements DataRangeService{
    
	private static final Logger  log = LoggerFactory.getLogger(DataRangeServiceImpl.class);
	
	@Autowired
    private DataRangeMapper dataRangeMapper;
	
	@Override
    public DataRange getById(String dataRangeId){
        if(dataRangeId == null){
            throw new IllegalArgumentException("dataRangeId不能为空");
        }
        return  dataRangeMapper.select(dataRangeId);
    }
	
	@Override
    public Page<DataRange> listByPageCondition(DataRangeSelectVo dataRangeSelectVo, int pageNum, int pageSize) {
        PageInfo<DataRange> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataRangeMapper.selectList(dataRangeSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
	public 	List<DataRange> selectList(DataRangeSelectVo dataRangeSelectVo){
		return dataRangeMapper.selectList(dataRangeSelectVo);
	}

	@Override
    public void insertDataRange(DataRange dataRange){
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		dataRange.setDataRangeId(UUID.randomUUID().toString());
		
        String userName = SecurityUtilsExt.getUserName();
        dataRange.setCreatedBy(userName);
		dataRange.setUpdatedBy(userName);
        dataRangeMapper.insert(dataRange);
    }
	
	@Override
    public int deleteDataRange(String dataRangeId){
		return batchDelete(Arrays.asList(dataRangeId));
    }

	@Override
    public int batchDelete(Collection<String> dataRangeIds){
        if(!CollectionUtils.isEmpty(dataRangeIds)){
            return dataRangeMapper.batchDelete(dataRangeIds);
        }
        return 0;
    }

    @Override
    public int batchDeleteByDataStandardIds(Collection<String> dataStandardIds) {
	    if(!CollectionUtils.isEmpty(dataStandardIds)){
            return dataRangeMapper.batchDeleteByDataStandardIds(dataStandardIds);
        }
	    return 0;
    }

    @Override
    public int updateDataRange(DataRange dataRange){
		dataRange.setUpdatedBy(SecurityUtilsExt.getUserName());
		return dataRangeMapper.update(dataRange);
    }

    @Override
    public int batchInsert(List<DataRange> dataRanges) {
	    if(dataRanges!=null){
            String userName = SecurityUtilsExt.getUserName();
            for(DataRange item:dataRanges){
                item.setDataRangeId(UUID.randomUUID().toString());
                item.setCreatedBy(userName);
                item.setUpdatedBy(userName);
            }
            dataRangeMapper.batchInsert(dataRanges);
        }
        return 0;
    }

}
