package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.data.mgt.entity.vo.PublishHistoryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description 工作空间和瓦片
 * @date:2022/1/26 17:31
 * @author:yw
 */
@Mapper
@Component
public interface PublishHistoryMapper {
    /**
     * 根据主键 id 查询
     *
     */
    PublishHistory selectById(@Param("id") String id);

    /**
     * 多条件查询
     * @param publishHistoryVo 查询条件
     * @return 若未查询到则返回空集合
     */
    List<PublishHistory> pageList(PublishHistoryVo publishHistoryVo);

    /**
     * 新增
     *
     */
    int insert(PublishHistory publishHistory);

    /**
     * 刪除
     *
     */
    int delete(@Param("id") String workspaceAndTileId);

    /**
     * 更新
     *
     **/
    int update(PublishHistory publishHistory);
}
