package com.supermap.gaf.data.mgt.service;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataCode;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeSelectVo;
import java.util.*;

/**
 * 数据代码服务类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
public interface DataCodeService {

	/**
	 * id查询数据代码
	 *
	 * @param dataCodeId the data code id
	 * @return by id
	 */
	DataCode getById(String dataCodeId);

	/**
	 * 分页条件查询
	 *
	 * @param dataCodeSelectVo 查询条件
	 * @param pageNum          当前页数
	 * @param pageSize         页面大小
	 * @return 分页对象 page
	 */
	Page<DataCode> listByPageCondition(DataCodeSelectVo dataCodeSelectVo, int pageNum, int pageSize);

	/**
	 * 多条件查询
	 *
	 * @param dataCodeSelectVo 查询条件
	 * @return 若未查询到则返回空集合 list
	 */
	List<DataCode> selectList(DataCodeSelectVo dataCodeSelectVo);


	/**
	 * 新增数据代码
	 *
	 * @param dataCode the data code
	 * @return
	 */
	void insertDataCode(DataCode dataCode);

	/**
	 * 删除数据代码
	 *
	 * @param dataCodeId the data code id
	 * @return the int
	 */
	int deleteDataCode(String dataCodeId);

	/**
	 * 批量删除
	 *
	 * @param dataCodeIds the data code ids
	 * @return the int
	 */
	int batchDelete(List<String> dataCodeIds);

	/**
	 * 更新数据代码
	 *
	 * @param dataCode the data code
	 * @return int int
	 */
	int updateDataCode(DataCode dataCode);

	/**
	 * All nodes list.
	 *
	 * @return the list
	 */
	List<TreeNode> allNodes();
}
