package com.supermap.gaf.data.mgt.service.impl;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetImage;
import com.supermap.data.SteppedListener;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.processing.CacheImageType;
import com.supermap.data.processing.CacheProcessTools;
import com.supermap.data.processing.ImageCacheBuilder;
import com.supermap.data.processing.StorageType;
import com.supermap.gaf.data.mgt.service.DatasetTypeCacheService;
import com.supermap.gaf.data.mgt.util.ScenesUtils;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import com.supermap.realspace.Layer3DType;
import com.supermap.tilestorage.TileStorageConnection;
import org.slf4j.Logger;

import java.util.Map;
import java.util.function.Consumer;


/**
 * ClassName:ImageCacheServiceImpl
 * Package:com.supermap.cim.data.service.impl
 * @description 影像数据集
 * @date:2022/1/24 9:10
 * @author:Yw
 */
public class ImageCacheServiceImpl  implements DatasetTypeCacheService {
    private static final Logger logger = LogUtil.getLocLogger(ImageCacheServiceImpl.class);
    private String fileSuffix = ".sci3d";
    @Override
    public boolean executeS3MBuilder(Dataset dataset, String resultOutputFolder, String fileName, Map<String,Object> otherParam, Consumer<Integer> stepPercentHandler) {
        ImageCacheBuilder builder = new ImageCacheBuilder();
        SteppedListener steppedListener = steppedEvent -> {
            logger.info(String.format("切片完成百分比： %d%%", steppedEvent.getPercent()));
            try {
                stepPercentHandler.accept(steppedEvent.getPercent());
            } catch (Exception e) {
                logger.error("切片完成百分比后置处理失败",e);
            }
        };
        boolean isBuild = false;
        try {
            builder.setStorageType(StorageType.Original);
            DatasetImage datasetImage = (DatasetImage) dataset;
            builder.setDataset(datasetImage);
            builder.setStorageType(StorageType.Original);
            builder.setOutputFolder(resultOutputFolder);
            builder.setCacheName(fileName);
            builder.setBeginLevel(0);
            builder.setEndLevel(3);
            builder.setProcessThreadsCount(4);
            builder.setImageType(CacheImageType.WEBP);
            builder.addSteppedListener(steppedListener);
            isBuild = builder.build();
            Object obj = otherParam.get("workspaceConnectionInfo");
            if (null != obj) {
                String datasetPath = new StringBuilder()
                        .append(resultOutputFolder)
                        .append("/")
                        .append(fileName)
                        .append("/")
                        .append(fileName)
                        .append(fileSuffix)
                        .toString();
                WorkspaceConnectionInfo workspaceConnectionInfo = (WorkspaceConnectionInfo)obj;
                ScenesUtils.addLayer(fileName,workspaceConnectionInfo,datasetPath,Layer3DType.IMAGEFILE);
            }
            return isBuild;
        } catch (Exception e) {
            logger.error("影像切片缓存失败，原因：" + e.getMessage());
            throw new GafException(e);
        } finally {
            builder.removeSteppedListener(steppedListener);
            builder.dispose();
        }
    }

    @Override
    public boolean writeToMongo(String configFile, TileStorageConnection ts, String datasetName) {
        String realConfigFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        String configMongoFile = new StringBuilder()
                .append(configFile)
                .append("/")
                .append("new_")
                .append(datasetName)
                .append(fileSuffix)
                .toString();
        return  CacheProcessTools.imageCacheToMongoDB(realConfigFile, ts, configMongoFile);
    }
}
