package com.supermap.gaf.data.mgt.entity.vo;

import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据质量管理-规则表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-规则表条件查询实体")
public class DataQualityRuleSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = DataQualityRule.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataQualityRule.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataQualityRule.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("dataQualityRuleId")
    @ApiModelProperty("主键")
    private String dataQualityRuleId;
    @QueryParam("dataQualityRuleName")
    @ApiModelProperty("数据质量管理规则名称")
    private String dataQualityRuleName;
    @QueryParam("dataQualityModelId")
    @ApiModelProperty("所属模型id")
    private String dataQualityModelId;
    @QueryParam("typeCode")
    @ApiModelProperty("规则种类code")
    private String typeCode;
    @QueryParam("importance")
    @ApiModelProperty("严重级别")
    private String importance;
    @QueryParam("params")
    @ApiModelProperty("参数")
    private String params;
    @QueryParam("sortSn")
    @ApiModelProperty("排序")
    private Integer sortSn;
    @QueryParam("description")
    @ApiModelProperty("描述")
    private String description;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}