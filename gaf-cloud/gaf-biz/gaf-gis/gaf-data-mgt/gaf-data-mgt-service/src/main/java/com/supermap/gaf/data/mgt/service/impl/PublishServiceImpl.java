package com.supermap.gaf.data.mgt.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.supermap.data.*;
import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.entity.*;
import com.supermap.gaf.data.mgt.entity.iserver.MongoDBMVTTileProviderConfig;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishResult;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorTaskVo;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;
import com.supermap.gaf.data.mgt.enums.CacheBuildStatusEnum;
import com.supermap.gaf.data.mgt.enums.PublishHistoryStateEnum;
import com.supermap.gaf.data.mgt.enums.SourceTypeEnum;
import com.supermap.gaf.data.mgt.service.*;
import com.supermap.gaf.data.mgt.support.ConvertHelper;
import com.supermap.gaf.data.mgt.support.TtlContextHolder;
import com.supermap.gaf.data.mgt.wrapper.MyHttpServletRequestWrapper;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.utils.GlobalJacksonObjectMapper;
import com.supermap.gaf.webgis.client.WebgisServiceClient;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.services.rest.management.ServiceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.File;
import java.lang.Enum;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * @author wxl
 * @since 2022/5/27
 */
@Service
public class PublishServiceImpl implements PublishService {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PublishServiceImpl.class);


    @Autowired
    private DataWorkspaceService dataWorkspaceService;

    @Autowired
    private PublishHistoryService publishHistoryService;

    @Autowired
    private CacheBuildService cacheBuildService;

    @Autowired
    private TileService tileService;

    @Autowired
    private CacheBuildHistoryService cacheBuildHistoryService;

    @Autowired
    private WebgisServiceClient webgisServiceClient;

    @Autowired
    private SysResourceDatasourceService sysResourceDatasourceService;

    @Autowired
    private ConvertHelper convertHelper;

    @Autowired
    @Qualifier("DatamgtStorageClient")
    private StorageClient storageClient;


    @Autowired
    @Qualifier("taskExecutor")
    private Executor executor;

    private String filePathPrefix = "data/workspace/for_restdata";



    @Transactional
    @Override
    public PublishHistory publishDataWorkspace(String workspaceId, PublishParam publishParam) {

        DataWorkspace dataWorkspace = dataWorkspaceService.getById(workspaceId);

        PublishHistory publishHistory = new PublishHistory();
        publishHistory.setState(PublishHistoryStateEnum.NEW.getCode());
        publishHistory.setSourceName(dataWorkspace.getWsName());
        publishHistory.setSourceInfo("{workspaceId: \"" + dataWorkspace.getWorkspaceId() + "\"}");
        publishHistory.setSourceType(SourceTypeEnum.MANAGED_WORKSPACE.value());

        ServiceType[] servicesTypes = publishParam.getServicesTypes();
        if (servicesTypes != null && servicesTypes.length > 0)  {
            List<String> collect = Arrays.stream(servicesTypes).map(Enum::toString).collect(Collectors.toList());
            publishHistory.setServiceType(String.join(",",collect));
        }
        String setting = null;
        try {
            setting = GlobalJacksonObjectMapper.instance().writeValueAsString(publishParam);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
        publishHistory.setAdditionalSetting(setting);
        publishHistoryService.insert(publishHistory);
        try {
            List<PublishResult> publishResults = dataWorkspaceService.publish(workspaceId, publishParam);
            String publishResultsJsonStr = GlobalJacksonObjectMapper.instance().writeValueAsString(publishResults);

            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(publishResultsJsonStr);
            update.setState(PublishHistoryStateEnum.PUBLISHED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
            registryService(publishResults);
        } catch (Exception e) {
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(e.getMessage());
            update.setState(PublishHistoryStateEnum.PUBLISH_FAILED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
        }
        return publishHistoryService.selectById(publishHistory.getId());
    }

    @Transactional
    @Override
    public PublishCacheBuildHistorVo publishCacheBuild(CacheBuildPublishParam cacheBuildPublishParam) {
        PublishCacheBuildHistorTaskVo vo = cacheBuildService.cacheBuild(cacheBuildPublishParam);

        PublishCacheBuildHistorVo publishCacheBuildHistorVo = vo.getPublishCacheBuildHistorVo();
        PublishHistory publishHistory = publishCacheBuildHistorVo.getPublishHistory();

        Boolean isPublishRestData = cacheBuildPublishParam.getIsPublishRestData();
        Boolean isPublishAfterSlide = cacheBuildPublishParam.getIsPublishAfterSlide();

        if (isPublishAfterSlide) {
            List<CompletableFuture<CacheBuildResult>> allFutures = vo.getAllFutures();
            CompletableFuture<CacheBuildResult>[] arrays = allFutures.toArray(new CompletableFuture[allFutures.size()]);
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes copy = new ServletRequestAttributes(new MyHttpServletRequestWrapper(requestAttributes.getRequest()));
            TtlContextHolder.context.set(copy);
            CompletableFuture<Void> voidCompletableFuture = CompletableFuture.allOf(arrays).whenCompleteAsync((unused, throwable) -> {
                PublishParamemter publishParamemter = new PublishParamemter();
                publishParamemter.setPublishHistoryId(publishHistory.getId());
                publishParamemter.setIsPublishRestData(isPublishRestData);
                RequestContextHolder.setRequestAttributes(TtlContextHolder.context.get());
                try {
                    publishMongodbOrWorkspace(publishParamemter);
                } finally {
                    RequestContextHolder.resetRequestAttributes();
                }
            },executor);
            // 用于测试，需要删除
            publishCacheBuildHistorVo.setAllOf(voidCompletableFuture);
        }

        PublishHistory update2 = new PublishHistory();
        update2.setServiceType(ServiceType.RESTREALSPACE.name());
        Map<String,Object> additionalSettingMap  = new HashMap<>();
        additionalSettingMap.put("isPublishRestData",isPublishRestData);
        additionalSettingMap.put("isPublishAfterSlide",isPublishAfterSlide);
        update2.setAdditionalSetting(JSONObject.toJSONString(additionalSettingMap));
        publishHistoryService.update(update2);

        return publishCacheBuildHistorVo;
    }


    @Override
    public PublishCacheBuildHistorVo publishNewCacheBuildInPublishHistory(CacheBuildPublishParam2 cacheBuildPublishParam2) {

        PublishCacheBuildHistorTaskVo vo = cacheBuildService.cacheBuildInPublishHistory(cacheBuildPublishParam2);
        PublishCacheBuildHistorVo publishCacheBuildHistorVo = vo.getPublishCacheBuildHistorVo();

        String publishHistoryId = cacheBuildPublishParam2.getPublishHistoryId();
        PublishHistory publishHistory = publishHistoryService.selectById(publishHistoryId);

        Boolean isPublishAfterSlide = cacheBuildPublishParam2.getIsPublishAfterSlide();
        Boolean isPublishRestData = cacheBuildPublishParam2.getIsPublishRestData();

        if (isPublishAfterSlide) {
            List<CompletableFuture<CacheBuildResult>> allFutures = vo.getAllFutures();
            CompletableFuture<CacheBuildResult>[] arrays = allFutures.toArray(new CompletableFuture[allFutures.size()]);
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes copy = new ServletRequestAttributes(new MyHttpServletRequestWrapper(requestAttributes.getRequest()));
            TtlContextHolder.context.set(copy);
            CompletableFuture<Void> allOf = CompletableFuture.allOf(arrays).whenCompleteAsync((unused, throwable) -> {
                PublishParamemter publishParamemter = new PublishParamemter();
                publishParamemter.setPublishHistoryId(publishHistory.getId());
                publishParamemter.setIsPublishRestData(isPublishRestData);
                RequestContextHolder.setRequestAttributes(TtlContextHolder.context.get());
                try {
                    publishMongodbOrWorkspace(publishParamemter);
                } finally {
                    RequestContextHolder.resetRequestAttributes();
                }

            });
            publishCacheBuildHistorVo.setAllOf(allOf);
        }

        return publishCacheBuildHistorVo;
    }

    @Transactional
    @Override
    public PublishHistory publishTileMap(String tileId, PublishTileParam param) {
        Tile tile = tileService.getById(tileId);
        if (tile == null) throw new IllegalArgumentException("tileId不能为空");

        PublishHistory publishHistory = new PublishHistory();
        publishHistory.setState(PublishHistoryStateEnum.NEW.getCode());
        publishHistory.setSourceName(tile.getTileName());
        publishHistory.setSourceInfo("{tileId: \"" + tileId + "\"}");
        publishHistory.setSourceType(SourceTypeEnum.MANAGED_TILE.value());

        ServiceType[] servicesTypes = param.getServiceTypes();
        if (servicesTypes != null && servicesTypes.length > 0)  {
            List<String> collect = Arrays.stream(servicesTypes).map(Enum::toString).collect(Collectors.toList());
            publishHistory.setServiceType(String.join(",",collect));
        }
        String setting = null;
        try {
            setting = GlobalJacksonObjectMapper.instance().writeValueAsString(param);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
        publishHistory.setAdditionalSetting(setting);
        publishHistoryService.insert(publishHistory);

        try {
            List<PublishResult> publishResults = tileService.publishMap(tileId, param);

            String publishResultsJsonStr = GlobalJacksonObjectMapper.instance().writeValueAsString(publishResults);

            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(publishResultsJsonStr);
            update.setState(PublishHistoryStateEnum.PUBLISHED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);

            registryService(publishResults);

        } catch (Exception e) {
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(e.getMessage());
            update.setState(PublishHistoryStateEnum.PUBLISH_FAILED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
        }
        return publishHistoryService.selectById(publishHistory.getId());
    }

    @Override
    public List<CacheBuildStatus> getLatestCacheStatus(String publishHistoryId) {
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryService.listLastests(publishHistoryId);
        List<String> cacheBuildHistoryIds = cacheBuildHistories.stream().map(CacheBuildHistory::getId).collect(Collectors.toList());
        List<CacheBuildStatus> cacheSatus = cacheBuildService.getCacheSatus(cacheBuildHistoryIds);
        return cacheSatus;
    }

    @Override
    public void publishMongodbOrWorkspace(PublishParamemter publishParamemter) {
        String publishHistoryId = publishParamemter.getPublishHistoryId();
        PublishHistory publishHistory = publishHistoryService.selectById(publishHistoryId);
        if (publishHistory == null) throw  new IllegalArgumentException("未找到发布历史");
        Boolean isPublishRestData = publishParamemter.getIsPublishRestData();
        Integer state = publishHistory.getState();
        String sourceType = publishHistory.getSourceType();
        List<CacheBuildHistory> cacheBuildHistories = cacheBuildHistoryService.listByPublishHistoryId(publishHistoryId);
        if (PublishHistoryStateEnum.NEW.getCode() == state || PublishHistoryStateEnum.PUBLISH_FAILED.getCode() == state) {
            // 从未发布过
            if (SourceTypeEnum.FILE_WORKSPACE_FOR_STORAGE_TILE.value().equals(sourceType)) {
                // 全部发布数据服务，不论切片是否成功
                publishWorkspace(publishHistory, isPublishRestData, cacheBuildHistories);
            } else if (SourceTypeEnum.MONGODB.value().equals(sourceType)) {
                // 切片成功的瓦片才发布
                List<String> tileNames = cacheBuildHistories.stream().filter(cacheBuildHistory -> cacheBuildHistory.getStatus() == CacheBuildStatusEnum.SUCCESSED.getCode()).map(CacheBuildHistory::getStorageName).collect(Collectors.toList());
                // 全部发布数据服务，不论切片是否成功
                pulishMongoDB(publishHistory,isPublishRestData,tileNames,cacheBuildHistories);
            }
        } else if (PublishHistoryStateEnum.PUBLISHED.getCode() == state ){
            // 已经发布过了
            if (SourceTypeEnum.MONGODB.value().equals(sourceType)) {
                // 切片成功的瓦片才发布
                List<String> tileNames = cacheBuildHistories.stream().filter(cacheBuildHistory -> cacheBuildHistory.getStatus() == CacheBuildStatusEnum.SUCCESSED.getCode()).map(CacheBuildHistory::getStorageName).collect(Collectors.toList());
                String sourceName = publishHistory.getSourceName();
                publishHistoryService.mongoUpdateService(sourceName,tileNames.toArray(new String[0]));
            }
        }

    }


    private void pulishMongoDB(PublishHistory publishHistory,Boolean isPublishRestData,List<String> tileNames,List<CacheBuildHistory> cacheBuildHistorys) {
        String sourceInfo = publishHistory.getSourceInfo();
        ServerInfo serverInfo = JSONObject.parseObject(sourceInfo, ServerInfo.class);
        String name = publishHistory.getSourceName();
        try {
            List<PublishResult> publishResults = publish3DTileInMongoDb(serverInfo, tileNames.toArray(new String[tileNames.size()]), name);
            if (isPublishRestData) {
                List<PublishResult> restDatResults = publishRestData(publishHistory.getSourceName(), cacheBuildHistorys);
                publishResults.addAll(restDatResults);
            }
            String publishResultsJsonStr = GlobalJacksonObjectMapper.instance().writeValueAsString(publishResults);
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(publishResultsJsonStr);
            update.setState(PublishHistoryStateEnum.PUBLISHED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
            registryService(publishResults);
        } catch (Exception e) {
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(e.getMessage());
            update.setState(PublishHistoryStateEnum.PUBLISH_FAILED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
        }
    }

    private void registryService(List<PublishResult> publishResults) {
        if (publishResults == null || publishResults.isEmpty()) return;
        try {

            // 注册服务，不影响发布
            for (PublishResult publishResult : publishResults) {
                WebgisService webgisService = new WebgisService();
                webgisService.setAddress(publishResult.getServiceAddress());
                webgisService.setTypeCode(publishResult.getServiceType().name());
                webgisServiceClient.insertWebgisService(webgisService, "batch", null, null);
            }
        } catch (Exception e) {
            logger.error("注册服务失败", e);
        }
    }

    private void publishWorkspace(PublishHistory publishHistory,Boolean isPublishRestData,List<CacheBuildHistory> cacheBuildHistorys) {
        String sourceInfo = publishHistory.getSourceInfo();
        JSONObject jsonObject = JSONObject.parseObject(sourceInfo);
        String workspaceId = jsonObject.getString("workspaceId");

        PublishParam publishParam  = new PublishParam();
        publishParam.setServicesTypes(new ServiceType[]{ServiceType.RESTREALSPACE});
        try {
            List<PublishResult> publishResults = dataWorkspaceService.publish(workspaceId, publishParam);
            if (isPublishRestData) {
                List<PublishResult> restDatResults = publishRestData(publishHistory.getSourceName(), cacheBuildHistorys);
                publishResults.addAll(restDatResults);
            }
            String publishResultsJsonStr = GlobalJacksonObjectMapper.instance().writeValueAsString(publishResults);
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(publishResultsJsonStr);
            update.setState(PublishHistoryStateEnum.PUBLISHED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
            registryService(publishResults);
        } catch (Exception e) {
            // 更新publishHistory 状态
            PublishHistory update = new PublishHistory();
            update.setResult(e.getMessage());
            update.setState(PublishHistoryStateEnum.PUBLISH_FAILED.getCode());
            update.setId(publishHistory.getId());
            publishHistoryService.update(update);
        }
    }

    private List<PublishResult> publish3DTileInMongoDb(ServerInfo serverInfo,String[]tilesetNames,String name ){
        String username = serverInfo.getUsername();
        Integer port = serverInfo.getPort();
        String password = serverInfo.getPassword();
        String database = serverInfo.getDatabase();
        String host = serverInfo.getHost();
        String address = host + ":" + port;
        MongoDBMVTTileProviderConfig param = new MongoDBMVTTileProviderConfig();
        param.setName(name);
        param.setServerAdresses(new String[]{address});
        param.setDatabase(database);
        param.setUsername(username);
        param.setPassword(password);
        param.setTilesetNames(tilesetNames);
        // 发布服务
        logger.info("开始发布工作空间...");
        List<PublishResult> re = tileService.publish3D(param);
        logger.info("发布完成");
        return re;
    }

    private  List<PublishResult> publishRestData(String publisHistorySourceName,List<CacheBuildHistory> cacheBuildHistories) {
        Date date = new Date();
        SimpleDateFormat format0 = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String time = format0.format(date.getTime());
        String workspceName = time+"_" + publisHistorySourceName+"_for_restdata";
        String path = createNewFileWorkspace(workspceName);
        Path serverPath = Paths.get(storageClient.getVolumePath(path, SecurityUtilsExt.getTenantId(), false).getPath()); // Paths.get("F:/tmpfortest/" + path);

        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setServer(serverPath.toString());
        connectionInfo.setType(WorkspaceType.SMWU);
        Workspace workspace = new Workspace();
        boolean open = workspace.open(connectionInfo);
        if (!open) throw new GafException("打开文件型工作空间失败");

        Set<String> datasourceIds = cacheBuildHistories.stream().map(CacheBuildHistory::getDatasourceId).collect(Collectors.toSet());
        for (String datasourceId : datasourceIds) {
            SysResourceDatasource sysResourceDatasource = sysResourceDatasourceService.getById(datasourceId);
            DatasourceConnectionInfo dsConn = convertHelper.conver2DatasourceConnectionInfo(sysResourceDatasource);
            Datasource datasource = workspace.getDatasources().open(dsConn);
            workspace.save();
            datasource.close();
        }

        workspace.close();
        workspace.dispose();

        PublishParam publishParam = new PublishParam();
        publishParam.setWorkspaceConnectionInfo(connectionInfo.getServer());
        publishParam.setServicesTypes(new ServiceType[]{ServiceType.RESTDATA});
        return dataWorkspaceService.publish(publishParam);


    }

    private String createNewFileWorkspace(String workspaceName) {
        String path = filePathPrefix + File.separator+ workspaceName + ".smwu";
        Path serverPath =  Paths.get(storageClient.getVolumePath(path, SecurityUtilsExt.getTenantId(), false).getPath());// Paths.get("F:/tmpfortest/" + path);
        File descFile = serverPath.toFile();
        File availableFile = getAvailableFile(descFile, workspaceName, ".smwu");
        Workspace workspace = new Workspace();
        WorkspaceConnectionInfo connectionInfo = new WorkspaceConnectionInfo();
        connectionInfo.setServer(availableFile.toPath().toString());
        connectionInfo.setType(WorkspaceType.SMWU);
        connectionInfo.setName(workspaceName);
        workspace.create(connectionInfo);
        workspace.close();
        workspace.dispose();
        return filePathPrefix + File.separator + availableFile.getName();
    }

    private File getAvailableFile(File file,String fileName,String suffix) {
        File tmp = file;
        int i = 1;
        //若文件存在重命名
        while(tmp.exists()) {
            String newFilename = fileName+"("+i+")" + suffix;
            String parentPath = tmp.getParent();
            tmp = new File(parentPath+ File.separator+newFilename);
            i++;
            if (i > 100000000) {
                throw new GafException("找不到合适的文件名");
            }
        }
        return tmp;
    }
}
