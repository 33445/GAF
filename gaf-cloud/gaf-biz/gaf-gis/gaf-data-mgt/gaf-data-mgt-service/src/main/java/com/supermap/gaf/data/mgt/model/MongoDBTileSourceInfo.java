package com.supermap.gaf.data.mgt.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wxl
 * @since 2021/9/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("mongodb瓦片来源信息VO")
public class MongoDBTileSourceInfo {
    @ApiModelProperty("mongodb 服务地址. 多个地址使用英文逗号分隔")
    String serverAdresses;
    @ApiModelProperty("数据库名")
    String databse;
    @ApiModelProperty("用户名")
    String username;
    @ApiModelProperty("密码")
    String password;
}
