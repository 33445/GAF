/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.MmField;
import com.supermap.gaf.data.mgt.entity.vo.MmFieldSelectVo;
import com.supermap.gaf.data.mgt.model.FieldTypeInfo;

import java.util.List;

/**
 * 字段服务类
 *
 * @author wxl
 * @date yyyy -mm-dd
 */
public interface MmFieldService {

    /**
     * id查询字段
     *
     * @param fieldId the field id
     * @return by id
     */
    MmField getById(String fieldId);

    /**
     * 分页条件查询
     *
     * @param mmFieldSelectVo 查询条件
     * @param pageNum         当前页数
     * @param pageSize        页面大小
     * @return 分页对象 page
     */
    Page<MmField> listByPageCondition(MmFieldSelectVo mmFieldSelectVo, int pageNum, int pageSize);

    /**
     * 条件查询列表
     *
     * @param mmFieldSelectVo the mm field select vo
     * @return list
     */
    List<MmField> selectList(MmFieldSelectVo mmFieldSelectVo);

    /**
     * 新增字段
     *
     * @param mmField the mm field
     * @return mm field
     */
    MmField insertMmField(MmField mmField);

    /**
     * 批量插入
     *
     * @param mmFields the mm fields
     */
    void batchInsert(List<MmField> mmFields);

    /**
     * 删除字段
     *
     * @param fieldId the field id
     */
    void deleteMmField(String fieldId);

    /**
     * 批量删除
     *
     * @param fieldIds the field ids
     */
    void batchDelete(List<String> fieldIds);

    /**
     * 更新字段
     *
     * @param mmField the mm field
     * @return mm field
     */
    MmField updateMmField(MmField mmField);

    /**
     * 根据模型类型查询对应的字段的数据类型
     *
     * @param modelType 模型类型
     * @return 字段的数据类型映射 list
     */
    List<FieldTypeInfo> listTypeInfos(String modelType);

    /**
     * Gets by data standard ids.
     *
     * @param dataStandardIds the data standard ids
     * @return the by data standard ids
     */
    List<MmField> getByDataStandardIds(List<String> dataStandardIds);
}
