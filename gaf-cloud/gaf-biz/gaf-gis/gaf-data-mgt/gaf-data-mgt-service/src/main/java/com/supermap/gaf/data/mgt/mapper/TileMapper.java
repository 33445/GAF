package com.supermap.gaf.data.mgt.mapper;

import com.supermap.gaf.data.mgt.entity.Tile;
import com.supermap.gaf.data.mgt.entity.vo.TileSelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 瓦片数据访问类
 * @author wxl 
 * @date yyyy-mm-dd
 */
@Mapper
@Component
public interface TileMapper{
	/**
     * 根据主键 tileId查询
     * 
	 */
    Tile select(@Param("tileId")String tileId);
	
	/**
     * 多条件查询
     * @param tileSelectVo 查询条件
     * @return 若未查询到则返回空集合
     */
	List<Tile> selectList(TileSelectVo tileSelectVo);

    /**
	 * 新增
	 * 
	 */
    int insert(Tile tile);
	
	/**
     * 批量插入
     * 
	 */
    int batchInsert(List<Tile> tiles);
	
	/**
     * 批量删除
     *
	 */
    int batchDelete(List<String> tileIds);

	/**
     * 刪除
     * 
	 */
    int delete(@Param("tileId")String tileId);
    /**
     * 更新
     *
	 */
    int update(Tile tile);
}
