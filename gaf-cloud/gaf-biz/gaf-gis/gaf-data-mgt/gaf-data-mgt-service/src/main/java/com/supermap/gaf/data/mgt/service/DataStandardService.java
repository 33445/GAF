package com.supermap.gaf.data.mgt.service;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardVo;
import com.supermap.gaf.sys.mgt.commontype.SysDict;
import com.supermap.gaf.sys.mgt.model.DictData;

import java.util.*;

/**
 * 数据标准服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataStandardService {
	
	/**
    * id查询数据标准
    * @return
    */
	DataStandard getById(String dataStandardId);

    DataStandardVo getVoById(String dataStandardId);
	
	/**
     * 分页条件查询
     * @param dataStandardSelectVo 查询条件
     * @param pageNum 当前页数
     * @param pageSize 页面大小
     * @return 分页对象
     */
	Page<DataStandard> listByPageCondition(DataStandardSelectVo dataStandardSelectVo, int pageNum, int pageSize);
	
	
    /**
     * 新增数据标准
     * @return 
     */
    void insertDataStandard(DataStandardVo dataStandardVo);

    /**
     * 删除数据标准
     * 
     */
    int deleteDataStandard(String dataStandardId);

    /**
     * 批量删除
     * 
	 */
    int batchDelete(List<String> dataStandardIds);

    /**
     * 更新数据标准
     * @return 
     */
    int updateDataStandard(DataStandardVo dataStandardVo);


    List<DataStandard> getByRefCodes(List<String> refCodes);

    List<TreeNode> allNodes();

}
