package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.PublishHistory;
import com.supermap.gaf.data.mgt.entity.ServerInfo;
import com.supermap.gaf.data.mgt.entity.vo.PublishHistoryVo;
import com.supermap.gaf.data.mgt.service.PublishHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 *
 */
@Component
@Api("发布历史接口")
public class PublishHistoryResource {
    @Autowired
    private PublishHistoryService publishHistoryService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询发布历史")
    @ApiImplicitParam(name = "id", value = "发布历史id", paramType = "path", dataType = "String")
    @Path("/{id}")
    public MessageResult<PublishHistory> getById(@PathParam("id")String id){
        PublishHistory publishHistory = publishHistoryService.selectById(id);
        return MessageResult.successe(PublishHistory.class).data(publishHistory).status(200).message("查询成功").build();
    }
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询", notes = "分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "publishHistoryVo", value = "发布历史vo",paramType = "body", dataType = "object"),
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[1,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range(0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<Page<PublishHistory>> pageList(@Valid @BeanParam PublishHistoryVo publishHistoryVo,
                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<PublishHistory> workspaceAndTileManagerPage = publishHistoryService.pageList(pageNum, pageSize, publishHistoryVo);
        return MessageResult.data(workspaceAndTileManagerPage).status(200).message("查询成功").build();
    }
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增", notes = "新增")
    @ApiImplicitParam(name = "publishHistory", value = "发布历史", paramType = "body", dataType = "object")
    public MessageResult<Void> insertRegulatoryApplication(PublishHistory publishHistory){
        publishHistoryService.insert(publishHistory);
        return MessageResult.successe(Void.class).status(200).message("新增操作成功").build();
    }

    @ApiOperation(value = "根据id删除发布历史及其下的缓存构建历史，包括删除发布的服务,其下的缓存切片资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "发布历史id", paramType = "path", dataType = "string"),
            @ApiImplicitParam(name = "isDeleteServices", value = "是否删除发布的服务", defaultValue = "true", paramType = "query", dataType = "boolean"),
            @ApiImplicitParam(name = "isDeleteCacheTiles", value = "是否删除缓存切片产物", defaultValue = "true", paramType = "query", dataType = "boolean")
    })
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}/with-resources")
    public MessageResult<Void> deleteWithResource(@PathParam("id")String id,
                                                  @DefaultValue("true") @QueryParam("isDeleteServices") Boolean isDeleteServices,
                                                  @DefaultValue("true") @QueryParam("isDeleteCacheTiles") Boolean isDeleteCacheTiles){
        publishHistoryService.deleteWithResource(id,isDeleteServices,isDeleteCacheTiles);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @Path("/{id}")
    @ApiImplicitParam(name = "id", value = "发布历史id", paramType = "path", dataType = "string")
    public MessageResult<Void> deleteRegulatoryApplication(@PathParam("id")String id){
        publishHistoryService.delete(id);
        return MessageResult.successe(Void.class).status(200).message("删除操作成功").build();
    }
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @Path("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "publishHistory", value = "发布历史", paramType = "body", dataType = "object"),
            @ApiImplicitParam(name = "id", value = "发布历史id", paramType = "query", dataType = "string")
    })
    public MessageResult<Void> updateRegulatoryApplication(PublishHistory publishHistory, @PathParam("id")String id){
        publishHistory.setId(id);
        publishHistoryService.update(publishHistory);
        return MessageResult.successe(Void.class).status(200).message("更新操作成功").build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "测试连接", notes = "测试连接")
    @Path("/check-conn")
    @ApiImplicitParam(name = "serverInfo", value = "mongo连接信息", paramType = "body", dataType = "object")
    public MessageResult<Boolean>checkMongoConn(ServerInfo serverInfo){
        boolean b = publishHistoryService.checkMongoConn(serverInfo);
        String message = b?"连接成功":"连接失败";
        return MessageResult.successe(Boolean.class).data(b).status(200).message(message).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "获取mongo连接信息下所有表信息", notes = "获取mongo连接信息下所有表信息")
    @Path("/get-db-collection")
    @ApiImplicitParam(name = "serverInfo", value = "mongo连接信息", paramType = "body", dataType = "object")
    public MessageResult<List>getDBcollectionNames(@Valid @BeanParam ServerInfo serverInfo){
        List<String> mongoCollectionNames = publishHistoryService.getMongoCollection(serverInfo);
//        String message = b?"连接成功":"连接失败";
        return MessageResult.successe(List.class).data(mongoCollectionNames).status(200).message("获取集合成功").build();
    }
}
