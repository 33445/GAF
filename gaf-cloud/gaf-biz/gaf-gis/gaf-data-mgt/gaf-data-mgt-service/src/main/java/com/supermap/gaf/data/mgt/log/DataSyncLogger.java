package com.supermap.gaf.data.mgt.log;

import com.supermap.gaf.data.mgt.dao.DataSyncLogMapper;
import com.supermap.gaf.data.mgt.datasync.DataSyncNotifier;
import com.supermap.gaf.data.mgt.datasync.MailDataSyncNotifier;
import com.supermap.gaf.data.mgt.entity.DataSyncLog;
import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import com.supermap.gaf.data.mgt.publisher.MailSendPublisher;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * The type Data sync logger.
 */
public class DataSyncLogger implements AutoCloseable, SimpleLogger {
    private static final Logger log = LoggerFactory.getLogger(DataSyncLogger.class);
    private static final String SHANGHAI_TIME_ZONE = "Asia/Shanghai";
    private static final String DATE_FORMAT_PATERN = "yyyy-MM-dd HH:mm:ss";

    private DataSyncLogMapper mapper;

    private DataSyncLog logInfo;
    private StringBuffer logBuffer = new StringBuffer();
    private DataSyncTask syncTask;
    private MailSendPublisher mailSendPublisher;
    private DataSyncNotifier<String> notifier;

    /**
     * Instantiates a new Data sync logger.
     *
     * @param mapper            the mapper
     * @param syncTask          the sync task
     * @param mailSendPublisher the mail send publisher
     */
    public DataSyncLogger(DataSyncLogMapper mapper, DataSyncTask syncTask, MailSendPublisher mailSendPublisher) {
        this.mapper = mapper;
        this.syncTask = syncTask;
        this.logInfo = DataSyncLog.builder()
                .dataSyncLogId(UUID.randomUUID().toString())
                .dataSyncTaskId(syncTask.getDataSyncTaskId())
                .resultStatus(DataSyncLog.ResultStatus.SUCCESS.getCode())
                .createdTime(new Date()).build();
        this.mailSendPublisher = mailSendPublisher;
        try{
            init();
        }catch (Exception e){
            close();
        }

    }

    void init() throws  Exception{
        // 创建数据同步邮件通知器
        this.notifier = this.createNotifier(syncTask, mailSendPublisher);
        info("发送任务开始通知");
            // 发出开始通知
        sendNotify(DataSyncNotifier.START, String.format("开始执行数据同步任务【%s】", syncTask.getDataSyncTaskName()));
    }
    void sendNotify(String type, String message){
        try {
            if(notifier != null){
                notifier.send(type,message);
            }
        } catch (Exception e) {
            warn("发送通知失败：{}",e.getMessage());
        }

    }


    /**
     * Create notifier data sync notifier.
     *
     * @param syncTask          the sync task
     * @param mailSendPublisher the mail send publisher
     * @return the data sync notifier
     */
    DataSyncNotifier<String> createNotifier(DataSyncTask syncTask, MailSendPublisher mailSendPublisher) {
        info("创建数据同步邮件通知器");
        try {
            String alterType = syncTask.getAlertType();
            return new MailDataSyncNotifier(alterType.split(","), mailSendPublisher, syncTask.getAlertEmail());
        } catch (Exception e) {
            warn("创建数据同步邮件通知器失败");
        }
        return null;
    }


    /**
     * Gets date format string.
     *
     * @return the date format string
     */
    String getDateFormatString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_PATERN);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(SHANGHAI_TIME_ZONE));
        return simpleDateFormat.format(new Date());
    }

    @Override
    public void info(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        log.info(format,arguments);
        logBuffer.append(getDateFormatString()).append(" INFO ").append(ft.getMessage()).append("\n");
    }

    @Override
    public void error(String format, Object... arguments) {
        log.error(format,arguments);
        logInfo.setResultStatus(DataSyncLog.ResultStatus.FAIL.getCode());
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        logBuffer.append(getDateFormatString()).append(" ERROR ").append(ft.getMessage()).append("\n");
    }

    @Override
    public void warn(String format, Object... arguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(format, arguments);
        log.warn(format,arguments);
        logBuffer.append(getDateFormatString()).append(" WARN ").append(ft.getMessage()).append("\n");
    }


    /**
     * Add sync amount.
     *
     * @param amount the amount
     */
    public void addSyncAmount(Long amount) {

        Long totalAmount = logInfo.getTotalAmount();
        if (totalAmount == null) {
            logInfo.setTotalAmount(amount);
        } else {
            logInfo.setTotalAmount(totalAmount + amount);
        }
    }

    @Override
    public void close() {
        logInfo.setUpdatedTime(new Date());
        long castTime = (logInfo.getUpdatedTime().getTime() - logInfo.getCreatedTime().getTime());
        logInfo.setCostTime(castTime);
        logInfo.setContent(logBuffer.toString());
        mapper.insert(logInfo);
        if (DataSyncLog.ResultStatus.FAIL.getCode().equals(logInfo.getResultStatus())) {
            // 发出错误通知
            info("发送任务失败通知");
            sendNotify(DataSyncNotifier.SUCCESS, String.format("同步任务【%s】执行失败", syncTask.getDataSyncTaskName()));
        }
        if (DataSyncLog.ResultStatus.SUCCESS.getCode().equals(logInfo.getResultStatus())) {
            // 发出成功通知
            info("发送任务完成通知");
            sendNotify(DataSyncNotifier.SUCCESS, String.format("同步任务【%s】执行成功", syncTask.getDataSyncTaskName()));
        }
    }
}
