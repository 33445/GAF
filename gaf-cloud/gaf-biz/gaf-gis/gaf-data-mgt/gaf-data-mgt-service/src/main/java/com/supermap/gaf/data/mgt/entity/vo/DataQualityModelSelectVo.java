package com.supermap.gaf.data.mgt.entity.vo;
import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 数据质量管理-模型表 条件查询实体
 * @author zrc
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据质量管理-模型表条件查询实体")
public class DataQualityModelSelectVo {
    @QueryParam("searchFieldName")
    @ApiModelProperty("模糊查询字段名")
    @StringRange(entityClass = DataQualityModel.class)
    private String searchFieldName;
    @QueryParam("searchFieldValue")
    @ApiModelProperty("模糊查询字段值")
    private String searchFieldValue;
    @QueryParam("equalFieldName")
    @ApiModelProperty("等值查询字段名")
    @StringRange(entityClass = DataQualityModel.class)
    private String equalFieldName;
    @QueryParam("equalFieldValue")
    @ApiModelProperty("等值查询字段值")
    private String equalFieldValue;
    @QueryParam("orderFieldName")
    @ApiModelProperty("排序字段名")
    @StringRange(entityClass = DataQualityModel.class)
    private String orderFieldName;
    @QueryParam("orderMethod")
    @ApiModelProperty("排序方法")
    @StringRange(value = {"asc", "desc"},ignoreCase = true)
    private String orderMethod;
    @QueryParam("dataQualityModelId")
    @ApiModelProperty("主键")
    private String dataQualityModelId;
    @QueryParam("dataQualityModelName")
    @ApiModelProperty("数据质量管理模型名称")
    private String dataQualityModelName;
    @QueryParam("datasourceId")
    @ApiModelProperty("数据源id")
    private String datasourceId;
    @QueryParam("datasourceType")
    @ApiModelProperty("数据源类型")
    private String datasourceType;
    @QueryParam("sortSn")
    @ApiModelProperty("排序")
    private Integer sortSn;
    @QueryParam("description")
    @ApiModelProperty("描述")
    private String description;
    @QueryParam("createdTime")
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @QueryParam("createdBy")
    @ApiModelProperty("创建人")
    private String createdBy;
    @QueryParam("updatedTime")
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @QueryParam("updatedBy")
    @ApiModelProperty("更新人")
    private String updatedBy;
}