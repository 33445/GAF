package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.authority.enums.NodeTypeEnum;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataStandardVo;
import com.supermap.gaf.data.mgt.service.DataStandardService;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.TreeUtil;
import com.supermap.gaf.validGroup.AddGroup;
import com.supermap.gaf.validGroup.UpdateGroup;
import com.supermap.gaf.validator.StringRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Comparator;
import java.util.List;


/**
 * 数据标准接口
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Component
@Api(value = "数据标准接口")
public class DataStandardResource{
    @Autowired
    private DataStandardService dataStandardService;

    public static String getCatalogFirstSortValue(TreeNode treeNode,String catalogSortField,String leafSortField){
        StringBuilder re = new StringBuilder();
        try{
            if(NodeTypeEnum.CATALOG.getValue().equals(treeNode.getType())){
                // 目录在后
                re.append("1").append(PropertyUtils.getProperty(treeNode,catalogSortField));
            }else{
                re.append("0").append(PropertyUtils.getProperty(treeNode,leafSortField));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new GafException(e);
        }
        return re.toString();
    }

	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询数据标准", notes = "根据id查询数据标准")
	@Path("/{dataStandardId}")
    public MessageResult<DataStandard> getById(@PathParam("dataStandardId")String dataStandardId){
        DataStandard dataStandard = dataStandardService.getById(dataStandardId);
		return MessageResult.data(dataStandard).message("查询成功").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id查询数据标准（返回数据范围）", notes = "根据id查询数据标准（返回数据范围）")
    @Path("/vos/{dataStandardId}")
    public MessageResult<DataStandardVo> getVoById(@PathParam("dataStandardId")String dataStandardId){
        DataStandardVo re = dataStandardService.getVoById(dataStandardId);
        return MessageResult.data(re).message("查询成功").build();
    }



    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "查询数据标准+目录树", notes = "查询数据标准+目录树")
    @Path("/tree")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "catalogSortField", value = "目录的排序依据：字段名", paramType = "query", dataType = "string", example = "title"),
            @ApiImplicitParam(name = "catalogSortField", value = "叶子的排序依据：字段名", paramType = "query", dataType = "string", example = "title")
    })
    public MessageResult<List<TreeNode>> tree(@StringRange(entityClass = TreeNode.class,isCamelCaseToUnderscore = false) @QueryParam("catalogSortField")@DefaultValue("title") String catalogSortField,
                                              @StringRange(entityClass = TreeNode.class,isCamelCaseToUnderscore = false) @QueryParam("leafSortField")@DefaultValue("title")String leafSortField){
        List<TreeNode> nodes = dataStandardService.allNodes();
        List<TreeNode> re = TreeUtil.build(nodes, Comparator.comparing(n -> DataStandardResource.getCatalogFirstSortValue(n, catalogSortField, leafSortField)));
        return MessageResult.data(re).message("查询成功").build();
    }



	@GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页条件查询数据标准", notes = "分页条件查询数据标准")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[0,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range[0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<Page<DataStandard>> pageList(@Valid @BeanParam DataStandardSelectVo dataStandardSelectVo,
                                                      @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                                      @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        Page<DataStandard> page = dataStandardService.listByPageCondition(dataStandardSelectVo, pageNum, pageSize);
		return MessageResult.data(page).message("查询成功").build();
    }


	@POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "新增数据标准", notes = "新增数据标准")
    @ApiImplicitParam(name ="dataStandardVo",value = "dataStandardVo",paramType = "body",dataTypeClass = DataStandardVo.class,required = true )
    public MessageResult<Void> insertDataStandard(@NotNull @Valid @ConvertGroup(to= AddGroup.class)DataStandardVo dataStandardVo){
        dataStandardService.insertDataStandard(dataStandardVo);
		return MessageResult.successe(Void.class).message("新增操作成功").build();
    }


    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id删除数据标准", notes = "根据id删除数据标准")
	@Path("/{dataStandardId}")
    public MessageResult<Integer> deleteDataStandard(@PathParam("dataStandardId")String dataStandardId){
        int re = dataStandardService.deleteDataStandard(dataStandardId);
		return MessageResult.data(re).message("删除操作成功").build();
    }


	@DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "批量删除数据标准", notes = "批量删除数据标准")
    public MessageResult<Integer> batchDelete(@NotEmpty List<String> dataStandardIds){
        int re = dataStandardService.batchDelete(dataStandardIds);
		return MessageResult.data(re).message("批量删除操作成功").build();
    }

	
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "根据id更新数据标准", notes = "根据id更新数据标准")
	@Path("/{dataStandardId}")
    public MessageResult<Integer> updateDataStandard(@NotNull @Valid @ConvertGroup(to= UpdateGroup.class)DataStandardVo dataStandardVo, @PathParam("dataStandardId")String dataStandardId){
        dataStandardVo.setDataStandardId(dataStandardId);
        int re = dataStandardService.updateDataStandard(dataStandardVo);
		return MessageResult.data(re).message("更新操作成功").build();
    }


}
