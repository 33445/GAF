package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataSyncTask;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataSyncTaskVo;

import java.util.List;

/**
 * 数据同步任务表服务类
 *
 * @author zrc
 * @date yyyy -mm-dd
 */
public interface DataSyncTaskService {

    /**
     * 根据id查询数据同步任务表
     *
     * @param dataSyncTaskId the data sync task id
     * @return by id
     */
    DataSyncTask getById(String dataSyncTaskId);


    /**
     * Gets vo by id.
     *
     * @param dataSyncTaskId the data sync task id
     * @return the vo by id
     */
    DataSyncTaskVo getVoById(String dataSyncTaskId);

    /**
     * 分页条件查询
     *
     * @param dataSyncTaskSelectVo 查询条件
     * @param pageNum              当前页码
     * @param pageSize             每页数量
     * @return 分页对象 page
     */
    Page<DataSyncTask> listByPageCondition(DataSyncTaskSelectVo dataSyncTaskSelectVo, int pageNum, int pageSize);


    /**
     * 新增数据同步任务表
     *
     * @param dataSyncTask the data sync task
     * @return 新增的dataSyncTask data sync task
     */
    DataSyncTask insertDataSyncTask(DataSyncTask dataSyncTask);


    /**
     * 删除数据同步任务表
     *
     * @param dataSyncTaskId the data sync task id
     */
    void deleteDataSyncTask(String dataSyncTaskId);

    /**
     * 批量删除
     *
     * @param dataSyncTaskIds the data sync task ids
     */
    void batchDelete(List<String> dataSyncTaskIds);

    /**
     * 更新数据同步任务表
     *
     * @param dataSyncTask the data sync task
     * @return 更新后的dataSyncTask data sync task
     */
    int updateDataSyncTask(DataSyncTask dataSyncTask);

    /**
     * Execute.
     *
     * @param dataSyncTaskId the data sync task id
     */
    void execute(String dataSyncTaskId);

    /**
     * Start sync job.
     *
     * @param dataSyncTaskId the data sync task id
     */
    void startSyncJob(String dataSyncTaskId);

}
