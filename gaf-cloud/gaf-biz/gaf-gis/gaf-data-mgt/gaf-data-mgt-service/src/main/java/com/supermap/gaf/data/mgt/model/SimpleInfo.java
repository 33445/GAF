package com.supermap.gaf.data.mgt.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 数据源或数据集简略信息
 * @author wxl
 * @since 2021/9/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据源或数据集简略信息")
public class SimpleInfo {
    // 数据源别名 或 数据集名
    @ApiModelProperty("数据源别名 或 数据集名")
    private String name;
    // 数据源的数据引擎类型名 或 数据集类型名
    @ApiModelProperty("数据源的数据引擎类型名 或 数据集类型名")
    private String typeName;
    // 坐标系名
    @ApiModelProperty("坐标系名 ")
    private String coordSysName;
    // 坐标单位
    @ApiModelProperty("坐标单位")
    private String unit;
    // 数据源最后更新的时间
    @ApiModelProperty("数据源最后更新的时间 ")
    private String dateLastUpdated;
    @ApiModelProperty("用户添加的关于数据源的描述信息 或 数据集的描述信息 ")
    // 用户添加的关于数据源的描述信息 或 数据集的描述信息
    private String description;
    @ApiModelProperty("数据源下的数据集信息 ")
    private List<SimpleInfo> children;
}
