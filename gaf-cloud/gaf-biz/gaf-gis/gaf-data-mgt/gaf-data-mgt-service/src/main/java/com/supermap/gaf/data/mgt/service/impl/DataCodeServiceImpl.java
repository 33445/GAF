package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.authority.enums.NodeTypeEnum;
import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataCode;
import com.supermap.gaf.data.mgt.entity.DataStandard;
import com.supermap.gaf.data.mgt.entity.vo.DataCodeSelectVo;
import com.supermap.gaf.data.mgt.mapper.DataCodeMapper;
import com.supermap.gaf.data.mgt.service.CommonValidateService;
import com.supermap.gaf.data.mgt.service.DataCodeContentService;
import com.supermap.gaf.data.mgt.service.DataCodeService;
import com.supermap.gaf.data.mgt.service.DataStandardService;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.sys.mgt.client.SysCatalogClient;
import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 数据代码服务实现类
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Service
public class DataCodeServiceImpl implements DataCodeService{
    
	private static final Logger  log = LoggerFactory.getLogger(DataCodeServiceImpl.class);
	
	@Autowired
    private DataCodeMapper dataCodeMapper;

	@Autowired
	private CommonValidateService commonValidateService;

	@Autowired
	private SysCatalogClient sysCatalogClient;

	@Autowired
	private DataCodeContentService dataCodeContentService;

	@Autowired
	private DataStandardService dataStandardService;
	
	@Override
    public DataCode getById(String dataCodeId){
        if(dataCodeId == null){
            throw new IllegalArgumentException("dataCodeId不能为空");
        }
        DataCode re =  dataCodeMapper.select(dataCodeId);
        if(re == null){
        	throw new GafException("指定代码不存在");
		}
        return re;
    }
	
	@Override
    public Page<DataCode> listByPageCondition(DataCodeSelectVo dataCodeSelectVo, int pageNum, int pageSize) {
        PageInfo<DataCode> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataCodeMapper.selectList(dataCodeSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }

	@Override
	public 	List<DataCode> selectList(DataCodeSelectVo dataCodeSelectVo){
		return dataCodeMapper.selectList(dataCodeSelectVo);
	}

	@Override
    public void insertDataCode(DataCode dataCode){
		commonValidateService.validateCatalogId(dataCode.getCatalogId(), CatalogTypeEnum.DATA_CODE_GROUP_TYPE);
        // 主键非GeneratedKey，此处添加自定义主键生成策略
		dataCode.setDataCodeId(UUID.randomUUID().toString());
		String userName = SecurityUtilsExt.getUserName();
		dataCode.setCreatedBy(userName);
		dataCode.setUpdatedBy(userName);
        dataCodeMapper.insert(dataCode);
    }
	
	@Override
    public int deleteDataCode(String dataCodeId){
		return batchDelete(Arrays.asList(dataCodeId));
    }

	@Override
    public int batchDelete(List<String> dataCodeIds){
		if(!CollectionUtils.isEmpty(dataCodeIds)){
			List<DataStandard> dataStandards = dataStandardService.getByRefCodes(dataCodeIds);
			if(!CollectionUtils.isEmpty(dataStandards)){
				throw new GafException(String.format("【%s】标准正在使用。",dataStandards.stream().map(DataStandard::getChineseName).collect(Collectors.joining(","))));
			}
			dataCodeContentService.batchDeleteByDataCodeId(dataCodeIds);
			return dataCodeMapper.batchDelete(dataCodeIds);
		}
		return 0;
    }
	
	@Override
    public int updateDataCode(DataCode dataCode){
		commonValidateService.validateCatalogId(dataCode.getCatalogId(), CatalogTypeEnum.DATA_CODE_GROUP_TYPE);
		dataCode.setUpdatedBy(SecurityUtilsExt.getUserName());
		return dataCodeMapper.update(dataCode);
    }

	@Override
	public List<TreeNode> allNodes() {
		List<TreeNode> result = new LinkedList<>();
		MessageResult<List<TreeNode>> messageResult = sysCatalogClient.getNodesByType(CatalogTypeEnum.DATA_CODE_GROUP_TYPE.getValue());
		if(!messageResult.isSuccessed()){
			log.error(messageResult.getMessage());
			throw new GafException("获取目录失败");
		}
		List<TreeNode> catalogNodes = messageResult.getData();
		List<DataCode> dataCodes = dataCodeMapper.selectList(new DataCodeSelectVo());
		List<TreeNode> dataStandardNodes = dataCodes.stream().map(dataCode -> {
			TreeNode node = new TreeNode();
			node.setTitle(dataCode.getName());
			node.setParentId(dataCode.getCatalogId());
			node.setType(NodeTypeEnum.DATA_CODE.getValue());
			node.setKey(dataCode.getDataCodeId());
			return node;
		}).collect(Collectors.toList());
		result.addAll(catalogNodes);
		result.addAll(dataStandardNodes);
		return result;
	}

}
