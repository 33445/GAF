package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.data.mgt.dq.DqRuleType;

import java.util.List;

public interface DqRuleTypeService {

    List<DqRuleType> selectAll();

    void add(DqRuleType type);

    DqRuleType selectByCode(String code);

}
