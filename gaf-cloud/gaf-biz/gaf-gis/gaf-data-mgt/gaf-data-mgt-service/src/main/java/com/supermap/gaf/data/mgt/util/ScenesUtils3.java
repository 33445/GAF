package com.supermap.gaf.data.mgt.util;

import com.supermap.data.Scenes;
import com.supermap.data.Workspace;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.utils.LogUtil;
import com.supermap.realspace.*;
import org.slf4j.Logger;

/**
 * @description
 * @date:2022/2/21 10:08
 * @author:yw
 */
public class ScenesUtils3 {
    private static final Logger logger = LogUtil.getLocLogger(ScenesUtils3.class);
    public void addGridLayer(String cacheFileName, Workspace workspace, String datasetPath) {
        Scenes scenes = workspace.getScenes();
        Scene scene = new Scene(workspace);
        int sceneCont = scenes.getCount();
        if (sceneCont > 0) {
            scene.open(workspace.getScenes().get(0));
        }
        TerrainLayers terrainLayers = scene.getTerrainLayers();
        if (terrainLayers.get(cacheFileName) != null) {
            terrainLayers.remove(cacheFileName);
        }
        TerrainLayer terrainLayer = terrainLayers.add(datasetPath, false, cacheFileName);
//        scene.ensureVisible(terrainLayer);
        //通过相机定位
        saveScene(workspace, scene);
    }
    /**
     * 添加场景图层
     * @param cacheFileName 场景下图层名称
     * @param workspace 工作空间
     * @param datasetPath sci scp等切片缓存文件地址
     */
    public void addLayer(String cacheFileName, Workspace workspace, String datasetPath,Layer3DType layer3DType) {
        Scenes scenes = workspace.getScenes();
        Scene scene = new Scene(workspace);
        int sceneCont = scenes.getCount();
        logger.info("当前工作空间场景数为" + sceneCont);
        if (sceneCont > 0) {
            String senceName = workspace.getScenes().get(0);
            logger.info("当前工作空间场景名为" + senceName);
            scene.open(workspace.getScenes().get(0));
        }
        Layer3Ds layer3Ds = scene.getLayers();
        if (layer3Ds.get(cacheFileName) != null) {
            layer3Ds.remove(cacheFileName);
        }
        //快速定位到图层
//        Layer3D layer3D = layer3Ds.add(datasetScpPath, Layer3DType.OSGB, true, cacheFileName);
        Layer3D layer3D = layer3Ds.add(datasetPath, layer3DType, true, cacheFileName);
        scene.ensureVisible(layer3D);
        scene.refresh();
        //通过相机定位
        saveScene(workspace, scene);
    }

    /**
     * 保存场景
     *
     * @param workspace 工作空间
     * @param scene     场景信息
     * @return
     */
    private void saveScene(Workspace workspace, Scene scene) {
        String sceneXml = scene.toXML();
        boolean result;
        logger.info("保存场景中，当前工作空间为:" );
        try {
            if (workspace.getScenes().getCount() == 0) {
                int count = workspace.getScenes().add(workspace.getConnectionInfo().getName(), sceneXml);
//                result =  > 0;
                logger.info("新增场景至工作空间中，场景名为："+workspace.getConnectionInfo().getName()+"，结果为:" + count);
            } else {
                result = workspace.getScenes().setSceneXML(0, sceneXml);
                logger.info("更新场景至工作空间中，场景名为："+workspace.getConnectionInfo().getName()+"结果为:" + result);
            }
        }catch (Exception e) {
            throw new GafException("保存场景失败，原因:"+e.getMessage());
        }finally {
            scene.close();
            scene.dispose();
            workspace.save();
        }
    }
}
