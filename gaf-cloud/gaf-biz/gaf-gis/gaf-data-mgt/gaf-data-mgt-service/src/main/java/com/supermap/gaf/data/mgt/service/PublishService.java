package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.data.mgt.entity.*;
import com.supermap.gaf.data.mgt.entity.iserver.PublishParam;
import com.supermap.gaf.data.mgt.entity.iserver.PublishTileParam;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorVo;

import java.util.List;

public interface PublishService {

    /**
     * 新的根据工作空间发布服务,并记录发布历史
     * @param workspaceId
     * @param publishParam
     * @return
     */
    PublishHistory publishDataWorkspace(String workspaceId, PublishParam publishParam);


    /**
     * 将数据集切片并发布为三维服务
     * @param cacheBuildPublishParam
     * @return
     */
    PublishCacheBuildHistorVo publishCacheBuild(CacheBuildPublishParam cacheBuildPublishParam);

    /**
     * 在某发布历史下新增数据集切片并发布为三维服务
     * @param cacheBuildPublishParam2
     * @return
     */
    PublishCacheBuildHistorVo publishNewCacheBuildInPublishHistory(CacheBuildPublishParam2 cacheBuildPublishParam2);


    /**
     *
     * 将管理的瓦片发布为地图服务
     *
     * @param tileId
     * @param param
     * @return
     */
    PublishHistory publishTileMap(String tileId, PublishTileParam param);

    /**
     * 根据发布历史id查询其下的最新的缓存构建任务的状态
     * @param publishHistoryId
     * @return
     */
    List<CacheBuildStatus> getLatestCacheStatus(String publishHistoryId);


    /**
     *
     * @param publishParamemter
     */
    void publishMongodbOrWorkspace(PublishParamemter publishParamemter);
}
