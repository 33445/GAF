package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityProgram;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityProgramSelectVo;

import java.util.List;

/**
 * 数据质量管理-方案表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataQualityProgramService {
	
	/**
    * 根据id查询数据质量管理-方案表
    * @return
    */
    DataQualityProgram getById(String dataQualityProgramId);
	
	/**
     * 分页条件查询
     * @param dataQualityProgramSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<DataQualityProgram> listByPageCondition(DataQualityProgramSelectVo dataQualityProgramSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增数据质量管理-方案表
    * @return 新增的dataQualityProgram
    */
    DataQualityProgram insertDataQualityProgram(DataQualityProgram dataQualityProgram);

    /**
    * 删除数据质量管理-方案表
    * 
    */
    void deleteDataQualityProgram(String dataQualityProgramId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dataQualityProgramIds);
    /**
    * 更新数据质量管理-方案表
    * @return 更新后的dataQualityProgram
    */
    DataQualityProgram updateDataQualityProgram(DataQualityProgram dataQualityProgram);

    void executeProgram(String dataQualityProgramId);

    void startProgramJon(String dataQualityProgramId);
    
}
