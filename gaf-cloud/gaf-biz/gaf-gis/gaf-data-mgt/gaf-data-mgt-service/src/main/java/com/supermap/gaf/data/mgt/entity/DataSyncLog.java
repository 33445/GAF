package com.supermap.gaf.data.mgt.entity;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.supermap.gaf.data.mgt.jackson.CustomDurationSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;
import javax.validation.constraints.*;
import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * 数据同步日志表
 * @author zrc 
 * @date yyyy-mm-dd
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("数据同步日志表")
public class  DataSyncLog implements Serializable{
    @NotNull
    @ApiModelProperty("主键")
    private String dataSyncLogId;
    @NotNull
    @ApiModelProperty("所属任务")
    private String dataSyncTaskId;
    @NotNull
    @ApiModelProperty("记录日志")
    private String content;
    /**
    * 默认值1：now()
    */
    @ApiModelProperty("创建时间")
    private Date createdTime;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("更新时间")
    private Date updatedTime;
    @ApiModelProperty("更新人")
    private String updatedBy;
    @NotNull
    @ApiModelProperty("耗费时间-秒")
    @JsonSerialize(using= CustomDurationSerializer.class)
    private Long costTime;
    @ApiModelProperty("数据总量")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long totalAmount;
    /**
    * 默认值1：0
    */
    @ApiModelProperty("执行状态。0代表成功，1代表失败。")
    private String resultStatus;

    public enum ResultStatus{
        SUCCESS("0"),FAIL("1");
        private String code;

        ResultStatus(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
}