package com.supermap.gaf.data.mgt.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author : duke
 * @since 2021/11/12 5:02 PM
 */
public class SysResourceDatasourceDeleteEvent extends ApplicationEvent {
    private static final long serialVersionUID = 707737522089202973L;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public SysResourceDatasourceDeleteEvent(Object source) {
        super(source);
    }
}
