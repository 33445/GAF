package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.data.mgt.entity.CacheBuildParam;
import com.supermap.gaf.data.mgt.entity.CacheBuildPublishParam2;
import com.supermap.gaf.data.mgt.entity.CacheBuildStatus;
import com.supermap.gaf.data.mgt.entity.vo.PublishCacheBuildHistorTaskVo;

import java.util.List;

/**
 * cim切片服务类
 *
 * @author wsw
 * @date yyyy-mm-dd
 */
public interface CacheBuildService {


    /**
     * 根据缓存构建参数集合执行切片
     * @return
     */
    PublishCacheBuildHistorTaskVo cacheBuild(CacheBuildParam cacheBuildParam);

    /**
     * 在发布历史下 新增 缓存构建切片任务
     * @param cacheBuildPublishParam2
     * @return
     */
    PublishCacheBuildHistorTaskVo cacheBuildInPublishHistory(CacheBuildPublishParam2 cacheBuildPublishParam2);



    /**
     * 根据缓存构建历史id集合查询服务切片状态
     * @param cacheBuildHistoryIds
     * @return
     */
    List<CacheBuildStatus> getCacheSatus(List<String> cacheBuildHistoryIds);

   }
