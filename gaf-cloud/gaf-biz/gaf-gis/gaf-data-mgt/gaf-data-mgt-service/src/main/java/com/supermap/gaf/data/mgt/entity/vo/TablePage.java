package com.supermap.gaf.data.mgt.entity.vo;


import com.supermap.gaf.commontypes.pagination.Page;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class TablePage<T> extends Page<T> {

    public static class TableHeader extends HashMap {
        private static final String ROW_NAME_KEY = "rowName";
        private static final String ROW_ID_KEY = "rowId";
        public TableHeader(String rowId,String rowName, int propertyCount) {
            super(propertyCount);
            setRowId(rowId);
            setRowName(rowName);
        }
        public TableHeader(String rowId,String rowName) {
            super();
            setRowId(rowId);
            setRowName(rowName);
        }

        public void setRowName(String rowName){
            put(ROW_NAME_KEY,rowName);
        }
        public String getRowName(String rowName){
            return (String) get(ROW_NAME_KEY);
        }

        public void setRowId(String rowId){
            put(ROW_ID_KEY,rowId);
        }
        public String getRowId(String rowId){
            return (String) get(ROW_ID_KEY);
        }

    }
    private List<TableHeader> tableHeader;

    public TablePage(int pageNum, int pageSize, int totalCount, int totalPage, List<TableHeader> tableHeader, List<T> pageList) {
        super(pageNum, pageSize, totalCount, totalPage, pageList);
        this.tableHeader = tableHeader;
    }

    public List<TableHeader> getTableHeader() {
        return tableHeader;
    }

    public void setTableHeader(List<TableHeader> tableHeader) {
        this.tableHeader = tableHeader;
    }

    public static <T> TablePage<T> create(int pageNum, int pageSize, int totalCount, List<TableHeader> tableHeader , List<T> pageList) {
        int totalPage = 0;
        if ((pageSize > 0) && (totalCount > 0)) {
            double count = totalCount;
            totalPage = (int) Math.ceil(count / pageSize);
        }
        return new TablePage<>(pageNum,pageSize,totalCount,totalPage,tableHeader, pageList);
    }
}
