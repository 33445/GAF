package com.supermap.gaf.data.mgt.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("缓存构建参数")
public class CacheBuildParam {

    // 或者新建文件型工作空间 或者 存储在mongodb里面
    @ApiModelProperty("存储类型. WORKSPACE(工作空间)、MONGODB(MongoDB)")
    private String storageType;

    @ApiModelProperty("存储信息. json格式 WORKSPACE(工作空间): 新文件型 {workspaceName: 'xxx'}或者 已有的 {workspaceId: 'xxx'}、" +
            "MONGODB(MongoDB): {connectionName: 'xxx', host: '192.xxx.xxx.xxx',port: 3306,database: 'xxx', username: 'xxx',password: 'xxx'}")
    private String storageInfo;

    @ApiModelProperty("数据集切片设置集合")
    List<DatasetCacheBuildSetting> datasetCacheBuildSettings;

}
