package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DqProgramRule;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DqProgramRuleVo;
import com.supermap.gaf.data.mgt.mapper.DqProgramRuleMapper;
import com.supermap.gaf.data.mgt.service.DqProgramRuleService;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * 数据质量管理-方案-规则联系表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DqProgramRuleServiceImpl implements DqProgramRuleService{
    
	private static final Logger  log = LoggerFactory.getLogger(DqProgramRuleServiceImpl.class);
	
	@Autowired
    private DqProgramRuleMapper dqProgramRuleMapper;
	

	@Override
    public DqProgramRule getById(String dqProgramRuleId){
        if(dqProgramRuleId == null){
            throw new IllegalArgumentException("dqProgramRuleId不能为空");
        }
        return dqProgramRuleMapper.select(dqProgramRuleId);
    }
	
	@Override
    public Page<DqProgramRuleVo> listByPageCondition(DqProgramRuleSelectVo dqProgramRuleSelectVo, int pageNum, int pageSize) {
        PageInfo<DqProgramRuleVo> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dqProgramRuleMapper.selectVoList(dqProgramRuleSelectVo);
        });
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
    }


	@Override
    public DqProgramRule insertDqProgramRule(DqProgramRule dqProgramRule){
        dqProgramRule.setDqProgramRuleId(UUID.randomUUID().toString());
        String userName = SecurityUtilsExt.getUserName();
        dqProgramRule.setCreatedBy(userName);
        dqProgramRule.setUpdatedBy(userName);
        dqProgramRuleMapper.insert(dqProgramRule);
        return dqProgramRule;
    }
	
	@Override
    public void batchInsert(List<DqProgramRule> dqProgramRules){
		if (dqProgramRules != null && dqProgramRules.size() > 0) {
            String userName = SecurityUtilsExt.getUserName();
            dqProgramRules.forEach(dqProgramRule -> {
                dqProgramRule.setDqProgramRuleId(UUID.randomUUID().toString());
                dqProgramRule.setCreatedBy(userName);
                dqProgramRule.setUpdatedBy(userName);
            });
            dqProgramRuleMapper.batchInsert(dqProgramRules);
        }
        
    }
	

	@Override
    public void deleteDqProgramRule(String dqProgramRuleId){
        dqProgramRuleMapper.delete(dqProgramRuleId);
    }

	@Override
    public void batchDelete(List<String> dqProgramRuleIds){
        dqProgramRuleMapper.batchDelete(dqProgramRuleIds);
    }
	

	@Override
    public DqProgramRule updateDqProgramRule(DqProgramRule dqProgramRule){
        dqProgramRuleMapper.update(dqProgramRule);
        return dqProgramRule;
    }
    
}
