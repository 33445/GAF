package com.supermap.gaf.data.mgt.entity;

import lombok.Data;

/**
 * @author wxl
 * @since 2022/5/24
 */
@Data
public class CacheBuildResult {

    private String cacheBuildHistroyId;

    private Boolean success;

    private String errorMessage;
}

