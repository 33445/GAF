package com.supermap.gaf.data.mgt.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.commontype.SysResourceDatasource;
import com.supermap.gaf.data.mgt.entity.DataQualityModel;
import com.supermap.gaf.data.mgt.entity.DataQualityProgram;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelSelectVo;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityModelVo;
import com.supermap.gaf.data.mgt.mapper.DataQualityModelMapper;
import com.supermap.gaf.data.mgt.mapper.DataQualityProgramMapper;
import com.supermap.gaf.data.mgt.mapper.DataQualityRuleMapper;
import com.supermap.gaf.data.mgt.mapper.SysResourceDatasourceMapper;
import com.supermap.gaf.data.mgt.service.DataQualityModelService;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 数据质量管理-模型表服务实现类
 * @author zrc
 * @date yyyy-mm-dd
 */
@Service
public class DataQualityModelServiceImpl implements DataQualityModelService {
    
	private static final Logger  log = LoggerFactory.getLogger(DataQualityModelServiceImpl.class);
	
	@Autowired
    private DataQualityModelMapper dataQualityModelMapper;

	@Autowired
	private SysResourceDatasourceMapper sysResourceDatasourceMapper;

	@Autowired
	private DataQualityRuleMapper dataQualityRuleMapper;

	@Autowired
	private DataQualityProgramMapper dataQualityProgramMapper;

    DataQualityModelVo getVo(DataQualityModel model){
        DataQualityModelVo re = new DataQualityModelVo();
        BeanUtils.copyProperties(model,re);
        SysResourceDatasource datasource = sysResourceDatasourceMapper.select(re.getDatasourceId());
        if(datasource != null){
            re.setDatasourceName(datasource.getDsName());
        }
        return re;
    }


	@Override
    public DataQualityModelVo getById(String dataQualityModelId){
        if(dataQualityModelId == null){
            throw new IllegalArgumentException("dataQualityModelId不能为空");
        }
        DataQualityModel model = dataQualityModelMapper.select(dataQualityModelId);
        if(model == null){
            throw new GafException("资源不存在");
        }
        return getVo(model);
    }
	
	@Override
    public Page<DataQualityModelVo> listByPageCondition(DataQualityModelSelectVo dataQualityModelSelectVo, int pageNum, int pageSize) {
        PageInfo<DataQualityModel> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
            dataQualityModelMapper.selectList(dataQualityModelSelectVo);
        });
        List<DataQualityModelVo> vos = new ArrayList<>();
        if(pageInfo.getSize()>0){
            List<DataQualityModel> list = pageInfo.getList();
            vos = list.stream().map(item->getVo(item)).collect(Collectors.toList());
        }
        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),vos);
    }


	@Override
    public DataQualityModel insertDataQualityModel(DataQualityModel dataQualityModel){
        String userName = SecurityUtilsExt.getUserName();
        dataQualityModel.setCreatedBy(userName);
        dataQualityModel.setUpdatedBy(userName);
	    dataQualityModel.setDataQualityModelId(UUID.randomUUID().toString());
        dataQualityModelMapper.insert(dataQualityModel);
        return dataQualityModel;
    }
	
	@Override
    public void batchInsert(List<DataQualityModel> dataQualityModels){
		if (dataQualityModels != null && dataQualityModels.size() > 0) {
            String userName = SecurityUtilsExt.getUserName();
            dataQualityModels.forEach(dataQualityModel -> {
                dataQualityModel.setCreatedBy(userName);
                dataQualityModel.setUpdatedBy(userName);
                dataQualityModel.setDataQualityModelId(UUID.randomUUID().toString());
            });
            dataQualityModelMapper.batchInsert(dataQualityModels);
        }
        
    }
	
    void canDelete(List<String> dataQualityModelIds){
        List<DataQualityProgram> list = dataQualityProgramMapper.selectByModelIds(dataQualityModelIds);
        if(!CollectionUtils.isEmpty(list)){
            throw new GafException("先删除使用的方案才能删除模型");
        }
    }

    @Transactional
    @Override
    public void deleteDataQualityModel(String dataQualityModelId){
        canDelete(Arrays.asList(dataQualityModelId));
        dataQualityModelMapper.delete(dataQualityModelId);
        dataQualityRuleMapper.deleteByModelIds(Arrays.asList(dataQualityModelId));
    }


    @Transactional
    @Override
    public void batchDelete(List<String> dataQualityModelIds){
        canDelete(dataQualityModelIds);
        dataQualityModelMapper.batchDelete(dataQualityModelIds);
        dataQualityRuleMapper.deleteByModelIds(dataQualityModelIds);
    }
	

	@Override
    public DataQualityModel updateDataQualityModel(DataQualityModel dataQualityModel){
        dataQualityModelMapper.update(dataQualityModel);
        return dataQualityModel;
    }

    @Override
    public List<DataQualityRule> rules(String dataQualityModelId) {
        return null;
    }

}
