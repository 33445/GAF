package com.supermap.gaf.data.mgt.datasync;

import com.supermap.gaf.data.mgt.service.DataQualityProgramService;
import com.supermap.gaf.data.mgt.service.DataSyncTaskService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DataSyncJob extends QuartzJobBean {

    @Autowired
    private DataSyncTaskService dataSyncTaskService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobKey jobKey = jobExecutionContext.getJobDetail().getKey();
        String name = jobKey.getGroup()+":"+jobKey.getName();
        JobDataMap map = jobExecutionContext.getJobDetail().getJobDataMap();
        String id = map.getString("id");
        log.info("执行同步任务:{}",name);
        dataSyncTaskService.execute(id);
        log.info("执行结束");
    }


}
