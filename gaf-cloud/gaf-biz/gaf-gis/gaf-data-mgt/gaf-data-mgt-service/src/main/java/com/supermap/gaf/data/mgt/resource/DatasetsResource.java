package com.supermap.gaf.data.mgt.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.data.mgt.entity.MmField;
import com.supermap.gaf.data.mgt.entity.vo.TablePage;
import com.supermap.gaf.data.mgt.service.FieldsService;
import com.supermap.gaf.data.mgt.service.RecordsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
public class DatasetsResource {

    @Autowired
    private FieldsService fieldsService;

    @Autowired
    private RecordsService recordsService;


    @ApiOperation(value = "向指定数据集添加字段", notes = "向指定数据集添加字段")
    @Path("/fields")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "datasourceId",value =  "数据源id",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasetName",value =  "数据集名称",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name ="mmField",value =  "字段属性实体",paramType = "body",dataTypeClass = MmField.class,required = true )
    })
    public MessageResult<Void> addField(@PathParam("datasourceId") String datasourceId, @PathParam("datasetName")String datasetName, MmField mmField){
        fieldsService.addField(datasourceId,datasetName,mmField);
        return MessageResult.successe(Void.class).build();
    }

    @ApiOperation(value = "修改指定数据集的指定字段", notes = "修改指定数据集的指定字段")
    @Path("/fields/{fieldName}")
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParams({
        @ApiImplicitParam(name = "fieldName",value =  "字段名",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasourceId",value =  "数据源id",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasetName",value =  "数据集名称",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name ="mmField",value =  "字段属性实体",paramType = "body",dataTypeClass = MmField.class,required = true )
    })
    public MessageResult<Void> modifyField(@PathParam("fieldName") String fieldName, @PathParam("datasourceId") String datasourceId, @PathParam("datasetName")String datasetName, MmField mmField){
        fieldsService.modifyField(datasourceId,datasetName,fieldName,mmField);
        return MessageResult.successe(Void.class).build();
    }

    @ApiOperation(value = "删除指定数据集的指定字段", notes = "删除指定数据集的指定字段")
    @Path("/fields/{fieldName}")
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldName",value =  "字段名",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasourceId",value =  "数据源id",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasetName",value =  "数据集名称",paramType = "path",dataType = "string",required = true )
    })
    public MessageResult<Void> removeField(@PathParam("fieldName") String fieldName, @PathParam("datasourceId") String datasourceId, @PathParam("datasetName")String datasetName){
        fieldsService.removeField(datasourceId,datasetName,fieldName);
        return MessageResult.successe(Void.class).build();
    }
    @ApiOperation(value = "查询指定数据集的所有字段", notes = "查询指定数据集的所有字段")
    @Path("/fields")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "datasourceId",value =  "数据源id",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasetName",value =  "数据集名称",paramType = "path",dataType = "string",required = true )
    })
    public MessageResult<List<MmField>> fields( @PathParam("datasourceId") String datasourceId, @PathParam("datasetName")String datasetName){
        return MessageResult.data(fieldsService.fields(datasourceId,datasetName)).build();
    }

    @Path("/records")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "分页查询指定数据集的记录", notes = "分页查询指定数据集的记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "datasourceId",value =  "数据源id",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "datasetName",value =  "数据集名称",paramType = "path",dataType = "string",required = true ),
            @ApiImplicitParam(name = "returnGeo", value = "是否返回Geo", paramType = "query", dataType = "boolean", example = "false"),
            @ApiImplicitParam(name = "pageNum", value = "页码", example = "1",defaultValue = "1", allowableValues = "range[0,infinity]",paramType = "query", dataType = "integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", example = "10", defaultValue = "10",allowableValues = "range[0,infinity]", paramType = "query", dataType = "integer")
    })
    public MessageResult<TablePage> selectRecordsByPage(@PathParam("datasourceId") String datasourceId, @PathParam("datasetName")String datasetName,
                                                        @QueryParam("returnGeo")boolean returnGeo,
                                                        @DefaultValue("1")@QueryParam("pageNum")Integer pageNum,
                                                        @DefaultValue("10")@QueryParam("pageSize")Integer pageSize){
        return MessageResult.data(recordsService.selectRecordsByPage(datasourceId,datasetName,pageNum,pageSize,returnGeo)).build();
    }
}
