package com.supermap.gaf.data.mgt.enums;

/**
 * @description 切片类型枚举
 * @date:2022/2/14 16:04
 * @author:yw
 */
public enum PublishHistoryStateEnum {
    NEW(0,"新建"),
    //SLICING(1,"切片中"),
    //SLICED(2,"已切片"),
    //SLICE_FAILD(3,"切片失败"),
    PUBLISHED(4,"已发布"),
    PUBLISH_FAILED(5,"发布失败");



    private int code;

    private String description;


    PublishHistoryStateEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }


    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
