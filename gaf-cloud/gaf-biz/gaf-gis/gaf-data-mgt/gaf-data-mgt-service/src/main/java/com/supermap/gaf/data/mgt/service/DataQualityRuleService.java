package com.supermap.gaf.data.mgt.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.data.mgt.entity.DataQualityRule;
import com.supermap.gaf.data.mgt.entity.vo.DataQualityRuleSelectVo;

import java.util.List;

/**
 * 数据质量管理-规则表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface DataQualityRuleService {
	
	/**
    * 根据id查询数据质量管理-规则表
    * @return
    */
    DataQualityRule getById(String dataQualityRuleId);
	
	/**
     * 分页条件查询
     * @param dataQualityRuleSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<DataQualityRule> listByPageCondition(DataQualityRuleSelectVo dataQualityRuleSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增数据质量管理-规则表
    * @return 新增的dataQualityRule
    */
    DataQualityRule insertDataQualityRule(DataQualityRule dataQualityRule);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<DataQualityRule> dataQualityRules);

    /**
    * 删除数据质量管理-规则表
    * 
    */
    void deleteDataQualityRule(String dataQualityRuleId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> dataQualityRuleIds);
    /**
    * 更新数据质量管理-规则表
    * @return 更新后的dataQualityRule
    */
    DataQualityRule updateDataQualityRule(DataQualityRule dataQualityRule);
    
}
