package com.supermap.gaf.data.mgt.mapper;

//import com.supermap.cim.data.commontype.CacheBuildHistory;
//import com.supermap.cim.data.vo.CacheBuildHistoryVo;
import com.supermap.gaf.data.mgt.entity.CacheBuildHistory;
import com.supermap.gaf.data.mgt.entity.vo.CacheBuildHistoryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description
 * @date:2022/2/11 10:17
 * @author:yw
 */
@Mapper
@Component
public interface CacheBuildHistoryMapper {
    /**
     * 新增
     * @param cacheBuildHistory
     */
    void insert(CacheBuildHistory cacheBuildHistory);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    CacheBuildHistory selectById(@Param("id")String id);

    /**
     * 分页条件查询
     * @param cacheBuildHistoryVo
     * @return
     */
    List<CacheBuildHistory> selectList(CacheBuildHistoryVo cacheBuildHistoryVo);

    /**
     * 更新
     * @param cacheBuildHistory
     */
    void update(CacheBuildHistory cacheBuildHistory);

    /**
     * 删除
     * @param id
     */
    void delete(@Param("id") String id);


    List<CacheBuildHistory> selectByIds(@Param("ids") List<String> ids);
}
