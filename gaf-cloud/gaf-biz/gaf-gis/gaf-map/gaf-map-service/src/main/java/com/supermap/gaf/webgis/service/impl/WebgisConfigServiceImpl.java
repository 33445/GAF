///*
// * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
// * This program are made available under the terms of the Apache License, Version 2.0
// * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
// */
//package com.supermap.gaf.webgis.service.impl;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
//import com.supermap.gaf.sys.mgt.service.SysCatalogQueryService;
//import com.supermap.gaf.utils.AppConfigParser;
//import com.supermap.gaf.webgis.cache.PreviewConfigCacheI;
//import com.supermap.gaf.webgis.dao.WebgisAppBottomLayerMapper;
//import com.supermap.gaf.webgis.dao.WebgisAppMapper;
//import com.supermap.gaf.webgis.dao.WebgisCatalogLayerMapper;
//import com.supermap.gaf.webgis.dao.WebgisServiceMapper;
//import com.supermap.gaf.webgis.domain.WebgisAppPreviewParam;
//import com.supermap.gaf.webgis.domain.WebgisConfigData;
//import com.supermap.gaf.webgis.entity.*;
//import com.supermap.gaf.webgis.enums.PositionTypeEnum;
//import com.supermap.gaf.webgis.service.*;
//import com.supermap.gaf.webgis.theme.entity.ThemeService;
//import com.supermap.gaf.webgis.theme.service.ThemeServiceService;
//import com.supermap.gaf.webgis.util.BeanUtils;
//import com.supermap.gaf.webgis.vo.*;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * pro
// * 地图应用运行配置相关方法
// *
// * @author zhurongcheng
// * @date 2020 -12-05
// */
//@Service
//public class WebgisConfigServiceImpl implements WebgisConfigService {
//    private static final Logger log = LoggerFactory.getLogger(WebgisConfigServiceImpl.class);
//    /**
//     * The constant BASIC_TYPE.
//     */
//    public final static String BASIC_TYPE = "1";
//    @Autowired
//    private WebgisAppMapper webgisAppMapper;
//
//    @Autowired
//    private WebgisButtonService webgisButtonService;
//    @Autowired
//    private WebgisAppToolbarService webgisAppToolbarService;
//    @Autowired
//    private WebgisAppLocactionService webgisAppLocactionService;
//    @Autowired
//    private PreviewConfigCacheI previewConfigCache;
//    @Autowired
//    private WebgisAppBottomLayerMapper webgisAppBottomLayerMapper;
//    @Autowired
//    private WebgisAppBottomLayerService webgisAppBottomLayerService;
//    @Autowired
//    private WebgisServiceMapper webgisServiceMapper;
//    @Autowired
//    private WebgisCatalogLayerMapper webgisCatalogLayerMapper;
//    @Autowired
//    private SysCatalogQueryService sysCatalogQueryService;
//
//    @Autowired
//    private WebgisAppThemeServiceService webgisAppThemeServiceService;
//
//    @Autowired
//    private ThemeServiceService themeServiceService;
//
//    @Autowired
//    private WebgisAppService webgisAppService;
//
//    @Autowired
//    private WebgisCatalogService webgisCatalogService;
//
//    @Override
//    public WebgisConfigData convert2BottomLayerConfig(List<WebgisAppBottomLayer> appBottomLayers, boolean proxyDisable){
//        WebgisConfigData re = new WebgisConfigData();
//        if(!CollectionUtils.isEmpty(appBottomLayers)){
//            List list = new ArrayList();
//            for(WebgisAppBottomLayer appBottomLayer:appBottomLayers){
//                WebgisService service = null;
//                if(proxyDisable){
//                    service = webgisServiceMapper.selectDisableProxy(appBottomLayer.getBottomServiceId());
//                }else{
//                    service = webgisServiceMapper.select(appBottomLayer.getBottomServiceId());
//                }
//                if(service==null){
//                    continue;
//                }
//                Map<String,Object> bottomLayer = (Map<String, Object>) parseConfig(service);
//                bottomLayer.putAll((Map)parseConfig(appBottomLayer));
//                if(!StringUtils.isEmpty(appBottomLayer.getName())){
//                    bottomLayer.put("resourceName",appBottomLayer.getName());
//                }
//                list.add(bottomLayer);
//            }
//            re.setBottomLayers(list);
//        }else{
//            re.setBottomLayers(Collections.emptyList());
//        }
//        return re;
//    }
//    @Override
//    public WebgisConfigData convert2LocationConfig(WebgisAppLocaction appLocaction){
//        WebgisConfigData re = new WebgisConfigData();
//        if(appLocaction != null){
//            Object location = parseConfig(appLocaction);
//            re.setLocation((Map<String, Object>) location);
//        }else{
//            re.setLocation(Collections.emptyMap());
//        }
//
//        return re;
//    }
//
//    /**
//     * 通过根目录id解析出资源目录树配置
//     *
//     * @return
//     */
//    @Override
//    public WebgisConfigData convert2ResourceTreeConfig(WebgisCatalogTreeVo treeVo) {
//        WebgisConfigData re = new WebgisConfigData();
//        // 属性不为null，前端工程不用判断
//        re.setResourceTree(new HashMap<>());
//        WebgisCatalogServiceImpl.CreateTreeResult result = webgisCatalogService.createTree(treeVo, false, null, false);
//        List<SysCatalog> allCatalog = result.getAllCatalog();
//        List<WebgisCatalogLayerVo> allLayer = result.getAllLayer();
//        Map<String, Object> resourceTree = convert2ResourceTreeConfig(result.getRootId(),allCatalog, allLayer);
//        re.setResourceTree(resourceTree);
//        return re;
//    }
//
//
//    /**
//     * 通过根目录id解析出资源目录树配置
//     *
//     * @return
//     */
//    @Override
//    public WebgisConfigData convert2ResourceTreeConfig(String rootCatalogId,boolean proxyDisable) {
//        WebgisConfigData re = new WebgisConfigData();
//        // 属性不为null，前端工程不用判断
//        re.setResourceTree(new HashMap<>());
//        List<SysCatalog> sysCatalogs = sysCatalogQueryService.getCatalogTreeListByRootId(rootCatalogId);
//        if (CollectionUtils.isEmpty(sysCatalogs)) {
//            return re;
//        }
//        List<WebgisCatalogLayerVo> catalogLayerVos = null;
//        if(proxyDisable){
//            catalogLayerVos =  webgisCatalogLayerMapper.getVoByIdsDisableProxy(
//                    sysCatalogs.stream().map(sysCatalog -> sysCatalog.getCatalogId()).collect(Collectors.toList())
//            );
//        }else{
//            catalogLayerVos =  webgisCatalogLayerMapper.getVoByIds(
//                    sysCatalogs.stream().map(sysCatalog -> sysCatalog.getCatalogId()).collect(Collectors.toList())
//            );
//        }
//
//        Map<String, Object> resourceTree = convert2ResourceTreeConfig(rootCatalogId, sysCatalogs, catalogLayerVos);
//        re.setResourceTree(resourceTree);
//        return re;
//    }
//
//    @Override
//    public WebgisConfigData convert2ThemeServiceConfig(List<String> themeServiceCodes) {
//        WebgisConfigData re = new WebgisConfigData();
//        if(themeServiceCodes!=null){
//            List<WebgisToolbarButtonVo> buttonDos = new ArrayList<>();
//            for(String item:themeServiceCodes){
//                ThemeService themeService = themeServiceService.getByCode(item);
//                if(themeService!=null){
//                    buttonDos.add(WebgisToolbarButtonVo.builder().method("GafMapQueryByDataSource")
//                            .icon("icon-sousuo2")
//                            .name(themeService.getName())
//                            .params(String.format("{\"code\":\"%s\",\"visible\":true,\"showByTable\":true,\"isGetStatus\":true,\"title\":\"%s\"}",themeService.getCode(),themeService.getName()))
//                            .toggle(true).build()
//                    );
//                }
//            }
//            if(!buttonDos.isEmpty()){
//                WebgisAppToolbarVo toolbarDo = WebgisAppToolbarVo.builder().name("专题分析").toolbarLocation(PositionTypeEnum.bottomLeft.getName()).toolbarButtonVos(buttonDos).build();
//                re.setThemeToolbar((Map<String, Object>) parseConfig(toolbarDo));
//            }
//        }
//        return re;
//    }
//
//    @Override
//    public WebgisConfigData convert2ToolbarConfig(List<WebgisAppToolbarVo> toolbarVos) {
//        WebgisConfigData re = new WebgisConfigData();
//        List horizontalToolbars = new ArrayList();
//        List verticalToolbars = new ArrayList();
//        if (!CollectionUtils.isEmpty(toolbarVos)) {
//            for (WebgisAppToolbarVo toolbarVo : toolbarVos) {
//                toolbarVo.setToolbarLocation(PositionTypeEnum.get(toolbarVo.getToolbarLocation()).name());
//                if (BASIC_TYPE.equals(toolbarVo.getType())) {
//                    verticalToolbars.add(parseConfig(toolbarVo));
//                } else {
//                    horizontalToolbars.add(parseConfig(toolbarVo));
//                }
//            }
//            handleButtonChildren(horizontalToolbars);
//            handleButtonChildren(verticalToolbars);
//        }
//        re.setHorizontalToolbars(horizontalToolbars);
//        re.setVerticalToolbars(verticalToolbars);
//        return re;
//    }
//
//    @Override
//    public WebgisConfigData convert2AppConfig(WebgisApp webgisApp, String previewCode, boolean proxyDisable) {
//        WebgisConfigData re = new WebgisConfigData();
//        if(!StringUtils.isEmpty(previewCode)){
//            re = previewConfigCache.get(previewCode);
//        }
//        String gisAppId = webgisApp.getGisAppId();
//        re.setToken(webgisApp.getToken3p());
//        if(re.getResourceTree()==null){
//            // 图层目录配置
//            BeanUtils.copyPropertiesIgnoreNull(convert2ResourceTreeConfig(webgisApp.getLayerRootCatalogId(),proxyDisable),re);
//        }
//        if(re.getHorizontalToolbars()==null && re.getVerticalToolbars()==null){
//            // 工具条配置
//            List<WebgisAppToolbarVo> toolbarVos = webgisAppToolbarService.selectList(WebgisAppToolbarSelectVo.builder().gisAppId(gisAppId).build());
//            BeanUtils.copyPropertiesIgnoreNull(convert2ToolbarConfig(toolbarVos), re);
//        }
//        // 底图配置
//        if (WebgisApp.AppBottomServiceType.TIANDITU.getCode().equals(webgisApp.getBottomServiceType())) {
//            // 天地圖底圖
//            List<JSONObject> tianditus = new ArrayList<>();
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_shiliang\",\"resourceName\":\"矢量\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"VEC_C\"}"));
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_dixing\",\"resourceName\":\"地形\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"TER_C\"}"));
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_yingxinag\",\"resourceName\":\"影像\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"IMG_C\"}"));
//            re.setBottomLayers(tianditus);
//        } else if (WebgisApp.AppBottomServiceType.CUSTOM.getCode().equals(webgisApp.getBottomServiceType())) {
//            if(re.getBottomLayers()==null) {
//                List<WebgisAppBottomLayer> appBottomLayers = webgisAppBottomLayerMapper.selectList(WebgisAppBottomLayerSelectVo.builder().gisAppId(webgisApp.getGisAppId()).orderFieldName("sort_sn").build());
//                BeanUtils.copyPropertiesIgnoreNull(convert2BottomLayerConfig(appBottomLayers,proxyDisable),re);
//            }
//        }else{
//            log.error("应用{}不存在底图类型{}", webgisApp.getAppAliasEn(), webgisApp.getBottomServiceType());
//        }
//
//        if(re.getLocation()==null){
//            // 定位配置
//            WebgisAppLocaction appLocaction = webgisAppLocactionService.selectByGisAppId(webgisApp.getGisAppId());
//            if(appLocaction != null){
//                BeanUtils.copyPropertiesIgnoreNull(convert2LocationConfig(appLocaction),re);
//            }
//        }
//        if(re.getThemeToolbar()==null){
//            List<WebgisAppThemeService> webgisAppThemeServices = webgisAppThemeServiceService.selectList(WebgisAppThemeServiceSelectVo.builder().gisAppId(webgisApp.getGisAppId()).build());
//            if(webgisAppThemeServices!=null){
//                BeanUtils.copyPropertiesIgnoreNull(convert2ThemeServiceConfig(webgisAppThemeServices.stream().map(item->item.getThemeServiceCode()).collect(Collectors.toList())),re);
//            }
//        }
//        return re;
//    }
//
//    @Override
//    public String previewCode(WebgisApp webgisApp, WebgisAppPreviewParam previewParam) {
//        WebgisConfigData config = previewConfig(previewParam);
//        String previewCode = previewConfigCache.generateKey(webgisApp.getGisAppId());
//        previewConfigCache.add(previewCode,config);
//        return previewCode;
//    }
//
//    @Override
//    public WebgisConfigData previewConfig(WebgisAppPreviewParam previewParam) {
//        WebgisConfigData re = new WebgisConfigData();
//        //  工具条预览配置
//        if(previewParam.getToolbars()!=null){
//            for (WebgisAppToolbarVo item : previewParam.getToolbars()) {
//                if (item.getToolbarButtonVos() != null) {
//                    for (WebgisToolbarButtonVo buttonVo : item.getToolbarButtonVos()) {
//                        WebgisButton button = webgisButtonService.getById(buttonVo.getButtonId());
//                        buttonVo.setMethod(button.getMethod());
//                        buttonVo.setName(button.getName());
//                        if (StringUtils.isEmpty(buttonVo.getIcon())) {
//                            buttonVo.setIcon(button.getIcon());
//                        }
//                    }
//                }
//            }
//            BeanUtils.copyPropertiesIgnoreNull(convert2ToolbarConfig(previewParam.getToolbars()), re);
//        }
//        //  图层目录预览配置
//        if (previewParam.getWebgisCatalogTreeVo() != null) {
//            BeanUtils.copyPropertiesIgnoreNull(convert2ResourceTreeConfig(previewParam.getWebgisCatalogTreeVo()), re);
//        }
//        // 底图配置
//        if (WebgisApp.AppBottomServiceType.TIANDITU.getCode().equals(previewParam.getBottomLayersType())) {
//            // 天地圖底圖
//            List<JSONObject> tianditus = new ArrayList<>();
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_shiliang\",\"resourceName\":\"矢量\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"VEC_C\"}"));
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_dixing\",\"resourceName\":\"地形\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"TER_C\"}"));
//            tianditus.add(JSON.parseObject("{\"resourceId\":\"tianditus_yingxinag\",\"resourceName\":\"影像\",\"resourceTag\":\"MAPWORLD\",\"serviceType\":\"IMG_C\"}"));
//            re.setBottomLayers(tianditus);
//        } else if (WebgisApp.AppBottomServiceType.CUSTOM.getCode().equals(previewParam.getBottomLayersType())) {
//            if(re.getBottomLayers()==null) {
//                BeanUtils.copyPropertiesIgnoreNull(convert2BottomLayerConfig(previewParam.getWebgisAppBottomLayers(),false),re);
//            }
//        }else{
//            throw new GafException("bottomLayersType valid value: 0,1");
//        }
//        // 定位预览配置
//        if(previewParam.getLocation()!=null){
//            BeanUtils.copyPropertiesIgnoreNull(convert2LocationConfig(previewParam.getLocation()),re);
//        }
//        // 专题分析配置
//        if(previewParam.getThemeServiceCodes()!=null){
//            BeanUtils.copyPropertiesIgnoreNull(convert2ThemeServiceConfig(previewParam.getThemeServiceCodes()),re);
//        }
//        return re;
//    }
//
//    @Transactional
//    @Override
//    public void cloneAppConfig(String sourceGisAppId,String targetGisAppId){
//        if(StringUtils.isEmpty(sourceGisAppId) || StringUtils.isEmpty(targetGisAppId)){
//            return;
//        }
//        WebgisApp sourceGisApp = webgisAppService.getById(sourceGisAppId);
//        WebgisApp targetGisApp = webgisAppService.getById(targetGisAppId);
//        if(sourceGisApp==null || targetGisApp == null){
//            throw new GafException("应用不存在");
//        }
//        if (sourceGisApp.getLayerRootCatalogId() != null) {
//            String newTreeId = webgisCatalogService.cloneTree(sourceGisApp.getLayerRootCatalogId(), false);
//            targetGisApp.setLayerRootCatalogId(newTreeId);
//        }
//        targetGisApp.setBottomServiceType(sourceGisApp.getBottomServiceType());
//        if (WebgisApp.AppBottomServiceType.TIANDITU.getCode().equals(sourceGisApp.getBottomServiceType())) {
//            targetGisApp.setToken3p(sourceGisApp.getToken3p());
//        } else if (WebgisApp.AppBottomServiceType.CUSTOM.getCode().equals(sourceGisApp.getBottomServiceType())) {
//            // 拉取自定义底图配置
//            List<WebgisAppBottomLayer> appBottomLayers = webgisAppBottomLayerMapper.selectList(WebgisAppBottomLayerSelectVo.builder().gisAppId(sourceGisApp.getGisAppId()).build());
//            if(!CollectionUtils.isEmpty(appBottomLayers)){
//                for (WebgisAppBottomLayer appBottomLayer : appBottomLayers) {
//                    appBottomLayer.setGisAppId(targetGisAppId);
//                }
//                webgisAppBottomLayerService.batchInsert(appBottomLayers);
//            }
//        }
//
//        // 拉取定位配置
//        WebgisAppLocaction webgisAppLocaction = webgisAppLocactionService.selectByGisAppId(sourceGisAppId);
//        if(webgisAppLocaction != null){
//            webgisAppLocaction.setGisAppId(targetGisAppId);
//            webgisAppLocactionService.insertWebgisAppLocaction(webgisAppLocaction);
//        }
//        // 拉取工具条配置
//        List<WebgisAppToolbarVo> re = webgisAppToolbarService.selectList(WebgisAppToolbarSelectVo.builder().gisAppId(sourceGisAppId).build());
//        if (re != null) {
//            re.forEach(item -> {
//                item.setGisAppId(targetGisAppId);
//            });
//            webgisAppToolbarService.batchInsert(re);
//        }
//        webgisAppMapper.updateSelective(targetGisApp);
//    }
//
//    public Object parseConfig(Object obj) {
//        try {
//            return AppConfigParser.parse(obj);
//        } catch (Exception e) {
//            throw new GafException(e.getMessage());
//        }
//    }
//
//    void handleButtonChildren(List toolbars) {
//        for (Object toolbar : toolbars) {
//            if (toolbar instanceof Map) {
//                Map<String, Object> toolbarsMap = (Map) toolbar;
//                List buttons = (List) toolbarsMap.get("buttons");
//                if (CollectionUtils.isEmpty(buttons)) {
//                    return;
//                }
//                for (Object button : buttons) {
//                    if (button instanceof Map) {
//                        Map<String, Object> buttonMap = (Map) button;
//                        String name = (String) buttonMap.get("name");
//                        List children = (List) buttonMap.get("children");
//                        if (children != null) {
//                            for (Object childrenButton : children) {
//                                if (childrenButton instanceof Map) {
//                                    Map<String, Object> childrenButtonMap = (Map) childrenButton;
//                                    childrenButtonMap.put("selectedName", name);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    Map<String, Object> convert2ResourceTreeConfig(String rootId, List<SysCatalog> allCatalog, List<WebgisCatalogLayerVo> allLayer) {
//        Map<String, Object> resourceTree = new HashMap<>();
//        List<Map<String,Object>> catalogTree = new ArrayList();
//        for (SysCatalog sysCatalog : allCatalog) {
//            // 剔除根节点，变成多棵树显示
//            if(!rootId.equals(sysCatalog.getCatalogId())){
//                if (rootId.equals(sysCatalog.getParentId())) {
//                    sysCatalog.setParentId("");
//                }
//                catalogTree.add((Map<String, Object>) parseConfig(sysCatalog));
//            }
//        }
//        if(allLayer!=null){
//            for(WebgisCatalogLayerVo layerVo:allLayer){
//                // 剔除根节点
//                if(rootId.equals(layerVo.getLayerCatalogId())){
//                    layerVo.setLayerCatalogId("");
//                }
//                catalogTree.add((Map<String, Object>) parseConfig(layerVo));
//            }
//        }
//        sort(catalogTree);
//        resourceTree.put("allDataList", catalogTree);
//        resourceTree.put("replaceFields", JSON.parse("{\n" +
//                "                       title: 'resourceName',\n" +
//                "                       key: 'resourceId'\n" +
//                "                     }"));
//        return resourceTree;
//    }
//
//    void sort(List<Map<String,Object>> catalogTree){
//        if(CollectionUtils.isEmpty(catalogTree)){
//            return;
//        }
//        Collections.sort(catalogTree,(s1, s2)->{
//            Integer x = s1.get("sortSn")==null?Integer.MIN_VALUE:(Integer) s1.get("sortSn");
//            Integer y = s2.get("sortSn")==null?Integer.MIN_VALUE:(Integer) s2.get("sortSn");
//            return (x < y) ? -1 : 1;
//        });
//    }
//}
