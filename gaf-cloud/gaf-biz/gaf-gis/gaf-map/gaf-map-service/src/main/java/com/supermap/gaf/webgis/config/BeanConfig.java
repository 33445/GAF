package com.supermap.gaf.webgis.config;

import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.oauthmock.IportalOauthMocker;
import com.supermap.gaf.webgis.iportal.IportalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration("com.supermap.gaf.webgis.config.BeanConfig")
@EnableConfigurationProperties(IportalProperties.class)
public class BeanConfig {

    @Value("${GAF_WEBGIS_STORAGE_CONFIG_NAME:default}")
    private String configName;

    @Value("${GAF_WEBGIS_STORAGE_PRE_URL:http://gaf-storage/storage/api/tenant-created-first/}")
    private String storagePreUrl;

    final IportalProperties iportalProperties;

    public BeanConfig(IportalProperties iportalProperties) {
        this.iportalProperties = iportalProperties;
    }

    /* @Autowired
    private SqlSessionFactoryBean sqlSessionFactory;

    @Autowired
    private ProxyAddressHandler proxyAddressHandler;*/

    @Autowired
    @Qualifier("storageRestTemplate")
    private RestTemplate restTemplate;

    @Bean("WebgisStorageClient")
    public StorageClient storageClient() {
        return new StorageClient(storagePreUrl, configName, restTemplate);
    }


    @Bean
    @ConditionalOnMissingBean
    public IportalOauthMocker iportalOauthMocker(){
        return new IportalOauthMocker(iportalProperties.getServerPath(),iportalProperties.getOauth2ConfigId());
    }

    @Bean
    @ConditionalOnMissingBean
    public IportalClient iportalClient(){
        return new IportalClient(iportalProperties.getServerPath());
    }

   /* @PostConstruct
    void setTypeHandlers(){
        TypeHandler[] typeHandlers = {proxyAddressHandler};
        sqlSessionFactory.setTypeHandlers(typeHandlers);
    }*/
}
