package com.supermap.gaf.webgis.proxy;


import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import org.apache.ibatis.type.StringTypeHandler;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class RealAddressAuthHandler extends StringTypeHandler {

    private String handle(String address,String createdBy){
        try{
            if(address == null || createdBy == null || !SecurityUtilsExt.getUserName().equals(createdBy)){
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new GafException("获取用户信息失败，无法查询服务真实地址");
        }

        return address;
    }

    @Override
    public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return handle(super.getNullableResult(rs, columnIndex),getCreatedBy(rs));
    }

    @Override
    public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return handle(super.getNullableResult(rs, columnName),getCreatedBy(rs));
    }

    @Override
    public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return handle(super.getNullableResult(cs, columnIndex),getCreatedBy(cs));
    }

    String getCreatedBy(ResultSet rs){
        String re = null;
        try{
            re = rs.getString("created_by");
        } catch (Exception e) {
        }
        return re;
    }
    String getCreatedBy(CallableStatement rs){
        String re = null;
        try{
            re = rs.getString("created_by");
        } catch (Exception e) {
        }
        return re;
    }
}
