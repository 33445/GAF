//package com.supermap.gaf.webgis.dao;
//import com.supermap.gaf.webgis.entity.WebgisAppLocaction;
//import com.supermap.gaf.webgis.vo.WebgisAppLocactionSelectVo;
//import java.util.*;
//import org.apache.ibatis.annotations.*;
//import org.springframework.stereotype.Component;
//
///**
// * 初始定位数据访问类
// * @author zhurongcheng
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisAppLocactionMapper{
//	/**
//     * 根据主键 gisAppLocationId 查询
//     *
//	 */
//    WebgisAppLocaction select(@Param("gisAppLocationId")String gisAppLocationId);
//
//	/**
//     * 单字段条件模糊查询
//     * @param webgisAppLocactionSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisAppLocaction> selectList(WebgisAppLocactionSelectVo webgisAppLocactionSelectVo);
//
//    /**
//     * 新增
//     *
//	 */
//    int insert(WebgisAppLocaction webgisAppLocaction);
//
//	/**
//     * 批量插入
//     *
//	 */
//    int batchInsert(List<WebgisAppLocaction> webgisAppLocactions);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(List<String> gisAppLocationIds);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("gisAppLocationId")String gisAppLocationId);
//
//    int deleteByAppId(@Param("gisAppId")String gisAppId);
//
//    /**
//    * 更新
//    *
//    **/
//    int update(WebgisAppLocaction webgisAppLocaction);
//
//	WebgisAppLocaction selectBygisAppId(@Param("gisAppId")String gisAppId);
//}
