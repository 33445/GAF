///*
// * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
// * This program are made available under the terms of the Apache License, Version 2.0
// * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
// */
//package com.supermap.gaf.webgis.service.impl;
//
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.data.access.service.BatchSortAndCodeService;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.security.SecurityUtilsExt;
//import com.supermap.gaf.webgis.dao.WebgisCatalogLayerMapper;
//import com.supermap.gaf.webgis.dao.WebgisServiceAssociationMapper;
//import com.supermap.gaf.webgis.entity.WebgisCatalogLayer;
//import com.supermap.gaf.webgis.entity.WebgisServiceAssociation;
//import com.supermap.gaf.webgis.entity.WebgisServiceDo;
//import com.supermap.gaf.webgis.service.WebgisCatalogLayerService;
//import com.supermap.gaf.webgis.vo.WebgisCatalogLayerSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisCatalogLayerVo;
//import com.supermap.gaf.webgis.vo.WebgisServiceSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisServiceToLayerVo;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * 图层服务实现类
// *
// * @author wangxiaolong
// * @date 2020-12-05
// */
//@Service
//@Deprecated
//public class WebgisCatalogLayerServiceImpl implements WebgisCatalogLayerService {
//    @Autowired
//    private WebgisCatalogLayerMapper webgisCatalogLayerMapper;
//
//    @Autowired
//    private BatchSortAndCodeService batchSortAndCodeService;
//
//    @Autowired
//    private WebgisServiceAssociationMapper webgisServiceAssociationMapper;
//
//    @Override
//    public WebgisCatalogLayer getAndCheckById(String catalogLayerId) {
//        if (catalogLayerId == null) {
//            throw new IllegalArgumentException("catalogLayerId不能为空");
//        }
//        WebgisCatalogLayer layer = webgisCatalogLayerMapper.select(catalogLayerId);
//        if(layer == null){
//            throw new GafException("图层不存在");
//        }
//        return layer;
//    }
//
//    @Override
//    public Page<WebgisCatalogLayer> listByPageCondition(WebgisCatalogLayerSelectVo webgisCatalogLayerSelectVo, int pageNum, int pageSize) {
//        PageInfo<WebgisCatalogLayer> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            webgisCatalogLayerMapper.selectByCondition(webgisCatalogLayerSelectVo);
//        });
//        List<WebgisCatalogLayer> list = pageInfo.getList();
//        return Page.create(pageInfo.getPageNum(), pageInfo.getPageSize(), (int) pageInfo.getTotal(), pageInfo.getPages(), list);
//    }
//
//    @Override
//    public 	List<String> selectLayerNames(String rootCatalogId){
//        return webgisCatalogLayerMapper.selectLayerNames(rootCatalogId);
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public WebgisCatalogLayer insertWebgisCatalogLayer(WebgisCatalogLayer webgisCatalogLayer) {
//        webgisCatalogLayer.setCatalogLayerId(UUID.randomUUID().toString());
//        String userName = SecurityUtilsExt.getUserName();
//        webgisCatalogLayer.setCreatedBy(userName);
//        webgisCatalogLayer.setUpdatedBy(userName);
//        webgisCatalogLayerMapper.insert(webgisCatalogLayer);
//        batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisCatalogLayer.class, Collections.singletonList(webgisCatalogLayer.getLayerCatalogId()));
//        return webgisCatalogLayer;
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void batchInsert(List<WebgisCatalogLayer> webgisCatalogLayers) {
//        if (webgisCatalogLayers != null && webgisCatalogLayers.size() > 0) {
//            String userName = SecurityUtilsExt.getUserName();
//            webgisCatalogLayers.forEach(webgisCatalogLayer -> {
//                webgisCatalogLayer.setCatalogLayerId(UUID.randomUUID().toString());
//                webgisCatalogLayer.setCreatedBy(userName);
//                webgisCatalogLayer.setUpdatedBy(userName);
//            });
//            webgisCatalogLayerMapper.batchInsert(webgisCatalogLayers);
//            batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisCatalogLayer.class, Collections.singletonList(webgisCatalogLayers.get(0).getLayerCatalogId()));
//        }
//
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void batchInsertVos(List<WebgisCatalogLayerVo> catalogLayerVos) {
//        if (!CollectionUtils.isEmpty(catalogLayerVos)) {
//            List<String> serviceIds = new ArrayList<>();
//            List<WebgisCatalogLayer> webgisCatalogLayers = new ArrayList<>();
//            List<WebgisServiceAssociation> serviceAssociations = new ArrayList<>();
//            for (WebgisCatalogLayerVo item : catalogLayerVos) {
//                WebgisCatalogLayer catalogLayer = new WebgisCatalogLayerVo();
//                BeanUtils.copyProperties(item,catalogLayer);
//                webgisCatalogLayers.add(catalogLayer);
//                serviceIds.add(item.getGisServiceId());
//                catalogLayer.setCatalogLayerId(UUID.randomUUID().toString());
//                if(item.getAssociationServices()!=null){
//                    for(WebgisServiceDo associationService:item.getAssociationServices()){
//                        serviceAssociations.add(WebgisServiceAssociation.builder().gisServiceAssocId(UUID.randomUUID().toString()).gisServiceId(item.getGisServiceId()).gisDataServiceId(associationService.getGisServiceId()).build());
//                    }
//                }
//            }
//            webgisCatalogLayerMapper.batchInsert(webgisCatalogLayers);
//            if(!CollectionUtils.isEmpty(serviceIds)){
//                webgisServiceAssociationMapper.deleteByServiceIds(serviceIds);
//                if(!CollectionUtils.isEmpty(serviceAssociations)){
//                    webgisServiceAssociationMapper.batchInsert(serviceAssociations);
//                }
//            }
//        }
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void deleteWebgisCatalogLayer(String catalogLayerId) {
//        WebgisCatalogLayer layer = getAndCheckById(catalogLayerId);
//        if (layer != null) {
//            webgisCatalogLayerMapper.delete(catalogLayerId);
//            batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisCatalogLayer.class, Collections.singletonList(layer.getLayerCatalogId()));
//        }
//    }
//
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void batchDelete(List<String> catalogLayerIds) {
//        List<WebgisCatalogLayer> layers = webgisCatalogLayerMapper.selectByIds(catalogLayerIds);
//        if (layers != null && layers.size() > 0) {
//            Set<String> catalogIds = layers.stream().map(WebgisCatalogLayer::getLayerCatalogId).collect(Collectors.toSet());
//            webgisCatalogLayerMapper.batchDelete(catalogLayerIds);
//            batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisCatalogLayer.class, catalogIds);
//        }
//    }
//
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public WebgisCatalogLayer updateWebgisCatalogLayer(WebgisCatalogLayer webgisCatalogLayer) {
//        webgisCatalogLayer.setUpdatedBy(SecurityUtilsExt.getUserName());
//        WebgisCatalogLayer oldLayer = getAndCheckById(webgisCatalogLayer.getCatalogLayerId());
//        webgisCatalogLayerMapper.update(webgisCatalogLayer);
//        if (!Objects.equals(oldLayer.getSortSn(), webgisCatalogLayer.getSortSn())) {
//            batchSortAndCodeService.revisionSortSnForUpdate(WebgisCatalogLayer.class, webgisCatalogLayer.getLayerCatalogId(), oldLayer.getSortSn(), webgisCatalogLayer.getSortSn());
//        }
//        return webgisCatalogLayer;
//    }
//
//    @Override
//    public List<WebgisCatalogLayerVo> getVoByIds(List<String> catalogIds) {
//        if(!CollectionUtils.isEmpty(catalogIds)){
//            List<WebgisCatalogLayerVo> layerVos = webgisCatalogLayerMapper.getVoByIds(catalogIds);
//            if(layerVos!=null){
//                for(WebgisCatalogLayerVo item:layerVos){
//                    List<WebgisServiceDo> associationServices = webgisServiceAssociationMapper.selectAssociationServices(item.getGisServiceId(),new WebgisServiceSelectVo());
//                    item.setAssociationServices(associationServices);
//                }
//            }
//            return layerVos;
//        }
//       return null;
//    }
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public void batchInsertByService(List<WebgisServiceToLayerVo> webgisServiceToLayerVos) {
//        // 查询已经关联的数据
//        WebgisServiceToLayerVo webgisServiceToLayerVo = webgisServiceToLayerVos.get(0);
//        String layerCatalogId = webgisServiceToLayerVo.getLayerCatalogId();
//        WebgisCatalogLayerSelectVo webgisCatalogLayerSelectVo = new WebgisCatalogLayerSelectVo();
//        webgisCatalogLayerSelectVo.setLayerCatalogId(layerCatalogId);
//        long count = PageHelper.count(() -> webgisCatalogLayerMapper.selectByCondition(webgisCatalogLayerSelectVo));
//        List<WebgisCatalogLayer> needInsertLayerList = new LinkedList<>();
//        String userName = SecurityUtilsExt.getUserName();
//        for (int i = 0; i < webgisServiceToLayerVos.size(); i++) {
//            WebgisServiceToLayerVo vo = webgisServiceToLayerVos.get(i);
//            WebgisCatalogLayer webgisCatalogLayer = new WebgisCatalogLayer();
//            webgisCatalogLayer.setName(vo.getName());
//            webgisCatalogLayer.setLayerCatalogId(vo.getLayerCatalogId());
//            webgisCatalogLayer.setGisServiceId(vo.getGisServiceId());
//            webgisCatalogLayer.setSortSn((int) (i + 1 + count));
//            webgisCatalogLayer.setInitLoad(false);
//            webgisCatalogLayer.setCatalogLayerId(UUID.randomUUID().toString());
//            webgisCatalogLayer.setCreatedBy(userName);
//            webgisCatalogLayer.setUpdatedBy(userName);
//            needInsertLayerList.add(webgisCatalogLayer);
//        }
//        webgisCatalogLayerMapper.batchInsert(needInsertLayerList);
//    }
//
//    @Override
//    public List<String> listGisIdsByCatalogId(String layerCatalogId) {
//        return webgisCatalogLayerMapper.selectServiceIdsByCatalogId(layerCatalogId);
//    }
//
//    @Override
//    public Integer countByCatalogId(String layerCatalogId) {
//        WebgisCatalogLayerSelectVo webgisCatalogLayerSelectVo = new WebgisCatalogLayerSelectVo();
//        webgisCatalogLayerSelectVo.setLayerCatalogId(layerCatalogId);
//        long count = PageHelper.count(() -> webgisCatalogLayerMapper.selectByCondition(webgisCatalogLayerSelectVo));
//        return (int) count;
//    }
//
//    @Override
//    public void deleteByCatalogIds(List<String> catalogIds) {
//        if(!CollectionUtils.isEmpty(catalogIds)){
//            webgisCatalogLayerMapper.deleteByCatalogIds(catalogIds);
//        }
//    }
//
//}
