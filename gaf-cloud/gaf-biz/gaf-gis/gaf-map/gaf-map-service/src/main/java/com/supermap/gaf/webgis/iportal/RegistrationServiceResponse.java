package com.supermap.gaf.webgis.iportal;

import com.supermap.gaf.data.mgt.entity.iserver.ErrorState;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegistrationServiceResponse {
    private boolean succeed;
    private Long newResourceID;
    private String newResourceLocation;
    private ErrorState error;
}
