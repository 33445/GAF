package com.supermap.gaf.webgis.resource;

import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.webgis.service.IportalService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Component
@Api(value = "iportal")
public class IportalResource {
    @Autowired
    private IportalService iportalService;



    String[] getGAFAuthHeaderNameAndValues(HttpServletRequest req){
        String cookies = req.getHeader("Cookie");
        String authorization = req.getHeader("Authorization");
        return new String[]{"Cookie",cookies,"Authorization",authorization};
    }

    @GET
    @Path("/handler/import-service/{serviceId}")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult<Integer> importServices(@PathParam("serviceId") String serviceId, @Context HttpServletRequest req) {
        try{
            SecurityUtilsExt.getUserName();
        }catch (Exception e){
            throw new GafException("未登录，无法获取用户登录信息");
        }
        int re = iportalService.importService(serviceId,getGAFAuthHeaderNameAndValues(req));
        return MessageResult.data(re).build();
    }



}
