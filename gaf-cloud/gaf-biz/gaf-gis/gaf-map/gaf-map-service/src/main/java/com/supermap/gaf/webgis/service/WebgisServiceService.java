/*
 * Copyright© 2000 - 2021 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.
 */
package com.supermap.gaf.webgis.service;

import com.supermap.gaf.authority.vo.TreeNode;
import com.supermap.gaf.commontypes.KgGraph;
import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.webgis.domain.BatchRegistryServiceResult;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.gaf.webgis.vo.WebgisServiceSelectVo;
import com.supermap.gaf.webgis.vo.WebgisServiceVO;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;

/**
 * GIS服务服务类
 *
 * @author wangxiaolong
 * @date 2020 -12-05
 */
public interface WebgisServiceService {


    /**
     * 过滤，仅返回自己创建的
     *
     * @param gisServiceIds the gis service ids
     * @return the list
     */
    List<String> filterByOwn(Collection<String> gisServiceIds);


    /**
     * 过滤，仅返回自己创建的
     *
     * @param gisServiceId the gis service id
     * @return the string
     */
    String filterByOwn(String gisServiceId);

    /**
     * id查询GIS服务
     * 包含真实的地址
     * @param gisServiceId the gis service id
     * @return by id
     */
    WebgisService getByIdHasRealAddress(String gisServiceId);

    /**
     * id查询GIS服务
     *
     * @param gisServiceId the gis service id
     * @return by id
     */
    WebgisService getById(String gisServiceId);


    /**
     * Gets registry service result.
     *
     * @param code the code
     * @return the registry service result
     */
    BatchRegistryServiceResult getRegistryServiceResult(String code);

    /**
     * 分页条件查询
     *
     * @param webgisServiceSelectVo 查询条件
     * @param pageNum               当前页数
     * @param pageSize              页面大小
     * @return 分页对象 page
     */
    Page<WebgisServiceVO> listByPageCondition(WebgisServiceSelectVo webgisServiceSelectVo, int pageNum, int pageSize);

    /**
     * Select no real address list list.
     *
     * @param proxyAddress the proxy address
     * @param typeCode     the type code
     * @return the list
     */
    List<WebgisService> selectNoRealAddressList(List<String> proxyAddress, String typeCode);

    /**
     * 新增GIS服务
     *
     * @param webgisService the webgis service
     * @param sourceId      the source id
     * @param sourceType    the source type
     * @return webgis service
     */
    WebgisService insertWebgisService(WebgisService webgisService, @Nullable String sourceId, @Nullable Integer sourceType);


    /**
     * Registry webgis webgis service.
     *
     * @param webgisService the webgis service
     * @param sourceId      the source id
     * @param sourceType    the source type
     * @return the webgis service
     */
    WebgisService registryWebgis(WebgisService webgisService, @Nullable String sourceId, @Nullable Integer sourceType);

    /**
     * 批量插入
     *
     * @param webgisServices the webgis services
     */
    void batchInsert(List<WebgisService> webgisServices);

    /**
     * 删除GIS服务
     *
     * @param gisServiceId the gis service id
     */
    void deleteWebgisService(String gisServiceId);

    /**
     * 批量删除
     *
     * @param gisServiceIds the gis service ids
     */
    void batchDelete(List<String> gisServiceIds);

    /**
     * 更新GIS服务
     *
     * @param webgisService the webgis service
     * @return webgis service
     */
    WebgisService updateWebgisService(WebgisService webgisService);

    /**
     * 查询所有服务类别
     *
     * @return service types
     */
    List<TreeNode> getServiceTypes();

    /**
     * 根据id集合批量查询webgis服务
     *
     * @param ids id集合
     * @return 若未查询到则返回空对象 list
     */
    List<WebgisService> listByIds(List<String> ids);

    ///**
    // * 分页条件查询 未挂载在资源目录下的服务
    // *
    // * @param webgisServiceConditonVo 查询条件
    // * @param pageNum                 当前页数
    // * @param pageSize                页面大小
    // * @param layerCatalogId          资源目录id
    // * @return 分页对象 page
    // */
    //Page<WebgisService> listByPageCondition(WebgisServiceConditonVo webgisServiceConditonVo, Integer pageNum, Integer pageSize, String layerCatalogId);

    /**
     * Select association data services list.
     *
     * @param gisServiceId the gis service id
     * @return the list
     */
    List selectAssociationDataServices(String gisServiceId);

    /**
     * 根据多种类型分页查询服务
     *
     * @param typeCodes the type codes
     * @param pageNum   the page num
     * @param pageSize  the page size
     * @return page page
     */
    Page<WebgisService> listByTypeCodes(String typeCodes, int pageNum, int pageSize);

    /**
     * Share to me services page list page.
     *
     * @param pageNum  the page num
     * @param pageSize the page size
     * @return the page
     */
    Page<WebgisService> share2mePageList(Integer pageNum, Integer pageSize);

    /**
     * Disable int.
     *
     * @param gisServiceId the gis service id
     * @return the int
     */
    int disable(String gisServiceId);

    /**
     * Enable int.
     *
     * @param gisServiceId the gis service id
     * @return the int
     */
    int enable(String gisServiceId);


    /**
     * 根据数据源名称和数据集名称查询匹配的数据服务
     * @param datasourceName
     * @param datasetName
     * @return
     */
    List<WebgisService> selectRestDataByDatasourceNameAndDatasetName(String datasourceName, String datasetName);

    /**
     * 根据数据源名称和数据集名称查询数据服务
     *
     * @param datasourceName the datasource name
     * @param datasetName    the dataset name
     * @return the kg graph
     */
    KgGraph selectConsanguinityByDateset(String datasourceName, String datasetName);


}
