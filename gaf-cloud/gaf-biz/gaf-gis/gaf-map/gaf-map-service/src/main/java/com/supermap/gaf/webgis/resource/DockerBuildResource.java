package com.supermap.gaf.webgis.resource;

import com.supermap.gaf.common.storage.client.StorageClient;
import com.supermap.gaf.common.storage.entity.VolumePathReturn;
import com.supermap.gaf.commontypes.MessageResult;
import com.supermap.gaf.docker.entity.DockerBuildParamVo;
import com.supermap.gaf.docker.util.DockerClientUtil;
import com.supermap.gaf.exception.GafException;
import com.supermap.gaf.security.RequestUtil;
import com.supermap.gaf.security.SecurityUtilsExt;
import com.supermap.gaf.webgis.service.AsyncService;
import io.swagger.annotations.Api;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * /map/docker
 */
@Component
@Api(value = "docker-build")
public class DockerBuildResource {
    @Autowired
    @Qualifier("storageRestTemplate")
    private RestTemplate restTemplate;
    @Autowired
    private AsyncService asyncService;


    @POST
    @Path("/{selectMode}/{configName}/{path:.*}")
    @Produces({MediaType.APPLICATION_JSON})
    public MessageResult dockerBuild(@PathParam("selectMode") String selectMode,
                                     @PathParam("path") String path,
                                     @PathParam("configName") String configName,
                                     DockerBuildParamVo param) {
        String username = SecurityUtilsExt.getUserName();
        path = path + "/Dockerfile";
        StorageClient storageClient = new StorageClient("http://gaf-storage/storage/api/" + selectMode + "/", configName, restTemplate);
        VolumePathReturn volumePath = storageClient.getVolumePath(path, RequestUtil.getTenantId(), false);
        String dockerfilePath = volumePath.getPath();
        if (!StringUtils.isEmpty(param.getRegistryAddress()) &&
                !StringUtils.isEmpty(param.getRegistryUsername()) &&
                !StringUtils.isEmpty(param.getRegistryPassword())){
            if (!param.getTagFullName().startsWith(param.getRegistryAddress())){
                throw new GafException("镜像名称与仓库名不匹配");
            }
            asyncService.buildAndPush(dockerfilePath, param.getTagFullName(), param.getRegistryAddress(), param.getRegistryUsername(), param.getRegistryPassword(), username);
        }else {
            asyncService.buildAndSave(dockerfilePath, param.getTagFullName(), username);
        }
        return MessageResult.successe().data(dockerfilePath).build();
    }



}
