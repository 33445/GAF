//package com.supermap.gaf.webgis.dao;
//
//import com.supermap.gaf.webgis.entity.WebgisAppToolbar;
//import com.supermap.gaf.webgis.vo.WebgisAppToolbarSelectVo;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//import java.util.List;
//
///**
// * 地图工具条模板数据访问类
// * @author zrc
// * @date yyyy-mm-dd
// */
//@Mapper
//@Component
//public interface WebgisAppToolbarMapper {
//	/**
//     * 根据主键 appToolbarId 查询
//     *
//	 */
//    WebgisAppToolbar select(@Param("appToolbarId") String appToolbarId, @Param("templateOp")boolean templateOp);
//
//
//	/**
//     * 多条件查询
//     * @param WebgisAppToolbarSelectVo 查询条件
//     * @return 若未查询到则返回空集合
//     */
//	List<WebgisAppToolbar> selectList(@Param("entity") WebgisAppToolbarSelectVo WebgisAppToolbarSelectVo,  @Param("templateOp")boolean templateOp);
//
//    /**
//     * 新增
//     *
//	 */
//    void insert(WebgisAppToolbar WebgisAppToolbar);
//
//	/**
//     * 批量插入
//     *
//	 */
//	void batchInsert(@Param("list") Collection<WebgisAppToolbar> WebgisAppToolbars);
//
//	/**
//     * 批量删除
//     *
//	 */
//    int batchDelete(@Param("list") Collection<String> appToolbarIds, @Param("templateOp")boolean templateOp);
//
//	/**
//     * 刪除
//     *
//	 */
//    int delete(@Param("appToolbarId") String appToolbarId, @Param("templateOp")boolean templateOp);
//    /**
//    * 更新
//    *
//    **/
//    int update(@Param("entity") WebgisAppToolbar WebgisAppToolbar, @Param("templateOp")boolean templateOp);
//	/**
//	 * 选择性更新
//	 *
//	 **/
//	int updateSelective(@Param("entity") WebgisAppToolbar WebgisAppToolbar, @Param("templateOp")boolean templateOp);
//
//	int deleteByGisAppIds(@Param("list") Collection<String> gisAppIds);
//
//	List<WebgisAppToolbar> selectByGisAppIds(@Param("list") Collection<String> gisAppIds);
//
//}
