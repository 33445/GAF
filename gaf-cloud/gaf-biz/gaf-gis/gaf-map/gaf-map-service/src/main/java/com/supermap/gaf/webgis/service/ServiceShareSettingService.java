package com.supermap.gaf.webgis.service;

import com.supermap.gaf.commontypes.pagination.Page;
import com.supermap.gaf.webgis.entity.ServiceShareSetting;
import com.supermap.gaf.webgis.entity.ServiceShareSettingBatchUpdateParam;
import com.supermap.gaf.webgis.vo.ServiceShareSettingSelectVo;

import java.util.List;

/**
 * 服务共享设置表服务类
 * @author zrc 
 * @date yyyy-mm-dd
 */
public interface ServiceShareSettingService {
	
	/**
    * 根据id查询服务共享设置表
    * @return
    */
    ServiceShareSetting getById(String serviceShareSettingId);
	
	/**
     * 分页条件查询
     * @param serviceShareSettingSelectVo 查询条件
     * @param pageNum 当前页码
     * @param pageSize 每页数量
     * @return 分页对象
     */
	Page<ServiceShareSetting> listByPageCondition(ServiceShareSettingSelectVo serviceShareSettingSelectVo, int pageNum, int pageSize);
	
	
    /**
    * 新增服务共享设置表
    * @return 新增的serviceShareSetting
    */
    ServiceShareSetting insertServiceShareSetting(ServiceShareSetting serviceShareSetting);
	
	/**
    * 批量插入
    * 
    **/
    void batchInsert(List<ServiceShareSetting> serviceShareSettings);

    /**
    * 删除服务共享设置表
    * 
    */
    void deleteServiceShareSetting(String serviceShareSettingId);

    /**
    * 批量删除
    * 
    **/
    void batchDelete(List<String> serviceShareSettingIds);
    /**
    * 更新服务共享设置表
    * @return 更新后的serviceShareSetting
    */
    ServiceShareSetting updateServiceShareSetting(ServiceShareSetting serviceShareSetting);


    /**
     * 根据ids批量更新空间范围或字段
     * @param serviceShareSettingBatchUpdateParam
     */
    void batchUpdateByIds(ServiceShareSettingBatchUpdateParam serviceShareSettingBatchUpdateParam);

    /**
     * 根据用户id和服务id集合查询 服务共享设置
     * @param userId 用户id
     * @param gisServiceIds 服务id集合
     * @return 服务共享设置列表
     */
    List<ServiceShareSetting> listByUserIdAndServiceIds(String userId, List<String> gisServiceIds);
}
