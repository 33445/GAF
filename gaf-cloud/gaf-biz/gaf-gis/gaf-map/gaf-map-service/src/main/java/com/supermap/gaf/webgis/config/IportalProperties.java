package com.supermap.gaf.webgis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * The type Iportal properties.
 */
@ConfigurationProperties(prefix = IportalProperties.IPORTAL_PREFIX)
@Data
public class IportalProperties {

    public static final String IPORTAL_PREFIX = "gaf.iportal";
    /**
     * iportal of gaf oauth2 client server path
     */
    private String serverPath;
    /**
     * iportal of gaf oauth2 config id
     */
    private String oauth2ConfigId;
}
