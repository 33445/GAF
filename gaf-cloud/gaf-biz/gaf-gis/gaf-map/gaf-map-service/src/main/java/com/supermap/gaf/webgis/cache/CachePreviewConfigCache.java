//package com.supermap.gaf.webgis.cache;
//
//import com.supermap.gaf.webgis.domain.WebgisConfigData;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.Cache;
//import org.springframework.cache.CacheManager;
//import org.springframework.context.annotation.Primary;
//import org.springframework.stereotype.Service;
//
//import java.util.UUID;
//
///**
// * @author heykb
// */
//@Service
//@Primary
//public class CachePreviewConfigCache implements PreviewConfigCacheI, InitializingBean {
//    public static final String PREVIEW_CACHE_PREFIX = "Gaf-Map:PREVIEW_CODE:";
//
//    @Autowired
//    private CacheManager cacheManager;
//    private Cache cache;
//
//    @Override
//    public void afterPropertiesSet() throws Exception {
//        this.cache = cacheManager.getCache("Gaf-Map");
//    }
//
//    @Override
//    public String generateKey(String appAliasEn) {
//        return UUID.randomUUID().toString().replaceAll("-","");
//    }
//
//    @Override
//    public void add(String previewCode, WebgisConfigData appConfigData) {
//        cache.put(PREVIEW_CACHE_PREFIX+previewCode, appConfigData);
//    }
//
//    @Override
//    public WebgisConfigData get(String previewCode) {
//        return cache.get(PREVIEW_CACHE_PREFIX+previewCode, WebgisConfigData.class);
//    }
//}
