package com.supermap.gaf.webgis.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.supermap.gaf.webgis.entity.WebgisService;
import com.supermap.gaf.webgis.enums.ServiceTypeEnum;
import com.supermap.services.rest.management.ServiceType;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.function.Function;

public class WebgisCommonUtils {



    public static  void listService(String typeCode,String url, Function<WebgisService,Object> forEach) {
//        String typeCode = source.getTypeCode();
//        String url = source.getAddress();
        WebgisService webgisService = new WebgisService();
        RestTemplate restTemplate = new RestTemplate();
        if ("RESTMAP".equals(typeCode)) {
            url += "/maps.json";
            JSONArray list = restTemplate.getForObject(url, JSONArray.class);
            for (int i = 0; i < list.size(); ++i) {
                JSONObject item = list.getJSONObject(i);
                webgisService.setAddress(item.getString("path"));
                webgisService.setName(item.getString("name"));
                webgisService.setTypeCode(typeCode);
                forEach.apply(webgisService);
            }
        } else if ("RESTDATA".equals(typeCode)) {
            url += "/data/datasources";
            // 遍历数据集
            JSONObject datasources = restTemplate.getForObject(url+".json", JSONObject.class);
            JSONArray datasourceNames = datasources.getJSONArray("datasourceNames");
            for (int i = 0; i < datasourceNames.size(); ++i) {
                String datasourceName = datasourceNames.getString(i);
                String datasetsUrl = url+"/"+datasourceName+"/datasets";
                JSONObject datasets = restTemplate.getForObject(datasetsUrl+".json", JSONObject.class);
                JSONArray datasetNames = datasets.getJSONArray("datasetNames");
                for (int j = 0; j < datasetNames.size(); ++j) {
                    String datasetName = datasetNames.getString(j);
                    webgisService.setAddress(datasetsUrl+"/"+datasetName);
                    webgisService.setName(datasourceName+"@"+datasetName);
                    webgisService.setTypeCode(typeCode);
                    forEach.apply(webgisService);
                }
            }

        } else if ("RESTREALSPACE".equals(typeCode)) {
            url += "/realspace/datas.json";
            JSONArray list = restTemplate.getForObject(url, JSONArray.class);
            for (int i = 0; i < list.size(); ++i) {
                JSONObject item = list.getJSONObject(i);
                webgisService.setAddress(item.getString("path"));
                webgisService.setName(item.getString("name"));
                webgisService.setTypeCode(typeCode);
                forEach.apply(webgisService);
            }
        } /*else if ("RESTSPATIALANALYST".equals(typeCode)) {
            url += "/spatialanalyst/datasets.json";
            JSONArray list = restTemplate.getForObject(url, JSONArray.class);
            for (int i = 0; i < list.size(); ++i) {
                JSONObject item = list.getJSONObject(i);
                webgisService.setAddress(item.getString("path"));
                webgisService.setName(item.getString("name"));
                webgisService.setTypeCode(typeCode);
                forEach.apply(webgisService);
            }
//        } */else {
            webgisService.setTypeCode(typeCode);
            webgisService.setAddress(url);
            String[] items = url.split("/");
            String name = items.length>2?items[items.length-2]+"/"+items[items.length-1]:url;
            webgisService.setName(name);
            forEach.apply(webgisService);
        }
    }

    public static   String parseServiceType(String interfaceType, String componentType) {
        if ("com.supermap.services.rest.RestServlet".equals(interfaceType) ||
                "com.supermap.services.rest.JaxrsServletForJersey".equals(interfaceType) ||
                "com.supermap.services.rest.BaiduRestServlet".equals(interfaceType) ||
                "com.supermap.services.rest.OSMRestServlet".equals(interfaceType) ||
                "com.supermap.services.rest.TMSRestServlet".equals(interfaceType)) {
            if ("com.supermap.services.components.impl.DataImpl".equals(componentType) || "com.supermap.services.components.impl.DataHistoryImpl".equals(componentType)) {
                // 数据服务组件
                return "RESTDATA";
            } else if ("com.supermap.services.components.impl.MapImpl".equals(componentType)) {
                // 地图服务组件
                return "RESTMAP";
            } else if ("com.supermap.services.components.impl.RealspaceImpl".equals(componentType)) {
                // 三维服务组件
                return "RESTREALSPACE";
            } else if ("com.supermap.services.components.impl.SpatialAnalystImpl".equals(componentType)) {
                // 空间分析服务组件
                return "RESTSPATIALANALYST";
            }
        } else if ("com.supermap.services.components.spi.ogc.WMS".equals(interfaceType)) {
            return "WMS";
        } else if ("com.supermap.services.wmts.WMTSServlet".equals(interfaceType)) {
            return "WMTS";
        } else if ("com.supermap.services.protocols.wfs.WFS".equals(interfaceType)) {
            return "WFS";
        } else if ("com.supermap.services.rest.RestServlet".equals(interfaceType)) {
            return "WCS";
        } else if ("com.supermap.services.protocols.wcs.WCS".equals(interfaceType)) {
            return "WPS";
        } else if ("com.supermap.services.rest.AGSRestServlet".equals(interfaceType)) {
            return "ARCGISMAP";
        }
        return "OTHER";
    }

    public static boolean checkServiceUrlFormat(String url, String typeCode){
        boolean re = false;
        ServiceTypeEnum type = ServiceTypeEnum.valueOf(typeCode);
        try{
            URL url1 = new URL(url);
            for(String regix:type.getPathPatterns()){
                re = re || url1.getPath().matches(regix);
            }
        }catch (Exception e){
        }
        return re;
    }

    public static ServiceTypeEnum convert2GafTypeCode(ServiceType serviceType){
        switch (serviceType){
            case RESTDATA:return ServiceTypeEnum.RESTDATA;
            case RESTMAP:return ServiceTypeEnum.RESTMAP;
            case RESTREALSPACE:return ServiceTypeEnum.RESTREALSPACE;
            case RESTSPATIALANALYST:return ServiceTypeEnum.RESTSPATIALANALYST;
            case AGSRESTMAP:return ServiceTypeEnum.ARCGISMAP;
            case WMS111:return ServiceTypeEnum.WMS;
            case WMS130:return ServiceTypeEnum.WMS;
            case WMTS100:return ServiceTypeEnum.WMTS;
            case WMTSCHINA:return ServiceTypeEnum.WMTS;
            case WFS100:return ServiceTypeEnum.WFS;
            case WFS200:return ServiceTypeEnum.WFS;
            case WCS111:return ServiceTypeEnum.WCS;
            case WCS112:return ServiceTypeEnum.WCS;
            case WPS100:return ServiceTypeEnum.WPS;
            case BAIDUREST:return ServiceTypeEnum.BAIDUREST;
            case GOOGLEREST:return ServiceTypeEnum.GOOGLEREST;
            default:return ServiceTypeEnum.OTHER;
        }
    }


}
