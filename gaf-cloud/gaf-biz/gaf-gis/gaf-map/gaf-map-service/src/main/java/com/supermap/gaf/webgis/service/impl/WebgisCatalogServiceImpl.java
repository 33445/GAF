//package com.supermap.gaf.webgis.service.impl;
//
//import com.supermap.gaf.authority.util.TreeConvertUtil;
//import com.supermap.gaf.commontypes.MessageResult;
//import com.supermap.gaf.commontypes.Page;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.license.util.AssitUtil;
//import com.supermap.gaf.sys.mgt.client.SysCatalogClient;
//import com.supermap.gaf.sys.mgt.commontype.SysCatalog;
//import com.supermap.gaf.sys.mgt.enums.CatalogTypeEnum;
//import com.supermap.gaf.sys.mgt.service.SysCatalogQueryService;
//import com.supermap.gaf.utils.PropertiesUtil;
//import com.supermap.gaf.webgis.entity.WebgisCatalogLayer;
//import com.supermap.gaf.webgis.service.WebgisCatalogLayerService;
//import com.supermap.gaf.webgis.service.WebgisCatalogService;
//import com.supermap.gaf.webgis.vo.WebgisCatalogLayerVo;
//import com.supermap.gaf.webgis.vo.WebgisCatalogListVo;
//import com.supermap.gaf.webgis.vo.WebgisCatalogTreeVo;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.experimental.SuperBuilder;
//import org.apache.commons.lang3.BooleanUtils;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.CollectionUtils;
//
//import javax.annotation.Nullable;
//import java.util.*;
//
//@Service
//public class WebgisCatalogServiceImpl implements WebgisCatalogService {
//
//    @Autowired
//    private WebgisCatalogLayerService webgisCatalogLayerService;
//
//    @Autowired
//    private SysCatalogClient sysCatalogClient;
//
//    @Autowired
//    private SysCatalogQueryService sysCatalogQueryService;
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public String createTree(WebgisCatalogTreeVo treeVo, boolean templateOp) {
//        return createTree(treeVo,templateOp,null,true).getRootId();
//    }
//
//    @Override
//    public WebgisCatalogTreeVo select(String rootId) {
//        // 查询所有相关目录
//        List<SysCatalog> catalogs = sysCatalogQueryService.getCatalogTreeListByRootId(rootId);
//        if(CollectionUtils.isEmpty(catalogs)){
//            return null;
//        }
//        WebgisCatalogTreeVo root = null;
//        // catalogId->CatalogNode 建立树结构备用
//        Map<String,WebgisCatalogTreeVo> catalogId2CatalogNode = new HashMap<>();
//        List<String> catalogIds = new ArrayList<>();
//        for(SysCatalog item:catalogs){
//            WebgisCatalogTreeVo catalogNode = new WebgisCatalogTreeVo();
//            PropertiesUtil.copyProperties2Map(item,catalogNode);
//            catalogNode.setIsCatalog(true);
//            catalogNode.setName(item.getName());
//            catalogNode.setKey(item.getCatalogId());
//            catalogNode.setSortSn(item.getSortSn());
//            catalogNode.setChildren(new ArrayList<>());
//            if(rootId.equals(item.getCatalogId())){
//                root = catalogNode;
//            }
//            catalogIds.add(item.getCatalogId());
//            catalogId2CatalogNode.put(item.getCatalogId(),catalogNode);
//        }
//        List<WebgisCatalogLayerVo> layers = webgisCatalogLayerService.getVoByIds(catalogIds);
//        // catalogId->layerNode 建立树结构备用
//        Map<String,List<WebgisCatalogTreeVo>> catalogId2layerNode = new HashMap<>();
//        if(layers!=null){
//            for(WebgisCatalogLayer item:layers){
//                WebgisCatalogTreeVo layerNode = new WebgisCatalogTreeVo();
//                PropertiesUtil.copyProperties2Map(item,layerNode);
//                layerNode.setIsCatalog(false);
//                layerNode.setName(item.getName());
//                layerNode.setKey(item.getCatalogLayerId());
//                layerNode.setSortSn(item.getSortSn());
//                List<WebgisCatalogTreeVo> children = catalogId2layerNode.get(item.getLayerCatalogId());
//                if(children == null){
//                    children = new ArrayList<>();
//                    catalogId2layerNode.put(item.getLayerCatalogId(),children);
//                }
//                children.add(layerNode);
//            }
//        }
//        // 建立树结构
//        for(SysCatalog item:catalogs){
//            WebgisCatalogTreeVo catalogNode = catalogId2CatalogNode.get(item.getCatalogId());
//            if(item.getParentId()!=null){
//                // 父子目录关系建立
//                WebgisCatalogTreeVo parent = catalogId2CatalogNode.get(item.getParentId());
//                if(parent!=null){
//                    List<WebgisCatalogTreeVo> children = parent.getChildren();
//                    if(children == null){
//                        children = new ArrayList<>();
//                        parent.setChildren(children);
//                    }
//                    children.add(catalogNode);
//                }
//            }
//
//        }
//        // 目录树中加入图层并排序
//        for(WebgisCatalogTreeVo item:catalogId2CatalogNode.values()){
//            // 目录图层关系
//            List<WebgisCatalogTreeVo> layerNodes = catalogId2layerNode.get(item.getKey());
//            List<WebgisCatalogTreeVo> children = item.getChildren();
//            if(layerNodes!=null){
//                children.addAll(layerNodes);
//            }
//            // 排序
//            sort(children);
//        }
//        return root;
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public int delete(String rootId) {
//        // 查询所有相关目录
//        List<SysCatalog> catalogs = sysCatalogQueryService.getCatalogTreeListByRootId(rootId);
//        if(CollectionUtils.isEmpty(catalogs)){
//            return 0;
//        }
//        List<String> catalogIds = new ArrayList<>();
//        for(SysCatalog item:catalogs){
//            catalogIds.add(item.getCatalogId());
//        }
//        webgisCatalogLayerService.deleteByCatalogIds(catalogIds);
//        sysCatalogClient.batchDelete(catalogIds,false);
//        return 1;
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void updateTree(String rootId, WebgisCatalogTreeVo treeVo, boolean b) {
//        int re = delete(rootId);
//        if(re==0){
//            throw new GafException("资源目录不存在");
//        }
//        createTree(treeVo,b,rootId,true);
//    }
//
//    @Override
//    public String cloneTree(String rootId, boolean template) {
//        // 查询所有相关目录
//        WebgisCatalogTreeVo treeVo = select(rootId);
//        if(treeVo==null){
//            return null;
//        }
//        return createTree(treeVo,template);
//    }
//
//    @Override
//    public com.supermap.gaf.commontypes.pagination.Page<WebgisCatalogListVo> pageList(Boolean isTemplate, String searchFieldName, String searchFieldValue, String orderFieldName, String orderMethod, Integer pageNum, Integer pageSize) {
//        MessageResult<Page<SysCatalog>> re = sysCatalogClient.pageListResourceRootCatalog(searchFieldName,searchFieldValue,
//                BooleanUtils.isTrue(isTemplate)?CatalogTypeEnum.RESOURCE_TP_GROUP_TYPE.getValue():CatalogTypeEnum.RESOURCE_GROUP_TYPE.getValue(),
//                orderFieldName,orderMethod,pageNum,pageSize);
//        Page<SysCatalog> sysCatalogPage = re.checkAndGetData();
//        List<WebgisCatalogListVo> vos = new ArrayList<>();
//        if(sysCatalogPage!=null){
//            List<SysCatalog> sysCatalogs = sysCatalogPage.getContent();
//            if(!CollectionUtils.isEmpty(sysCatalogs)){
//                for(SysCatalog item:sysCatalogs){
//                    WebgisCatalogListVo vo = new WebgisCatalogListVo();
//                    BeanUtils.copyProperties(item,vo);
//                    List<String> layerNames = webgisCatalogLayerService.selectLayerNames(item.getCatalogId());
//                    if(layerNames!=null){
//                        String names = String.join("、",layerNames);
//                        vo.setLayers(names);
//                    }
//                    vos.add(vo);
//                }
//            }
//
//        }
//        return com.supermap.gaf.commontypes.pagination.Page.create(sysCatalogPage.getPageIndex(),sysCatalogPage.getPageSize(),(int)sysCatalogPage.getTotal(),sysCatalogPage.getTotalPage(),vos);
//    }
//
//
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public CreateTreeResult createTree(WebgisCatalogTreeVo treeVo, boolean templateOp, @Nullable String specifyRootId, boolean executeInsert) {
//        List<SysCatalog> allCatalog = new ArrayList<>();
//        List<WebgisCatalogLayerVo> allLayer = new ArrayList<>();
//        if(!BooleanUtils.isTrue(treeVo.getIsCatalog())){
//            throw new GafException("根节点必须是目录");
//        }
//        String rootId = specifyRootId==null?UUID.randomUUID().toString():specifyRootId;
//        SysCatalog root = AssitUtil.string2Obj(AssitUtil.obj2String(treeVo),SysCatalog.class);
//        root.setType(templateOp? CatalogTypeEnum.RESOURCE_TP_GROUP_TYPE.getValue():CatalogTypeEnum.RESOURCE_GROUP_TYPE.getValue());
//        root.setCatalogId(rootId);
//        root.setParentId(TreeConvertUtil.ROOT_PARENT_ID);
//        allCatalog.add(root);
//        visitor(rootId, rootId,treeVo.getChildren(),templateOp,allCatalog,allLayer);
//        if(executeInsert){
//            webgisCatalogLayerService.batchInsertVos(allLayer);
//            sysCatalogClient.batchImport(allCatalog);
//        }
//        return CreateTreeResult.builder().rootId(rootId).allCatalog(allCatalog).allLayer(allLayer).build();
//    }
//
//    @Data
//    @AllArgsConstructor
//    @NoArgsConstructor
//    @SuperBuilder
//    public static class CreateTreeResult{
//        private String rootId;
//        private List<WebgisCatalogLayerVo> allLayer;
//        private List<SysCatalog> allCatalog;
//    }
//    /**
//     * 中序遍历。创建出待插入的目录和图层
//     * @param pid
//     * @param treeVos
//     * @param templateOp
//     * @param allCatalog 待插入的目录
//     * @param allLayer 待插入的图层
//     */
//    void visitor(String rootId, String pid, List<WebgisCatalogTreeVo> treeVos, boolean templateOp,List<SysCatalog> allCatalog,List<WebgisCatalogLayerVo> allLayer){
//        if(CollectionUtils.isEmpty(treeVos)){
//            return;
//        }
//        resetSortSn(treeVos);
//        for(WebgisCatalogTreeVo item:treeVos){
//            if(BooleanUtils.isTrue(item.getIsCatalog())){
//                String catalogId = UUID.randomUUID().toString();
//                SysCatalog sysCatalog = AssitUtil.string2Obj(AssitUtil.obj2String(item),SysCatalog.class);
//                sysCatalog.setType(templateOp? CatalogTypeEnum.RESOURCE_TP_GROUP_TYPE.getValue():CatalogTypeEnum.RESOURCE_GROUP_TYPE.getValue());
//                sysCatalog.setCatalogId(catalogId);
//                sysCatalog.setParentId(pid);
//                allCatalog.add(sysCatalog);
//                visitor(rootId,catalogId,item.getChildren(),templateOp,allCatalog,allLayer);
//            }else{
//                WebgisCatalogLayerVo catalogLayerVo = AssitUtil.string2Obj(AssitUtil.obj2String(item),WebgisCatalogLayerVo.class);
//                catalogLayerVo.setLayerCatalogId(pid);
//                catalogLayerVo.setRootCatalogId(rootId);
//                allLayer.add(catalogLayerVo);
//            }
//        }
//    }
//
//    void resetSortSn(List<WebgisCatalogTreeVo> treeVos){
//        if(CollectionUtils.isEmpty(treeVos)){
//            return;
//        }
//        sort(treeVos);
//        for (int i = 0; i < treeVos.size(); ++i) {
//            treeVos.get(i).setSortSn(i+1);
//        }
//    }
//
//     void sort(List<WebgisCatalogTreeVo> treeVos){
//         if(CollectionUtils.isEmpty(treeVos)){
//             return;
//         }
//         Collections.sort(treeVos,(s1, s2)->{
//             Integer x = s1.getSortSn()==null?Integer.MAX_VALUE:s1.getSortSn();
//             Integer y = s2.getSortSn()==null?Integer.MAX_VALUE:s2.getSortSn();
//             return (x < y) ? -1 : 1;
//         });
//     }
//
//
//}
