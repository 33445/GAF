//package com.supermap.gaf.webgis.service.impl;
//
//import com.github.pagehelper.PageHelper;
//import com.github.pagehelper.PageInfo;
//import com.supermap.gaf.commontypes.pagination.Page;
//import com.supermap.gaf.data.access.service.BatchSortAndCodeService;
//import com.supermap.gaf.exception.GafException;
//import com.supermap.gaf.security.SecurityUtilsExt;
//import com.supermap.gaf.webgis.dao.WebgisAppBottomLayerMapper;
//import com.supermap.gaf.webgis.dao.WebgisServiceMapper;
//import com.supermap.gaf.webgis.entity.WebgisAppBottomLayer;
//import com.supermap.gaf.webgis.entity.WebgisService;
//import com.supermap.gaf.webgis.service.WebgisAppBottomLayerService;
//import com.supermap.gaf.webgis.vo.WebgisAppBottomLayerSelectVo;
//import com.supermap.gaf.webgis.vo.WebgisAppBottomLayerVo;
//import org.apache.commons.lang3.BooleanUtils;
//import org.apache.http.util.Asserts;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.*;
//
///**
// * 地图应用底图服务实现类
// * @author zhurongcheng
// * @date yyyy-mm-dd
// */
//@Service
//public class WebgisAppBottomLayerServiceImpl implements WebgisAppBottomLayerService{
//    @Autowired
//    private WebgisAppBottomLayerMapper webgisAppBottomLayerMapper;
//    @Autowired
//    private BatchSortAndCodeService batchSortAndCodeService;
//    @Autowired
//    private WebgisServiceMapper webgisServiceMapper;
//
//
//    @Override
//    public WebgisAppBottomLayer getAndCheckById(String gisAppBottomLayerId){
//        WebgisAppBottomLayer re = getById(gisAppBottomLayerId);
//        if(re == null){
//            throw new GafException("底图配置不存在");
//        }
//        return re;
//    }
//
//	@Override
//    public WebgisAppBottomLayer getById(String gisAppBottomLayerId){
//        if(gisAppBottomLayerId == null){
//            throw new GafException("gisAppBottomLayerId不能为空");
//        }
//        return  webgisAppBottomLayerMapper.select(gisAppBottomLayerId);
//    }
//
//    @Override
//    public WebgisAppBottomLayerVo getVoById(String gisAppBottomLayerId){
//        WebgisAppBottomLayer appBottomLayer = getAndCheckById(gisAppBottomLayerId);
//        return toVo(appBottomLayer);
//    }
//
//
//
//
//    @Override
//    public Page<WebgisAppBottomLayer> listByPageCondition(WebgisAppBottomLayerSelectVo webgisAppBottomLayerSelectVo, int pageNum, int pageSize) {
//        PageInfo<WebgisAppBottomLayer> pageInfo = PageHelper.startPage(pageNum, pageSize).doSelectPageInfo(() -> {
//            List<WebgisAppBottomLayer> appBottomLayers = webgisAppBottomLayerMapper.selectList(webgisAppBottomLayerSelectVo);
//        });
//
//        return Page.create(pageInfo.getPageNum(),pageInfo.getPageSize(),(int)pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getList());
//    }
//
//	@Override
//    @Transactional
//    public WebgisAppBottomLayer insertWebgisAppBottomLayer(WebgisAppBottomLayer webgisAppBottomLayer){
//        //TODO: 主键非GeneratedKey，此处添加自定义主键生成策略
//		webgisAppBottomLayer.setGisAppBottomLayerId(UUID.randomUUID().toString());
//        String userName = SecurityUtilsExt.getUserName();
//        webgisAppBottomLayer.setCreatedBy(userName);
//		webgisAppBottomLayer.setUpdatedBy(userName);
//        webgisAppBottomLayerMapper.insert(webgisAppBottomLayer);
//        if(BooleanUtils.isTrue(webgisAppBottomLayer.getIsDefault())){
//            resetDefault(webgisAppBottomLayer.getGisAppId(),webgisAppBottomLayer.getGisAppBottomLayerId());
//        }
//        batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisAppBottomLayer.class,Arrays.asList(webgisAppBottomLayer.getGisAppId()));
//        return webgisAppBottomLayer;
//    }
//
//    @Transactional
//    public void resetDefault(String gisAppId, String gisAppBottomLayerId){
//        webgisAppBottomLayerMapper.updateSelectiveByGisAppId(WebgisAppBottomLayer.builder().isDefault(false).gisAppId(gisAppId).build());
//        webgisAppBottomLayerMapper.updateSelective(WebgisAppBottomLayer.builder().isDefault(true).gisAppBottomLayerId(gisAppBottomLayerId).build());
//    }
//
//    @Override
//    public List<WebgisAppBottomLayerVo> selectVoList(WebgisAppBottomLayerSelectVo selectVo){
//        List<WebgisAppBottomLayerVo> re = new ArrayList<>();
//        List<WebgisAppBottomLayer> appBottomLayers = webgisAppBottomLayerMapper.selectList(selectVo);
//        if(appBottomLayers != null){
//            for(WebgisAppBottomLayer item:appBottomLayers){
//                WebgisAppBottomLayerVo appBottomLayerVo = toVo(item);
//                re.add(appBottomLayerVo);
//            }
//        }
//        return re;
//    }
//
//	@Override
//    @Transactional
//    public void batchInsert(List<WebgisAppBottomLayer> webgisAppBottomLayers){
//		if (webgisAppBottomLayers != null && webgisAppBottomLayers.size() > 0) {
//		    Map<String,String> defaultLayers = new HashMap<>();
//            String userName = SecurityUtilsExt.getUserName();
//            webgisAppBottomLayers.forEach(webgisAppBottomLayer -> {
//                webgisAppBottomLayer.setGisAppBottomLayerId(UUID.randomUUID().toString());
//                webgisAppBottomLayer.setCreatedBy(userName);
//				webgisAppBottomLayer.setUpdatedBy(userName);
//				if(BooleanUtils.isTrue(webgisAppBottomLayer.getIsDefault())){
//				    // 多个默认 排在最前面的优先
//				    if(!defaultLayers.containsKey(webgisAppBottomLayer.getGisAppId())){
//                        defaultLayers.put(webgisAppBottomLayer.getGisAppId(),webgisAppBottomLayer.getGisAppBottomLayerId());
//                    }
//                }
//            });
//            webgisAppBottomLayerMapper.batchInsert(webgisAppBottomLayers);
//            for(String gisAppId:defaultLayers.keySet()){
//                resetDefault(gisAppId,defaultLayers.get(gisAppId));
//            }
//            batchSortAndCodeService.revisionSortSnForInsertOrDelete(WebgisAppBottomLayer.class,defaultLayers.keySet());
//        }
//
//    }
//
//	@Override
//    public void deleteWebgisAppBottomLayer(String gisAppBottomLayerId){
//        webgisAppBottomLayerMapper.delete(gisAppBottomLayerId);
//    }
//
//	@Override
//    public void batchDelete(List<String> gisAppBottomLayerIds){
//        webgisAppBottomLayerMapper.batchDelete(gisAppBottomLayerIds);
//    }
//
//	@Override
//    public WebgisAppBottomLayer updateWebgisAppBottomLayer(WebgisAppBottomLayer webgisAppBottomLayer){
//	    boolean needResetDefault = false;
//        WebgisAppBottomLayer old = getAndCheckById(webgisAppBottomLayer.getGisAppBottomLayerId());
//        if(BooleanUtils.isTrue(webgisAppBottomLayer.getIsDefault())){
//            if(old == null){
//                throw new GafException("指定资源不存在");
//            }
//           needResetDefault = !BooleanUtils.isTrue(old.getIsDefault()) || !old.getGisAppId().equals(webgisAppBottomLayer.getGisAppId());
//        }
//		webgisAppBottomLayer.setUpdatedBy(SecurityUtilsExt.getUserName());
//		webgisAppBottomLayerMapper.update(webgisAppBottomLayer);
//		if(needResetDefault){
//		    resetDefault(webgisAppBottomLayer.getGisAppId(),webgisAppBottomLayer.getGisAppBottomLayerId());
//        }
//        batchSortAndCodeService.revisionSortSnForUpdate(WebgisAppBottomLayer.class,webgisAppBottomLayer.getGisAppId(),old.getSortSn(),webgisAppBottomLayer.getSortSn());
//        return webgisAppBottomLayer;
//    }
//
//    WebgisAppBottomLayerVo toVo(WebgisAppBottomLayer appBottomLayer){
//        Asserts.notNull(appBottomLayer,"appBottomLayer 不能为null");
//        WebgisAppBottomLayerVo re = new WebgisAppBottomLayerVo();
//        BeanUtils.copyProperties(appBottomLayer,re);
//        WebgisService webgisService = webgisServiceMapper.select(re.getBottomServiceId());
//        if(webgisService!=null){
//            // 服务不存在 serviceName为null
//            re.setServiceName(webgisService.getName()==null?"":webgisService.getName());
//        }
//        return re;
//    }
//
//}
